-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 31, 2019 at 12:23 PM
-- Server version: 5.7.24-0ubuntu0.16.04.1
-- PHP Version: 7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `etouch_auto`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add test steps', 1, 'add_teststeps'),
(2, 'Can change test steps', 1, 'change_teststeps'),
(3, 'Can delete test steps', 1, 'delete_teststeps'),
(4, 'Can add test cases', 2, 'add_testcases'),
(5, 'Can change test cases', 2, 'change_testcases'),
(6, 'Can delete test cases', 2, 'delete_testcases'),
(7, 'Can add test results', 3, 'add_testresults'),
(8, 'Can change test results', 3, 'change_testresults'),
(9, 'Can delete test results', 3, 'delete_testresults'),
(10, 'Can add builds', 4, 'add_builds'),
(11, 'Can change builds', 4, 'change_builds'),
(12, 'Can delete builds', 4, 'delete_builds'),
(13, 'Can add projects', 5, 'add_projects'),
(14, 'Can change projects', 5, 'change_projects'),
(15, 'Can delete projects', 5, 'delete_projects'),
(16, 'Can add devices', 6, 'add_devices'),
(17, 'Can change devices', 6, 'change_devices'),
(18, 'Can delete devices', 6, 'delete_devices'),
(19, 'Can add test result consolidation', 7, 'add_testresultconsolidation'),
(20, 'Can change test result consolidation', 7, 'change_testresultconsolidation'),
(21, 'Can delete test result consolidation', 7, 'delete_testresultconsolidation'),
(22, 'Can add log entry', 8, 'add_logentry'),
(23, 'Can change log entry', 8, 'change_logentry'),
(24, 'Can delete log entry', 8, 'delete_logentry'),
(25, 'Can add group', 9, 'add_group'),
(26, 'Can change group', 9, 'change_group'),
(27, 'Can delete group', 9, 'delete_group'),
(28, 'Can add permission', 10, 'add_permission'),
(29, 'Can change permission', 10, 'change_permission'),
(30, 'Can delete permission', 10, 'delete_permission'),
(31, 'Can add user', 11, 'add_user'),
(32, 'Can change user', 11, 'change_user'),
(33, 'Can delete user', 11, 'delete_user'),
(34, 'Can add content type', 12, 'add_contenttype'),
(35, 'Can change content type', 12, 'change_contenttype'),
(36, 'Can delete content type', 12, 'delete_contenttype'),
(37, 'Can add session', 13, 'add_session'),
(38, 'Can change session', 13, 'change_session'),
(39, 'Can delete session', 13, 'delete_session');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(8, 'admin', 'logentry'),
(9, 'auth', 'group'),
(10, 'auth', 'permission'),
(11, 'auth', 'user'),
(12, 'contenttypes', 'contenttype'),
(4, 'reports', 'builds'),
(6, 'reports', 'devices'),
(5, 'reports', 'projects'),
(2, 'reports', 'testcases'),
(7, 'reports', 'testresultconsolidation'),
(3, 'reports', 'testresults'),
(1, 'reports', 'teststeps'),
(13, 'sessions', 'session');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2019-01-17 16:01:07.899840'),
(2, 'auth', '0001_initial', '2019-01-17 16:01:22.092309'),
(3, 'admin', '0001_initial', '2019-01-17 16:01:25.878814'),
(4, 'admin', '0002_logentry_remove_auto_add', '2019-01-17 16:01:26.539722'),
(5, 'contenttypes', '0002_remove_content_type_name', '2019-01-17 16:01:29.618670'),
(6, 'auth', '0002_alter_permission_name_max_length', '2019-01-17 16:01:29.887302'),
(7, 'auth', '0003_alter_user_email_max_length', '2019-01-17 16:01:30.285705'),
(8, 'auth', '0004_alter_user_username_opts', '2019-01-17 16:01:30.541932'),
(9, 'auth', '0005_alter_user_last_login_null', '2019-01-17 16:01:31.481236'),
(10, 'auth', '0006_require_contenttypes_0002', '2019-01-17 16:01:31.574413'),
(11, 'auth', '0007_alter_validators_add_error_messages', '2019-01-17 16:01:31.667615'),
(12, 'auth', '0008_alter_user_username_max_length', '2019-01-17 16:01:31.854647'),
(13, 'reports', '0001_initial', '2019-01-17 16:01:49.653943'),
(14, 'sessions', '0001_initial', '2019-01-17 16:01:50.533250'),
(15, 'reports', '0002_auto_20190118_1231', '2019-01-18 12:31:40.425529'),
(16, 'reports', '0003_testresults_test_errors', '2019-01-21 08:43:34.319535');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reports_builds`
--

CREATE TABLE `reports_builds` (
  `id` int(11) NOT NULL,
  `build` varchar(300) NOT NULL,
  `executed_date` date NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports_builds`
--

INSERT INTO `reports_builds` (`id`, `build`, `executed_date`, `project_id`) VALUES
(1, 'dynamite-android_20190104-09_RC04', '2019-01-16', 1),
(2, 'dynamite-android_20190111-03_RC03', '2019-01-17', 1),
(3, 'dynamite-android_20190116-01_RC00', '2019-01-17', 1);

-- --------------------------------------------------------

--
-- Table structure for table `reports_devices`
--

CREATE TABLE `reports_devices` (
  `id` int(11) NOT NULL,
  `device_name` longtext NOT NULL,
  `device_os` longtext NOT NULL,
  `device_udid` longtext NOT NULL,
  `created` datetime(6) NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports_devices`
--

INSERT INTO `reports_devices` (`id`, `device_name`, `device_os`, `device_udid`, `created`, `project_id`) VALUES
(1, 'Pixel 2', '8.1.0', 'FA6A40307885', '2019-01-16 00:00:00.000000', 1),
(2, 'Samsung', '8.0.0', 'JKISDSK24923', '2019-01-24 00:00:00.000000', 1);

-- --------------------------------------------------------

--
-- Table structure for table `reports_projects`
--

CREATE TABLE `reports_projects` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` longtext NOT NULL,
  `created` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports_projects`
--

INSERT INTO `reports_projects` (`id`, `name`, `description`, `created`) VALUES
(1, 'Dynamite Automation', 'Dynamite Automation', '2019-01-17 00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `reports_testcases`
--

CREATE TABLE `reports_testcases` (
  `id` int(11) NOT NULL,
  `test_feature` longtext NOT NULL,
  `test_title` longtext NOT NULL,
  `test_name` longtext NOT NULL,
  `test_suite_type` longtext NOT NULL,
  `test_priority` int(11) NOT NULL,
  `created` datetime(6) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports_testcases`
--

INSERT INTO `reports_testcases` (`id`, `test_feature`, `test_title`, `test_name`, `test_suite_type`, `test_priority`, `created`, `status`, `project_id`) VALUES
(1, 'App Launch', 'Verify the Onboarding message upon app launch', 'Onboarding message should be displayed upon app launch', 'Regression', 2, '2019-01-16 00:00:00.000000', 1, 1),
(2, 'App Launch', 'Verify Tapping on "Get started" Button', 'Choose an Account should be displayed on tapping Get Started', 'Regression', 0, '2019-01-16 00:00:00.000000', 1, 1),
(3, 'App Launch', 'Verify login with White listed Accounts', 'To Successfully login to application', 'Regression', 2, '2019-01-16 00:00:00.000000', 1, 1),
(4, 'App Launch', 'Verify login with Non White listed Accounts', 'No Access for Non White listed Accounts', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(5, 'App Launch', 'Verify adding account', 'Adding account upon launching Application', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(6, 'App Launch', 'Verify launching on Offline Mode', 'Verify launching on Offline Mode', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(7, 'World View', 'Verify the UI Sections/Elements displayed in world view', 'All the UI sections/elements must be verified in world view', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(8, 'World View', 'Verify the Unread section rooms rendering in world view', 'Verify the Unread section rooms rendering in world view', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(9, 'World View', 'Verify the Recent section rooms rendering in world view', 'Verify the recent section rooms rendering in world view', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(10, 'World View', 'Verify the starred section rooms rendering in world view', 'Verify the starred section rooms rendering in world view', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(11, 'World View', 'Verify the FAB button functionality displayed in World view', 'Verify the FAB button functionality displayed in World view', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(12, 'World View', 'Verify the Filter rooms/DM functionality from World view', 'Verify the Filter rooms/DM functionality from World view', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(13, 'Create & Browse Rooms', 'Verify Create Room Page', 'To Verify all details of Create Room Page', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(14, 'Create & Browse Rooms', 'Verify Creating a Room', 'To Create a new room', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(15, 'Create & Browse Rooms', 'Verify Browse Rooms page', 'To verify \'Browse Rooms\' page', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(16, 'Create & Browse Rooms', 'Verify Joining & Leaving Room from Browse rooms', 'Joining and Leaving Room', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(17, 'Create & Browse Rooms', 'Verify Room Preview from Browse rooms', 'Room Preview Page', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(18, 'Create & Browse Rooms', 'Verify Members Count of Rooms in Rooms You\'ve been invited to', 'Verify the count in rooms', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(19, 'Create & Browse Rooms', 'Verify Add room in offline mode', 'Behaviour of add room page in offline mode', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(20, 'Create & Browse Rooms', 'Verify New (red label) for Rooms you\'ve been invited to', 'Verify New (red label) for Rooms you\'ve been invited to', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(21, 'Room Preview', 'Verify the user navigation to room preview screens', 'Verify the user navigation to room preview screens', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(22, 'Room Preview', 'Verify the Join option from room details through room preview', 'Verify the Join option from room details through room preview', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(23, 'Room Preview', 'Verify the Join option from room preview', 'Verify the Join option from room preview', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(24, 'Room Preview', 'Verify the collapsed summary functionality in Room preview', 'Verify the collapsed summary functionality in Room preview', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(25, 'Room Preview', 'Verify the UI elements/sections rendering in Room preview', 'Verify the UI elements/sections rendering in Room preview', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(26, 'Room Preview', 'Verify the Long press message options in Room preview', 'Verify the Long press message options in Room preview', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(27, 'Faster DM & Group DM', 'Verify the navigation to Conversation launcher from world view', 'Verify the navigation to Direct message screen from world view', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(28, 'Faster DM & Group DM', 'Verify the Faster DM screen functionality', 'Verify the Direct Message contacts screen functionality', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(29, 'Faster DM & Group DM', 'Verify if the user is able to create a group DM', 'Verify if the user is able to create a group DM', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(30, 'Faster DM & Group DM', 'Verify the OTR functionality', 'Verify the OTR functionality', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(31, 'DM Details', 'Verify DM Details Page', 'To Verify DM Details Page', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(32, 'DM Details', 'Verify Star & Unstar', 'To Verify Star & Unstar', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(33, 'DM Details', 'Verify Notifications setting for Room Details', 'To Verify Notifications setting for Room Details', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(34, 'DM Details', 'Verify the Members list', 'To Verify the Members list', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(35, 'DM Details', 'Verify Hide functionality from DM', 'To Verify Hide functionality from DM', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(36, 'Space View', 'Verify the Nav bar functionality in space view', 'Verify the functionality of the nav bar in space view', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(37, 'Space View', 'Verify the persistent header in space view', 'Verify the persistent header in space view', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(38, 'Space View', 'Verify topics pinning in Space view', 'Verify topics pinning in Space view', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(39, 'Space View', 'Verify timestamps, avatars, date dividers and unread labels in space view', 'Verify timestamps, date dividers and unread labels in space view', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(40, 'Space View', 'Verify timestamps, avatars, date dividers and unread labels in space view', 'Verify the collapsed summary functionality in space view', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(41, 'Space View', 'Verify the senders summary naming in Collapsed messages summary', 'Verify the senders summary naming in Collapsed messages summary', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(42, 'Space View', 'Verify the new conversation FAB in space view', 'Verify the new conversation FAB in space view', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(43, 'Space View', 'Verify if the files are doc files are launched in native apps from space view', 'Verify if the files are doc files are launched in native apps from space view', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(44, 'Space View', 'Verify the space-topic view transitions', 'Verify the space-topic view transitions', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(45, 'Space View', 'Verify the space view rendering with topics containing different types of data', 'Verify the space view rendering with topics containing different types of data', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(46, 'Space View', 'Verify the System messages generated in space view', 'Verify the System messages generated in space view', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(47, 'Space View', 'Verify editing messages functionality from Space view', 'Verify editing messages functionality from Space view', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(48, 'Space View', 'Verify Add people functionality from persistent header in space view', 'Verify Add people functionality from persistent header in space view', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(49, 'Room Details', 'Verify Room Details Page', 'Verify Room Details Page', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(50, 'Room Details', 'Verify Edit Room name', 'Verify Edit Room name', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(51, 'Room Details', 'Verify adding People & Bots', 'Verify adding People & Bots', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(52, 'Room Details', 'Verify Star & Unstar', 'Verify Star & Unstar', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(53, 'Room Details', 'Verify Notifications for Room Details', 'Verify Notifications for Room Details', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(54, 'Room Details', 'Verify Leave Room', 'Verify Leave Room', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(55, 'Room Details', 'Verify the Members list', 'Verify the Members list', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(56, 'Room Details', 'Verify the Can join list', 'Verify the Can join list', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(57, 'Topic View', 'Verify the existing Topic view rendering of UI elements/Sections', 'Verify the existing Topic view rendering of UI elements/Sections', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(58, 'Topic View', 'Verify the New Conversation view rendering of UI elements/Sections', 'Verify the New Conversation view rendering of UI elements/Sections', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(59, 'Topic View', 'Verify the Compose field components in topic view', 'Verify the Compose field components in topic view', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(60, 'Topic View', 'Verify the Send button functionality in compose view', 'Verify the Send button functionality in compose view', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(61, 'Topic View', 'Verify the Upload icon functionality in compose view', 'Verify the Upload icon functionality in compose view', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(62, 'Topic View', 'Verify the Camera icon functionality in compose', 'Verify the Camera icon functionality in compose', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(63, 'Topic View', 'Verify the Thor icon functionality in compose', 'Verify the Thor icon functionality in compose', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(64, 'Topic View', 'Verify the Attachment replace functionality in compose view', 'Verify the Attachment replace functionality in compose view', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(65, 'Topic View', 'Verify the edit message functionality in topic view', 'Verify the edit message functionality in topic view', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(66, 'Topic View', 'Verify the failed message options in offline mode', 'Verify the failed message options in offline mode', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(67, 'Topic View', 'Verify notification settings in Topic view.', 'Verify notification settings in Topic view.', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(68, 'Topic View', 'Verify pagination in Topic view', 'Verify pagination in Topic view', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(69, 'Space Search', 'Verify notification settings in Topic view.', 'Verify notification settings in Topic view.', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(70, 'Space Search', 'Verify pagination in Topic view', 'Verify pagination in Topic view', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(71, 'Space Search', 'Verify the Space search navigation and UI of space search in Zero state', 'Verify the Space search navigation and UI of space search in Zero state', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(72, 'Space Search', 'Verify the Space search functionality based on keyword', 'Verify the Space search functionality based on keyword', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(73, 'Space Search', 'Verify the Space search functionality based on member selection from the filter panel', 'Verify the Space search functionality based on member selection from the filter panel', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(74, 'Space Search', 'Verify the Space search functionality based on Content type selection from the filter panel', 'Verify the Space search functionality based on Content type selection from the filter panel', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(75, 'Notifications', 'Verify the notifications options menu from Settings', 'Verify the notifications options menu from Settings', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(76, 'Notifications', 'Verify notifications disabling from device settings', 'Verify notifications disabling from device settings', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(77, 'Autocomplete', 'Verify the Autocomplete rendering in Room_Existing Topic view', 'Verify the Autocomplete rendering in Room_Existing Topic view', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(78, 'Autocomplete', 'Verify the Autocomplete rendering and functionality in New room', 'Verify the Autocomplete rendering and functionality in New room', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(79, 'Autocomplete', 'Verify the Autocomplete rendering and functionality in 1:1 DM', 'Verify the Autocomplete rendering and functionality in 1:1 DM', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(80, 'Autocomplete', 'Verify the Autocomplete rendering and functionality in group DM', 'Verify the Autocomplete rendering and functionality in group DM', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(81, '@Mentions', 'Verify @mention functionality', 'Verify @mention functionality', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(82, '@Mentions', 'Verify @all mentions functionality', 'Verify @all mentions functionality', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(83, '@Mentions', 'Verify adding people functionality using @mention', 'Verify adding people functionality using @mention', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(84, 'Presence Indicators', 'Verify the presence indicators display across the app', 'Verify the presence indicators display across the app', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(85, 'Miscellaneous', 'Verify the app behaviour and UI upon font settings change', 'Verify the app behaviour and UI upon font settings change', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(86, 'Miscellaneous', 'Verify feedback functionality in app', 'Verify feedback functionality in app', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(87, 'Message Delete', 'Verify the Delete message functionality in topic view', 'Verify the Delete message functionality in topic view', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(88, 'Message Delete', 'Verify the Delete message functionality in topic view', 'Verify the Delete message functionality in topic view', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(89, 'Message Delete', 'Verify the Delete message functionality in space view', 'Verify the Delete message functionality in space view', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(90, 'Message Delete', 'Verify the Delete message functionality in Offline', 'Verify the Delete message functionality in Offline', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(91, 'Bots', 'Verify the Bot option displayed in FAB of World view', 'Verify the Bot option displayed in FAB of World view', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(92, 'Bots', 'Verify the Bot DM functionality from the "Add Bot" screen', 'Verify the Bot DM functionality from the "Add Bot" screen', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(93, 'Bots', 'Verify the Bot DM Details screen', 'Verify the Bot DM Details screen', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(94, 'Bots', 'Verify the remove option functionality for Bot DM', 'Verify the remove option functionality for Bot DM', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(95, 'Content Sharing', 'Verify Content sharing without app login', 'Verify Content sharing without app login', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(96, 'Content Sharing', 'Verify Content sharing with app login', 'Verify Content sharing with app login', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(97, 'Content Sharing', 'Verify content sharing in offline mode', 'Verify content sharing in offline mode', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(98, 'Bots in Rooms', 'Verify if the user is able to add a bot to the room by @mentioning', 'Verify if the user is able to add a bot to the room by @mentioning', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(99, 'Bots in Rooms', 'Verify if the user is able to add a bot to the room through invite flow', 'Verify if the user is able to add a bot to the room through invite flow', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(100, 'Bots in Rooms', 'Verify if the user is able to remove a bot from the room', 'Verify if the user is able to remove a bot from the room', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(101, 'Enhanced Invite Emails', 'Verify if the invite email is sent to the user on invitation to room', 'Verify if the invite email is sent to the user on invitation to room', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(102, 'Enhanced Invite Emails', 'Verify if the invite email is sent to all the users of group on invitation to room', 'Verify if the invite email is sent to all the users of group on invitation to room', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(103, 'Flat DM', 'Verify UI changes of DM', 'Observe UI changes of DM', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(104, 'Flat DM', 'Verify History ON/OFF functionality', 'Verify History ON/OFF functionality', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(105, 'Flat DM', 'Verify edit/delete message functionality in flat DMs.', 'Verify edit/delete message functionality in flat DMs.', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(106, 'Flat DM', 'Verify offline handling in flat DMs', 'Verify offline handling in flat DMs', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(107, 'Both Auth & Disabled bots', 'Verify @mentioned bot successfully respond.', 'Verify @mentioned bot successfully respond.', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(108, 'Local stream Data', 'Verify messages loading in Offline/Slow-network loading', 'Verify messages loading in Offline/Slow-network loading', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(109, 'Jump to Bottom', 'Verify jump to bottom FAB in READ rooms', 'Verify jump to bottom FAB in READ rooms', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(110, 'Jump to Bottom', 'Verify that jump to bottom FAB is not displayed in DM\'s', 'Verify that jump to bottom FAB is not displayed in DM\'s', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(111, 'Navigation Drawer', 'Verify Navigation Panel available after Login', 'Verify Navigation Panel available after Login', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(112, 'Navigation Drawer', 'Verify menu contents in Navigation', 'Verify menu contents in Navigation', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(113, 'Do Not Disturb & Dedicated Settings', 'Verify user status on world view header', 'Verify user status on world view header', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(114, 'Do Not Disturb & Dedicated Settings', 'Verify Do Not Disturb feature from navigation drawer', 'Verify Do Not Disturb feature from navigation drawer', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(115, 'Do Not Disturb & Dedicated Settings', 'Verify (Dedicated Settings) Settings -> Mobile Notifications from nav drawer', 'Verify (Dedicated Settings) Settings -> Mobile Notifications from nav drawer', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(116, 'Reactions', 'Verify adding reaction on self/other member/bot message in rooms/dms', 'Verify adding reaction on self/other member/bot message in rooms/dms', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(117, 'Topic View', 'Verify adding reaction on self/other member/bot message in rooms/dms', 'Verify if the user is able to successfully create the draft in topic view', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(118, 'Topic View', 'Verify if the user is able to successfully create a draft with attachment', 'Verify if the user is able to successfully create a draft with attachment', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(119, 'Presence Indicators', 'Verify the presence indicator for active status', 'Verify the presence indicator for active status', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(120, 'Presence Indicators', 'Verify the presence indicator for inactive status', 'Verify the presence indicator for inactive status', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `reports_testresultconsolidation`
--

CREATE TABLE `reports_testresultconsolidation` (
  `id` int(11) NOT NULL,
  `test_status` int(11) NOT NULL,
  `bugid` varchar(50) NOT NULL,
  `bug_status` varchar(50) NOT NULL,
  `execution_time` double NOT NULL,
  `executed_date` date NOT NULL,
  `test_build_id` int(11) NOT NULL,
  `test_case_id` int(11) NOT NULL,
  `test_device_id` int(11) NOT NULL,
  `test_device_logs` longtext,
  `test_server_logs` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports_testresultconsolidation`
--

INSERT INTO `reports_testresultconsolidation` (`id`, `test_status`, `bugid`, `bug_status`, `execution_time`, `executed_date`, `test_build_id`, `test_case_id`, `test_device_id`, `test_device_logs`, `test_server_logs`) VALUES
(1, 1, '', '', 2, '2019-01-16', 1, 1, 1, NULL, NULL),
(2, 1, '', '', 2, '2019-01-16', 1, 2, 1, NULL, NULL),
(3, 0, 'b/123456', '', 1, '2019-01-16', 1, 3, 1, NULL, NULL),
(4, 1, '', '', 2, '2019-01-17', 2, 1, 1, NULL, NULL),
(5, 0, 'b/123456', '', 1, '2019-01-17', 2, 2, 1, NULL, NULL),
(6, 1, '', '', 2, '2019-01-17', 3, 1, 1, NULL, NULL),
(7, 1, '', '', 2, '2019-01-17', 3, 2, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reports_testresults`
--

CREATE TABLE `reports_testresults` (
  `id` int(11) NOT NULL,
  `test_result` tinyint(1) NOT NULL COMMENT '1:pass,0:fail',
  `screen_generated` longtext NOT NULL,
  `screen_diff` longtext NOT NULL,
  `executed` datetime(6) NOT NULL,
  `start_time` datetime(6) NOT NULL,
  `end_time` datetime(6) NOT NULL,
  `device_id` int(11) NOT NULL,
  `test_build_id` int(11) DEFAULT NULL,
  `test_name_id` int(11) NOT NULL,
  `test_step_id` int(11) NOT NULL,
  `test_errors` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports_testresults`
--

INSERT INTO `reports_testresults` (`id`, `test_result`, `screen_generated`, `screen_diff`, `executed`, `start_time`, `end_time`, `device_id`, `test_build_id`, `test_name_id`, `test_step_id`, `test_errors`) VALUES
(1, 1, '', '', '2019-01-16 00:00:00.000000', '2019-01-16 00:00:00.000000', '2019-01-16 00:00:00.000000', 1, 1, 1, 1, NULL),
(2, 1, '', '', '2019-01-16 00:00:00.000000', '2019-01-16 00:00:00.000000', '2019-01-16 00:00:00.000000', 1, 1, 1, 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reports_teststeps`
--

CREATE TABLE `reports_teststeps` (
  `id` int(11) NOT NULL,
  `test_step` longtext NOT NULL,
  `test_step_desc` longtext NOT NULL,
  `golden_screen` longtext NOT NULL,
  `created` datetime(6) NOT NULL,
  `test_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports_teststeps`
--

INSERT INTO `reports_teststeps` (`id`, `test_step`, `test_step_desc`, `golden_screen`, `created`, `test_id`) VALUES
(1, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-16 00:00:00.000000', 1),
(2, 'Onboarding message saying - "Welcome to the New Hangouts Chat - An Intelligent communication app built for teams" should be displayed with a "Get started" button', 'Onboarding message saying - "Welcome to the New Hangouts Chat - An Intelligent communication app built for teams" should be displayed with a "Get started" button', '', '2019-01-17 00:00:00.000000', 1),
(3, 'Choose an account should be displayed with all accounts associated with device', 'Choose an account should be displayed with all accounts associated with device', '', '2019-01-16 00:00:00.000000', 2),
(4, 'Add account option should also be available', 'Add account option should also be available', '', '2019-01-16 00:00:00.000000', 2),
(5, 'On choosing account "cancel" and "ok" buttons should be tappable"', 'On choosing account "cancel" and "ok" buttons should be tappable"', '', '2019-01-16 00:00:00.000000', 2),
(6, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-16 00:00:00.000000', 3),
(7, 'Choose an account should be displayed with all accounts associated with device', 'Choose an account should be displayed with all accounts associated with device', '', '2019-01-16 00:00:00.000000', 3),
(8, '"Allow chat to notify you of new messages" dialog should appear on first launch', '"Allow chat to notify you of new messages" dialog should appear on first launch', '', '2019-01-16 00:00:00.000000', 3),
(9, 'Verify whether World view page is displayed', 'Verify whether World view page is displayed', '', '2019-01-16 00:00:00.000000', 3),
(10, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 4),
(11, 'Choose an account should be displayed with all accounts associated with device', 'Choose an account should be displayed with all accounts associated with device', '', '2019-01-31 00:00:00.000000', 4),
(12, '"You don\'t have access to Hangouts Chat - Your account doesn\'t have access To use Hangouts Chat during this early access period. - Exit and try a different account" Page should be displayed', '"You don\'t have access to Hangouts Chat - Your account doesn\'t have access To use Hangouts Chat during this early access period. - Exit and try a different account" Page should be displayed', '', '2019-01-31 00:00:00.000000', 4),
(13, 'Upon tapping "Exit and try a different account" application should Exit', 'Upon tapping "Exit and try a different account" application should Exit.', '', '2019-01-31 00:00:00.000000', 4),
(14, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 5),
(15, 'Choose an account should be displayed with all accounts associated with device', 'Choose an account should be displayed with all accounts associated with device', '', '2019-01-31 00:00:00.000000', 5),
(16, 'Add White listed account and verify if application has launched', 'Add White listed account and verify if application has launched', '', '2019-01-31 00:00:00.000000', 5),
(17, 'Add non white listed account and verify if application displayed "You don\'t have access to Hangouts Chat - Your account doesn\'t have access to use Hangouts Chat during this early access period. - Exit and try a different account" page', 'Add non white listed account and verify if application displayed "You don\'t have access to Hangouts Chat - Your account doesn\'t have access to use Hangouts Chat during this early access period. - Exit and try a different account" page', '', '2019-01-31 00:00:00.000000', 5),
(18, 'No Network Connection - You need to be connected to the internet to log in. Please check your network connection and try again - Try Again', 'No Network Connection - You need to be connected to the internet to log in. Please check your network connection and try again - Try Again', '', '2019-01-31 00:00:00.000000', 6),
(19, 'Tap on Try again and Verify application should remain on "Welcome to the New Hangouts Chat - An Intelligent Communication app built for teams" should be displayed with a "Get started" button', 'Tap on Try again and Verify application should remain on "Welcome to the New Hangouts Chat - An Intelligent Communication app built for teams" should be displayed with a "Get started" button', '', '2019-01-31 00:00:00.000000', 6),
(20, 'App must be launched successfully', 'App must be launched successfully', '', '2019-01-31 00:00:00.000000', 7),
(21, 'The following must be displayed properly without any UI issues', 'The following must be displayed properly without any UI issues', '', '2019-01-31 00:00:00.000000', 7),
(22, 'The header name - Username and active/DND status below it with presence indicator', 'The header name - Username and active/DND status below it with presence indicator', '', '2019-01-31 00:00:00.000000', 7),
(23, 'FAB button', 'FAB button', '', '2019-01-31 00:00:00.000000', 7),
(24, 'Sections.- Unread, Starred, Recent', 'Sections.- Unread, Starred, Recent', '', '2019-01-31 00:00:00.000000', 7),
(25, 'Presence indicator - against member names for DM\'s', 'Presence indicator - against member names for DM\'s', '', '2019-01-31 00:00:00.000000', 7),
(26, 'Longer room names with ellipses', 'Longer room names with ellipses', '', '2019-01-31 00:00:00.000000', 7),
(27, 'badge icons with count for unread messages', 'badge icons with count for unread messages', '', '2019-01-31 00:00:00.000000', 7),
(28, 'Mute icon if rooms are muted from room details screen', 'Mute icon if rooms are muted from room details screen', '', '2019-01-31 00:00:00.000000', 7),
(29, 'Timestamp as per last last activity of the room/DM', 'Timestamp as per last last activity of the room/DM', '', '2019-01-31 00:00:00.000000', 7),
(30, 'User Should be able to launch the app', 'User Should be able to launch the app', '', '2019-01-31 00:00:00.000000', 8),
(31, 'World view must be displayed with three sections : \'Unread\', \'Starred\', \'Recent\'', 'World view must be displayed with three sections : \'Unread\', \'Starred\', \'Recent\'', '', '2019-01-31 00:00:00.000000', 8),
(32, 'Unread rooms must be displayed under "Unread" section that are sorted reverse chronologically', 'Unread rooms must be displayed under "Unread" section that are sorted reverse chronologically', '', '2019-01-31 00:00:00.000000', 8),
(33, 'Rooms must have the star icon(for starred rooms), badge count (if unread topics) along with the timestamp', 'Rooms must have the star icon(for starred rooms), badge count (if unread topics) along with the timestamp', '', '2019-01-31 00:00:00.000000', 8),
(34, 'User Should be able to launch the app', 'User Should be able to launch the app', '', '2019-01-31 00:00:00.000000', 9),
(35, 'World view must be displayed with three sections : \'Unread\', \'Starred\', \'Recent\'', 'World view must be displayed with three sections : \'Unread\', \'Starred\', \'Recent\'', '', '2019-01-31 00:00:00.000000', 9),
(36, 'Recent rooms must be displayed under "Recent" section that are sorted reverse chronologically', 'Recent rooms must be displayed under "Recent" section that are sorted reverse chronologically', '', '2019-01-31 00:00:00.000000', 9),
(37, 'Timestamp is displayed as per the below standards', 'Timestamp is displayed as per the below standards', '', '2019-01-31 00:00:00.000000', 9),
(38, 'Starred rooms are rendered in A-Z order (NOT timestamps)', 'Starred rooms are rendered in A-Z order (NOT timestamps)', '', '2019-01-31 00:00:00.000000', 10),
(39, 'Uppercase and lowercase don\'t differentiate in Starred section', 'Uppercase and lowercase don\'t differentiate in Starred section', '', '2019-01-31 00:00:00.000000', 10),
(40, 'Star icon must be displayed while filtering rooms for unread and read rooms', 'Star icon must be displayed while filtering rooms for unread and read rooms', '', '2019-01-31 00:00:00.000000', 10),
(41, 'Verify the Filter rooms/DM functionality from World view', 'Verify the Filter rooms/DM functionality from World view', '', '2019-01-31 00:00:00.000000', 11),
(42, 'Verify the FAB button functionality displayed in World view', 'Verify the FAB button functionality displayed in World view', '', '2019-01-31 00:00:00.000000', 12),
(43, 'App should be successfully launched', 'App should be successfully launched', '', '2019-01-31 00:00:00.000000', 13),
(44, 'Verify that Header should be "Create room"', 'Verify that Header should be "Create room"', '', '2019-01-31 00:00:00.000000', 13),
(45, 'Verify close button is available for Add room at top left.', 'Verify close button is available for Add room at top left.', '', '2019-01-31 00:00:00.000000', 13),
(46, 'Verify if user is able to enter text in \'Room name\' compose field', 'Verify if user is able to enter text in \'Room name\' compose field', '', '2019-01-31 00:00:00.000000', 13),
(47, 'Verify if keyboard pops up automatically upon tapping Create Room', 'Verify if keyboard pops up automatically upon tapping Create Room', '', '2019-01-31 00:00:00.000000', 13),
(48, 'Verify if Tick mark enables on entering text, and gets disabled if no text in compose field.', 'Verify if Tick mark enables on entering text, and gets disabled if no text in compose field.', '', '2019-01-31 00:00:00.000000', 13),
(49, 'App should be successfully launched', 'App should be successfully launched', '', '2019-01-31 00:00:00.000000', 14),
(50, 'Verify if app opens "Create Room" page', 'Verify if app opens "Create Room" page', '', '2019-01-31 00:00:00.000000', 14),
(51, 'Verify if user able to enter text in "room name" compose field.', 'Verify if user able to enter text in "room name" compose field.', '', '2019-01-31 00:00:00.000000', 14),
(52, 'Verify if you greyed out tickmark is enabled(teal color) on text input.', 'Verify if you greyed out tickmark is enabled(teal color) on text input.', '', '2019-01-31 00:00:00.000000', 14),
(53, 'Tapping on tick mark should create a room with name entered and the new room view is presented.', 'Tapping on tick mark should create a room with name entered and the new room view is presented.', '', '2019-01-31 00:00:00.000000', 14),
(54, 'Verify if "Give your room a name" banner appears upon typing text and clearing it.', 'Verify if "Give your room a name" banner appears upon typing text and clearing it.', '', '2019-01-31 00:00:00.000000', 14),
(55, 'Title of the section: "Browse rooms"', 'Title of the section: "Browse rooms"', '', '2019-01-31 00:00:00.000000', 15),
(56, 'Text box should be available to filter rooms with place holder: Find a room.', 'Text box should be available to filter rooms with place holder: Find a room.', '', '2019-01-31 00:00:00.000000', 15),
(57, 'Invited rooms and rooms you have left previously should be listed below.', 'Invited rooms and rooms you have left previously should be listed below.', '', '2019-01-31 00:00:00.000000', 15),
(58, '"find a room" compose box should be visible.', '"find a room" compose box should be visible.', '', '2019-01-31 00:00:00.000000', 15),
(59, 'App should be successfully launched', 'App should be successfully launched', '', '2019-01-31 00:00:00.000000', 16),
(60, 'Verify if app opens \'Browse Rooms\' page', 'Verify if app opens \'Browse Rooms\' page', '', '2019-01-31 00:00:00.000000', 16),
(61, 'On tapping "+" button, user should be able to join the room', 'On tapping "+" button, user should be able to join the room', '', '2019-01-31 00:00:00.000000', 16),
(62, 'Toast message should be displayed at bottom upon joining', 'Toast message should be displayed at bottom upon joining', '', '2019-01-31 00:00:00.000000', 16),
(63, 'On tapping tick mark button user should leave the Room', 'On tapping tick mark button user should leave the Room', '', '2019-01-31 00:00:00.000000', 16),
(64, 'Toast message should be displayed at bottom upon leaving ', 'Toast message should be displayed at bottom upon leaving ', '', '2019-01-31 00:00:00.000000', 16),
(65, 'Verify joining the room with "join" buttons displayed at the bottom of room preview and "room details" of room preview', 'Verify joining the room with "join" buttons displayed at the bottom of room preview and "room details" of room preview', '', '2019-01-31 00:00:00.000000', 16),
(66, 'App should be successfully launched', 'App should be successfully launched', '', '2019-01-31 00:00:00.000000', 17),
(67, 'Verify if app opens Browse rooms page ', 'Verify if app opens Browse rooms page ', '', '2019-01-31 00:00:00.000000', 17),
(68, 'Tapping on eye icon it should redirect to Room Preview.', 'Tapping on eye icon it should redirect to Room Preview.', '', '2019-01-31 00:00:00.000000', 17),
(69, 'All the content of the room with \'JOIN" button at the bottom should be displayed.', 'All the content of the room with \'JOIN" button at the bottom should be displayed.', '', '2019-01-31 00:00:00.000000', 17),
(70, 'In room details, "join" button with MEMBERS list should be displayed.', 'In room details, "join" button with MEMBERS list should be displayed.', '', '2019-01-31 00:00:00.000000', 17),
(71, 'App should be successfully launched', 'App should be successfully launched', '', '2019-01-31 00:00:00.000000', 18),
(72, 'Verify if app opens Browse Rooms page ', 'Verify if app opens Browse Rooms page ', '', '2019-01-31 00:00:00.000000', 18),
(73, 'Verify the Members count of Rooms is correct under rooms listed and header of room preview.', 'Verify the Members count of Rooms is correct under rooms listed and header of room preview.', '', '2019-01-31 00:00:00.000000', 18),
(74, 'App should be successfully launched', 'App should be successfully launched', '', '2019-01-31 00:00:00.000000', 19),
(75, 'Verify if app opens Create Room page ', 'Verify if app opens Create Room page ', '', '2019-01-31 00:00:00.000000', 19),
(76, 'Header should have "No Connection" banner.', 'Header should have "No Connection" banner.', '', '2019-01-31 00:00:00.000000', 19),
(77, 'On creating room it should display a toast message: "This room couldn\'t be created. Try again in few minutes"', 'On creating room it should display a toast message: "This room couldn\'t be created. Try again in few minutes"', '', '2019-01-31 00:00:00.000000', 19),
(78, 'App should be successfully launched', 'App should be successfully launched', '', '2019-01-31 00:00:00.000000', 20),
(79, 'Verify if New (red label) is available for Rooms you\'ve been invite to', 'Verify if New (red label) is available for Rooms you\'ve been invite to', '', '2019-01-31 00:00:00.000000', 20),
(80, 'New (red label) should not be visible once add room window is closed', 'New (red label) should not be visible once add room window is closed', '', '2019-01-31 00:00:00.000000', 20),
(81, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 21),
(82, 'Group message, Message a bot and Create room, Browse Rooms options should be displayed.', 'Group message, Message a bot and Create room, Browse Rooms options should be displayed.', '', '2019-01-31 00:00:00.000000', 21),
(83, 'Browse rooms screen must be displayed', 'Browse rooms screen must be displayed', '', '2019-01-31 00:00:00.000000', 21),
(84, 'Room preview screen for the selected room must be displayed.', 'Room preview screen for the selected room must be displayed.', '', '2019-01-31 00:00:00.000000', 21),
(85, 'User should be taken back to world view.', 'User should be taken back to world view.', '', '2019-01-31 00:00:00.000000', 21),
(86, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 22),
(87, 'Group message, Message a bot and Create room, Browse Rooms options should be displayed.', 'Group message, Message a bot and Create room, Browse Rooms options should be displayed.', '', '2019-01-31 00:00:00.000000', 22),
(88, 'Add room screen must be displayed', 'Add room screen must be displayed', '', '2019-01-31 00:00:00.000000', 22),
(89, 'Room preview screen for the selected room must be displayed.Join button along with the room name should be displayed in the bottom of the screen.', 'Room preview screen for the selected room must be displayed.Join button along with the room name should be displayed in the bottom of the screen.', '', '2019-01-31 00:00:00.000000', 22),
(90, ' Room details screen should be displayed along with "Join" link (room name should be greyed out)', ' Room details screen should be displayed along with "Join" link (room name should be greyed out)', '', '2019-01-31 00:00:00.000000', 22),
(91, 'User should join the room and user should be directed to space view with a toast message saying "You Joined <<RoomName>>"', 'User should join the room and user should be directed to space view with a toast message saying "You Joined <<RoomName>>"', '', '2019-01-31 00:00:00.000000', 22),
(92, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 23),
(93, 'Group message, Message a bot,Create a Room, Browse rooms options should be displayed.', 'Group message, Message a bot,Create a Room, Browse rooms options should be displayed.', '', '2019-01-31 00:00:00.000000', 23),
(94, 'Browse rooms screen must be displayed', 'Browse rooms screen must be displayed', '', '2019-01-31 00:00:00.000000', 23),
(95, 'Room preview screen for the selected room must be displayed.(Join button along with the room name should be displayed in the bottom of the screen.)', 'Room preview screen for the selected room must be displayed.(Join button along with the room name should be displayed in the bottom of the screen.)', '', '2019-01-31 00:00:00.000000', 23),
(96, '5. User should join the room and user should be directed to space view with a toast message saying "You Joined <<roomname>>"', ' User should join the room and user should be directed to space view with a toast message saying "You Joined <<roomname>>"', '', '2019-01-31 00:00:00.000000', 23),
(97, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 24),
(98, 'Room, Direct message options should be displayed.', 'Room, Direct message options should be displayed.', '', '2019-01-31 00:00:00.000000', 24),
(99, 'Add room screen must be displayed', 'Add room screen must be displayed', '', '2019-01-31 00:00:00.000000', 24),
(100, 'Room preview screen for the selected room must be displayed.Join button along with the room name should be displayed in the bottom of the screen.', 'Room preview screen for the selected room must be displayed.Join button along with the room name should be displayed in the bottom of the screen.', '', '2019-01-31 00:00:00.000000', 24),
(101, 'The collapsed messages summary in the Room Preview must be displayed with the following:', 'The collapsed messages summary in the Room Preview must be displayed with the following:', '', '2019-01-31 00:00:00.000000', 24),
(102, 'Collapsed summary should expand and show 10 messages at once and remaining messages should still be collapsed with collapsed count - 10 as the count ', 'Collapsed summary should expand and show 10 messages at once and remaining messages should still be collapsed with collapsed count - 10 as the count ', '', '2019-01-31 00:00:00.000000', 24),
(103, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 25),
(104, 'Room, Direct message options should be displayed.', 'Room, Direct message options should be displayed.', '', '2019-01-31 00:00:00.000000', 25),
(105, 'Browse Rooms screen must be displayed', 'Browse Rooms screen must be displayed', '', '2019-01-31 00:00:00.000000', 25),
(106, 'Room preview screen for the selected room must be displayed.Join button along with the room name should be displayed in the bottom of the screen.', 'Room preview screen for the selected room must be displayed.Join button along with the room name should be displayed in the bottom of the screen.', '', '2019-01-31 00:00:00.000000', 25),
(107, 'The room should be rendered with data ( image, doc files, meet video chip, web chips/URL\'s, CML attachments) properly without any UI issues', 'The room should be rendered with data ( image, doc files, meet video chip, web chips/URL\'s, CML attachments) properly without any UI issues', '', '2019-01-31 00:00:00.000000', 25),
(108, 'Room preview should render the system messages and its appropriate icons properly without any UI issues.', 'Room preview should render the system messages and its appropriate icons properly without any UI issues.', '', '2019-01-31 00:00:00.000000', 25),
(109, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 26),
(110, 'Room, Direct message options should be displayed.', 'Room, Direct message options should be displayed.', '', '2019-01-31 00:00:00.000000', 26),
(111, 'Add room screen must be displayed', 'Add room screen must be displayed', '', '2019-01-31 00:00:00.000000', 26),
(112, 'Room preview screen for the selected room must be displayed.Join button along with the room name should be displayed in the bottom of the screen.', 'Room preview screen for the selected room must be displayed.Join button along with the room name should be displayed in the bottom of the screen.', '', '2019-01-31 00:00:00.000000', 26),
(113, 'User should be displayed an overlay displaying two options:', 'User should be displayed an overlay displaying two options:', '', '2019-01-31 00:00:00.000000', 26),
(114, 'User should be able to launch the app', 'User should be able to launch the app', '', '2019-01-31 00:00:00.000000', 27),
(115, 'The FAB button must be present in the bottom right corner of the screen and always in the visible region.', 'The FAB button must be present in the bottom right corner of the screen and always in the visible region.', '', '2019-01-31 00:00:00.000000', 27),
(116, 'Conversation launcher screen should be displayed with focus in "Find people" search field', 'Conversation launcher screen should be displayed with focus in "Find people" search field', '', '2019-01-31 00:00:00.000000', 27),
(117, '"Group message", "Message a bot", "Create room", "Browse rooms" options should be present below "find people" text box.', '"Group message", "Message a bot", "Create room", "Browse rooms" options should be present below "find people" text box.', '', '2019-01-31 00:00:00.000000', 27),
(118, 'Suggest contacts under "frequent" header should be present below "browse rooms"', 'Suggest contacts under "frequent" header should be present below "browse rooms"', '', '2019-01-31 00:00:00.000000', 27),
(119, 'User should be able to launch the app', 'User should be able to launch the app', '', '2019-01-31 00:00:00.000000', 28),
(120, 'The FAB button must be present in the bottom right corner of the screen and always in the visible region.', 'The FAB button must be present in the bottom right corner of the screen and always in the visible region.', '', '2019-01-31 00:00:00.000000', 28),
(121, 'Conversation launcher screen must be displayed along with their corresponding icons.', 'Conversation launcher screen must be displayed along with their corresponding icons.', '', '2019-01-31 00:00:00.000000', 28),
(122, 'Launch the app', 'Launch the app', '', '2019-01-31 00:00:00.000000', 29),
(123, 'Verify if the FAB button exists in the world view', 'Verify if the FAB button exists in the world view', '', '2019-01-31 00:00:00.000000', 29),
(124, 'Tap on the FAB button', 'Tap on the FAB button', '', '2019-01-31 00:00:00.000000', 29),
(125, 'Tap on the Group message option', 'Tap on the Group message option', '', '2019-01-31 00:00:00.000000', 29),
(126, 'Verify the Group message contacts screen', 'Verify the Group message contacts screen', '', '2019-01-31 00:00:00.000000', 29),
(127, 'Tap on more than one contact from the suggestions.', 'Tap on more than one contact from the suggestions.', '', '2019-01-31 00:00:00.000000', 29),
(128, 'tap on the enabled tick mark button from top right corner', 'tap on the enabled tick mark button from top right corner', '', '2019-01-31 00:00:00.000000', 29),
(129, 'Type any text message in compose and tap on send button', 'Type any text message in compose and tap on send button', '', '2019-01-31 00:00:00.000000', 29),
(130, 'App should be successfully launched', 'App should be successfully launched', '', '2019-01-31 00:00:00.000000', 31),
(131, 'Verify the header should be "DM details"', 'Verify the header should be "DM details"', '', '2019-01-31 00:00:00.000000', 31),
(132, 'Navigate back button should be available for DM details page', 'Navigate back button should be available for DM details page', '', '2019-01-31 00:00:00.000000', 31),
(133, 'Verify Star button.', 'Verify Star button.', '', '2019-01-31 00:00:00.000000', 31),
(134, 'Verify Notifications toggle button', 'Verify Notifications toggle button', '', '2019-01-31 00:00:00.000000', 31),
(135, 'Verify Hide button', 'Verify Hide button', '', '2019-01-31 00:00:00.000000', 31),
(136, 'App should be successfully launched', 'App should be successfully launched', '', '2019-01-31 00:00:00.000000', 32),
(137, 'On starring DM user conversation should be listed under Starred Section', 'On starring DM user conversation should be listed under Starred Section', '', '2019-01-31 00:00:00.000000', 32),
(138, 'On Unstarring DM user conversation should not be listed under Starred Section ', 'On Unstarring DM user conversation should not be listed under Starred Section ', '', '2019-01-31 00:00:00.000000', 32),
(139, 'Verify if icon color fills with green color upon starring', 'Verify if icon color fills with green color upon starring', '', '2019-01-31 00:00:00.000000', 32),
(140, 'Verify if icon color changes to white upon Unstarring', 'Verify if icon color changes to white upon Unstarring', '', '2019-01-31 00:00:00.000000', 32),
(141, 'App should be successfully launched', 'App should be successfully launched', '', '2019-01-31 00:00:00.000000', 33),
(142, 'Toggle off for Notifications for the DM should only receive @ mention Notifications', 'Toggle off for Notifications for the DM should only receive @ mention Notifications', '', '2019-01-31 00:00:00.000000', 33),
(143, 'Toggle On for Notifications for DM should receive all notifications', 'Toggle On for Notifications for DM should receive all notifications', '', '2019-01-31 00:00:00.000000', 33),
(144, 'Verify the Bell (mute) symbol on toggle off and on', 'Verify the Bell (mute) symbol on toggle off and on', '', '2019-01-31 00:00:00.000000', 33),
(145, 'Verify the text "Only @mentions" on Notifications Toggle off', 'Verify the text "Only @mentions" on Notifications Toggle off', '', '2019-01-31 00:00:00.000000', 33),
(146, 'Verify the text "General setting will apply" on Notifications Toggle on', 'Verify the text "General setting will apply" on Notifications Toggle on', '', '2019-01-31 00:00:00.000000', 33),
(147, 'App should be successfully launched', 'App should be successfully launched', '', '2019-01-31 00:00:00.000000', 34),
(148, 'Verify if other is listed under this section along with you', 'Verify if other is listed under this section along with you', '', '2019-01-31 00:00:00.000000', 34),
(149, 'if any member is online, presence indicator should be ON with green color', 'if any member is online, presence indicator should be ON with green color', '', '2019-01-31 00:00:00.000000', 34),
(150, 'Verify both Members name and email\'s are mentioned', 'Verify both Members name and email\'s are mentioned', '', '2019-01-31 00:00:00.000000', 34),
(151, 'App should be successfully launched', 'App should be successfully launched', '', '2019-01-31 00:00:00.000000', 35),
(152, 'Toast message should display "Messages with \'username\' are hidden" with UNDO option and should Redirect to world view', 'Toast message should display "Messages with \'username\' are hidden" with UNDO option and should Redirect to world view', '', '2019-01-31 00:00:00.000000', 35),
(153, 'Type the same user name in "Find room or DM" from world view and verify if the DM conversation is not available', 'Type the same user name in "Find room or DM" from world view and verify if the DM conversation is not available', '', '2019-01-31 00:00:00.000000', 35),
(154, 'Verify if hidden user sends a message you should receive notification and should Reappear in recent world view', 'Verify if hidden user sends a message you should receive notification and should Reappear in recent world view', '', '2019-01-31 00:00:00.000000', 35),
(155, 'DM conversation should unhide when you search the same member from Search panel', 'DM conversation should unhide when you search the same member from Search panel', '', '2019-01-31 00:00:00.000000', 35),
(156, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 36),
(157, 'User should be directed to space view', 'User should be directed to space view', '', '2019-01-31 00:00:00.000000', 36),
(158, 'The nav bar should contain ', 'The nav bar should contain ', '', '2019-01-31 00:00:00.000000', 36),
(159, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 37),
(160, 'User should be directed to the space view', 'User should be directed to the space view', '', '2019-01-31 00:00:00.000000', 37),
(161, 'The persistent header must be displayed in the primary app color with the room creation information', 'The persistent header must be displayed in the primary app color with the room creation information', '', '2019-01-31 00:00:00.000000', 37),
(162, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 38),
(163, 'User should be taken to space view of the selected room', 'User should be taken to space view of the selected room', '', '2019-01-31 00:00:00.000000', 38),
(164, 'User should be taken to topic view', 'User should be taken to topic view', '', '2019-01-31 00:00:00.000000', 38),
(165, 'User should be able to successfully post the message', 'User should be able to successfully post the message', '', '2019-01-31 00:00:00.000000', 38),
(166, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 39),
(167, 'User should be taken to the space view', 'User should be taken to the space view', '', '2019-01-31 00:00:00.000000', 39),
(168, 'There must a divider labelled - "Unread" and the unread topics must be displayed below it with appropriate date dividers and timestamps. "New" label will be displayed and then be replaced with a blue dot for new messages which perishes in short time', 'There must a divider labelled - "Unread" and the unread topics must be displayed below it with appropriate date dividers and timestamps. "New" label will be displayed and then be replaced with a blue dot for new messages which perishes in short time', '', '2019-01-31 00:00:00.000000', 39),
(169, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 40),
(170, 'User should be taken to the space view', 'User should be taken to the space view', '', '2019-01-31 00:00:00.000000', 40),
(171, 'There must a divider labelled - "Unread" and the unread topics must be displayed below it with appropriate date dividers and timestamps. "New" label will be displayed and then be replaced with a blue dot for new messages which perishes in short time', 'There must a divider labelled - "Unread" and the unread topics must be displayed below it with appropriate date dividers and timestamps. "New" label will be displayed and then be replaced with a blue dot for new messages which perishes in short time', '', '2019-01-31 00:00:00.000000', 40),
(172, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 41),
(173, 'User should be navigated to space view', 'User should be navigated to space view', '', '2019-01-31 00:00:00.000000', 41),
(174, 'The summary of the sender names should follow the below convention', 'The summary of the sender names should follow the below convention', '', '2019-01-31 00:00:00.000000', 41),
(175, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 42),
(176, 'User should be directed to the space view', 'User should be directed to the space view', '', '2019-01-31 00:00:00.000000', 42),
(177, 'New Conversation view should be opened.', 'New Conversation view should be opened.', '', '2019-01-31 00:00:00.000000', 42),
(178, 'User should return to space view', 'User should return to space view', '', '2019-01-31 00:00:00.000000', 42),
(179, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 43),
(180, 'User should be directed to space view', 'User should be directed to space view', '', '2019-01-31 00:00:00.000000', 43),
(181, 'The file opens in its native app(sheets/slides/docs)', 'The file opens in its native app(sheets/slides/docs)', '', '2019-01-31 00:00:00.000000', 43),
(182, 'User returns back to space view', 'User returns back to space view', '', '2019-01-31 00:00:00.000000', 43),
(183, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 44),
(184, 'User should be directed to the space view', 'User should be directed to the space view', '', '2019-01-31 00:00:00.000000', 44),
(185, 'In transition from space to topic, the topic summaries from space should disappear, and then the messages in the topic being rendered should fade in.', 'In transition from space to topic, the topic summaries from space should disappear, and then the messages in the topic being rendered should fade in.', '', '2019-01-31 00:00:00.000000', 44),
(186, 'In the transition back from the topic and space (both using the system back button and the back button in the action bar), the messages on screen should smoothly translate from their current position to their position in the space', 'In the transition back from the topic and space (both using the system back button and the back button in the action bar), the messages on screen should smoothly translate from their current position to their position in the space', '', '2019-01-31 00:00:00.000000', 44),
(187, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 45),
(188, 'User should be directed to the space view', 'User should be directed to the space view', '', '2019-01-31 00:00:00.000000', 45),
(189, 'The room should be rendered with data ( image, doc files, meet video chip, web chips/URL\'s, CML attachments) properly without any UI issues', 'The room should be rendered with data ( image, doc files, meet video chip, web chips/URL\'s, CML attachments) properly without any UI issues', '', '2019-01-31 00:00:00.000000', 45),
(190, 'System messages should be displayed', 'System messages should be displayed', '', '2019-01-31 00:00:00.000000', 46),
(191, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 47),
(192, 'User should be taken to space view of the selected room', 'User should be taken to space view of the selected room', '', '2019-01-31 00:00:00.000000', 47),
(193, 'User should be shown an overlay with three options: ', 'User should be shown an overlay with three options: ', '', '2019-01-31 00:00:00.000000', 47),
(194, 'Focus should shift to Compose field with old message', 'Focus should shift to Compose field with old message', '', '2019-01-31 00:00:00.000000', 47),
(195, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 48),
(196, 'User should be directed to the space view', 'User should be directed to the space view', '', '2019-01-31 00:00:00.000000', 48),
(197, 'App should be successfully launched', 'App should be successfully launched', '', '2019-01-31 00:00:00.000000', 49),
(198, 'Verify the header should be "Room details"', 'Verify the header should be "Room details"', '', '2019-01-31 00:00:00.000000', 49),
(199, 'Navigate back button should be available for Room details page', 'Navigate back button should be available for Room details page', '', '2019-01-31 00:00:00.000000', 49),
(200, 'Verify the Ui of Room details page', 'Verify the Ui of Room details page', '', '2019-01-31 00:00:00.000000', 49),
(201, ' App should be successfully launched', ' App should be successfully launched', '', '2019-01-31 00:00:00.000000', 50),
(202, 'Verify if Room name is editable', 'Verify if Room name is editable', '', '2019-01-31 00:00:00.000000', 50),
(203, 'On editing Right symbol should appear beside Room details', 'On editing Right symbol should appear beside Room details', '', '2019-01-31 00:00:00.000000', 50),
(204, 'Editing Room name should intake all special characters & emoji\'s', 'Editing Room name should intake all special characters & emoji\'s', '', '2019-01-31 00:00:00.000000', 50),
(205, 'On keeping Room name empty and same previous name Right Symbol should not appear', 'On keeping Room name empty and same previous name Right Symbol should not appear', '', '2019-01-31 00:00:00.000000', 50),
(206, 'On Editing room name and tapping on right symbol, Room name should change and toast should display like Successfully changed room name to (new name)', 'On Editing room name and tapping on right symbol, Room name should change and toast should display like Successfully changed room name to (new name)', '', '2019-01-31 00:00:00.000000', 50),
(207, 'App should be successfully launched', 'App should be successfully launched', '', '2019-01-31 00:00:00.000000', 51),
(208, 'Verify if Add people page opens', 'Verify if Add people page opens', '', '2019-01-31 00:00:00.000000', 51),
(209, 'Verify the Ui of Add people page', 'Verify the Ui of Add people page', '', '2019-01-31 00:00:00.000000', 51),
(210, 'On closing Add people page app should stay on Room details page', 'On closing Add people page app should stay on Room details page', '', '2019-01-31 00:00:00.000000', 51),
(211, 'Verify if we can Add single or multiple users to the Room', 'Verify if we can Add single or multiple users to the Room', '', '2019-01-31 00:00:00.000000', 51),
(212, '  App should be successfully launched', '  App should be successfully launched', '', '2019-01-31 00:00:00.000000', 52),
(213, 'On starring Room should be listed under Starred Section', 'On starring Room should be listed under Starred Section', '', '2019-01-31 00:00:00.000000', 52),
(214, 'On Unstarring Room should not be listed under Starred Section ', 'On Unstarring Room should not be listed under Starred Section ', '', '2019-01-31 00:00:00.000000', 52),
(215, 'Verify if icon color fills with green color upon starring', 'Verify if icon color fills with green color upon starring', '', '2019-01-31 00:00:00.000000', 52),
(216, 'Verify if icon color changes to white upon Unstarring', 'Verify if icon color changes to white upon Unstarring', '', '2019-01-31 00:00:00.000000', 52),
(217, 'App should be successfully launched', 'App should be successfully launched', '', '2019-01-31 00:00:00.000000', 53),
(218, 'Toggle off for Notifications for the Room should only receive @ mention Notifications', 'Toggle off for Notifications for the Room should only receive @ mention Notifications', '', '2019-01-31 00:00:00.000000', 53),
(219, 'Toggle On for Notifications for the Room should receive all notifications', 'Toggle On for Notifications for the Room should receive all notifications', '', '2019-01-31 00:00:00.000000', 53),
(220, 'Verify the Bell (mute) symbol on toggle off and on', 'Verify the Bell (mute) symbol on toggle off and on', '', '2019-01-31 00:00:00.000000', 53),
(221, 'Verify the text "Only @mentions" on Notifications Toggle off', 'Verify the text "Only @mentions" on Notifications Toggle off', '', '2019-01-31 00:00:00.000000', 53),
(222, 'Verify the text "General setting will apply" on Notifications Toggle on', 'Verify the text "General setting will apply" on Notifications Toggle on', '', '2019-01-31 00:00:00.000000', 53),
(223, 'App should be successfully launched', 'App should be successfully launched', '', '2019-01-31 00:00:00.000000', 54),
(224, 'Verify the text "you will be able to return" under Leave option', 'Verify the text "you will be able to return" under Leave option', '', '2019-01-31 00:00:00.000000', 54),
(225, 'Tapping on leave should show a dialog message stating "Leave (room name) ? - You won\'t receive updates or be able to post messages Until you rejoin the Room, but you can rejoin at any time" ', 'Tapping on leave should show a dialog message stating "Leave (room name) ? - You won\'t receive updates or be able to post messages Until you rejoin the Room, but you can rejoin at any time" ', '', '2019-01-31 00:00:00.000000', 54),
(226, 'Verify both cancel and leave room buttons are working', 'Verify both cancel and leave room buttons are working', '', '2019-01-31 00:00:00.000000', 54),
(227, 'Tapping on Leave room should redirect to World view with a toast message stating "You left (room name)"', 'Tapping on Leave room should redirect to World view with a toast message stating "You left (room name)"', '', '2019-01-31 00:00:00.000000', 54),
(228, 'App should be successfully launched', 'App should be successfully launched', '', '2019-01-31 00:00:00.000000', 55),
(229, 'Verify if all the Room members are listed under this section', 'Verify if all the Room members are listed under this section', '', '2019-01-31 00:00:00.000000', 55),
(230, 'if any member is online, presence indicator should be ON with green color', 'if any member is online, presence indicator should be ON with green color', '', '2019-01-31 00:00:00.000000', 55),
(231, 'Verify Members name and Members email\'s are mentioned ', 'Verify Members name and Members email\'s are mentioned ', '', '2019-01-31 00:00:00.000000', 55),
(232, 'Tapping on member menu, it should show Remove from room option', 'Tapping on member menu, it should show Remove from room option', '', '2019-01-31 00:00:00.000000', 55),
(233, 'Tapping on Remove from room, it should display a dialog message stating " Remove (username) from (room name) ? - (user name) will lose all access To this room and will not be able to join until invited again"', 'Tapping on Remove from room, it should display a dialog message stating " Remove (username) from (room name) ? - (user name) will lose all access To this room and will not be able to join until invited again"', '', '2019-01-31 00:00:00.000000', 55),
(234, 'Verify both cancel and remove buttons are working', 'Verify both cancel and remove buttons are working', '', '2019-01-31 00:00:00.000000', 55),
(235, 'Tapping on remove should remove the user without any toast message', 'Tapping on remove should remove the user without any toast message', '', '2019-01-31 00:00:00.000000', 55),
(236, 'App should be successfully launched', 'App should be successfully launched', '', '2019-01-31 00:00:00.000000', 56),
(237, 'Verify the text "These people can view and join this room at any Time , but will not receive updates unless mentioned" written under Can Join ', 'Verify the text "These people can view and join this room at any Time , but will not receive updates unless mentioned" written under Can Join ', '', '2019-01-31 00:00:00.000000', 56),
(238, 'Verify if all invited members are listed under this section', 'Verify if all invited members are listed under this section', '', '2019-01-31 00:00:00.000000', 56),
(239, 'if any member is online, presence indicator should be ON with green color', 'if any member is online, presence indicator should be ON with green color', '', '2019-01-31 00:00:00.000000', 56),
(240, 'Verify Members name and Members email\'s are mentioned ', 'Verify Members name and Members email\'s are mentioned ', '', '2019-01-31 00:00:00.000000', 56),
(241, 'Tapping on member menu, it should show Remove from room option', 'Tapping on member menu, it should show Remove from room option', '', '2019-01-31 00:00:00.000000', 56),
(242, 'Tapping on Remove from room, it should display a dialog message stating "Remove (username) from (room name) ? - (user name) will lose all access To this room and will not be able to join until invited again"', 'Tapping on Remove from room, it should display a dialog message stating "Remove (username) from (room name) ? - (user name) will lose all access To this room and will not be able to join until invited again"', '', '2019-01-31 00:00:00.000000', 56),
(243, 'Verify both cancel and remove buttons are working', 'Verify both cancel and remove buttons are working', '', '2019-01-31 00:00:00.000000', 56),
(244, 'Tapping on remove should remove the user without any toast message', 'Tapping on remove should remove the user without any toast message', '', '2019-01-31 00:00:00.000000', 56),
(245, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 57),
(246, 'User should be taken to the space view', 'User should be taken to the space view', '', '2019-01-31 00:00:00.000000', 57),
(247, 'User should be taken to the topic view of the selected topic.', 'User should be taken to the topic view of the selected topic.', '', '2019-01-31 00:00:00.000000', 57),
(248, 'The following verifications should be successful:', 'The following verifications should be successful:', '', '2019-01-31 00:00:00.000000', 57),
(249, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 58),
(250, 'User should be taken to the space view', 'User should be taken to the space view', '', '2019-01-31 00:00:00.000000', 58),
(251, 'User should be taken to the new topic view', 'User should be taken to the new topic view', '', '2019-01-31 00:00:00.000000', 58),
(252, 'The following verifications should be successful:', 'The following verifications should be successful:', '', '2019-01-31 00:00:00.000000', 58),
(253, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 59),
(254, 'User should be taken to the space view', 'User should be taken to the space view', '', '2019-01-31 00:00:00.000000', 59),
(255, 'User should be taken to the topic view of the selected topic.', 'User should be taken to the topic view of the selected topic.', '', '2019-01-31 00:00:00.000000', 59),
(256, 'The compose field should contain the following components:', 'The compose field should contain the following components:', '', '2019-01-31 00:00:00.000000', 59),
(257, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 60),
(258, 'User should be taken to the space view', 'User should be taken to the space view', '', '2019-01-31 00:00:00.000000', 60),
(259, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 61),
(260, 'User should be taken to the space view', 'User should be taken to the space view', '', '2019-01-31 00:00:00.000000', 61),
(261, 'User should be taken to the topic view of the selected topic.', 'User should be taken to the topic view of the selected topic.', '', '2019-01-31 00:00:00.000000', 61),
(262, 'User should be directed to "Select a Photo" screen', 'User should be directed to "Select a Photo" screen', '', '2019-01-31 00:00:00.000000', 61),
(263, 'User selected file should be shown as attachment with a cancel button on the chip generated', 'User selected file should be shown as attachment with a cancel button on the chip generated', '', '2019-01-31 00:00:00.000000', 61),
(264, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 62),
(265, 'User should be taken to the space view', 'User should be taken to the space view', '', '2019-01-31 00:00:00.000000', 62),
(266, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 63),
(267, 'User should be taken to the space view', 'User should be taken to the space view', '', '2019-01-31 00:00:00.000000', 63),
(268, 'User should be taken to the topic view of the selected topic.', 'User should be taken to the topic view of the selected topic.', '', '2019-01-31 00:00:00.000000', 63),
(269, 'Video meeting chip should be generated ', 'Video meeting chip should be generated ', '', '2019-01-31 00:00:00.000000', 63),
(270, 'The video meeting chip should have the label - "Video meeting" with a subtitle - "Hangouts Meet" and cancel button on top right with another label - "Join Video Meeting" with meet icon.', 'The video meeting chip should have the label - "Video meeting" with a subtitle - "Hangouts Meet" and cancel button on top right with another label - "Join Video Meeting" with meet icon.', '', '2019-01-31 00:00:00.000000', 63),
(271, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 65),
(272, 'User should be taken to space view of the selected room', 'User should be taken to space view of the selected room', '', '2019-01-31 00:00:00.000000', 65),
(273, 'User should navigate to topic view', 'User should navigate to topic view', '', '2019-01-31 00:00:00.000000', 65),
(274, 'User should be shown an overlay with three options: ', 'User should be shown an overlay with three options: ', '', '2019-01-31 00:00:00.000000', 65),
(275, 'User should be taken to space view', 'User should be taken to space view', '', '2019-01-31 00:00:00.000000', 66),
(276, 'User should be taken to topic view ', 'User should be taken to topic view ', '', '2019-01-31 00:00:00.000000', 66),
(277, 'No connection grey bar should be displayed', 'No connection grey bar should be displayed', '', '2019-01-31 00:00:00.000000', 66),
(278, 'Message should be posted in grey color and with "failed to send" message with \'i\' icon should be displayed', 'Message should be posted in grey color and with "failed to send" message with \'i\' icon should be displayed', '', '2019-01-31 00:00:00.000000', 66),
(279, 'Overlay displaying - "Copy text, Resend, delete" options must be displayed', 'Overlay displaying - "Copy text, Resend, delete" options must be displayed', '', '2019-01-31 00:00:00.000000', 66);
INSERT INTO `reports_teststeps` (`id`, `test_step`, `test_step_desc`, `golden_screen`, `created`, `test_id`) VALUES
(280, 'failed message should be deleted.', 'failed message should be deleted.', '', '2019-01-31 00:00:00.000000', 66),
(281, 'Message should be posted in grey color and with "failed to send" message with \'i\' icon should be displayed', 'Message should be posted in grey color and with "failed to send" message with \'i\' icon should be displayed', '', '2019-01-31 00:00:00.000000', 66),
(282, 'No connection grey bar should be removed.', 'No connection grey bar should be removed.', '', '2019-01-31 00:00:00.000000', 66),
(283, 'Message should be posted in grey color and with "failed to send" message with \'i\' icon should be displayed', 'Message should be posted in grey color and with "failed to send" message with \'i\' icon should be displayed', '', '2019-01-31 00:00:00.000000', 66),
(284, 'Every topic should have the notification icon in the top right.', 'Every topic should have the notification icon in the top right.', '', '2019-01-31 00:00:00.000000', 67),
(285, 'When pressed, the type of icon should toggle.', 'When pressed, the type of icon should toggle.', '', '2019-01-31 00:00:00.000000', 67),
(286, 'If offline, the icon will revert to the original.', 'If offline, the icon will revert to the original.', '', '2019-01-31 00:00:00.000000', 67),
(287, 'By default it should be off unless the user has replied to the topic (or started it).', 'By default it should be off unless the user has replied to the topic (or started it).', '', '2019-01-31 00:00:00.000000', 67),
(288, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 68),
(289, 'User should be navigated to Topic view', 'User should be navigated to Topic view', '', '2019-01-31 00:00:00.000000', 68),
(290, 'Verify upon Scrolling Up/Down the previous Messages should be loaded accordingly.', 'Verify upon Scrolling Up/Down the previous Messages should be loaded accordingly.', '', '2019-01-31 00:00:00.000000', 68),
(291, 'Verify upon Scrolling Up/Down the previous Messages should be loaded accordingly when entered from Search View.', 'Verify upon Scrolling Up/Down the previous Messages should be loaded accordingly when entered from Search View.', '', '2019-01-31 00:00:00.000000', 68),
(292, 'Every topic should have the notification icon in the top right.', 'Every topic should have the notification icon in the top right.', '', '2019-01-31 00:00:00.000000', 69),
(293, 'When pressed, the type of icon should toggle.', 'When pressed, the type of icon should toggle.', '', '2019-01-31 00:00:00.000000', 69),
(294, 'If offline, the icon will revert to the original.', 'If offline, the icon will revert to the original.', '', '2019-01-31 00:00:00.000000', 69),
(295, 'By default it should be off unless the user has replied to the topic (or started it).', 'By default it should be off unless the user has replied to the topic (or started it).', '', '2019-01-31 00:00:00.000000', 69),
(296, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 70),
(297, 'User should be navigated to Topic view', 'User should be navigated to Topic view', '', '2019-01-31 00:00:00.000000', 70),
(298, 'Verify upon Scrolling Up/Down the previous Messages should be loaded accordingly.', 'Verify upon Scrolling Up/Down the previous Messages should be loaded accordingly.', '', '2019-01-31 00:00:00.000000', 70),
(299, 'Verify upon Scrolling Up/Down the previous Messages should be loaded accordingly when entered from Search View.', 'Verify upon Scrolling Up/Down the previous Messages should be loaded accordingly when entered from Search View.', '', '2019-01-31 00:00:00.000000', 70),
(300, 'User should be navigated to the selected room', 'User should be navigated to the selected room', '', '2019-01-31 00:00:00.000000', 71),
(301, 'Search icon should be available on the header at the top right corner.', 'Search icon should be available on the header at the top right corner.', '', '2019-01-31 00:00:00.000000', 71),
(302, 'User should be navigated to space search', 'User should be navigated to space search', '', '2019-01-31 00:00:00.000000', 71),
(303, 'There should be Back\' button/arrow appears in the upper left, to cancel out of Search, The search box, Filter panel - room members and the contacts based on affinity sections with a separator in between ', 'There should be Back\' button/arrow appears in the upper left, to cancel out of Search, The search box, Filter panel - room members and the contacts based on affinity sections with a separator in between ', '', '2019-01-31 00:00:00.000000', 71),
(304, 'User should be navigated to the selected room', 'User should be navigated to the selected room', '', '2019-01-31 00:00:00.000000', 72),
(305, 'Search icon should be available on the header at the top right corner.', 'Search icon should be available on the header at the top right corner.', '', '2019-01-31 00:00:00.000000', 72),
(306, 'User should be navigated to space search', 'User should be navigated to space search', '', '2019-01-31 00:00:00.000000', 72),
(307, 'No results in <<current room=">> text along with Search all" button must be displayed and a blue spinner must be displayed in place of black button while loading', 'No results in <<current room=">> text along with Search all" button must be displayed and a blue spinner must be displayed in place of black button while loading', '', '2019-01-31 00:00:00.000000', 72),
(308, 'Search results pertaining to that keyword must be displayed and any collapsed messages should be displayed just with the count(99+ for more than 99) and the results must be in chronological order', 'Search results pertaining to that keyword must be displayed and any collapsed messages should be displayed just with the count(99+ for more than 99) and the results must be in chronological order', '', '2019-01-31 00:00:00.000000', 72),
(309, 'User should navigate to topic page(topic header and mute functionalities doesn\'t work currently)', 'User should navigate to topic page(topic header and mute functionalities doesn\'t work currently)', '', '2019-01-31 00:00:00.000000', 72),
(310, 'Search results must be retained and also the search keyword with clear search button ', 'Search results must be retained and also the search keyword with clear search button ', '', '2019-01-31 00:00:00.000000', 72),
(311, 'Search results from all rooms must be fetched and displayed with proper room name and the date dividers appropriately', 'Search results from all rooms must be fetched and displayed with proper room name and the date dividers appropriately', '', '2019-01-31 00:00:00.000000', 72),
(312, 'User should be navigated to the selected room', 'User should be navigated to the selected room', '', '2019-01-31 00:00:00.000000', 73),
(313, 'Search icon should be available on the header at the top right corner.', 'Search icon should be available on the header at the top right corner.', '', '2019-01-31 00:00:00.000000', 73),
(314, 'User should be navigated to space search', 'User should be navigated to space search', '', '2019-01-31 00:00:00.000000', 73),
(315, 'The results pertaining the member selected must be displayed', 'The results pertaining the member selected must be displayed', '', '2019-01-31 00:00:00.000000', 73),
(316, 'Search results pertaining to that member selected must be fetched from all the rooms and displayed', 'Search results pertaining to that member selected must be fetched from all the rooms and displayed', '', '2019-01-31 00:00:00.000000', 73),
(317, 'User should be navigated to the selected room', 'User should be navigated to the selected room', '', '2019-01-31 00:00:00.000000', 74),
(318, 'Search icon should be available on the header at the top right corner.', 'Search icon should be available on the header at the top right corner.', '', '2019-01-31 00:00:00.000000', 74),
(319, 'User should be navigated to space search', 'User should be navigated to space search', '', '2019-01-31 00:00:00.000000', 74),
(320, 'The results pertaining the member selected must be displayed', 'The results pertaining the member selected must be displayed', '', '2019-01-31 00:00:00.000000', 74),
(321, 'Search results pertaining to that content type selected must be fetched from all the rooms and displayed', 'Search results pertaining to that content type selected must be fetched from all the rooms and displayed', '', '2019-01-31 00:00:00.000000', 74),
(322, 'Search results pertaining to that contents selected must be fetched from all the rooms and displayed', 'Search results pertaining to that contents selected must be fetched from all the rooms and displayed', '', '2019-01-31 00:00:00.000000', 74),
(323, 'Launch the app', 'Launch the app', '', '2019-01-31 00:00:00.000000', 75),
(324, 'Mobile Notifications page should be shown', 'Mobile Notifications page should be shown', '', '2019-01-31 00:00:00.000000', 75),
(325, 'The overlay should contain the following:The heading label: "Mobile Notifications", All Messages, New conversations and those I\'ve replied to, Conversations I\'ve replied to. Only @mentions & direct messages', 'The overlay should contain the following:The heading label: "Mobile Notifications", All Messages, New conversations and those I\'ve replied to, Conversations I\'ve replied to. Only @mentions & direct messages', '', '2019-01-31 00:00:00.000000', 75),
(326, 'User should be able to navigate to settings of the device ', 'User should be able to navigate to settings of the device ', '', '2019-01-31 00:00:00.000000', 76),
(327, 'User should be able to navigate to app settings screen', 'User should be able to navigate to app settings screen', '', '2019-01-31 00:00:00.000000', 76),
(328, 'User should be able to disable the notifications for the app', 'User should be able to disable the notifications for the app', '', '2019-01-31 00:00:00.000000', 76),
(329, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 77),
(330, 'User should be taken to space view of the selected room', 'User should be taken to space view of the selected room', '', '2019-01-31 00:00:00.000000', 77),
(331, 'User should be taken to topic view of the selected topic', 'User should be taken to topic view of the selected topic', '', '2019-01-31 00:00:00.000000', 77),
(332, 'Autocomplete pop up should be rendered and below things should be verified successfully:1. all and existing members should be displayed in the bottom most section of the popup with their profile avatars rendered properly 2. Next to the existing members section should be the "Add to room" members section displaying the contacts who can be added to the space with text "Add to room" against the user names with profile avatars rendered properly.', 'Autocomplete pop up should be rendered and below things should be verified successfully:1. all and existing members should be displayed in the bottom most section of the popup with their profile avatars rendered properly 2. Next to the existing members section should be the "Add to room" members section displaying the contacts who can be added to the space with text "Add to room" against the user names with profile avatars rendered properly.', '', '2019-01-31 00:00:00.000000', 77),
(333, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 78),
(334, 'User should be taken to space view of the selected room', 'User should be taken to space view of the selected room', '', '2019-01-31 00:00:00.000000', 78),
(335, 'User should be taken to topic view of the selected topic', 'User should be taken to topic view of the selected topic', '', '2019-01-31 00:00:00.000000', 78),
(336, 'Autocomplete pop up should be rendered and below things should be verified successfully:1. \'all\' alone should be displayed in the  bottom most section of the popup with corresponding profile avatar rendered properly 2. Next to that section should be the "Add to room" members section displaying the contacts who can be added to the space with text "Add to room" against the user names with profile avatars rendered properly. 3. User should be able to scroll through the contacts list in the autocomplete pop up.', 'Autocomplete pop up should be rendered and below things should be verified successfully:1. \'all\' alone should be displayed in the  bottom most section of the popup with corresponding profile avatar rendered properly 2. Next to that section should be the "Add to room" members section displaying the contacts who can be added to the space with text "Add to room" against the user names with profile avatars rendered properly. 3. User should be able to scroll through the contacts list in the autocomplete pop up.', '', '2019-01-31 00:00:00.000000', 78),
(337, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 79),
(338, 'User should be taken to space view of the selected room', 'User should be taken to space view of the selected room', '', '2019-01-31 00:00:00.000000', 79),
(339, 'User should be taken to topic view of the selected topic', 'User should be taken to topic view of the selected topic', '', '2019-01-31 00:00:00.000000', 79),
(340, 'Autocomplete pop up should contain only one other contact of the DM with their profile avatar and username rendered without any UI issues.', 'Autocomplete pop up should contain only one other contact of the DM with their profile avatar and username rendered without any UI issues.', '', '2019-01-31 00:00:00.000000', 79),
(341, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 80),
(342, 'User should be taken to space view of the selected room', 'User should be taken to space view of the selected room', '', '2019-01-31 00:00:00.000000', 80),
(343, 'User should be taken to topic view of the selected topic', 'User should be taken to topic view of the selected topic', '', '2019-01-31 00:00:00.000000', 80),
(344, 'Autocomplete pop up should contain "all" and other existing members username with their profile avatars rendered properly ', 'Autocomplete pop up should contain "all" and other existing members username with their profile avatars rendered properly ', '', '2019-01-31 00:00:00.000000', 80),
(345, 'Selected user token is generated ', 'Selected user token is generated ', '', '2019-01-31 00:00:00.000000', 81),
(346, '@mention for the selected user is posted and the notification is sent for the user.without any UI issues', '@mention for the selected user is posted and the notification is sent for the user.without any UI issues', '', '2019-01-31 00:00:00.000000', 81),
(347, '@all is tokenised in primary app color', '@all is tokenised in primary app color', '', '2019-01-31 00:00:00.000000', 82),
(348, '@all mention is posted and the notification is sent for the user.', '@all mention is posted and the notification is sent for the user.', '', '2019-01-31 00:00:00.000000', 82),
(349, 'User should be navigated to space view', 'User should be navigated to space view', '', '2019-01-31 00:00:00.000000', 83),
(350, 'User should be navigated to topic view', 'User should be navigated to topic view', '', '2019-01-31 00:00:00.000000', 83),
(351, 'The user selected contact token should be generated in compose ', 'The user selected contact token should be generated in compose ', '', '2019-01-31 00:00:00.000000', 83),
(352, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 84),
(353, 'The presence indicator should be available in World view against 1:1 DM\'s, Autocomplete list, Room details page - Members list and Can join list, Invite people screen, DM contacts screen locations either active (green circle) or inactive (grey circle)', 'The presence indicator should be available in World view against 1:1 DM\'s, Autocomplete list, Room details page - Members list and Can join list, Invite people screen, DM contacts screen locations either active (green circle) or inactive (grey circle)', '', '2019-01-31 00:00:00.000000', 84),
(354, 'The app should rendered all the strings in all screens without any UI issues (padding, overlapping etc).\nEllipsis will be displayed for strings which doesn\'t fit the screen', 'The app should rendered all the strings in all screens without any UI issues (padding, overlapping etc).\nEllipsis will be displayed for strings which doesn\'t fit the screen', '', '2019-01-31 00:00:00.000000', 85),
(355, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 86),
(356, 'User should be displayed a overlay with "Help&feedback" option', 'User should be displayed a overlay with "Help&feedback" option', '', '2019-01-31 00:00:00.000000', 86),
(357, 'User should be taken to Help screen', 'User should be taken to Help screen', '', '2019-01-31 00:00:00.000000', 86),
(358, 'User should be taken to Feedback screen', 'User should be taken to Feedback screen', '', '2019-01-31 00:00:00.000000', 86),
(359, 'User should be able to successfully post the feedback', 'User should be able to successfully post the feedback', '', '2019-01-31 00:00:00.000000', 86),
(360, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 87),
(361, 'User should be taken to space view of the selected room', 'User should be taken to space view of the selected room', '', '2019-01-31 00:00:00.000000', 87),
(362, 'User should navigate to topic view', 'User should navigate to topic view', '', '2019-01-31 00:00:00.000000', 87),
(363, 'User should be shown an overlay with three options Copy Text, Edit, Delete, Send Feedback on this message', 'User should be shown an overlay with three options Copy Text, Edit, Delete, Send Feedback on this message', '', '2019-01-31 00:00:00.000000', 87),
(364, 'Delete message alert should be displayed', 'Delete message alert should be displayed', '', '2019-01-31 00:00:00.000000', 87),
(365, 'Delete message alert should be displayed with message saying: "Delete this message permanently <<username>>: "<<Message>>"" with "Cancel" and "Delete" buttons.', 'Delete message alert should be displayed with message saying: "Delete this message permanently <<username>>: "<<Message>>"" with "Cancel" and "Delete" buttons.', '', '2019-01-31 00:00:00.000000', 87),
(366, 'Alert must be dismissed.', 'Alert must be dismissed.', '', '2019-01-31 00:00:00.000000', 87),
(367, 'User should be shown an overlay with three options Copy Text, Edit, Delete, Send Feedback on this message ', 'User should be shown an overlay with three options Copy Text, Edit, Delete, Send Feedback on this message ', '', '2019-01-31 00:00:00.000000', 87),
(368, 'Delete message alert should be displayed', 'Delete message alert should be displayed', '', '2019-01-31 00:00:00.000000', 87),
(369, 'Message selected should be deleted and the deleted message should not be see', 'Message selected should be deleted and the deleted message should not be see', '', '2019-01-31 00:00:00.000000', 87),
(370, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 88),
(371, 'User should be taken to space view of the selected room/DM', 'User should be taken to space view of the selected room/DM', '', '2019-01-31 00:00:00.000000', 88),
(372, 'User should navigate to new conversation screen', 'User should navigate to new conversation screen', '', '2019-01-31 00:00:00.000000', 88),
(373, 'User should be able to post the message successfully.', 'User should be able to post the message successfully.', '', '2019-01-31 00:00:00.000000', 88),
(374, 'User should be shown an overlay with three options, Copy Text, Edit, Delete, Send Feedback on this message ', 'User should be shown an overlay with three options, Copy Text, Edit, Delete, Send Feedback on this message ', '', '2019-01-31 00:00:00.000000', 88),
(375, 'Delete message alert should be displayed', 'Delete message alert should be displayed', '', '2019-01-31 00:00:00.000000', 88),
(376, 'Message selected should be deleted and user should be taken back to space view with a toast saying - "oops this topic has been deleted"', 'Message selected should be deleted and user should be taken back to space view with a toast saying - "oops this topic has been deleted"', '', '2019-01-31 00:00:00.000000', 88),
(377, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 89),
(378, 'User should be taken to space view of the selected room/DM', 'User should be taken to space view of the selected room/DM', '', '2019-01-31 00:00:00.000000', 89),
(379, 'User should be shown an overlay with three options, Copy Text, Edit, Delete, Send Feedback on this message ', 'User should be shown an overlay with three options, Copy Text, Edit, Delete, Send Feedback on this message ', '', '2019-01-31 00:00:00.000000', 89),
(380, 'Delete message alert should be displayed', 'Delete message alert should be displayed', '', '2019-01-31 00:00:00.000000', 89),
(381, 'Message selected should be deleted and the date dividers and the timestamps for the other messages should not be affected. Other users in the room/dm should not be able to view the deleted message.', 'Message selected should be deleted and the date dividers and the timestamps for the other messages should not be affected. Other users in the room/dm should not be able to view the deleted message.', '', '2019-01-31 00:00:00.000000', 89),
(382, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 90),
(383, 'User should be taken to space view of the selected room/DM', 'User should be taken to space view of the selected room/DM', '', '2019-01-31 00:00:00.000000', 90),
(384, 'User should be shown an overlay with three options, Copy Text, Edit, Delete, Send Feedback on this message ', 'User should be shown an overlay with three options, Copy Text, Edit, Delete, Send Feedback on this message ', '', '2019-01-31 00:00:00.000000', 90),
(385, 'Delete message alert should be displayed', 'Delete message alert should be displayed', '', '2019-01-31 00:00:00.000000', 90),
(386, 'Message couldn\'t be deleted, Please try again in few minutes message should be displayed', 'Message couldn\'t be deleted, Please try again in few minutes message should be displayed', '', '2019-01-31 00:00:00.000000', 90),
(387, 'User should be able to launch the app', 'User should be able to launch the app', '', '2019-01-31 00:00:00.000000', 91),
(388, 'The FAB button must be present in the bottom right corner of the screen and always in the visible region.', 'The FAB button must be present in the bottom right corner of the screen and always in the visible region.', '', '2019-01-31 00:00:00.000000', 91),
(389, 'Room, Bot and Direct message options must be displayed along with their corresponding icons.(If the user has invite to rooms, the count will be displayed near the fab button which when tapped on FAB button shows up in place of Room icon)', 'Room, Bot and Direct message options must be displayed along with their corresponding icons.(If the user has invite to rooms, the count will be displayed near the fab button which when tapped on FAB button shows up in place of Room icon)', '', '2019-01-31 00:00:00.000000', 91),
(390, 'User should be taken to "Add bot" screen with a close button on top left corner and focus in input field and the available bots for that user listed ', 'User should be taken to "Add bot" screen with a close button on top left corner and focus in input field and the available bots for that user listed ', '', '2019-01-31 00:00:00.000000', 91),
(391, 'User should be able to launch the app', 'User should be able to launch the app', '', '2019-01-31 00:00:00.000000', 92),
(392, 'The FAB button must be present in the bottom right corner of the screen and always in the visible region.', 'The FAB button must be present in the bottom right corner of the screen and always in the visible region.', '', '2019-01-31 00:00:00.000000', 92),
(393, 'Room, Bot and Direct message options must be displayed along with their corresponding icons.(If the user has invite to rooms, the count will be displayed near the fab button which when tapped on FAB button shows up in place of Room icon)', 'Room, Bot and Direct message options must be displayed along with their corresponding icons.(If the user has invite to rooms, the count will be displayed near the fab button which when tapped on FAB button shows up in place of Room icon)', '', '2019-01-31 00:00:00.000000', 92),
(394, 'User should be taken to "Add bot" screen with a close button on top left corner and focus in input field and the available bots for that user listed ', 'User should be taken to "Add bot" screen with a close button on top left corner and focus in input field and the available bots for that user listed ', '', '2019-01-31 00:00:00.000000', 92),
(395, 'User should be taken to BOT DM screen', 'User should be taken to BOT DM screen', '', '2019-01-31 00:00:00.000000', 92),
(396, 'There should be appropriate Response from the BOT for all the content types (as per the BOT selected) with BOT avatar and timestamps properly displayed in space and topic views.', 'There should be appropriate Response from the BOT for all the content types (as per the BOT selected) with BOT avatar and timestamps properly displayed in space and topic views.', '', '2019-01-31 00:00:00.000000', 92),
(397, 'User should be able to launch the app', 'User should be able to launch the app', '', '2019-01-31 00:00:00.000000', 93),
(398, 'The FAB button must be present in the bottom right corner of the screen and always in the visible region.', 'The FAB button must be present in the bottom right corner of the screen and always in the visible region.', '', '2019-01-31 00:00:00.000000', 93),
(399, 'Room, Bot and Direct message options must be displayed along with their corresponding icons.(If the user has invite to rooms, the count will be displayed near the fab button which when tapped on FAB button shows up in place of Room icon)', 'Room, Bot and Direct message options must be displayed along with their corresponding icons.(If the user has invite to rooms, the count will be displayed near the fab button which when tapped on FAB button shows up in place of Room icon)', '', '2019-01-31 00:00:00.000000', 93),
(400, 'User should be taken to "Add bot" screen with a close button on top left corner and focus in input field and the available bots for that user listed ', 'User should be taken to "Add bot" screen with a close button on top left corner and focus in input field and the available bots for that user listed ', '', '2019-01-31 00:00:00.000000', 93),
(401, 'User should be taken to BOT DM screen', 'User should be taken to BOT DM screen', '', '2019-01-31 00:00:00.000000', 93),
(402, 'User should be taken to DM details screen.', 'User should be taken to DM details screen.', '', '2019-01-31 00:00:00.000000', 93),
(403, 'The DM details screen should contain Star/Unstar, Notifications toggle button, Remove, Members section - listing the bot and the user', 'The DM details screen should contain Star/Unstar, Notifications toggle button, Remove, Members section - listing the bot and the user', '', '2019-01-31 00:00:00.000000', 93),
(404, 'User should be able to launch the app', 'User should be able to launch the app', '', '2019-01-31 00:00:00.000000', 94),
(405, 'The FAB button must be present in the bottom right corner of the screen and always in the visible region.', 'The FAB button must be present in the bottom right corner of the screen and always in the visible region.', '', '2019-01-31 00:00:00.000000', 94),
(406, 'Room, Bot and Direct message options must be displayed along with their corresponding icons.(If the user has invite to rooms, the count will be displayed near the fab button which when tapped on FAB button shows up in place of Room icon)', 'Room, Bot and Direct message options must be displayed along with their corresponding icons.(If the user has invite to rooms, the count will be displayed near the fab button which when tapped on FAB button shows up in place of Room icon)', '', '2019-01-31 00:00:00.000000', 94),
(407, 'User should be taken to "Add bot" screen with a close button on top left corner and focus in input field and the available bots for that user listed ', 'User should be taken to "Add bot" screen with a close button on top left corner and focus in input field and the available bots for that user listed ', '', '2019-01-31 00:00:00.000000', 94),
(408, 'User should be taken to BOT DM screen', 'User should be taken to BOT DM screen', '', '2019-01-31 00:00:00.000000', 94),
(409, 'User should be taken to DM details screen.', 'User should be taken to DM details screen.', '', '2019-01-31 00:00:00.000000', 94),
(410, 'The DM details screen should contain Star/Unstar, Notifications toggle button, Remove, Members section - listing the bot and the user', 'The DM details screen should contain Star/Unstar, Notifications toggle button, Remove, Members section - listing the bot and the user', '', '2019-01-31 00:00:00.000000', 94),
(411, 'User should be taken to world view with a message - "Bot removed" and world view should not contain the removed bot', 'User should be taken to world view with a message - "Bot removed" and world view should not contain the removed bot', '', '2019-01-31 00:00:00.000000', 94),
(412, 'User should be able to select the removed bot', 'User should be able to select the removed bot', '', '2019-01-31 00:00:00.000000', 94),
(413, 'User should be able to post the message in bot dm and the selected bot should be displayed back in world view.', 'User should be able to post the message in bot dm and the selected bot should be displayed back in world view.', '', '2019-01-31 00:00:00.000000', 94),
(414, '3rd party app should be launched', '3rd party app should be launched', '', '2019-01-31 00:00:00.000000', 95),
(415, 'On taping share all sharing apps should be available', 'On taping share all sharing apps should be available', '', '2019-01-31 00:00:00.000000', 95),
(416, 'App should be successfully launched with "Get Started" button, "Sharing to chat" toast should be visible', 'App should be successfully launched with "Get Started" button, "Sharing to chat" toast should be visible', '', '2019-01-31 00:00:00.000000', 95),
(417, 'Choose an account pop up should appear with all device accounts', 'Choose an account pop up should appear with all device accounts', '', '2019-01-31 00:00:00.000000', 95),
(418, 'Verify if app gets login and stays on World View, Close the app and try logging with a different account should get displayed', 'Verify if app gets login and stays on World View, Close the app and try logging with a different account should get displayed', '', '2019-01-31 00:00:00.000000', 95),
(419, '3rd party app should be launched', '3rd party app should be launched', '', '2019-01-31 00:00:00.000000', 96),
(420, 'On taping share all sharing apps should be available', 'On taping share all sharing apps should be available', '', '2019-01-31 00:00:00.000000', 96),
(421, 'Send to screen should be displayed with all Rooms, Dm\'s & Group Dm\'s', 'Send to screen should be displayed with all Rooms, Dm\'s & Group Dm\'s', '', '2019-01-31 00:00:00.000000', 96),
(422, 'Verify if New Conversation window gets opened with shared file attachment in compose field', 'Verify if New Conversation window gets opened with shared file attachment in compose field', '', '2019-01-31 00:00:00.000000', 96),
(423, 'Verify if shared file attachment visible in compose field with Off the record enabled.', 'Verify if shared file attachment visible in compose field with Off the record enabled.', '', '2019-01-31 00:00:00.000000', 96),
(424, 'attached file should be shared to respective Room/DM/Group DM', 'attached file should be shared to respective Room/DM/Group DM', '', '2019-01-31 00:00:00.000000', 96),
(425, 'Upon tapping close button it should redirect to World view', 'Upon tapping close button it should redirect to World view', '', '2019-01-31 00:00:00.000000', 96),
(426, 'Verify if the shared file has time stamp, username with avatar and today header', 'Verify if the shared file has time stamp, username with avatar and today header', '', '2019-01-31 00:00:00.000000', 96),
(427, 'photos/gallery should get launched', 'photos/gallery should get launched', '', '2019-01-31 00:00:00.000000', 97),
(428, 'N/A', 'N/A', '', '2019-01-31 00:00:00.000000', 97),
(429, 'Send to screen should be displayed with all Rooms, Dm\'s & Group Dm\'s', 'Send to screen should be displayed with all Rooms, Dm\'s & Group Dm\'s', '', '2019-01-31 00:00:00.000000', 97),
(430, 'File couldn\'t be uploaded . Check your internet connection should be displayed on image, send button should be disable by default', 'File couldn\'t be uploaded . Check your internet connection should be displayed on image, send button should be disable by default', '', '2019-01-31 00:00:00.000000', 97),
(431, 'Chat application must be launched successfully.', 'Chat application must be launched successfully.', '', '2019-01-31 00:00:00.000000', 98),
(432, 'User should be able to navigate to the selected room successfully.', 'User should be able to navigate to the selected room successfully.', '', '2019-01-31 00:00:00.000000', 98),
(433, 'Autocomplete must be invoked and the BOTs which are not in the room must be listed after all the members section with "Add to room" text', 'Autocomplete must be invoked and the BOTs which are not in the room must be listed after all the members section with "Add to room" text', '', '2019-01-31 00:00:00.000000', 98),
(434, 'User should be able to @mention the bot and the "Add to room" dialog must be displayed with the text related to that of BOT.', 'User should be able to @mention the bot and the "Add to room" dialog must be displayed with the text related to that of BOT.', '', '2019-01-31 00:00:00.000000', 98),
(435, 'Selected BOT must be added to the room and the room members count should increase and the system message should also get generated', 'Selected BOT must be added to the room and the room members count should increase and the system message should also get generated', '', '2019-01-31 00:00:00.000000', 98),
(436, 'Chat application must be launched successfully.', 'Chat application must be launched successfully.', '', '2019-01-31 00:00:00.000000', 99),
(437, 'User should be able to navigate to the selected room successfully.', 'User should be able to navigate to the selected room successfully.', '', '2019-01-31 00:00:00.000000', 99),
(438, 'User should navigate to the Invite screen', 'User should navigate to the Invite screen', '', '2019-01-31 00:00:00.000000', 99),
(439, 'Bots suggestions must be displayed at the end of the screen', 'Bots suggestions must be displayed at the end of the screen', '', '2019-01-31 00:00:00.000000', 99),
(440, 'Selected BOT must be added to the room automatically and the room members count should increase and the system message should also get generated', 'Selected BOT must be added to the room automatically and the room members count should increase and the system message should also get generated', '', '2019-01-31 00:00:00.000000', 99),
(441, 'Chat application must be launched successfully.', 'Chat application must be launched successfully.', '', '2019-01-31 00:00:00.000000', 100),
(442, 'User should be able to navigate to the selected room successfully.', 'User should be able to navigate to the selected room successfully.', '', '2019-01-31 00:00:00.000000', 100),
(443, 'User should navigate to the Invite screen', 'User should navigate to the Invite screen', '', '2019-01-31 00:00:00.000000', 100),
(444, 'Bots suggestions must be displayed at the end of the screen', 'Bots suggestions must be displayed at the end of the screen', '', '2019-01-31 00:00:00.000000', 100),
(445, 'Selected BOT must be added to the room automatically and the room members count should increase and the system message should also get generated', 'Selected BOT must be added to the room automatically and the room members count should increase and the system message should also get generated', '', '2019-01-31 00:00:00.000000', 100),
(446, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 101),
(447, 'User should be taken to space view of the selected room', 'User should be taken to space view of the selected room', '', '2019-01-31 00:00:00.000000', 101),
(448, 'User should be taken to the room details screen', 'User should be taken to the room details screen', '', '2019-01-31 00:00:00.000000', 101),
(449, 'User should be taken to the Invite screen', 'User should be taken to the Invite screen', '', '2019-01-31 00:00:00.000000', 101),
(450, 'Invite modal should be launched containing the below Notify people via email heading with a toggle button - default on position', 'Invite modal should be launched containing the below Notify people via email heading with a toggle button - default on position', '', '2019-01-31 00:00:00.000000', 101),
(451, 'Modal should be dismissed', 'Modal should be dismissed', '', '2019-01-31 00:00:00.000000', 101),
(452, 'Selected contact must be chipified and shown', 'Selected contact must be chipified and shown', '', '2019-01-31 00:00:00.000000', 101),
(453, 'User should be redirected to the Room details screen', 'User should be redirected to the Room details screen', '', '2019-01-31 00:00:00.000000', 101),
(454, 'The invited member should receive the room invite and also an email containing the invite to the room', 'The invited member should receive the room invite and also an email containing the invite to the room', '', '2019-01-31 00:00:00.000000', 101),
(455, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 102),
(456, 'User should be taken to space view of the selected room', 'User should be taken to space view of the selected room', '', '2019-01-31 00:00:00.000000', 102),
(457, 'User should be taken to the room details screen', 'User should be taken to the room details screen', '', '2019-01-31 00:00:00.000000', 102),
(458, 'User should be taken to the Invite screen', 'User should be taken to the Invite screen', '', '2019-01-31 00:00:00.000000', 102),
(459, 'Invite modal should be launched containing the below:, Notify people via email heading with a toggle button - default on position', 'Invite modal should be launched containing the below:, Notify people via email heading with a toggle button - default on position', '', '2019-01-31 00:00:00.000000', 102),
(460, 'Modal should be dismissed', 'Modal should be dismissed', '', '2019-01-31 00:00:00.000000', 102),
(461, 'Selected group must be chipified and shown', 'Selected group must be chipified and shown', '', '2019-01-31 00:00:00.000000', 102),
(462, 'User should be redirected to the Room details screen', 'User should be redirected to the Room details screen', '', '2019-01-31 00:00:00.000000', 102),
(463, 'The invited members of the group should receive the room invite and also an email containing the invite to the room', 'The invited members of the group should receive the room invite and also an email containing the invite to the room', '', '2019-01-31 00:00:00.000000', 102),
(464, 'Ensure no space (padding) is getting displayed between Threads in the DM', 'Ensure no space (padding) is getting displayed between Threads in the DM', '', '2019-01-31 00:00:00.000000', 103),
(465, 'DM background should be white in color', 'DM background should be white in color', '', '2019-01-31 00:00:00.000000', 103),
(466, 'Bell notification icon per conversation shouldn\'t be displayed', 'Bell notification icon per conversation shouldn\'t be displayed', '', '2019-01-31 00:00:00.000000', 103),
(467, 'New Conversation button for DM should not be displayed', 'New Conversation button for DM should not be displayed', '', '2019-01-31 00:00:00.000000', 103),
(468, 'Reply button for existing threads should not be displayed', 'Reply button for existing threads should not be displayed', '', '2019-01-31 00:00:00.000000', 103),
(469, 'Should be able to launch the DM.', 'Should be able to launch the DM.', '', '2019-01-31 00:00:00.000000', 104),
(470, 'Should be able to post a message with history ON.', 'Should be able to post a message with history ON.', '', '2019-01-31 00:00:00.000000', 104),
(471, 'Mobile blocker(banner in compose box) in TEAL with your username should be displayed at the other member\'s compose box.', 'Mobile blocker(banner in compose box) in TEAL with your username should be displayed at the other member\'s compose box.', '', '2019-01-31 00:00:00.000000', 104),
(472, 'Should be able to post a message with history OFF.', 'Should be able to post a message with history OFF.', '', '2019-01-31 00:00:00.000000', 104),
(473, 'Mobile blocker(banner in compose box) in BLUE with your username should be displayed at the other member\'s compose box.', 'Mobile blocker(banner in compose box) in BLUE with your username should be displayed at the other member\'s compose box.', '', '2019-01-31 00:00:00.000000', 104),
(474, 'New\' badge in light blue should be displayed for history ON messages, at send and receiver.', 'New\' badge in light blue should be displayed for history ON messages, at send and receiver.', '', '2019-01-31 00:00:00.000000', 104),
(475, 'New\' badge with trimer icon in dark blue should be displayed for history OFF messages, at send and receiver.', 'New\' badge with trimer icon in dark blue should be displayed for history OFF messages, at send and receiver.', '', '2019-01-31 00:00:00.000000', 104),
(476, 'On entering the READ DM, the timer icon without "new" should be displayed for history OFF message.', 'On entering the READ DM, the timer icon without "new" should be displayed for history OFF message.', '', '2019-01-31 00:00:00.000000', 104),
(477, 'Should be able to locate a history ON message.', 'Should be able to locate a history ON message.', '', '2019-01-31 00:00:00.000000', 105),
(478, 'Should be able to select message by TAP and "edit".', 'Should be able to select message by TAP and "edit".', '', '2019-01-31 00:00:00.000000', 105),
(479, 'Should be able to successfully post the edited message.', 'Should be able to successfully post the edited message.', '', '2019-01-31 00:00:00.000000', 105),
(480, 'Editing and posting a message to be empty, triggers delete flow.', 'Editing and posting a message to be empty, triggers delete flow.', '', '2019-01-31 00:00:00.000000', 105),
(481, 'Edited content should append "Edited" text to the time stamp.', 'Edited content should append "Edited" text to the time stamp.', '', '2019-01-31 00:00:00.000000', 105),
(482, 'Repeat steps 1-5 above, for History OFF messages.', 'Repeat steps 1-5 above, for History OFF messages.', '', '2019-01-31 00:00:00.000000', 105),
(483, 'Should be able to locate and delete a history ON message ', 'Should be able to locate and delete a history ON message ', '', '2019-01-31 00:00:00.000000', 105),
(484, 'Should be able to locate and delete a history OFF message', 'Should be able to locate and delete a history OFF message', '', '2019-01-31 00:00:00.000000', 105),
(485, 'Should be able to locate and delete a history ON message via search view.', 'Should be able to locate and delete a history ON message via search view.', '', '2019-01-31 00:00:00.000000', 105),
(486, 'Should be able to locate and delete a history OFF message via search view.', 'Should be able to locate and delete a history OFF message via search view.', '', '2019-01-31 00:00:00.000000', 105),
(487, 'Should be able to post message in offline mode.', 'Should be able to post message in offline mode.', '', '2019-01-31 00:00:00.000000', 106),
(488, 'Pending state messages should be delivered without user intervention after connecting back to network.', 'Pending state messages should be delivered without user intervention after connecting back to network.', '', '2019-01-31 00:00:00.000000', 106),
(489, 'Should be able to post messages in offline mode.', 'Should be able to post messages in offline mode.', '', '2019-01-31 00:00:00.000000', 106),
(490, 'Offline messages should convert to "Failed to send" state from Pending state after cold starting the app.', 'Offline messages should convert to "Failed to send" state from Pending state after cold starting the app.', '', '2019-01-31 00:00:00.000000', 106),
(491, 'Should be able to send/delete the "failed to send" messages by selecting the respective option from MODAL when connected back online.', 'Should be able to send/delete the "failed to send" messages by selecting the respective option from MODAL when connected back online.', '', '2019-01-31 00:00:00.000000', 106),
(492, 'Should be to an existing DM in offline mode.', 'Should be to an existing DM in offline mode.', '', '2019-01-31 00:00:00.000000', 106),
(493, 'Content available when online should be available in offline mode.', 'Content available when online should be available in offline mode.', '', '2019-01-31 00:00:00.000000', 106),
(494, 'Should be able to @mention the bots.', 'Should be able to @mention the bots.', '', '2019-01-31 00:00:00.000000', 107),
(495, '@mentioned message should go to "pending" state with a private message card: Bot requires configuration.', '@mentioned message should go to "pending" state with a private message card: Bot requires configuration.', '', '2019-01-31 00:00:00.000000', 107),
(496, 'Tap on "configure" link on private card should open browser and should ask for login(if not logged-in in browser.)', 'Tap on "configure" link on private card should open browser and should ask for login(if not logged-in in browser.)', '', '2019-01-31 00:00:00.000000', 107),
(497, 'After login/authentication is success in browser, private card should be removed with "thanks for authorizing" message on coming back from browser.', 'After login/authentication is success in browser, private card should be removed with "thanks for authorizing" message on coming back from browser.', '', '2019-01-31 00:00:00.000000', 107),
(498, 'Bottom up menu on single tapping the "pending" message should show "discard pending message" option.', 'Bottom up menu on single tapping the "pending" message should show "discard pending message" option.', '', '2019-01-31 00:00:00.000000', 107),
(499, 'Should be able to visit a room and messages should load.', 'Should be able to visit a room and messages should load.', '', '2019-01-31 00:00:00.000000', 108),
(500, 'On revisiting the room/DM while offline, already fetched messages should be displayed.', 'On revisiting the room/DM while offline, already fetched messages should be displayed.', '', '2019-01-31 00:00:00.000000', 108),
(501, 'Should be able to scroll to the top and J2B FAB should reveal.', 'Should be able to scroll to the top and J2B FAB should reveal.', '', '2019-01-31 00:00:00.000000', 109),
(502, 'Tapping on J2B FAB, space view scroll position should reach to the bottom and new topic FAB should be revealed.(j2b FAB should disappear)', 'Tapping on J2B FAB, space view scroll position should reach to the bottom and new topic FAB should be revealed.(j2b FAB should disappear)', '', '2019-01-31 00:00:00.000000', 109),
(503, 'Should be able to create a new room, post new topics, scroll to top and J2B FAB should reveal.', 'Should be able to create a new room, post new topics, scroll to top and J2B FAB should reveal.', '', '2019-01-31 00:00:00.000000', 109),
(504, 'Tapping on J2B FAB, space view scroll position should reach to the bottom and new topic FAB should be revealed.(j2b FAB should disappear)', 'Tapping on J2B FAB, space view scroll position should reach to the bottom and new topic FAB should be revealed.(j2b FAB should disappear)', '', '2019-01-31 00:00:00.000000', 109),
(505, 'Should be able to go to 1:1 DM .', 'Should be able to go to 1:1 DM .', '', '2019-01-31 00:00:00.000000', 110),
(506, 'On scrolling to the top, J2B FAB should NOT be revealed in DM.', 'On scrolling to the top, J2B FAB should NOT be revealed in DM.', '', '2019-01-31 00:00:00.000000', 110),
(507, 'Should be able to go to Group DM .', 'Should be able to go to Group DM .', '', '2019-01-31 00:00:00.000000', 110),
(508, 'On scrolling to the top, J2B FAB should NOT be revealed in DM.', 'On scrolling to the top, J2B FAB should NOT be revealed in DM.', '', '2019-01-31 00:00:00.000000', 110),
(509, 'The Navigation Panel should open and display user profile section and menu options.', 'The Navigation Panel should open and display user profile section and menu options.', '', '2019-01-31 00:00:00.000000', 111),
(510, 'Under Profile section the user name, profile image and the user email id should be displayed.', 'Under Profile section the user name, profile image and the user email id should be displayed.', '', '2019-01-31 00:00:00.000000', 112),
(511, 'below the profile section there should be, turn off do not disturb, set do not disturb, Settings, Help & Feedback, Logout ', 'below the profile section there should be, turn off do not disturb, set do not disturb, Settings, Help & Feedback, Logout ', '', '2019-01-31 00:00:00.000000', 112),
(512, 'Should be able to launch the app.', 'Should be able to launch the app.', '', '2019-01-31 00:00:00.000000', 113),
(513, 'World view header should be updated with logged-in user\'s username and "Active" status below it with online(green) indicator.', 'World view header should be updated with logged-in user\'s username and "Active" status below it with online(green) indicator.', '', '2019-01-31 00:00:00.000000', 113),
(514, 'Should be able to launch the app.', 'Should be able to launch the app.', '', '2019-01-31 00:00:00.000000', 114),
(515, 'Should be able to set DND.', 'Should be able to set DND.', '', '2019-01-31 00:00:00.000000', 114),
(516, 'DND status(on logged-in device) should update on world view header below username as "Do not disturb until <timestamp>".', 'DND status(on logged-in device) should update on world view header below username as "Do not disturb until <timestamp>".', '', '2019-01-31 00:00:00.000000', 114);
INSERT INTO `reports_teststeps` (`id`, `test_step`, `test_step_desc`, `golden_screen`, `created`, `test_id`) VALUES
(517, 'User\'s DND status(on logged-in device) should be displayed with "filled half moon" indicator in Room/DM details view (in rooms/dms where user is part of).', 'User\'s DND status(on logged-in device) should be displayed with "filled half moon" indicator in Room/DM details view (in rooms/dms where user is part of).', '', '2019-01-31 00:00:00.000000', 114),
(518, 'At other members, user\'s DND status should be displayed with the "filled half moon" indicator on, World view, Frequent list, Room/DM details view, @mention auto complete list, Conversation launcher', 'At other members, user\'s DND status should be displayed with the "filled half moon" indicator on, World view, Frequent list, Room/DM details view, @mention auto complete list, Conversation launcher', '', '2019-01-31 00:00:00.000000', 114),
(519, 'Should be able to go to "mobile notifications" from "settings"', 'Should be able to go to "mobile notifications" from "settings"', '', '2019-01-31 00:00:00.000000', 115),
(520, 'The mobile notifications view should contain, Mobile Notifications, New conversations and those I\'ve replied to, Conversations I\'ve replied to, Only @mentions & direct messages, Off', 'The mobile notifications view should contain, Mobile Notifications, New conversations and those I\'ve replied to, Conversations I\'ve replied to, Only @mentions & direct messages, Off', '', '2019-01-31 00:00:00.000000', 115),
(521, 'Toggle Button be displayed across the selected option.', 'Toggle Button be displayed across the selected option.', '', '2019-01-31 00:00:00.000000', 115),
(522, 'Incoming notifications should work according to the notification settings.', 'Incoming notifications should work according to the notification settings.', '', '2019-01-31 00:00:00.000000', 115),
(523, 'Should be able to go to a room/dm/bot-dm/interoped-room.', 'Should be able to go to a room/dm/bot-dm/interoped-room.', '', '2019-01-31 00:00:00.000000', 116),
(524, 'Action sheet should display "Add Reaction" option on long pressing the message.("Add reaction" should be verified from topic view as well)', 'Action sheet should display "Add Reaction" option on long pressing the message.("Add reaction" should be verified from topic view as well)', '', '2019-01-31 00:00:00.000000', 116),
(525, 'Emoji picker should show up on selecting "add reaction" from action sheet.', 'Emoji picker should show up on selecting "add reaction" from action sheet.', '', '2019-01-31 00:00:00.000000', 116),
(526, 'Emoji picker should contain two sections: \'Quick Reactions\', \'Smileys & Emojis\'.', 'Emoji picker should contain two sections: \'Quick Reactions\', \'Smileys & Emojis\'.', '', '2019-01-31 00:00:00.000000', 116),
(527, 'Tapping any emoji dismisses the picker and adds the selected emoji as reaction below the long pressed message.(No action takes place on re-adding a preselected reaction)', 'Tapping any emoji dismisses the picker and adds the selected emoji as reaction below the long pressed message.(No action takes place on re-adding a preselected reaction)', '', '2019-01-31 00:00:00.000000', 116),
(528, 'Adding first reaction to a message adds emoji picker icon next to the reaction.', 'Adding first reaction to a message adds emoji picker icon next to the reaction.', '', '2019-01-31 00:00:00.000000', 116),
(529, 'Count on emoji should be displayed in TEAL color if the logged is user is reacted.', 'Count on emoji should be displayed in TEAL color if the logged is user is reacted.', '', '2019-01-31 00:00:00.000000', 116),
(530, 'Count on emoji should be displayed in GREY color if the logged-in user is not reacted but reacted by other members.', 'Count on emoji should be displayed in GREY color if the logged-in user is not reacted but reacted by other members.', '', '2019-01-31 00:00:00.000000', 116),
(531, 'User should be able to see the message entered in step 3 in the compose bar.', 'User should be able to see the message entered in step 3 in the compose bar.', '', '2019-01-31 00:00:00.000000', 117),
(532, 'Cursor position should be at the end of the message with no white spaces preserved', 'Cursor position should be at the end of the message with no white spaces preserved', '', '2019-01-31 00:00:00.000000', 117),
(533, 'User should be able to see the attached file in drafts this should work for both existing and new topics', 'User should be able to see the attached file in drafts this should work for both existing and new topics', '', '2019-01-31 00:00:00.000000', 118),
(534, 'App should be launched successfully.', 'App should be launched successfully.', '', '2019-01-31 00:00:00.000000', 119),
(535, 'The presence indicator should be verified successfully in below locations with green dot indicating active presence status, World view against 1:1 DM\'s, Autocomplete list, Room details page - Members list and Can join list, Invite people screen, DM contacts screen', 'The presence indicator should be verified successfully in below locations with green dot indicating active presence status, World view against 1:1 DM\'s, Autocomplete list, Room details page - Members list and Can join list, Invite people screen, DM contacts screen', '', '2019-01-31 00:00:00.000000', 119),
(536, 'App should be launched successfully.', 'App should be launched successfully.', '', '2019-01-31 00:00:00.000000', 120),
(537, 'The presence indicator should be verified successfully in below locations with grey dot indicating inactive presence status, World view against 1:1 DM\'s, Autocomplete list, Room details page - Members list and Can join list, Invite people screen, DM contacts screen', 'The presence indicator should be verified successfully in below locations with grey dot indicating inactive presence status, World view against 1:1 DM\'s, Autocomplete list, Room details page - Members list and Can join list, Invite people screen, DM contacts screen', '', '2019-01-31 00:00:00.000000', 120),
(538, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-31 00:00:00.000000', 64),
(539, 'User should be taken to the space view', 'User should be taken to the space view', '', '2019-01-31 00:00:00.000000', 64),
(540, 'User should be taken to the topic view of the selected topic.', 'User should be taken to the topic view of the selected topic.', '', '2019-01-31 00:00:00.000000', 64),
(541, 'Web chip for the typed URL should be displayed', 'Web chip for the typed URL should be displayed', '', '2019-01-31 00:00:00.000000', 64),
(542, 'Replacing current item?" alert should be displayed', 'Replacing current item?" alert should be displayed', '', '2019-01-31 00:00:00.000000', 64),
(543, 'User should be directed to "Select a Photo" screen', 'User should be directed to "Select a Photo" screen', '', '2019-01-31 00:00:00.000000', 64),
(544, 'User selected file should be shown as attachment with a cancel button on the chip generated', 'User selected file should be shown as attachment with a cancel button on the chip generated', '', '2019-01-31 00:00:00.000000', 64),
(545, 'Replacing current item? alert should be displayed', 'Replacing current item? alert should be displayed', '', '2019-01-31 00:00:00.000000', 64),
(546, 'Replacing current item? alert displayed should contain the text: "Messages can only have one image, file or website preview attached. The new item will replace the current one" with "cancel" and "replace" buttons. ', 'Replacing current item? alert displayed should contain the text: "Messages can only have one image, file or website preview attached. The new item will replace the current one" with "cancel" and "replace" buttons. ', '', '2019-01-31 00:00:00.000000', 64),
(547, 'The dialog should be dismissed and the existing attachment should be retained.', 'The dialog should be dismissed and the existing attachment should be retained.', '', '2019-01-31 00:00:00.000000', 64),
(548, 'Replacing current item? alert should be displayed', 'Replacing current item? alert should be displayed', '', '2019-01-31 00:00:00.000000', 64),
(549, 'Image capture screen should be displayed', 'Image capture screen should be displayed', '', '2019-01-31 00:00:00.000000', 64),
(550, 'Capture image chip should be generated and displayed in the compose', 'Capture image chip should be generated and displayed in the compose', '', '2019-01-31 00:00:00.000000', 64),
(551, 'The attachment should be replaced with the camera captured image chip.', 'The attachment should be replaced with the camera captured image chip.', '', '2019-01-31 00:00:00.000000', 64),
(552, 'Replacing current item? alert should be displayed', 'Replacing current item? alert should be displayed', '', '2019-01-31 00:00:00.000000', 64),
(553, 'Dialog should be replaced with the Video meet chip ', 'Dialog should be replaced with the Video meet chip ', '', '2019-01-31 00:00:00.000000', 64),
(554, 'Verify the chip generated', 'Verify the chip generated', '', '2019-01-31 00:00:00.000000', 64),
(555, 'Tap on send button', 'Tap on send button', '', '2019-01-31 00:00:00.000000', 64),
(556, 'User should be able to launch the app', 'User should be able to launch the app', '', '2019-01-31 00:00:00.000000', 30),
(557, 'The FAB button must be present in the bottom right corner of the screen and always in the visible region.', 'The FAB button must be present in the bottom right corner of the screen and always in the visible region.', '', '2019-01-31 00:00:00.000000', 30),
(558, 'Room and Direct message options must be displayed along with their corresponding icons.', 'Room and Direct message options must be displayed along with their corresponding icons.', '', '2019-01-31 00:00:00.000000', 30),
(559, 'Direct message screen should be displayed with focus in "Search for people to message" field and header being "Direct Message", close button on top right corner and faded tick mark button.', 'Direct message screen should be displayed with focus in "Search for people to message" field and header being "Direct Message", close button on top right corner and faded tick mark button.', '', '2019-01-31 00:00:00.000000', 30),
(560, 'Direct message contacts with avatars, presence indicator, Username and email id should be displayed (if no contacts available this will be empty)', 'Direct message contacts with avatars, presence indicator, Username and email id should be displayed (if no contacts available this will be empty)', '', '2019-01-31 00:00:00.000000', 30),
(561, 'The selected contact must be displayed as chip in the search field with avatar and username and the DM preview should be loaded.', 'The selected contact must be displayed as chip in the search field with avatar and username and the DM preview should be loaded.', '', '2019-01-31 00:00:00.000000', 30),
(562, 'User must be taken to the DM view screen with header being the name of the contact selected and a back button in the top left corner.', 'User must be taken to the DM view screen with header being the name of the contact selected and a back button in the top left corner.', '', '2019-01-31 00:00:00.000000', 30),
(563, 'User should be taken to the New conversation screen with educational promo saying "Off the record Messages in this conversation will be deleted after 24 hrs." and the OTR toggle button being highlighted.', 'User should be taken to the New conversation screen with educational promo saying "Off the record Messages in this conversation will be deleted after 24 hrs." and the OTR toggle button being highlighted.', '', '2019-01-31 00:00:00.000000', 30),
(564, 'The label must now read: "New conversation" and toggle button should be set to off and the hint text must say - "Message <<username>>"', 'The label must now read: "New conversation" and toggle button should be set to off and the hint text must say - "Message <<username>>"', '', '2019-01-31 00:00:00.000000', 30),
(565, 'The label must now read: "Off the record" and toggle button should be set to on and the hint text must say - "Message will be deleted after 24 hours"', 'The label must now read: "Off the record" and toggle button should be set to on and the hint text must say - "Message will be deleted after 24 hours"', '', '2019-01-31 00:00:00.000000', 30),
(566, 'User should be able to successfully post the message and the posted message should be appended - [OFF-THE -RECORD] and the hint text in compose should still say - "Message will be deleted after 24 hours"', 'User should be able to successfully post the message and the posted message should be appended - [OFF-THE -RECORD] and the hint text in compose should still say - "Message will be deleted after 24 hours"', '', '2019-01-31 00:00:00.000000', 30);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indexes for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indexes for table `reports_builds`
--
ALTER TABLE `reports_builds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_builds_project_id_17fe8f51_fk_reports_projects_id` (`project_id`);

--
-- Indexes for table `reports_devices`
--
ALTER TABLE `reports_devices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_devices_project_id_30c9dc68_fk_reports_projects_id` (`project_id`);

--
-- Indexes for table `reports_projects`
--
ALTER TABLE `reports_projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reports_testcases`
--
ALTER TABLE `reports_testcases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_testcases_project_id_c210f3a3_fk_reports_projects_id` (`project_id`);

--
-- Indexes for table `reports_testresultconsolidation`
--
ALTER TABLE `reports_testresultconsolidation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_testresultco_test_build_id_625401e3_fk_reports_b` (`test_build_id`),
  ADD KEY `reports_testresultco_test_case_id_2426cbb0_fk_reports_t` (`test_case_id`),
  ADD KEY `reports_testresultco_test_device_id_4274a34e_fk_reports_d` (`test_device_id`);

--
-- Indexes for table `reports_testresults`
--
ALTER TABLE `reports_testresults`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_testresults_device_id_e87ffe02_fk_reports_devices_id` (`device_id`),
  ADD KEY `reports_testresults_test_build_id_5ec50097_fk_reports_builds_id` (`test_build_id`),
  ADD KEY `reports_testresults_test_name_id_b7280f5d_fk_reports_t` (`test_name_id`),
  ADD KEY `reports_testresults_test_step_id_3de6faeb_fk_reports_t` (`test_step_id`);

--
-- Indexes for table `reports_teststeps`
--
ALTER TABLE `reports_teststeps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_teststeps_test_id_b4702c33_fk_reports_testcases_id` (`test_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `reports_builds`
--
ALTER TABLE `reports_builds`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `reports_devices`
--
ALTER TABLE `reports_devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `reports_projects`
--
ALTER TABLE `reports_projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `reports_testcases`
--
ALTER TABLE `reports_testcases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;
--
-- AUTO_INCREMENT for table `reports_testresultconsolidation`
--
ALTER TABLE `reports_testresultconsolidation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `reports_testresults`
--
ALTER TABLE `reports_testresults`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `reports_teststeps`
--
ALTER TABLE `reports_teststeps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=567;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `reports_builds`
--
ALTER TABLE `reports_builds`
  ADD CONSTRAINT `reports_builds_project_id_17fe8f51_fk_reports_projects_id` FOREIGN KEY (`project_id`) REFERENCES `reports_projects` (`id`);

--
-- Constraints for table `reports_devices`
--
ALTER TABLE `reports_devices`
  ADD CONSTRAINT `reports_devices_project_id_30c9dc68_fk_reports_projects_id` FOREIGN KEY (`project_id`) REFERENCES `reports_projects` (`id`);

--
-- Constraints for table `reports_testcases`
--
ALTER TABLE `reports_testcases`
  ADD CONSTRAINT `reports_testcases_project_id_c210f3a3_fk_reports_projects_id` FOREIGN KEY (`project_id`) REFERENCES `reports_projects` (`id`);

--
-- Constraints for table `reports_testresultconsolidation`
--
ALTER TABLE `reports_testresultconsolidation`
  ADD CONSTRAINT `reports_testresultco_test_build_id_625401e3_fk_reports_b` FOREIGN KEY (`test_build_id`) REFERENCES `reports_builds` (`id`),
  ADD CONSTRAINT `reports_testresultco_test_case_id_2426cbb0_fk_reports_t` FOREIGN KEY (`test_case_id`) REFERENCES `reports_testcases` (`id`),
  ADD CONSTRAINT `reports_testresultco_test_device_id_4274a34e_fk_reports_d` FOREIGN KEY (`test_device_id`) REFERENCES `reports_devices` (`id`);

--
-- Constraints for table `reports_testresults`
--
ALTER TABLE `reports_testresults`
  ADD CONSTRAINT `reports_testresults_device_id_e87ffe02_fk_reports_devices_id` FOREIGN KEY (`device_id`) REFERENCES `reports_devices` (`id`),
  ADD CONSTRAINT `reports_testresults_test_build_id_5ec50097_fk_reports_builds_id` FOREIGN KEY (`test_build_id`) REFERENCES `reports_builds` (`id`),
  ADD CONSTRAINT `reports_testresults_test_name_id_b7280f5d_fk_reports_t` FOREIGN KEY (`test_name_id`) REFERENCES `reports_testcases` (`id`),
  ADD CONSTRAINT `reports_testresults_test_step_id_3de6faeb_fk_reports_t` FOREIGN KEY (`test_step_id`) REFERENCES `reports_teststeps` (`id`);

--
-- Constraints for table `reports_teststeps`
--
ALTER TABLE `reports_teststeps`
  ADD CONSTRAINT `reports_teststeps_test_id_b4702c33_fk_reports_testcases_id` FOREIGN KEY (`test_id`) REFERENCES `reports_testcases` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
