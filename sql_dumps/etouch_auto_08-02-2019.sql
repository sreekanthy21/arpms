-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 08, 2019 at 06:17 PM
-- Server version: 5.7.24-0ubuntu0.16.04.1
-- PHP Version: 7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `etouch_auto`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add test steps', 1, 'add_teststeps'),
(2, 'Can change test steps', 1, 'change_teststeps'),
(3, 'Can delete test steps', 1, 'delete_teststeps'),
(4, 'Can add test cases', 2, 'add_testcases'),
(5, 'Can change test cases', 2, 'change_testcases'),
(6, 'Can delete test cases', 2, 'delete_testcases'),
(7, 'Can add test results', 3, 'add_testresults'),
(8, 'Can change test results', 3, 'change_testresults'),
(9, 'Can delete test results', 3, 'delete_testresults'),
(10, 'Can add builds', 4, 'add_builds'),
(11, 'Can change builds', 4, 'change_builds'),
(12, 'Can delete builds', 4, 'delete_builds'),
(13, 'Can add projects', 5, 'add_projects'),
(14, 'Can change projects', 5, 'change_projects'),
(15, 'Can delete projects', 5, 'delete_projects'),
(16, 'Can add devices', 6, 'add_devices'),
(17, 'Can change devices', 6, 'change_devices'),
(18, 'Can delete devices', 6, 'delete_devices'),
(19, 'Can add test result consolidation', 7, 'add_testresultconsolidation'),
(20, 'Can change test result consolidation', 7, 'change_testresultconsolidation'),
(21, 'Can delete test result consolidation', 7, 'delete_testresultconsolidation'),
(22, 'Can add log entry', 8, 'add_logentry'),
(23, 'Can change log entry', 8, 'change_logentry'),
(24, 'Can delete log entry', 8, 'delete_logentry'),
(25, 'Can add group', 9, 'add_group'),
(26, 'Can change group', 9, 'change_group'),
(27, 'Can delete group', 9, 'delete_group'),
(28, 'Can add permission', 10, 'add_permission'),
(29, 'Can change permission', 10, 'change_permission'),
(30, 'Can delete permission', 10, 'delete_permission'),
(31, 'Can add user', 11, 'add_user'),
(32, 'Can change user', 11, 'change_user'),
(33, 'Can delete user', 11, 'delete_user'),
(34, 'Can add content type', 12, 'add_contenttype'),
(35, 'Can change content type', 12, 'change_contenttype'),
(36, 'Can delete content type', 12, 'delete_contenttype'),
(37, 'Can add session', 13, 'add_session'),
(38, 'Can change session', 13, 'change_session'),
(39, 'Can delete session', 13, 'delete_session');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(8, 'admin', 'logentry'),
(9, 'auth', 'group'),
(10, 'auth', 'permission'),
(11, 'auth', 'user'),
(12, 'contenttypes', 'contenttype'),
(4, 'reports', 'builds'),
(6, 'reports', 'devices'),
(5, 'reports', 'projects'),
(2, 'reports', 'testcases'),
(7, 'reports', 'testresultconsolidation'),
(3, 'reports', 'testresults'),
(1, 'reports', 'teststeps'),
(13, 'sessions', 'session');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2019-01-17 16:01:07.899840'),
(2, 'auth', '0001_initial', '2019-01-17 16:01:22.092309'),
(3, 'admin', '0001_initial', '2019-01-17 16:01:25.878814'),
(4, 'admin', '0002_logentry_remove_auto_add', '2019-01-17 16:01:26.539722'),
(5, 'contenttypes', '0002_remove_content_type_name', '2019-01-17 16:01:29.618670'),
(6, 'auth', '0002_alter_permission_name_max_length', '2019-01-17 16:01:29.887302'),
(7, 'auth', '0003_alter_user_email_max_length', '2019-01-17 16:01:30.285705'),
(8, 'auth', '0004_alter_user_username_opts', '2019-01-17 16:01:30.541932'),
(9, 'auth', '0005_alter_user_last_login_null', '2019-01-17 16:01:31.481236'),
(10, 'auth', '0006_require_contenttypes_0002', '2019-01-17 16:01:31.574413'),
(11, 'auth', '0007_alter_validators_add_error_messages', '2019-01-17 16:01:31.667615'),
(12, 'auth', '0008_alter_user_username_max_length', '2019-01-17 16:01:31.854647'),
(13, 'reports', '0001_initial', '2019-01-17 16:01:49.653943'),
(14, 'sessions', '0001_initial', '2019-01-17 16:01:50.533250'),
(15, 'reports', '0002_auto_20190118_1231', '2019-01-18 12:31:40.425529'),
(16, 'reports', '0003_testresults_test_errors', '2019-01-21 08:43:34.319535');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reports_builds`
--

CREATE TABLE `reports_builds` (
  `id` int(11) NOT NULL,
  `build` varchar(300) NOT NULL,
  `executed_date` date NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports_builds`
--

INSERT INTO `reports_builds` (`id`, `build`, `executed_date`, `project_id`) VALUES
(1, 'dynamite-android_20190104-09_RC04', '2019-01-16', 1),
(2, 'dynamite-android_20190111-03_RC03', '2019-01-17', 1),
(3, 'dynamite-android_20190116-01_RC00', '2019-01-17', 1),
(4, 'dynamite-android_20190201-09_RC01', '2019-02-06', 1),
(5, 'dynamite-android_20190205-07_RC00', '2019-02-06', 1),
(6, 'dynamite-android_20190125-02_RC04', '2019-02-07', 1),
(7, 'dynamite-android_20190125-02_RC06', '2019-02-08', 1),
(8, 'dynamite-android_20190201-09_RC02', '2019-02-08', 1),
(9, 'dynamite-android_20190208-07_RC00', '2019-02-08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `reports_devices`
--

CREATE TABLE `reports_devices` (
  `id` int(11) NOT NULL,
  `device_name` longtext NOT NULL,
  `device_os` longtext NOT NULL,
  `device_udid` longtext NOT NULL,
  `created` datetime(6) NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports_devices`
--

INSERT INTO `reports_devices` (`id`, `device_name`, `device_os`, `device_udid`, `created`, `project_id`) VALUES
(1, 'Pixel 2', '8.1.0', 'FA6A40307885', '2019-01-16 00:00:00.000000', 1),
(2, 'Samsung', '8.0.0', 'JKISDSK24923', '2019-01-24 00:00:00.000000', 1);

-- --------------------------------------------------------

--
-- Table structure for table `reports_projects`
--

CREATE TABLE `reports_projects` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` longtext NOT NULL,
  `created` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports_projects`
--

INSERT INTO `reports_projects` (`id`, `name`, `description`, `created`) VALUES
(1, 'Dynamite Automation', 'Dynamite Automation', '2019-01-17 00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `reports_testcases`
--

CREATE TABLE `reports_testcases` (
  `id` int(11) NOT NULL,
  `test_feature` longtext NOT NULL,
  `test_title` longtext NOT NULL,
  `test_name` longtext NOT NULL,
  `test_suite_type` longtext NOT NULL,
  `test_priority` int(11) NOT NULL,
  `created` datetime(6) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports_testcases`
--

INSERT INTO `reports_testcases` (`id`, `test_feature`, `test_title`, `test_name`, `test_suite_type`, `test_priority`, `created`, `status`, `project_id`) VALUES
(1, 'App Launch', 'Verify the Onboarding message upon app launch', 'Onboarding message should be displayed upon app launch', 'Regression', 2, '2019-02-05 18:56:50.463296', 1, 1),
(2, 'App Launch', 'Verify Tapping on "Get started" Button', 'Choose an Account should be displayed on tapping Get Started', 'Regression', 0, '2019-02-05 18:56:50.590293', 1, 1),
(3, 'App Launch', 'Verify login with White listed Accounts', 'To Successfully login to application', 'Regression', 2, '2019-02-05 18:56:50.712064', 1, 1),
(4, 'App Launch', 'Verify login with Non White listed Accounts', 'No Access for Non White listed Accounts', 'Regression', 2, '2019-02-05 18:56:50.911769', 1, 1),
(5, 'App Launch', 'Verify adding account', 'Adding account upon launching Application', 'Regression', 2, '2019-02-05 18:56:50.989266', 1, 1),
(6, 'App Launch', 'Verify launching on Offline Mode', 'Verify launching on Offline Mode', 'Regression', 2, '2019-02-05 18:56:51.033608', 1, 1),
(7, 'World View', 'Verify the UI Sections/Elements displayed in world view', 'All the UI sections/elements must be verified in world view', 'Regression', 0, '2019-02-05 18:56:51.133682', 1, 1),
(8, 'World View', 'Verify the Unread section rooms rendering in world view', 'Verify the Unread section rooms rendering in world view', 'Regression', 1, '2019-02-05 18:56:51.178195', 1, 1),
(9, 'World View', 'Verify the Recent section rooms rendering in world view', 'Verify the recent section rooms rendering in world view', 'Regression', 1, '2019-02-05 18:56:51.322403', 1, 1),
(10, 'World View', 'Verify the Starred section rooms rendering in world view', 'Verify the starred section rooms rendering in world view', 'Regression', 1, '2019-02-05 18:56:51.588167', 1, 1),
(11, 'World View', 'Verify the FAB button functionality displayed in World view', 'Verify the FAB button functionality displayed in World view', 'Regression', 0, '2019-02-05 18:56:51.632301', 1, 1),
(12, 'World View', 'Verify the Filter rooms/DM functionality from World view', 'Verify the Filter rooms/DM functionality from World view', 'Regression', 0, '2019-02-05 18:56:51.710241', 1, 1),
(13, 'Create & Browse Rooms', 'Verify Create Room Page', 'To Verify all details of Create Room Page', 'Regression', 0, '2019-02-05 18:56:51.754626', 1, 1),
(14, 'Create & Browse Rooms', 'Verify Creating a Room', 'To Create a new room', 'Regression', 0, '2019-02-05 18:56:51.832245', 1, 1),
(15, 'Create & Browse Rooms', 'Verify Browse Rooms page', 'To verify \'Browse Rooms\' page', 'Regression', 0, '2019-02-05 18:56:51.876537', 1, 1),
(16, 'Create & Browse Rooms', 'Verify Joining & Leaving Room from Browse rooms', 'Joining and Leaving Room', 'Regression', 0, '2019-02-05 18:56:51.921131', 1, 1),
(17, 'Create & Browse Rooms', 'Verify Room Preview from Browse rooms', 'Room Preview Page', 'Regression', 1, '2019-02-05 18:56:51.965363', 1, 1),
(18, 'Create & Browse Rooms', 'Verify Members Count of Rooms in Rooms You\'ve been invited to', 'Verify the count in rooms', 'Regression', 2, '2019-02-05 18:56:52.009842', 1, 1),
(19, 'Create & Browse Rooms', 'Verify Add room in offline mode', 'Behaviour of add room page in offline mode', 'Regression', 2, '2019-02-05 18:56:52.054063', 1, 1),
(20, 'Create & Browse Rooms', 'Verify New (red label) for Rooms you\'ve been invited to', 'Verify New (red label) for Rooms you\'ve been invited to', 'Regression', 2, '2019-02-05 18:56:52.098528', 1, 1),
(21, 'Room Preview', 'Verify the user navigation to room preview screens', 'Verify the user navigation to room preview screens', 'Regression', 1, '2019-02-05 18:56:52.143032', 1, 1),
(22, 'Room Preview', 'Verify the Join option from room details through room preview', 'Verify the Join option from room details through room preview', 'Regression', 1, '2019-02-05 18:56:52.187472', 1, 1),
(23, 'Room Preview', 'Verify the Join option from room preview', 'Verify the Join option from room preview', 'Regression', 1, '2019-02-05 18:56:52.264873', 1, 1),
(24, 'Room Preview', 'Verify the collapsed summary functionality in Room preview', 'Verify the collapsed summary functionality in Room preview', 'Regression', 1, '2019-02-05 18:56:52.342668', 1, 1),
(25, 'Room Preview', 'Verify the UI elements/sections rendering in Room preview', 'Verify the UI elements/sections rendering in Room preview', 'Regression', 1, '2019-02-05 18:56:52.387093', 1, 1),
(26, 'Room Preview', 'Verify the Long press message options in Room preview', 'Verify the Long press message options in Room preview', 'Regression', 2, '2019-02-05 18:56:52.431381', 1, 1),
(27, 'Faster DM & Group DM', 'Verify the navigation to Conversation launcher from world view', 'Verify the navigation to Direct message screen from world view', 'Regression', 0, '2019-02-05 18:56:52.475623', 1, 1),
(28, 'Faster DM & Group DM', 'Verify the Faster DM screen functionality', 'Verify the Direct Message contacts screen functionality', 'Regression', 0, '2019-02-05 18:56:52.520182', 1, 1),
(29, 'Faster DM & Group DM', 'Verify if the user is able to create a new DM', 'Verify if the user is able to create a new DM', 'Regression', 0, '2019-02-05 18:56:52.608900', 1, 1),
(30, 'Faster DM & Group DM', 'Verify if the user is able to create a group DM', 'Verify if the user is able to create a group DM', 'Regression', 1, '2019-02-05 18:56:52.664327', 1, 1),
(31, 'Faster DM & Group DM', 'Verify the OTR functionality', 'Verify the OTR functionality', 'Regression', 1, '2019-02-05 18:56:52.741797', 1, 1),
(32, 'Faster DM & Group DM', 'Verify the OTR topics in space view', 'Verify the OTR topics in space view', 'Regression', 1, '2019-02-05 18:56:52.797534', 1, 1),
(33, 'Faster DM & Group DM', 'Verify OTR Messages after 24 Hours', 'Verify OTR Messages after 24 Hours', 'Regression', 1, '2019-02-05 18:56:52.886042', 1, 1),
(34, 'DM Details', 'Verify DM Details Page', 'To Verify DM Details Page', 'Regression', 1, '2019-02-05 18:56:52.941666', 1, 1),
(35, 'DM Details', 'Verify Star & Unstar', 'To Verify Star & Unstar', 'Regression', 2, '2019-02-05 18:56:52.997215', 1, 1),
(36, 'DM Details', 'Verify Notifications setting for Room Details', 'To Verify Notifications setting for Room Details', 'Regression', 1, '2019-02-05 18:56:53.085856', 1, 1),
(37, 'DM Details', 'Verify the Members list', 'To Verify the Members list', 'Regression', 2, '2019-02-05 18:56:53.174387', 1, 1),
(38, 'DM Details', 'Verify Hide functionality from DM', 'To Verify Hide functionality from DM', 'Regression', 1, '2019-02-05 18:56:53.229873', 1, 1),
(39, 'Space View', 'Verify the Nav bar functionality in space view', 'Verify the functionality of the nav bar in space view', 'Regression', 0, '2019-02-05 18:56:53.285280', 1, 1),
(40, 'Space View', 'Verify the persistent header in space view', 'Verify the persistent header in space view', 'Regression', 1, '2019-02-05 18:56:53.340951', 1, 1),
(41, 'Space View', 'Verify topics pinning in Space view', 'Verify topics pinning in Space view', 'Regression', 2, '2019-02-05 18:56:53.396405', 1, 1),
(42, 'Space View', 'Verify timestamps, avatars, date dividers and unread labels in space view', 'Verify timestamps, date dividers and unread labels in space view', 'Regression', 0, '2019-02-05 18:56:53.451702', 1, 1),
(43, 'Space View', 'Verify the collapsed summary functionality in space view', 'Verify the collapsed summary functionality in space view', 'Regression', 2, '2019-02-05 18:56:53.540569', 1, 1),
(44, 'Space View', 'Verify the senders summary naming in Collapsed messages summary', 'Verify the senders summary naming in Collapsed messages summary', 'Regression', 2, '2019-02-05 18:56:53.596172', 1, 1),
(45, 'Space View', 'Verify the new conversation FAB in space view', 'Verify the new conversation FAB in space view', 'Regression', 0, '2019-02-05 18:56:53.651491', 1, 1),
(46, 'Space View', 'Verify if the files are doc files are launched in native apps from space view', 'Verify if the files are doc files are launched in native apps from space view', 'Regression', 1, '2019-02-05 18:56:53.706749', 1, 1),
(47, 'Space View', 'Verify the space-topic view transitions', 'Verify the space-topic view transitions', 'Regression', 2, '2019-02-05 18:56:53.762504', 1, 1),
(48, 'Space View', 'Verify the space view rendering with topics containing different types of data', 'Verify the space view rendering with topics containing different types of data', 'Regression', 1, '2019-02-05 18:56:53.851113', 1, 1),
(49, 'Space View', 'Verify Long press message options in space view', 'Verify Long press message options in space view', 'Regression', 1, '2019-02-05 18:56:53.906620', 1, 1),
(50, 'Space View', 'Verify real time updates in space view', 'Verify real time updates in space view', 'Regression', 1, '2019-02-05 18:56:53.962150', 1, 1),
(51, 'Space View', 'Verify the System messages generated in space view', 'Verify the System messages generated in space view', 'Regression', 1, '2019-02-05 18:56:54.017579', 1, 1),
(52, 'Space View', 'Verify editing messages functionality from Space view', 'Verify editing messages functionality from Space view', 'Regression', 1, '2019-02-05 18:56:54.106276', 1, 1),
(53, 'Space View', 'Verify Add people functionality from persistent header in space view', 'Verify Add people functionality from persistent header in space view', 'Regression', 1, '2019-02-05 18:56:54.161783', 1, 1),
(54, 'Space View', 'Verify pagination in Space view', 'Verify pagination in Space view', 'Regression', 2, '2019-02-05 18:56:54.217137', 1, 1),
(55, 'Room Details', 'Verify Room Details Page', 'Verify Room Details Page', 'Regression', 2, '2019-02-05 18:56:54.305830', 1, 1),
(56, 'Room Details', 'Verify Edit Room name', 'Verify Edit Room name', 'Regression', 1, '2019-02-05 18:56:54.361404', 1, 1),
(57, 'Room Details', 'Verify adding People & Bots', 'Verify adding People & Bots', 'Regression', 1, '2019-02-05 18:56:54.450001', 1, 1),
(58, 'Room Details', 'Verify Star & Unstar', 'Verify Star & Unstar', 'Regression', 2, '2019-02-05 18:56:54.505518', 1, 1),
(59, 'Room Details', 'Verify Notifications for Room Details', 'Verify Notifications for Room Details', 'Regression', 2, '2019-02-05 18:56:54.560933', 1, 1),
(60, 'Room Details', 'Verify Leave Room', 'Verify Leave Room', 'Regression', 1, '2019-02-05 18:56:54.649347', 1, 1),
(61, 'Room Details', 'Verify the Members list', 'Verify the Members list', 'Regression', 2, '2019-02-05 18:56:54.751247', 1, 1),
(62, 'Room Details', 'Verify the Can join list', 'Verify the Can join list', 'Regression', 2, '2019-02-05 18:56:54.829398', 1, 1),
(63, 'Topic View', 'Verify the existing Topic view rendering of UI elements/Sections', 'Verify the existing Topic view rendering of UI elements/Sections', 'Regression', 0, '2019-02-05 18:56:54.873881', 1, 1),
(64, 'Topic View', 'Verify the New Conversation view rendering of UI elements/Sections', 'Verify the New Conversation view rendering of UI elements/Sections', 'Regression', 1, '2019-02-05 18:56:54.951309', 1, 1),
(65, 'Topic View', 'Verify Long press message options in topic view', 'Verify Long press message options in topic view', 'Regression', 2, '2019-02-05 18:56:54.995804', 1, 1),
(66, 'Topic View', 'Verify the Compose field components in topic view', 'Verify the Compose field components in topic view', 'Regression', 0, '2019-02-05 18:56:55.073375', 1, 1),
(67, 'Topic View', 'Verify the Send button functionality in compose view', 'Verify the Send button functionality in compose view', 'Regression', 1, '2019-02-05 18:56:55.150997', 1, 1),
(68, 'Topic View', 'Verify the Upload icon functionality in compose view', 'Verify the Upload icon functionality in compose view', 'Regression', 1, '2019-02-05 18:56:55.195452', 1, 1),
(69, 'Topic View', 'Verify the Camera icon functionality in compose', 'Verify the Camera icon functionality in compose', 'Regression', 2, '2019-02-05 18:56:55.239854', 1, 1),
(70, 'Topic View', 'Verify the Thor icon functionality in compose', 'Verify the Thor icon functionality in compose', 'Regression', 2, '2019-02-05 18:56:55.284349', 1, 1),
(71, 'Topic View', 'Verify the Attachment replace functionality in compose view', 'Verify the Attachment replace functionality in compose view', 'Regression', 1, '2019-02-05 18:56:55.328781', 1, 1),
(72, 'Topic View', 'Verifying the message replying and the incoming messages functionality in topic view', 'Verifying the message replying and the real time updates functionality in topic view', 'Regression', 1, '2019-02-05 18:56:55.406153', 1, 1),
(73, 'Topic View', 'Verify the messages coalescing functionality in topic view', 'Verify the messages coalescing functionality in topic view', 'Regression', 2, '2019-02-05 18:56:55.450665', 1, 1),
(74, 'Topic View', 'Verify the edit message functionality in topic view', 'Verify the edit message functionality in topic view', 'Regression', 2, '2019-02-05 18:56:55.528247', 1, 1),
(75, 'Topic View', 'Verify the ACL fixing messages', 'Verify the ACL fixing messages', 'Regression', 1, '2019-02-05 18:56:55.605692', 1, 1),
(76, 'Topic View', 'Verify if the user is able to successfully create the draft in topic view', 'Verify if the user is able to successfully create the draft in topic view', 'Regression', 1, '2019-02-05 18:56:55.650250', 1, 1),
(77, 'Topic View', 'verify if the user is able to successfully create the draft in a new topic', 'verify if the user is able to successfully create the draft in a new topic', 'Regression', 2, '2019-02-05 18:56:55.738891', 1, 1),
(78, 'Topic View', 'Verify if the user is able to successfully create a draft with attachment', 'Verify if the user is able to successfully create a draft with attachment', 'Regression', 2, '2019-02-05 18:56:55.783248', 1, 1),
(79, 'Topic View', 'Verify the failed message options in offline mode', 'Verify the failed message options in offline mode', 'Regression', 1, '2019-02-05 18:56:55.861016', 1, 1),
(80, 'Topic View', 'Verify notification settings in Topic view.', 'Verify notification settings in Topic view.', 'Regression', 2, '2019-02-05 18:56:55.938614', 1, 1),
(81, 'Topic View', 'Verify pagination in Topic view', 'Verify pagination in Topic view', 'Regression', 2, '2019-02-05 18:56:56.016257', 1, 1),
(82, 'Space Search', 'Verify the Space search navigation and UI of space search in Zero state', 'Verify the Space search navigation and UI of space search in Zero state', 'Regression', 0, '2019-02-05 18:56:56.060526', 1, 1),
(83, 'Space Search', 'Verify the Space search functionality based on keyword', 'Verify the Space search functionality based on keyword', 'Regression', 1, '2019-02-05 18:56:56.104786', 1, 1),
(84, 'Space Search', 'Verify the Space search functionality based on member selection from the filter panel', 'Verify the Space search functionality based on member selection from the filter panel', 'Regression', 1, '2019-02-05 18:56:56.182579', 1, 1),
(85, 'Space Search', 'Verify the Space search functionality based on Content type selection from the filter panel', 'Verify the Space search functionality based on Content type selection from the filter panel', 'Regression', 1, '2019-02-05 18:56:56.260324', 1, 1),
(86, 'Notifications', 'Verify the notifications options menu from Settings', 'Verify the notifications options menu from Settings', 'Regression', 1, '2019-02-05 18:56:56.337727', 1, 1),
(87, 'Notifications', 'Verify notifications disabling from device settings', 'Verify notifications disabling from device settings', 'Regression', 1, '2019-02-05 18:56:56.515313', 1, 1),
(88, 'Notifications', 'Verify action buttons on Rich notifications', 'Verify action buttons on Rich notifications', 'Regression', 2, '2019-02-05 18:56:56.559577', 1, 1),
(89, 'Notifications', 'Verify inline replies from notifications [Android N+ Only]', 'Verify inline replies from notifications [Android N+ Only]', 'Regression', 2, '2019-02-05 18:56:56.603937', 1, 1),
(90, 'Notifications', 'Verify the notifications received and notifications messages coalescing', 'Verify the notifications received and notifications messages coalescing', 'Regression', 2, '2019-02-05 18:56:56.648644', 1, 1),
(91, 'Notifications', 'Verify notifications on receiving files', 'Verify notifications on receiving files', 'Regression', 2, '2019-02-05 18:56:56.726122', 1, 1),
(92, 'Autocomplete', 'Verify the Autocomplete rendering and functionality in Existing Room_Topic view', 'Verify the Autocomplete rendering in Room_Existing Topic view', 'Regression', 1, '2019-02-05 18:56:56.792646', 1, 1),
(93, 'Autocomplete', 'Verify the Autocomplete rendering and functionality in New room', 'Verify the Autocomplete rendering and functionality in New room', 'Regression', 2, '2019-02-05 18:56:56.837162', 1, 1),
(94, 'Autocomplete', 'Verify the Autocomplete rendering and functionality in 1:1 DM', 'Verify the Autocomplete rendering and functionality in 1:1 DM', 'Regression', 1, '2019-02-05 18:56:56.914740', 1, 1),
(95, 'Autocomplete', 'Verify the Autocomplete rendering and functionality in group DM', 'Verify the Autocomplete rendering and functionality in group DM', 'Regression', 2, '2019-02-05 18:56:56.992365', 1, 1),
(96, '@Mentions', 'Verify @mention functionality', 'Verify @mention functionality', 'Regression', 1, '2019-02-05 18:56:57.092139', 1, 1),
(97, '@Mentions', 'Verify @all mentions functionality', 'Verify @all mentions functionality', 'Regression', 2, '2019-02-05 18:56:57.169667', 1, 1),
(98, '@Mentions', 'Verify adding people functionality using @mention', 'Verify adding people functionality using @mention', 'Regression', 1, '2019-02-05 18:56:57.213959', 1, 1),
(99, 'Presence Indicators', 'Verify the presence indicators display across the app', 'Verify the presence indicators display across the app', 'Regression', 1, '2019-02-05 18:56:57.258400', 1, 1),
(100, 'Presence Indicators', 'Verify the presence indicator for active status', 'Verify the presence indicator for active status', 'Regression', 2, '2019-02-05 18:56:57.336067', 1, 1),
(101, 'Presence Indicators', 'Verify the presence indicator for inactive status', 'Verify the presence indicator for inactive status', 'Regression', 2, '2019-02-05 18:56:57.380605', 1, 1),
(102, 'Miscellaneous', 'Verify if all the strings in the client are localized properly', 'Verify if all the strings in the client are localized properly', 'Regression', 2, '2019-02-05 18:56:57.424998', 1, 1),
(103, 'Miscellaneous', 'Verify the app behaviour and UI upon font settings change', 'Verify the app behaviour and UI upon font settings change', 'Regression', 2, '2019-02-05 18:56:57.469429', 1, 1),
(104, 'Miscellaneous', 'Verify the data syncing of the clients across the platforms', 'Verify the data syncing of the clients across the platforms', 'Regression', 2, '2019-02-05 18:56:57.513794', 1, 1),
(105, 'Miscellaneous', 'Verify feedback functionality in app', 'Verify feedback functionality in app', 'Regression', 2, '2019-02-05 18:56:57.558041', 1, 1),
(106, 'Miscellaneous', 'Verify inactive world view redirect when app is in foreground', 'Verify inactive world view redirect when app is in foreground', 'Regression', 2, '2019-02-05 18:56:57.602225', 1, 1),
(107, 'Miscellaneous', 'Verify inactive world view redirect when app is in background', 'Verify inactive world view redirect when app is in background', 'Regression', 2, '2019-02-05 18:56:57.718426', 1, 1),
(108, 'Miscellaneous', 'Verify inactive world view redirect when there is draft', 'Verify inactive world view redirect when there is draft', 'Regression', 2, '2019-02-05 18:56:57.781898', 1, 1),
(109, 'Message Delete', 'Verify the Delete message functionality in topic view_Existing Topic', 'Verify the Delete message functionality in topic view', 'Regression', 0, '2019-02-05 18:56:57.837331', 1, 1),
(110, 'Message Delete', 'Verify the Delete message functionality in topic view_New Conversation', 'Verify the Delete message functionality in topic view', 'Regression', 1, '2019-02-05 18:56:57.892704', 1, 1),
(111, 'Message Delete', 'Verify the Delete message functionality in space view', 'Verify the Delete message functionality in space view', 'Regression', 1, '2019-02-05 18:56:57.948577', 1, 1),
(112, 'Message Delete', 'Verify the world view updates after deleting a recent message from room/DM', 'Verify the world view updates after deleting a recent message from room/DM', 'Regression', 1, '2019-02-05 18:56:58.011135', 1, 1),
(113, 'Message Delete', 'Verify the behaviour on tapping a notification which got deleted', 'Verify the Delete message functionality in space view', 'Regression', 1, '2019-02-05 18:56:58.081657', 1, 1),
(114, 'Message Delete', 'Verify the Delete message functionality in Offline', 'Verify the Delete message functionality in Offline', 'Regression', 2, '2019-02-05 18:56:58.137407', 1, 1),
(115, 'Bots', 'Verify the Bot option displayed in FAB of World view', 'Verify the Bot option displayed in FAB of World view', 'Regression', 0, '2019-02-05 18:56:58.192798', 1, 1),
(116, 'Bots', 'Verify the Bot DM functionality from the "Add Bot" screen', 'Verify the Bot DM functionality from the "Add Bot" screen', 'Regression', 1, '2019-02-05 18:56:58.248187', 1, 1),
(117, 'Bots', 'Verify the Bot DM Details screen', 'Verify the Bot DM Details screen', 'Regression', 1, '2019-02-05 18:56:58.337072', 1, 1),
(118, 'Bots', 'Verify the remove option functionality for Bot DM', 'Verify the remove option functionality for Bot DM', 'Regression', 2, '2019-02-05 18:56:58.392541', 1, 1),
(119, 'Content Sharing', 'Verify Content sharing without app login', 'Verify Content sharing without app login', 'Regression', 1, '2019-02-05 18:56:58.447991', 1, 1),
(120, 'Content Sharing', 'Verify Content sharing with app login', 'Verify Content sharing with app login', 'Regression', 0, '2019-02-05 18:56:58.503396', 1, 1),
(121, 'Content Sharing', 'Verify Multiple images sharing', 'Verify Multiple images sharing', 'Regression', 2, '2019-02-05 18:56:58.558888', 1, 1),
(122, 'Content Sharing', 'Verify sharing different content from other apps', 'Verify sharing different content from other apps', 'Regression', 2, '2019-02-05 18:56:58.614441', 1, 1),
(123, 'Content Sharing', 'Verify content sharing in offline mode', 'Verify content sharing in offline mode', 'Regression', 2, '2019-02-05 18:56:58.669903', 1, 1),
(124, 'Manual Conversation Bumping', 'Check new message blue bar functionality in space view', 'Check new message blue bar functionality in space view', 'Regression', 1, '2019-02-05 18:56:58.725273', 1, 1),
(125, 'Manual Conversation Bumping', 'Check the increment/decrement of the count in new messages blue bar', 'Check the increment/decrement of the count in new messages blue bar', 'Regression', 2, '2019-02-05 18:56:58.780818', 1, 1),
(126, 'Manual Conversation Bumping', 'Check if new system messages doesn\'t generate new message blue bar', 'Check if new system messages doesn\'t generate new message blue bar', 'Regression', 2, '2019-02-05 18:56:58.836329', 1, 1),
(127, 'Manual Conversation Bumping', 'Check if the new messages bar doesn\'t appear on revisiting the space view from any other view', 'Check if the new messages bar doesn\'t appear on revisiting the space view from any other view', 'Regression', 2, '2019-02-05 18:56:58.881681', 1, 1),
(128, 'Manual Conversation Bumping', 'Check the swipe behaviour of the new messages snack bar in space view', 'Check if the new messages bar doesn\'t appear on revisiting the space view from any other view', 'Regression', 2, '2019-02-05 18:56:58.926123', 1, 1),
(129, 'Bots in Rooms', 'Verify if the user is able to add a bot to the room by @mentioning', 'Verify if the user is able to add a bot to the room by @mentioning', 'Regression', 1, '2019-02-05 18:56:58.970561', 1, 1),
(130, 'Bots in Rooms', 'Verify if the user is able to add a bot to the room through invite flow', 'Verify if the user is able to add a bot to the room through invite flow', 'Regression', 2, '2019-02-05 18:56:59.015113', 1, 1),
(131, 'Bots in Rooms', 'Verify if the user is able to remove a bot from the room - TBD', 'Verify if the user is able to remove a bot from the room', 'Regression', 2, '2019-02-05 18:56:59.059438', 1, 1),
(132, 'Bots in Rooms', 'Verify the Bot response when @mentioned from autocomplete - TBD', 'Verify the Bot response when @mentioned from autocomplete', 'Regression', 2, '2019-02-05 18:56:59.103677', 1, 1),
(133, 'Interop & Group Interop', 'Verify sending different types of data in a 1:1 DM (Hangouts Classic - Hangouts Chat)', 'Verify sending different types of data in a 1:1 DM (Hangouts Classic - Hangouts Chat)', 'Regression', 0, '2019-02-05 18:56:59.148276', 1, 1),
(134, 'Interop & Group Interop', 'Verify sending different types of data in a 1:1 DM (Hangouts Chat - Hangouts Classic)', 'Verify sending different types of data in a 1:1 DM (Hangouts Chat - Hangouts Classic)', 'Regression', 0, '2019-02-05 18:56:59.192686', 1, 1),
(135, 'Interop & Group Interop', 'Verify Groups syncing from Hangouts Classic to Hangouts Chat (Existing groups)', 'Verify Groups syncing from Hangouts Classic to Hangouts Chat (Existing groups)', 'Regression', 1, '2019-02-05 18:56:59.236828', 1, 1),
(136, 'Interop & Group Interop', 'Verify Groups syncing from Hangouts Classic to Hangouts Chat (New groups)', 'Verify Groups syncing from Hangouts Classic to Hangouts Chat (New groups)', 'Regression', 1, '2019-02-05 18:56:59.392399', 1, 1),
(137, 'Interop & Group Interop', 'Verify OTR settings sync in groups (From Classic to Chat and Chat to Classic)', 'Verify OTR settings sync in groups (From Classic to Chat and Chat to Classic)', 'Regression', 2, '2019-02-05 18:56:59.436759', 1, 1),
(138, 'Interop & Group Interop', 'Verify Blue Message bar and Latest Thread being Active.', 'Verify Blue Message bar and Latest Thread being Active.', 'Regression', 2, '2019-02-05 18:56:59.481089', 1, 1),
(139, 'Fixed width Formatting', 'Verify Inline code functionality in Topic and Space view', 'Verify Inline code functionality in Topic and Space view', 'Regression', 2, '2019-02-05 18:56:59.525236', 1, 1),
(140, 'Fixed width Formatting', 'Verify Code Block functionality in Topic and Space view', 'Verify Code Block functionality in Topic and Space view', 'Regression', 2, '2019-02-05 18:56:59.569767', 1, 1),
(141, 'Enhanced Invite Emails', 'Verify the Invite modal in Invite screen', 'Verify the Invite modal in Invite screen', 'Regression', 2, '2019-02-05 18:56:59.614129', 1, 1),
(142, 'Enhanced Invite Emails', 'Verify if the invite email is sent to the user on invitation to room', 'Verify if the invite email is sent to the user on invitation to room', 'Regression', 1, '2019-02-05 18:56:59.658679', 1, 1),
(143, 'Enhanced Invite Emails', 'Verify if the invite email is sent to all the users of group on invitation to room', 'Verify if the invite email is sent to all the users of group on invitation to room', 'Regression', 2, '2019-02-05 18:56:59.703324', 1, 1),
(144, 'Deep Linking', 'Verify deep linking to the respective room/DM/bot_ Member', 'Verify deep linking to the respective room/DM/bot_ Member', 'Regression', 1, '2019-02-05 18:56:59.747828', 1, 1),
(145, 'Deep Linking', 'Verify deep linking to the respective room/DM/bot_ Non Member', 'Verify deep linking to the respective room/DM/bot_ Non Member', 'Regression', 1, '2019-02-05 18:56:59.792338', 1, 1),
(146, 'Flat DM', 'Verify UI changes of DM', 'Observe UI changes of DM', 'Regression', 0, '2019-02-05 18:56:59.836698', 1, 1),
(147, 'Flat DM', 'Verify creating a new 1:1/group DM', 'Verify creating a new 1:1/group DM', 'Regression', 1, '2019-02-05 18:56:59.881145', 1, 1),
(148, 'Flat DM', 'Verify History ON/OFF functionality', 'Verify History ON/OFF functionality', 'Regression', 1, '2019-02-05 18:56:59.925736', 1, 1),
(149, 'Flat DM', 'Verify edit/delete message functionality in flat DMs.', 'Verify edit/delete message functionality in flat DMs.', 'Regression', 2, '2019-02-05 18:56:59.970182', 1, 1),
(150, 'Flat DM', 'Verify manual message bumping in flat DM', 'Verify manual message bumping in flat DM', 'Regression', 1, '2019-02-05 18:57:00.014713', 1, 1),
(151, 'Flat DM', 'Verify offline handling in flat DMs', 'Verify offline handling in flat DMs', 'Regression', 2, '2019-02-05 18:57:00.058892', 1, 1),
(152, 'Both Auth & Disabled bots', 'Verify @mentioned bot successfully respond.', 'Verify @mentioned bot successfully respond.', 'Regression', 1, '2019-02-05 18:57:00.103481', 1, 1),
(153, 'Both Auth & Disabled bots', 'Verify @mentioned bot requires configuration', 'Verify @mentioned bot requires configuration', 'Regression', 1, '2019-02-05 18:57:00.147863', 1, 1),
(154, 'Both Auth & Disabled bots', 'Verify @mentioned bot fails to respond', 'Verify @mentioned bot fails to respond', 'Regression', 2, '2019-02-05 18:57:00.192219', 1, 1),
(155, 'Both Auth & Disabled bots', 'Verify Enable/Disable bots from Admin.', 'Verify Enable/Disable bots from Admin.', 'Regression', 2, '2019-02-05 18:57:00.236630', 1, 1),
(156, 'Both Auth & Disabled bots', 'Verify Enable/Disable bots from developer console', 'Verify Enable/Disable bots from developer console', 'Regression', 2, '2019-02-05 18:57:00.280958', 1, 1),
(157, 'Local stream Data', 'Verify Loading of messages in DM/Topic view on cold/warm start', 'Verify Loading of messages in DM/Topic view on cold/warm start', 'Regression', 1, '2019-02-05 18:57:00.325398', 1, 1),
(158, 'Local stream Data', 'Verify loading of messages after replying through notification', 'Verify loading of messages after replying through notification', 'Regression', 1, '2019-02-05 18:57:00.371508', 1, 1),
(159, 'Local stream Data', 'Verify messages loading in Offline/Slow-network loading.', 'Verify messages loading in Offline/Slow-network loading.', 'Regression', 2, '2019-02-05 18:57:00.414153', 1, 1),
(160, 'Jump to Bottom', 'Verify jump to bottom FAB in READ rooms', 'Verify jump to bottom FAB in READ rooms', 'Regression', 1, '2019-02-05 18:57:00.458437', 1, 1),
(161, 'Jump to Bottom', 'Verify jump to bottom FAB in UNREAD rooms', 'Verify jump to bottom FAB in UNREAD rooms', 'Regression', 1, '2019-02-05 18:57:00.502872', 1, 1),
(162, 'Jump to Bottom', 'Verify that jump to bottom FAB is not displayed in DM\'s', 'Verify that jump to bottom FAB is not displayed in DM\'s', 'Regression', 2, '2019-02-05 18:57:00.547259', 1, 1),
(163, 'Navigation Drawer', 'Verify Navigation Panel available after Login', 'Verify Navigation Panel available after Login', 'Regression', 1, '2019-02-05 18:57:00.591680', 1, 1),
(164, 'Navigation Drawer', 'Verify menu contents in Navigation', 'Verify menu contents in Navigation', 'Regression', 1, '2019-02-05 18:57:00.636155', 1, 1),
(165, 'Do Not Disturb & Dedicated Settings', 'Verify user status on world view header', 'Verify user status on world view header', 'Regression', 1, '2019-02-05 18:57:00.691779', 1, 1),
(166, 'Do Not Disturb & Dedicated Settings', 'Verify Do Not Disturb feature from navigation drawer', 'Verify Do Not Disturb feature from navigation drawer', 'Regression', 1, '2019-02-05 18:57:00.747342', 1, 1),
(167, 'Do Not Disturb & Dedicated Settings', 'Verify (Dedicated Settings) Settings -> Mobile Notifications from nav drawer', 'Verify (Dedicated Settings) Settings -> Mobile Notifications from nav drawer', 'Regression', 1, '2019-02-05 18:57:00.802860', 1, 1),
(168, 'Reactions', 'Verify adding reaction on self/other member/bot message in rooms/dms', 'Verify adding reaction on self/other member/bot message in rooms/dms', 'Regression', 1, '2019-02-05 18:57:00.858284', 1, 1),
(169, 'Reactions', 'Verify increment/Decrement of count on the reaction.', 'Verify increment/Decrement of count on the reaction.', 'Regression', 2, '2019-02-05 18:57:00.913990', 1, 1),
(170, 'Reactions', 'Verify list of reactors on long pressing a reaction.', 'Verify list of reactors on long pressing a reaction.', 'Regression', 2, '2019-02-05 18:57:01.002385', 1, 1),
(171, 'Stop Dual Notifications', 'Check Dual Notifications enabled by default for Classic from Dynamite flow', 'Check Dual Notifications enabled by default for Classic from Dynamite flow', 'Regression', 1, '2019-02-05 18:57:01.213023', 1, 1),
(172, 'Stop Dual Notifications', 'Disabling Classic Notification from Dynamite flow', 'Disabling Classic Notification from Dynamite flow', 'Regression', 2, '2019-02-05 18:57:01.279927', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `reports_testresultconsolidation`
--

CREATE TABLE `reports_testresultconsolidation` (
  `id` int(11) NOT NULL,
  `test_status` int(11) NOT NULL,
  `bugid` varchar(50) NOT NULL,
  `bug_status` varchar(50) NOT NULL,
  `execution_time` double NOT NULL,
  `executed_date` date NOT NULL,
  `test_build_id` int(11) NOT NULL,
  `test_case_id` int(11) NOT NULL,
  `test_device_id` int(11) NOT NULL,
  `test_device_logs` longtext,
  `test_server_logs` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports_testresultconsolidation`
--

INSERT INTO `reports_testresultconsolidation` (`id`, `test_status`, `bugid`, `bug_status`, `execution_time`, `executed_date`, `test_build_id`, `test_case_id`, `test_device_id`, `test_device_logs`, `test_server_logs`) VALUES
(1, 1, '', '', 39, '2019-02-08', 6, 45, 1, 'dat45logcat.txt', 'dat45server.txt'),
(2, 1, '', '', 30, '2019-02-08', 6, 13, 1, 'dat13logcat.txt', 'dat13server.txt'),
(3, 1, '', '', 63, '2019-02-08', 6, 14, 1, 'dat14logcat.txt', 'dat14server.txt'),
(4, 1, '', '', 68, '2019-02-08', 6, 28, 1, 'dat28logcat.txt', 'dat28server.txt'),
(5, 1, '', '', 71, '2019-02-08', 6, 16, 1, 'dat16logcat.txt', 'dat16server.txt'),
(6, 1, '', '', 7, '2019-02-08', 6, 7, 1, 'dat7logcat.txt', 'dat7server.txt'),
(7, 1, '', '', 31, '2019-02-08', 6, 146, 1, 'dat146logcat.txt', 'dat146server.txt'),
(8, 1, '', '', 29, '2019-02-08', 6, 66, 1, 'dat66logcat.txt', 'dat66server.txt'),
(9, 1, '', '', 63, '2019-02-08', 6, 39, 1, 'dat39logcat.txt', 'dat39server.txt'),
(10, 1, '', '', 23, '2019-02-08', 6, 115, 1, 'dat115logcat.txt', 'dat115server.txt'),
(11, 1, '', '', 41, '2019-02-08', 6, 2, 1, 'dat2logcat.txt', 'dat2server.txt'),
(12, 1, '', '', 17, '2019-02-08', 6, 15, 1, 'dat15logcat.txt', 'dat15server.txt'),
(13, 1, '', '', 40, '2019-02-08', 6, 82, 1, 'dat82logcat.txt', 'dat82server.txt'),
(14, 1, '', '', 13, '2019-02-08', 6, 27, 1, 'dat27logcat.txt', 'dat27server.txt'),
(15, 1, '', '', 12, '2019-02-08', 6, 11, 1, 'dat11logcat.txt', 'dat11server.txt'),
(16, 1, '', '', 96, '2019-02-08', 6, 109, 1, 'dat109logcat.txt', 'dat109server.txt'),
(17, 1, '', '', 142, '2019-02-08', 6, 42, 1, 'dat42logcat.txt', 'dat42server.txt'),
(18, 1, '', '', 71, '2019-02-08', 6, 12, 1, 'dat12logcat.txt', 'dat12server.txt'),
(19, 1, '', '', 27, '2019-02-08', 6, 63, 1, 'dat63logcat.txt', 'dat63server.txt'),
(20, 1, '', '', 85, '2019-02-08', 6, 120, 1, 'dat120logcat.txt', 'dat120server.txt'),
(21, 1, '', '', 34, '2019-02-08', 7, 45, 1, 'dat45logcat.txt', 'dat45server.txt'),
(22, 1, '', '', 27, '2019-02-08', 7, 13, 1, 'dat13logcat.txt', 'dat13server.txt'),
(23, 1, '', '', 59, '2019-02-08', 7, 14, 1, 'dat14logcat.txt', 'dat14server.txt'),
(24, 1, '', '', 62, '2019-02-08', 7, 28, 1, 'dat28logcat.txt', 'dat28server.txt'),
(25, 1, '', '', 70, '2019-02-08', 7, 16, 1, 'dat16logcat.txt', 'dat16server.txt'),
(26, 1, '', '', 7, '2019-02-08', 7, 7, 1, 'dat7logcat.txt', 'dat7server.txt'),
(27, 1, '', '', 26, '2019-02-08', 7, 146, 1, 'dat146logcat.txt', 'dat146server.txt'),
(28, 1, '', '', 24, '2019-02-08', 7, 66, 1, 'dat66logcat.txt', 'dat66server.txt'),
(29, 1, '', '', 61, '2019-02-08', 7, 39, 1, 'dat39logcat.txt', 'dat39server.txt'),
(30, 1, '', '', 23, '2019-02-08', 7, 115, 1, 'dat115logcat.txt', 'dat115server.txt'),
(31, 1, '', '', 44, '2019-02-08', 7, 2, 1, 'dat2logcat.txt', 'dat2server.txt'),
(32, 1, '', '', 19, '2019-02-08', 7, 15, 1, 'dat15logcat.txt', 'dat15server.txt'),
(33, 1, '', '', 35, '2019-02-08', 7, 82, 1, 'dat82logcat.txt', 'dat82server.txt'),
(34, 1, '', '', 14, '2019-02-08', 7, 27, 1, 'dat27logcat.txt', 'dat27server.txt'),
(35, 1, '', '', 12, '2019-02-08', 7, 11, 1, 'dat11logcat.txt', 'dat11server.txt'),
(36, 1, '', '', 92, '2019-02-08', 7, 109, 1, 'dat109logcat.txt', 'dat109server.txt'),
(37, 1, '', '', 133, '2019-02-08', 7, 42, 1, 'dat42logcat.txt', 'dat42server.txt'),
(38, 1, '', '', 61, '2019-02-08', 7, 12, 1, 'dat12logcat.txt', 'dat12server.txt'),
(39, 1, '', '', 26, '2019-02-08', 7, 63, 1, 'dat63logcat.txt', 'dat63server.txt'),
(40, 1, '', '', 82, '2019-02-08', 7, 120, 1, 'dat120logcat.txt', 'dat120server.txt'),
(41, 1, '', '', 35, '2019-02-08', 8, 45, 1, 'dat45logcat.txt', 'dat45server.txt'),
(42, 1, '', '', 27, '2019-02-08', 8, 13, 1, 'dat13logcat.txt', 'dat13server.txt'),
(43, 1, '', '', 57, '2019-02-08', 8, 14, 1, 'dat14logcat.txt', 'dat14server.txt'),
(44, 1, '', '', 64, '2019-02-08', 8, 28, 1, 'dat28logcat.txt', 'dat28server.txt'),
(45, 1, '', '', 70, '2019-02-08', 8, 16, 1, 'dat16logcat.txt', 'dat16server.txt'),
(46, 1, '', '', 7, '2019-02-08', 8, 7, 1, 'dat7logcat.txt', 'dat7server.txt'),
(47, 1, '', '', 26, '2019-02-08', 8, 146, 1, 'dat146logcat.txt', 'dat146server.txt'),
(48, 1, '', '', 23, '2019-02-08', 8, 66, 1, 'dat66logcat.txt', 'dat66server.txt'),
(49, 1, '', '', 60, '2019-02-08', 8, 39, 1, 'dat39logcat.txt', 'dat39server.txt'),
(50, 1, '', '', 21, '2019-02-08', 8, 115, 1, 'dat115logcat.txt', 'dat115server.txt'),
(51, 1, '', '', 44, '2019-02-08', 8, 2, 1, 'dat2logcat.txt', 'dat2server.txt'),
(52, 1, '', '', 19, '2019-02-08', 8, 15, 1, 'dat15logcat.txt', 'dat15server.txt'),
(53, 1, '', '', 37, '2019-02-08', 8, 82, 1, 'dat82logcat.txt', 'dat82server.txt'),
(54, 1, '', '', 13, '2019-02-08', 8, 27, 1, 'dat27logcat.txt', 'dat27server.txt'),
(55, 1, '', '', 12, '2019-02-08', 8, 11, 1, 'dat11logcat.txt', 'dat11server.txt'),
(56, 1, '', '', 94, '2019-02-08', 8, 109, 1, 'dat109logcat.txt', 'dat109server.txt'),
(57, 1, '', '', 131, '2019-02-08', 8, 42, 1, 'dat42logcat.txt', 'dat42server.txt'),
(58, 1, '', '', 62, '2019-02-08', 8, 12, 1, 'dat12logcat.txt', 'dat12server.txt'),
(59, 1, '', '', 25, '2019-02-08', 8, 63, 1, 'dat63logcat.txt', 'dat63server.txt'),
(60, 1, '', '', 81, '2019-02-08', 8, 120, 1, 'dat120logcat.txt', 'dat120server.txt'),
(61, 1, '', '', 34, '2019-02-08', 9, 45, 1, 'dat45logcat.txt', 'dat45server.txt'),
(62, 1, '', '', 28, '2019-02-08', 9, 13, 1, 'dat13logcat.txt', 'dat13server.txt'),
(63, 1, '', '', 59, '2019-02-08', 9, 14, 1, 'dat14logcat.txt', 'dat14server.txt'),
(64, 1, '', '', 64, '2019-02-08', 9, 28, 1, 'dat28logcat.txt', 'dat28server.txt'),
(65, 1, '', '', 71, '2019-02-08', 9, 16, 1, 'dat16logcat.txt', 'dat16server.txt'),
(66, 1, '', '', 7, '2019-02-08', 9, 7, 1, 'dat7logcat.txt', 'dat7server.txt'),
(67, 1, '', '', 27, '2019-02-08', 9, 146, 1, 'dat146logcat.txt', 'dat146server.txt'),
(68, 1, '', '', 24, '2019-02-08', 9, 66, 1, 'dat66logcat.txt', 'dat66server.txt'),
(69, 1, '', '', 61, '2019-02-08', 9, 39, 1, 'dat39logcat.txt', 'dat39server.txt'),
(70, 1, '', '', 21, '2019-02-08', 9, 115, 1, 'dat115logcat.txt', 'dat115server.txt'),
(71, 1, '', '', 44, '2019-02-08', 9, 2, 1, 'dat2logcat.txt', 'dat2server.txt'),
(72, 1, '', '', 19, '2019-02-08', 9, 15, 1, 'dat15logcat.txt', 'dat15server.txt'),
(73, 1, '', '', 37, '2019-02-08', 9, 82, 1, 'dat82logcat.txt', 'dat82server.txt'),
(74, 1, '', '', 14, '2019-02-08', 9, 27, 1, 'dat27logcat.txt', 'dat27server.txt'),
(75, 1, '', '', 12, '2019-02-08', 9, 11, 1, 'dat11logcat.txt', 'dat11server.txt'),
(76, 1, '', '', 96, '2019-02-08', 9, 109, 1, 'dat109logcat.txt', 'dat109server.txt'),
(77, 1, '', '', 132, '2019-02-08', 9, 42, 1, 'dat42logcat.txt', 'dat42server.txt'),
(78, 1, '', '', 61, '2019-02-08', 9, 12, 1, 'dat12logcat.txt', 'dat12server.txt'),
(79, 1, '', '', 24, '2019-02-08', 9, 63, 1, 'dat63logcat.txt', 'dat63server.txt'),
(80, 1, '', '', 82, '2019-02-08', 9, 120, 1, 'dat120logcat.txt', 'dat120server.txt');

-- --------------------------------------------------------

--
-- Table structure for table `reports_testresults`
--

CREATE TABLE `reports_testresults` (
  `id` int(11) NOT NULL,
  `test_result` tinyint(1) NOT NULL COMMENT '1:pass,0:fail',
  `screen_generated` longtext NOT NULL,
  `screen_diff` longtext NOT NULL,
  `executed` datetime(6) NOT NULL,
  `start_time` datetime(6) NOT NULL,
  `end_time` datetime(6) NOT NULL,
  `device_id` int(11) NOT NULL,
  `test_build_id` int(11) DEFAULT NULL,
  `test_name_id` int(11) NOT NULL,
  `test_step_id` int(11) NOT NULL,
  `test_errors` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports_testresults`
--

INSERT INTO `reports_testresults` (`id`, `test_result`, `screen_generated`, `screen_diff`, `executed`, `start_time`, `end_time`, `device_id`, `test_build_id`, `test_name_id`, `test_step_id`, `test_errors`) VALUES
(1, 1, '', '', '2019-02-08 08:55:26.000000', '2019-02-08 04:45:02.000000', '2019-02-08 04:45:05.000000', 1, 6, 45, 199, ''),
(2, 1, '2019-02-08_SamsungS9_8.0.0_test_consolidation/room_preview_layout_dat44.png', '2019-02-08_SamsungS9_8.0.0_test_consolidation/room_preview_layout_dat44Modified.png', '2019-02-08 08:55:26.000000', '2019-02-08 04:45:22.000000', '2019-02-08 04:45:36.000000', 1, 6, 45, 201, ''),
(3, 1, '', '', '2019-02-08 08:55:26.000000', '2019-02-08 04:45:05.000000', '2019-02-08 04:45:22.000000', 1, 6, 45, 200, ''),
(4, 1, '', '', '2019-02-08 08:55:26.000000', '2019-02-08 04:45:36.000000', '2019-02-08 04:45:41.000000', 1, 6, 45, 202, ''),
(5, 1, '', '', '2019-02-08 08:55:26.000000', '2019-02-08 04:37:41.000000', '2019-02-08 04:37:45.000000', 1, 6, 13, 41, ''),
(6, 1, '', '', '2019-02-08 08:55:26.000000', '2019-02-08 04:37:51.000000', '2019-02-08 04:37:57.000000', 1, 6, 13, 43, ''),
(7, 1, '', '', '2019-02-08 08:55:26.000000', '2019-02-08 04:37:45.000000', '2019-02-08 04:37:51.000000', 1, 6, 13, 42, ''),
(8, 1, '2019-02-08_SamsungS9_8.0.0_test_consolidation/verify_create_room_layout_dat13.png', '2019-02-08_SamsungS9_8.0.0_test_consolidation/verify_create_room_layout_dat13Modified.png', '2019-02-08 08:55:26.000000', '2019-02-08 04:37:57.000000', '2019-02-08 04:38:01.000000', 1, 6, 13, 45, ''),
(9, 1, '', '', '2019-02-08 08:55:26.000000', '2019-02-08 04:37:51.000000', '2019-02-08 04:37:57.000000', 1, 6, 13, 44, ''),
(10, 1, '', '', '2019-02-08 08:55:27.000000', '2019-02-08 04:37:57.000000', '2019-02-08 04:38:01.000000', 1, 6, 13, 46, ''),
(11, 1, '', '', '2019-02-08 08:55:27.000000', '2019-02-08 04:38:01.000000', '2019-02-08 04:38:05.000000', 1, 6, 14, 47, ''),
(12, 1, '2019-02-08_SamsungS9_8.0.0_test_consolidation/verify_create_room_page_dat15.png', '2019-02-08_SamsungS9_8.0.0_test_consolidation/verify_create_room_page_dat15Modified.png', '2019-02-08 08:55:27.000000', '2019-02-08 04:38:16.000000', '2019-02-08 04:38:22.000000', 1, 6, 14, 49, ''),
(13, 1, '', '', '2019-02-08 08:55:27.000000', '2019-02-08 04:38:05.000000', '2019-02-08 04:38:16.000000', 1, 6, 14, 48, ''),
(14, 1, '2019-02-08_SamsungS9_8.0.0_test_consolidation/verify_new_room_created_dat15.png', '2019-02-08_SamsungS9_8.0.0_test_consolidation/verify_new_room_created_dat15Modified.png', '2019-02-08 08:55:27.000000', '2019-02-08 04:38:22.000000', '2019-02-08 04:38:40.000000', 1, 6, 14, 51, ''),
(15, 1, '', '', '2019-02-08 08:55:27.000000', '2019-02-08 04:38:16.000000', '2019-02-08 04:38:22.000000', 1, 6, 14, 50, ''),
(16, 1, '', '', '2019-02-08 08:55:27.000000', '2019-02-08 04:38:22.000000', '2019-02-08 04:38:40.000000', 1, 6, 14, 52, ''),
(17, 1, '', '', '2019-02-08 08:55:27.000000', '2019-02-08 04:40:01.000000', '2019-02-08 04:40:05.000000', 1, 6, 28, 117, ''),
(18, 1, '', '', '2019-02-08 08:55:27.000000', '2019-02-08 04:40:11.000000', '2019-02-08 04:40:27.000000', 1, 6, 28, 119, ''),
(19, 1, '', '', '2019-02-08 08:55:27.000000', '2019-02-08 04:40:05.000000', '2019-02-08 04:40:11.000000', 1, 6, 28, 118, ''),
(20, 1, '2019-02-08_SamsungS9_8.0.0_test_consolidation/search_preview_layout_dat29.png', '2019-02-08_SamsungS9_8.0.0_test_consolidation/search_preview_layout_dat29Modified.png', '2019-02-08 08:55:27.000000', '2019-02-08 04:40:27.000000', '2019-02-08 04:40:29.000000', 1, 6, 28, 121, ''),
(21, 1, '', '', '2019-02-08 08:55:27.000000', '2019-02-08 04:40:11.000000', '2019-02-08 04:40:27.000000', 1, 6, 28, 120, ''),
(22, 1, '', '', '2019-02-08 08:55:27.000000', '2019-02-08 04:40:29.000000', '2019-02-08 04:40:35.000000', 1, 6, 28, 123, ''),
(23, 1, '', '', '2019-02-08 08:55:27.000000', '2019-02-08 04:40:27.000000', '2019-02-08 04:40:29.000000', 1, 6, 28, 122, ''),
(24, 1, '', '', '2019-02-08 08:55:27.000000', '2019-02-08 04:40:45.000000', '2019-02-08 04:40:51.000000', 1, 6, 28, 125, ''),
(25, 1, '2019-02-08_SamsungS9_8.0.0_test_consolidation/room_preview_layout_dat29.png', '2019-02-08_SamsungS9_8.0.0_test_consolidation/room_preview_layout_dat29Modified.png', '2019-02-08 08:55:27.000000', '2019-02-08 04:40:35.000000', '2019-02-08 04:40:45.000000', 1, 6, 28, 124, ''),
(26, 1, '', '', '2019-02-08 08:55:27.000000', '2019-02-08 04:39:01.000000', '2019-02-08 04:39:04.000000', 1, 6, 16, 57, ''),
(27, 1, '', '', '2019-02-08 08:55:27.000000', '2019-02-08 04:39:20.000000', '2019-02-08 04:39:26.000000', 1, 6, 16, 59, ''),
(28, 1, '2019-02-08_SamsungS9_8.0.0_test_consolidation/verify_browse_rooms_layout_dat16.png', '2019-02-08_SamsungS9_8.0.0_test_consolidation/verify_browse_rooms_layout_dat16Modified.png', '2019-02-08 08:55:27.000000', '2019-02-08 04:39:04.000000', '2019-02-08 04:39:20.000000', 1, 6, 16, 58, ''),
(29, 1, '', '', '2019-02-08 08:55:27.000000', '2019-02-08 04:39:26.000000', '2019-02-08 04:39:42.000000', 1, 6, 16, 61, ''),
(30, 1, '', '', '2019-02-08 08:55:27.000000', '2019-02-08 04:39:20.000000', '2019-02-08 04:39:26.000000', 1, 6, 16, 60, ''),
(31, 1, '', '', '2019-02-08 08:55:27.000000', '2019-02-08 04:39:42.000000', '2019-02-08 04:39:50.000000', 1, 6, 16, 63, ''),
(32, 1, '', '', '2019-02-08 08:55:27.000000', '2019-02-08 04:39:26.000000', '2019-02-08 04:39:42.000000', 1, 6, 16, 62, ''),
(33, 1, '', '', '2019-02-08 08:55:27.000000', '2019-02-08 04:36:11.000000', '2019-02-08 04:36:14.000000', 1, 6, 7, 20, ''),
(34, 1, '2019-02-08_SamsungS9_8.0.0_test_consolidation/world_view_layout_dat7.png', '2019-02-08_SamsungS9_8.0.0_test_consolidation/world_view_layout_dat7Modified.png', '2019-02-08 08:55:28.000000', '2019-02-08 04:36:14.000000', '2019-02-08 04:36:18.000000', 1, 6, 7, 21, 'Badge count is not found'),
(35, 1, '2019-02-08_SamsungS9_8.0.0_test_consolidation/verify_flat_dm_page_screen.png', '2019-02-08_SamsungS9_8.0.0_test_consolidation/verify_flat_dm_page_screenModified.png', '2019-02-08 08:55:28.000000', '2019-02-08 04:50:46.000000', '2019-02-08 04:51:15.000000', 1, 6, 146, 565, ''),
(36, 1, '', '', '2019-02-08 08:55:28.000000', '2019-02-08 04:51:15.000000', '2019-02-08 04:51:15.000000', 1, 6, 146, 567, ''),
(37, 1, '', '', '2019-02-08 08:55:28.000000', '2019-02-08 04:51:15.000000', '2019-02-08 04:51:15.000000', 1, 6, 146, 566, ''),
(38, 1, '', '', '2019-02-08 08:55:28.000000', '2019-02-08 04:51:15.000000', '2019-02-08 04:51:16.000000', 1, 6, 146, 569, ''),
(39, 1, '', '', '2019-02-08 08:55:28.000000', '2019-02-08 04:51:15.000000', '2019-02-08 04:51:15.000000', 1, 6, 146, 568, ''),
(40, 1, '', '', '2019-02-08 08:55:28.000000', '2019-02-08 04:51:15.000000', '2019-02-08 04:51:16.000000', 1, 6, 146, 570, ''),
(41, 1, '', '', '2019-02-08 08:55:28.000000', '2019-02-08 04:46:13.000000', '2019-02-08 04:46:17.000000', 1, 6, 66, 286, ''),
(42, 1, '2019-02-08_SamsungS9_8.0.0_test_consolidation/topic_preview_layout_dat64.png', '2019-02-08_SamsungS9_8.0.0_test_consolidation/topic_preview_layout_dat64Modified.png', '2019-02-08 08:55:28.000000', '2019-02-08 04:46:35.000000', '2019-02-08 04:46:43.000000', 1, 6, 66, 288, ''),
(43, 1, '', '', '2019-02-08 08:55:28.000000', '2019-02-08 04:46:19.000000', '2019-02-08 04:46:35.000000', 1, 6, 66, 287, ''),
(44, 1, '', '', '2019-02-08 08:55:28.000000', '2019-02-08 04:46:43.000000', '2019-02-08 04:46:44.000000', 1, 6, 66, 289, ''),
(45, 1, '', '', '2019-02-08 08:55:28.000000', '2019-02-08 04:40:51.000000', '2019-02-08 04:40:54.000000', 1, 6, 39, 171, ''),
(46, 1, '2019-02-08_SamsungS9_8.0.0_test_consolidation/room_preview_layout_dat38.png', '2019-02-08_SamsungS9_8.0.0_test_consolidation/room_preview_layout_dat38Modified.png', '2019-02-08 08:55:28.000000', '2019-02-08 04:41:21.000000', '2019-02-08 04:41:25.000000', 1, 6, 39, 173, ''),
(47, 1, '', '', '2019-02-08 08:55:28.000000', '2019-02-08 04:40:54.000000', '2019-02-08 04:41:21.000000', 1, 6, 39, 172, ''),
(48, 1, '', '', '2019-02-08 08:55:28.000000', '2019-02-08 04:41:31.000000', '2019-02-08 04:41:36.000000', 1, 6, 39, 175, ''),
(49, 1, '', '', '2019-02-08 08:55:28.000000', '2019-02-08 04:41:25.000000', '2019-02-08 04:41:31.000000', 1, 6, 39, 174, ''),
(50, 1, '', '', '2019-02-08 08:55:28.000000', '2019-02-08 04:41:44.000000', '2019-02-08 04:41:50.000000', 1, 6, 39, 177, ''),
(51, 1, '', '', '2019-02-08 08:55:28.000000', '2019-02-08 04:41:36.000000', '2019-02-08 04:41:42.000000', 1, 6, 39, 176, ''),
(52, 1, '', '', '2019-02-08 08:55:28.000000', '2019-02-08 04:41:50.000000', '2019-02-08 04:41:56.000000', 1, 6, 39, 178, ''),
(53, 1, '', '', '2019-02-08 08:55:28.000000', '2019-02-08 04:48:54.000000', '2019-02-08 04:48:57.000000', 1, 6, 115, 476, ''),
(54, 1, '2019-02-08_SamsungS9_8.0.0_test_consolidation/verify_find_people_screen.png', '2019-02-08_SamsungS9_8.0.0_test_consolidation/verify_find_people_screenModified.png', '2019-02-08 08:55:28.000000', '2019-02-08 04:49:06.000000', '2019-02-08 04:49:11.000000', 1, 6, 115, 478, ''),
(55, 1, '', '', '2019-02-08 08:55:29.000000', '2019-02-08 04:48:57.000000', '2019-02-08 04:49:04.000000', 1, 6, 115, 477, ''),
(56, 1, '2019-02-08_SamsungS9_8.0.0_test_consolidation/verify_add_bot_screen.png', '2019-02-08_SamsungS9_8.0.0_test_consolidation/verify_add_bot_screenModified.png', '2019-02-08 08:55:29.000000', '2019-02-08 04:49:11.000000', '2019-02-08 04:49:19.000000', 1, 6, 115, 479, ''),
(57, 1, '2019-02-08_SamsungS9_8.0.0_test_consolidation/onboard_get_started_dat2.png', '2019-02-08_SamsungS9_8.0.0_test_consolidation/onboard_get_started_dat2Modified.png', '2019-02-08 08:55:29.000000', '2019-02-08 10:14:20.000000', '2019-02-08 10:14:24.000000', 1, 6, 2, 3, 'Choose account text is missing'),
(58, 1, '', '', '2019-02-08 08:55:29.000000', '2019-02-08 10:14:30.000000', '2019-02-08 10:15:01.000000', 1, 6, 2, 5, ''),
(59, 1, '', '', '2019-02-08 08:55:29.000000', '2019-02-08 10:14:24.000000', '2019-02-08 10:14:30.000000', 1, 6, 2, 4, ''),
(60, 1, '', '', '2019-02-08 08:55:29.000000', '2019-02-08 05:25:27.000000', '2019-02-08 05:25:30.000000', 1, 6, 15, 53, ''),
(61, 1, '2019-02-08_SamsungS9_8.0.0_test_dat15/verify_browse_rooms_page_dat160.png', '2019-02-08_SamsungS9_8.0.0_test_dat15/verify_browse_rooms_page_dat160Modified.png', '2019-02-08 08:55:29.000000', '2019-02-08 05:25:34.000000', '2019-02-08 05:25:44.000000', 1, 6, 15, 55, ''),
(62, 1, '', '', '2019-02-08 08:55:29.000000', '2019-02-08 05:25:30.000000', '2019-02-08 05:25:34.000000', 1, 6, 15, 54, ''),
(63, 1, '', '', '2019-02-08 08:55:29.000000', '2019-02-08 05:25:44.000000', '2019-02-08 05:25:44.000000', 1, 6, 15, 56, ''),
(64, 1, '', '', '2019-02-08 08:55:29.000000', '2019-02-08 04:46:45.000000', '2019-02-08 04:47:10.000000', 1, 6, 82, 374, 'App is not launched'),
(65, 1, '', '', '2019-02-08 08:55:29.000000', '2019-02-08 04:47:20.000000', '2019-02-08 04:47:20.000000', 1, 6, 82, 376, ''),
(66, 1, '2019-02-08_SamsungS9_8.0.0_test_consolidation/search_bar_layout_DAT78.png', '2019-02-08_SamsungS9_8.0.0_test_consolidation/search_bar_layout_DAT78Modified.png', '2019-02-08 08:55:29.000000', '2019-02-08 04:47:10.000000', '2019-02-08 04:47:20.000000', 1, 6, 82, 375, ''),
(67, 1, '', '', '2019-02-08 08:55:29.000000', '2019-02-08 04:47:20.000000', '2019-02-08 04:47:25.000000', 1, 6, 82, 377, ''),
(68, 1, '', '', '2019-02-08 08:55:29.000000', '2019-02-08 04:39:50.000000', '2019-02-08 04:39:53.000000', 1, 6, 27, 112, ''),
(69, 1, '', '', '2019-02-08 08:55:29.000000', '2019-02-08 04:39:59.000000', '2019-02-08 04:39:59.000000', 1, 6, 27, 114, ''),
(70, 1, '', '', '2019-02-08 08:55:29.000000', '2019-02-08 04:39:53.000000', '2019-02-08 04:39:59.000000', 1, 6, 27, 113, ''),
(71, 1, '', '', '2019-02-08 08:55:29.000000', '2019-02-08 04:39:59.000000', '2019-02-08 04:40:01.000000', 1, 6, 27, 116, ''),
(72, 1, '2019-02-08_SamsungS9_8.0.0_test_consolidation/room_preview_layout_dat28.png', '2019-02-08_SamsungS9_8.0.0_test_consolidation/room_preview_layout_dat28Modified.png', '2019-02-08 08:55:29.000000', '2019-02-08 04:39:59.000000', '2019-02-08 04:40:01.000000', 1, 6, 27, 115, ''),
(73, 1, '', '', '2019-02-08 08:55:29.000000', '2019-02-08 04:36:18.000000', '2019-02-08 04:36:22.000000', 1, 6, 11, 33, ''),
(74, 1, '2019-02-08_SamsungS9_8.0.0_test_consolidation/verify_FAB_screen_dat11.png', '2019-02-08_SamsungS9_8.0.0_test_consolidation/verify_FAB_screen_dat11Modified.png', '2019-02-08 08:55:29.000000', '2019-02-08 04:36:27.000000', '2019-02-08 04:36:30.000000', 1, 6, 11, 35, ''),
(75, 1, '', '', '2019-02-08 08:55:29.000000', '2019-02-08 04:36:22.000000', '2019-02-08 04:36:27.000000', 1, 6, 11, 34, ''),
(76, 1, '', '', '2019-02-08 08:55:29.000000', '2019-02-08 04:48:41.000000', '2019-02-08 04:48:53.000000', 1, 6, 109, 458, ''),
(77, 1, '', '', '2019-02-08 08:55:30.000000', '2019-02-08 04:47:26.000000', '2019-02-08 04:47:29.000000', 1, 6, 109, 449, ''),
(78, 1, '', '', '2019-02-08 08:55:30.000000', '2019-02-08 04:47:48.000000', '2019-02-08 04:48:14.000000', 1, 6, 109, 451, ''),
(79, 1, '', '', '2019-02-08 08:55:30.000000', '2019-02-08 04:47:32.000000', '2019-02-08 04:47:48.000000', 1, 6, 109, 450, ''),
(80, 1, '2019-02-08_SamsungS9_8.0.0_test_consolidation/delete_press_layout_dat105.png', '2019-02-08_SamsungS9_8.0.0_test_consolidation/delete_press_layout_dat105Modified.png', '2019-02-08 08:55:30.000000', '2019-02-08 04:48:23.000000', '2019-02-08 04:48:30.000000', 1, 6, 109, 453, ''),
(81, 1, '2019-02-08_SamsungS9_8.0.0_test_consolidation/long_press_layout_dat105.png', '2019-02-08_SamsungS9_8.0.0_test_consolidation/long_press_layout_dat105Modified.png', '2019-02-08 08:55:30.000000', '2019-02-08 04:48:14.000000', '2019-02-08 04:48:23.000000', 1, 6, 109, 452, ''),
(82, 1, '', '', '2019-02-08 08:55:30.000000', '2019-02-08 04:48:30.000000', '2019-02-08 04:48:36.000000', 1, 6, 109, 455, ''),
(83, 1, '', '', '2019-02-08 08:55:30.000000', '2019-02-08 04:48:23.000000', '2019-02-08 04:48:30.000000', 1, 6, 109, 454, ''),
(84, 1, '', '', '2019-02-08 08:55:30.000000', '2019-02-08 04:48:36.000000', '2019-02-08 04:48:41.000000', 1, 6, 109, 457, ''),
(85, 1, '', '', '2019-02-08 08:55:30.000000', '2019-02-08 04:48:36.000000', '2019-02-08 04:48:41.000000', 1, 6, 109, 456, ''),
(86, 1, '', '', '2019-02-08 08:55:30.000000', '2019-02-08 04:41:57.000000', '2019-02-08 04:42:52.000000', 1, 6, 42, 189, 'set_message_41 is missing'),
(87, 1, '2019-02-08_SamsungS9_8.0.0_test_consolidation/room_preview_layout_dat41.png', '2019-02-08_SamsungS9_8.0.0_test_consolidation/room_preview_layout_dat41Modified.png', '2019-02-08 08:55:30.000000', '2019-02-08 04:43:38.000000', '2019-02-08 04:44:19.000000', 1, 6, 42, 191, ''),
(88, 1, '', '', '2019-02-08 08:55:30.000000', '2019-02-08 04:42:52.000000', '2019-02-08 04:43:38.000000', 1, 6, 42, 190, ''),
(89, 1, '', '', '2019-02-08 08:55:30.000000', '2019-02-08 04:36:30.000000', '2019-02-08 04:36:34.000000', 1, 6, 12, 36, ''),
(90, 1, '', '', '2019-02-08 08:55:30.000000', '2019-02-08 04:36:52.000000', '2019-02-08 04:36:58.000000', 1, 6, 12, 38, ''),
(91, 1, '2019-02-08_SamsungS9_8.0.0_test_consolidation/verify_test_keyword_filter_dat12.png', '2019-02-08_SamsungS9_8.0.0_test_consolidation/verify_test_keyword_filter_dat12Modified.png', '2019-02-08 08:55:30.000000', '2019-02-08 04:36:34.000000', '2019-02-08 04:36:52.000000', 1, 6, 12, 37, ''),
(92, 1, '2019-02-08_SamsungS9_8.0.0_test_consolidation/verify_selected_room_screen_dat12.png', '2019-02-08_SamsungS9_8.0.0_test_consolidation/verify_selected_room_screen_dat12Modified.png', '2019-02-08 08:55:30.000000', '2019-02-08 04:37:16.000000', '2019-02-08 04:37:41.000000', 1, 6, 12, 40, ''),
(93, 1, '2019-02-08_SamsungS9_8.0.0_test_consolidation/verify_room_keyword_filter_dat12.png', '2019-02-08_SamsungS9_8.0.0_test_consolidation/verify_room_keyword_filter_dat12Modified.png', '2019-02-08 08:55:30.000000', '2019-02-08 04:36:58.000000', '2019-02-08 04:37:16.000000', 1, 6, 12, 39, ''),
(94, 1, '', '', '2019-02-08 08:55:30.000000', '2019-02-08 04:45:42.000000', '2019-02-08 04:45:45.000000', 1, 6, 63, 278, ''),
(95, 1, '2019-02-08_SamsungS9_8.0.0_test_consolidation/room_preview_layout_dat61.png', '2019-02-08_SamsungS9_8.0.0_test_consolidation/room_preview_layout_dat61Modified.png', '2019-02-08 08:55:30.000000', '2019-02-08 04:46:03.000000', '2019-02-08 04:46:11.000000', 1, 6, 63, 280, ''),
(96, 1, '', '', '2019-02-08 08:55:30.000000', '2019-02-08 04:45:48.000000', '2019-02-08 04:46:03.000000', 1, 6, 63, 279, ''),
(97, 1, '', '', '2019-02-08 08:55:30.000000', '2019-02-08 04:46:11.000000', '2019-02-08 04:46:12.000000', 1, 6, 63, 281, ''),
(98, 1, '', '', '2019-02-08 08:55:30.000000', '2019-02-08 04:49:19.000000', '2019-02-08 04:49:23.000000', 1, 6, 120, 508, ''),
(99, 1, '', '', '2019-02-08 08:55:30.000000', '2019-02-08 04:49:54.000000', '2019-02-08 04:50:05.000000', 1, 6, 120, 510, ''),
(100, 1, '', '', '2019-02-08 08:55:30.000000', '2019-02-08 04:49:23.000000', '2019-02-08 04:49:54.000000', 1, 6, 120, 509, ''),
(101, 1, '', '', '2019-02-08 08:55:31.000000', '2019-02-08 04:50:16.000000', '2019-02-08 04:50:23.000000', 1, 6, 120, 512, ''),
(102, 1, '', '', '2019-02-08 08:55:31.000000', '2019-02-08 04:50:05.000000', '2019-02-08 04:50:16.000000', 1, 6, 120, 511, ''),
(103, 1, '2019-02-08_SamsungS9_8.0.0_test_consolidation/verify_sharing_screen.png', '2019-02-08_SamsungS9_8.0.0_test_consolidation/verify_sharing_screenModified.png', '2019-02-08 08:55:31.000000', '2019-02-08 04:50:29.000000', '2019-02-08 04:50:37.000000', 1, 6, 120, 514, ''),
(104, 1, '', '', '2019-02-08 08:55:31.000000', '2019-02-08 04:50:23.000000', '2019-02-08 04:50:29.000000', 1, 6, 120, 513, ''),
(105, 1, '2019-02-08_SamsungS9_8.0.0_test_consolidation/verify_sharing_world_view_screen.png', '2019-02-08_SamsungS9_8.0.0_test_consolidation/verify_sharing_world_view_screenModified.png', '2019-02-08 08:55:31.000000', '2019-02-08 04:50:37.000000', '2019-02-08 04:50:44.000000', 1, 6, 120, 515, ''),
(106, 1, '', '', '2019-02-08 09:09:02.000000', '2019-02-08 08:29:22.000000', '2019-02-08 08:29:25.000000', 1, 7, 45, 199, ''),
(107, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat44.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat44Modified.png', '2019-02-08 09:09:02.000000', '2019-02-08 08:29:40.000000', '2019-02-08 08:29:52.000000', 1, 7, 45, 201, ''),
(108, 1, '', '', '2019-02-08 09:09:02.000000', '2019-02-08 08:29:25.000000', '2019-02-08 08:29:40.000000', 1, 7, 45, 200, ''),
(109, 1, '', '', '2019-02-08 09:09:02.000000', '2019-02-08 08:29:52.000000', '2019-02-08 08:29:56.000000', 1, 7, 45, 202, ''),
(110, 1, '', '', '2019-02-08 09:09:02.000000', '2019-02-08 08:22:20.000000', '2019-02-08 08:22:23.000000', 1, 7, 13, 41, ''),
(111, 1, '', '', '2019-02-08 09:09:02.000000', '2019-02-08 08:22:29.000000', '2019-02-08 08:22:34.000000', 1, 7, 13, 43, ''),
(112, 1, '', '', '2019-02-08 09:09:02.000000', '2019-02-08 08:22:23.000000', '2019-02-08 08:22:29.000000', 1, 7, 13, 42, ''),
(113, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_create_room_layout_dat13.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_create_room_layout_dat13Modified.png', '2019-02-08 09:09:02.000000', '2019-02-08 08:22:34.000000', '2019-02-08 08:22:38.000000', 1, 7, 13, 45, ''),
(114, 1, '', '', '2019-02-08 09:09:02.000000', '2019-02-08 08:22:29.000000', '2019-02-08 08:22:34.000000', 1, 7, 13, 44, ''),
(115, 1, '', '', '2019-02-08 09:09:03.000000', '2019-02-08 08:22:34.000000', '2019-02-08 08:22:38.000000', 1, 7, 13, 46, ''),
(116, 1, '', '', '2019-02-08 09:09:03.000000', '2019-02-08 08:22:38.000000', '2019-02-08 08:22:41.000000', 1, 7, 14, 47, ''),
(117, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_create_room_page_dat15.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_create_room_page_dat15Modified.png', '2019-02-08 09:09:03.000000', '2019-02-08 08:22:53.000000', '2019-02-08 08:22:59.000000', 1, 7, 14, 49, ''),
(118, 1, '', '', '2019-02-08 09:09:03.000000', '2019-02-08 08:22:41.000000', '2019-02-08 08:22:53.000000', 1, 7, 14, 48, ''),
(119, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_new_room_created_dat15.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_new_room_created_dat15Modified.png', '2019-02-08 09:09:03.000000', '2019-02-08 08:22:59.000000', '2019-02-08 08:23:15.000000', 1, 7, 14, 51, ''),
(120, 1, '', '', '2019-02-08 09:09:03.000000', '2019-02-08 08:22:53.000000', '2019-02-08 08:22:59.000000', 1, 7, 14, 50, ''),
(121, 1, '', '', '2019-02-08 09:09:03.000000', '2019-02-08 08:22:59.000000', '2019-02-08 08:23:15.000000', 1, 7, 14, 52, ''),
(122, 1, '', '', '2019-02-08 09:09:03.000000', '2019-02-08 08:24:35.000000', '2019-02-08 08:24:38.000000', 1, 7, 28, 117, ''),
(123, 1, '', '', '2019-02-08 09:09:03.000000', '2019-02-08 08:24:44.000000', '2019-02-08 08:24:58.000000', 1, 7, 28, 119, ''),
(124, 1, '', '', '2019-02-08 09:09:03.000000', '2019-02-08 08:24:38.000000', '2019-02-08 08:24:44.000000', 1, 7, 28, 118, ''),
(125, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/search_preview_layout_dat29.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/search_preview_layout_dat29Modified.png', '2019-02-08 09:09:03.000000', '2019-02-08 08:24:58.000000', '2019-02-08 08:25:00.000000', 1, 7, 28, 121, ''),
(126, 1, '', '', '2019-02-08 09:09:03.000000', '2019-02-08 08:24:44.000000', '2019-02-08 08:24:58.000000', 1, 7, 28, 120, ''),
(127, 1, '', '', '2019-02-08 09:09:03.000000', '2019-02-08 08:25:00.000000', '2019-02-08 08:25:06.000000', 1, 7, 28, 123, ''),
(128, 1, '', '', '2019-02-08 09:09:03.000000', '2019-02-08 08:24:58.000000', '2019-02-08 08:25:00.000000', 1, 7, 28, 122, ''),
(129, 1, '', '', '2019-02-08 09:09:03.000000', '2019-02-08 08:25:15.000000', '2019-02-08 08:25:21.000000', 1, 7, 28, 125, ''),
(130, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat29.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat29Modified.png', '2019-02-08 09:09:03.000000', '2019-02-08 08:25:06.000000', '2019-02-08 08:25:15.000000', 1, 7, 28, 124, ''),
(131, 1, '', '', '2019-02-08 09:09:03.000000', '2019-02-08 08:23:34.000000', '2019-02-08 08:23:37.000000', 1, 7, 16, 57, ''),
(132, 1, '', '', '2019-02-08 09:09:03.000000', '2019-02-08 08:23:54.000000', '2019-02-08 08:24:00.000000', 1, 7, 16, 59, ''),
(133, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_browse_rooms_layout_dat16.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_browse_rooms_layout_dat16Modified.png', '2019-02-08 09:09:03.000000', '2019-02-08 08:23:37.000000', '2019-02-08 08:23:54.000000', 1, 7, 16, 58, ''),
(134, 1, '', '', '2019-02-08 09:09:04.000000', '2019-02-08 08:24:00.000000', '2019-02-08 08:24:15.000000', 1, 7, 16, 61, ''),
(135, 1, '', '', '2019-02-08 09:09:04.000000', '2019-02-08 08:23:54.000000', '2019-02-08 08:24:00.000000', 1, 7, 16, 60, ''),
(136, 1, '', '', '2019-02-08 09:09:04.000000', '2019-02-08 08:24:15.000000', '2019-02-08 08:24:23.000000', 1, 7, 16, 63, ''),
(137, 1, '', '', '2019-02-08 09:09:04.000000', '2019-02-08 08:24:00.000000', '2019-02-08 08:24:15.000000', 1, 7, 16, 62, ''),
(138, 1, '', '', '2019-02-08 09:09:04.000000', '2019-02-08 08:20:59.000000', '2019-02-08 08:21:02.000000', 1, 7, 7, 20, ''),
(139, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/world_view_layout_dat7.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/world_view_layout_dat7Modified.png', '2019-02-08 09:09:04.000000', '2019-02-08 08:21:02.000000', '2019-02-08 08:21:06.000000', 1, 7, 7, 21, 'Badge count is not found'),
(140, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_flat_dm_page_screen.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_flat_dm_page_screenModified.png', '2019-02-08 09:09:04.000000', '2019-02-08 08:34:45.000000', '2019-02-08 08:35:11.000000', 1, 7, 146, 565, ''),
(141, 1, '', '', '2019-02-08 09:09:04.000000', '2019-02-08 08:35:11.000000', '2019-02-08 08:35:11.000000', 1, 7, 146, 567, ''),
(142, 1, '', '', '2019-02-08 09:09:04.000000', '2019-02-08 08:35:11.000000', '2019-02-08 08:35:11.000000', 1, 7, 146, 566, ''),
(143, 1, '', '', '2019-02-08 09:09:04.000000', '2019-02-08 08:35:11.000000', '2019-02-08 08:35:11.000000', 1, 7, 146, 569, ''),
(144, 1, '', '', '2019-02-08 09:09:04.000000', '2019-02-08 08:35:11.000000', '2019-02-08 08:35:11.000000', 1, 7, 146, 568, ''),
(145, 1, '', '', '2019-02-08 09:09:04.000000', '2019-02-08 08:35:11.000000', '2019-02-08 08:35:11.000000', 1, 7, 146, 570, ''),
(146, 1, '', '', '2019-02-08 09:09:04.000000', '2019-02-08 08:30:27.000000', '2019-02-08 08:30:30.000000', 1, 7, 66, 286, ''),
(147, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/topic_preview_layout_dat64.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/topic_preview_layout_dat64Modified.png', '2019-02-08 09:09:04.000000', '2019-02-08 08:30:45.000000', '2019-02-08 08:30:53.000000', 1, 7, 66, 288, ''),
(148, 1, '', '', '2019-02-08 09:09:04.000000', '2019-02-08 08:30:32.000000', '2019-02-08 08:30:45.000000', 1, 7, 66, 287, ''),
(149, 1, '', '', '2019-02-08 09:09:04.000000', '2019-02-08 08:30:53.000000', '2019-02-08 08:30:53.000000', 1, 7, 66, 289, ''),
(150, 1, '', '', '2019-02-08 09:09:04.000000', '2019-02-08 08:25:21.000000', '2019-02-08 08:25:24.000000', 1, 7, 39, 171, ''),
(151, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat38.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat38Modified.png', '2019-02-08 09:09:04.000000', '2019-02-08 08:25:49.000000', '2019-02-08 08:25:53.000000', 1, 7, 39, 173, ''),
(152, 1, '', '', '2019-02-08 09:09:04.000000', '2019-02-08 08:25:24.000000', '2019-02-08 08:25:49.000000', 1, 7, 39, 172, ''),
(153, 1, '', '', '2019-02-08 09:09:04.000000', '2019-02-08 08:25:58.000000', '2019-02-08 08:26:04.000000', 1, 7, 39, 175, ''),
(154, 1, '', '', '2019-02-08 09:09:04.000000', '2019-02-08 08:25:53.000000', '2019-02-08 08:25:58.000000', 1, 7, 39, 174, ''),
(155, 1, '', '', '2019-02-08 09:09:04.000000', '2019-02-08 08:26:12.000000', '2019-02-08 08:26:18.000000', 1, 7, 39, 177, ''),
(156, 1, '', '', '2019-02-08 09:09:04.000000', '2019-02-08 08:26:04.000000', '2019-02-08 08:26:10.000000', 1, 7, 39, 176, ''),
(157, 1, '', '', '2019-02-08 09:09:04.000000', '2019-02-08 08:26:18.000000', '2019-02-08 08:26:24.000000', 1, 7, 39, 178, ''),
(158, 1, '', '', '2019-02-08 09:09:05.000000', '2019-02-08 08:32:56.000000', '2019-02-08 08:32:59.000000', 1, 7, 115, 476, ''),
(159, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_find_people_screen.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_find_people_screenModified.png', '2019-02-08 09:09:05.000000', '2019-02-08 08:33:08.000000', '2019-02-08 08:33:13.000000', 1, 7, 115, 478, ''),
(160, 1, '', '', '2019-02-08 09:09:05.000000', '2019-02-08 08:32:59.000000', '2019-02-08 08:33:06.000000', 1, 7, 115, 477, ''),
(161, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_add_bot_screen.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_add_bot_screenModified.png', '2019-02-08 09:09:05.000000', '2019-02-08 08:33:13.000000', '2019-02-08 08:33:21.000000', 1, 7, 115, 479, ''),
(162, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/onboard_get_started_dat2.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/onboard_get_started_dat2Modified.png', '2019-02-08 09:09:05.000000', '2019-02-08 13:58:37.000000', '2019-02-08 13:58:42.000000', 1, 7, 2, 3, 'Choose account text is missing'),
(163, 1, '', '', '2019-02-08 09:09:05.000000', '2019-02-08 13:58:47.000000', '2019-02-08 13:59:21.000000', 1, 7, 2, 5, ''),
(164, 1, '', '', '2019-02-08 09:09:05.000000', '2019-02-08 13:58:42.000000', '2019-02-08 13:58:47.000000', 1, 7, 2, 4, ''),
(165, 1, '', '', '2019-02-08 09:09:05.000000', '2019-02-08 08:23:15.000000', '2019-02-08 08:23:18.000000', 1, 7, 15, 53, ''),
(166, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_browse_rooms_page_dat160.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_browse_rooms_page_dat160Modified.png', '2019-02-08 09:09:05.000000', '2019-02-08 08:23:24.000000', '2019-02-08 08:23:34.000000', 1, 7, 15, 55, ''),
(167, 1, '', '', '2019-02-08 09:09:05.000000', '2019-02-08 08:23:18.000000', '2019-02-08 08:23:24.000000', 1, 7, 15, 54, ''),
(168, 1, '', '', '2019-02-08 09:09:05.000000', '2019-02-08 08:23:34.000000', '2019-02-08 08:23:34.000000', 1, 7, 15, 56, ''),
(169, 1, '', '', '2019-02-08 09:09:05.000000', '2019-02-08 08:30:55.000000', '2019-02-08 08:31:17.000000', 1, 7, 82, 374, 'App is not launched'),
(170, 1, '', '', '2019-02-08 09:09:05.000000', '2019-02-08 08:31:26.000000', '2019-02-08 08:31:26.000000', 1, 7, 82, 376, ''),
(171, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/search_bar_layout_DAT78.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/search_bar_layout_DAT78Modified.png', '2019-02-08 09:09:05.000000', '2019-02-08 08:31:17.000000', '2019-02-08 08:31:26.000000', 1, 7, 82, 375, ''),
(172, 1, '', '', '2019-02-08 09:09:05.000000', '2019-02-08 08:31:26.000000', '2019-02-08 08:31:30.000000', 1, 7, 82, 377, ''),
(173, 1, '', '', '2019-02-08 09:09:05.000000', '2019-02-08 08:24:23.000000', '2019-02-08 08:24:26.000000', 1, 7, 27, 112, ''),
(174, 1, '', '', '2019-02-08 09:09:05.000000', '2019-02-08 08:24:32.000000', '2019-02-08 08:24:33.000000', 1, 7, 27, 114, ''),
(175, 1, '', '', '2019-02-08 09:09:05.000000', '2019-02-08 08:24:26.000000', '2019-02-08 08:24:32.000000', 1, 7, 27, 113, ''),
(176, 1, '', '', '2019-02-08 09:09:05.000000', '2019-02-08 08:24:33.000000', '2019-02-08 08:24:35.000000', 1, 7, 27, 116, ''),
(177, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat28.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat28Modified.png', '2019-02-08 09:09:05.000000', '2019-02-08 08:24:33.000000', '2019-02-08 08:24:35.000000', 1, 7, 27, 115, ''),
(178, 1, '', '', '2019-02-08 09:09:05.000000', '2019-02-08 08:21:06.000000', '2019-02-08 08:21:09.000000', 1, 7, 11, 33, ''),
(179, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_FAB_screen_dat11.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_FAB_screen_dat11Modified.png', '2019-02-08 09:09:05.000000', '2019-02-08 08:21:15.000000', '2019-02-08 08:21:18.000000', 1, 7, 11, 35, ''),
(180, 1, '', '', '2019-02-08 09:09:05.000000', '2019-02-08 08:21:09.000000', '2019-02-08 08:21:15.000000', 1, 7, 11, 34, ''),
(181, 1, '', '', '2019-02-08 09:09:05.000000', '2019-02-08 08:32:43.000000', '2019-02-08 08:32:55.000000', 1, 7, 109, 458, ''),
(182, 1, '', '', '2019-02-08 09:09:06.000000', '2019-02-08 08:31:32.000000', '2019-02-08 08:31:35.000000', 1, 7, 109, 449, ''),
(183, 1, '', '', '2019-02-08 09:09:06.000000', '2019-02-08 08:31:51.000000', '2019-02-08 08:32:14.000000', 1, 7, 109, 451, ''),
(184, 1, '', '', '2019-02-08 09:09:06.000000', '2019-02-08 08:31:37.000000', '2019-02-08 08:31:51.000000', 1, 7, 109, 450, ''),
(185, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/delete_press_layout_dat105.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/delete_press_layout_dat105Modified.png', '2019-02-08 09:09:06.000000', '2019-02-08 08:32:22.000000', '2019-02-08 08:32:29.000000', 1, 7, 109, 453, ''),
(186, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/long_press_layout_dat105.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/long_press_layout_dat105Modified.png', '2019-02-08 09:09:06.000000', '2019-02-08 08:32:14.000000', '2019-02-08 08:32:22.000000', 1, 7, 109, 452, ''),
(187, 1, '', '', '2019-02-08 09:09:06.000000', '2019-02-08 08:32:29.000000', '2019-02-08 08:32:37.000000', 1, 7, 109, 455, ''),
(188, 1, '', '', '2019-02-08 09:09:06.000000', '2019-02-08 08:32:22.000000', '2019-02-08 08:32:29.000000', 1, 7, 109, 454, ''),
(189, 1, '', '', '2019-02-08 09:09:06.000000', '2019-02-08 08:32:38.000000', '2019-02-08 08:32:43.000000', 1, 7, 109, 457, ''),
(190, 1, '', '', '2019-02-08 09:09:06.000000', '2019-02-08 08:32:38.000000', '2019-02-08 08:32:43.000000', 1, 7, 109, 456, ''),
(191, 1, '', '', '2019-02-08 09:09:06.000000', '2019-02-08 08:26:24.000000', '2019-02-08 08:27:13.000000', 1, 7, 42, 189, 'set_message_41 is missing'),
(192, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat41.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat41Modified.png', '2019-02-08 09:09:06.000000', '2019-02-08 08:27:59.000000', '2019-02-08 08:28:37.000000', 1, 7, 42, 191, ''),
(193, 1, '', '', '2019-02-08 09:09:06.000000', '2019-02-08 08:27:13.000000', '2019-02-08 08:27:59.000000', 1, 7, 42, 190, ''),
(194, 1, '', '', '2019-02-08 09:09:06.000000', '2019-02-08 08:21:18.000000', '2019-02-08 08:21:21.000000', 1, 7, 12, 36, ''),
(195, 1, '', '', '2019-02-08 09:09:06.000000', '2019-02-08 08:21:37.000000', '2019-02-08 08:21:43.000000', 1, 7, 12, 38, ''),
(196, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_test_keyword_filter_dat12.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_test_keyword_filter_dat12Modified.png', '2019-02-08 09:09:06.000000', '2019-02-08 08:21:21.000000', '2019-02-08 08:21:37.000000', 1, 7, 12, 37, ''),
(197, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_selected_room_screen_dat12.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_selected_room_screen_dat12Modified.png', '2019-02-08 09:09:06.000000', '2019-02-08 08:21:59.000000', '2019-02-08 08:22:19.000000', 1, 7, 12, 40, ''),
(198, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_room_keyword_filter_dat12.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_room_keyword_filter_dat12Modified.png', '2019-02-08 09:09:06.000000', '2019-02-08 08:21:43.000000', '2019-02-08 08:21:59.000000', 1, 7, 12, 39, ''),
(199, 1, '', '', '2019-02-08 09:09:06.000000', '2019-02-08 08:29:57.000000', '2019-02-08 08:30:01.000000', 1, 7, 63, 278, ''),
(200, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat61.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat61Modified.png', '2019-02-08 09:09:06.000000', '2019-02-08 08:30:16.000000', '2019-02-08 08:30:24.000000', 1, 7, 63, 280, ''),
(201, 1, '', '', '2019-02-08 09:09:06.000000', '2019-02-08 08:30:03.000000', '2019-02-08 08:30:16.000000', 1, 7, 63, 279, ''),
(202, 1, '', '', '2019-02-08 09:09:06.000000', '2019-02-08 08:30:24.000000', '2019-02-08 08:30:25.000000', 1, 7, 63, 281, ''),
(203, 1, '', '', '2019-02-08 09:09:06.000000', '2019-02-08 08:33:21.000000', '2019-02-08 08:33:24.000000', 1, 7, 120, 508, ''),
(204, 1, '', '', '2019-02-08 09:09:06.000000', '2019-02-08 08:33:54.000000', '2019-02-08 08:34:04.000000', 1, 7, 120, 510, ''),
(205, 1, '', '', '2019-02-08 09:09:06.000000', '2019-02-08 08:33:24.000000', '2019-02-08 08:33:54.000000', 1, 7, 120, 509, ''),
(206, 1, '', '', '2019-02-08 09:09:06.000000', '2019-02-08 08:34:16.000000', '2019-02-08 08:34:23.000000', 1, 7, 120, 512, ''),
(207, 1, '', '', '2019-02-08 09:09:06.000000', '2019-02-08 08:34:04.000000', '2019-02-08 08:34:16.000000', 1, 7, 120, 511, ''),
(208, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_sharing_screen.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_sharing_screenModified.png', '2019-02-08 09:09:07.000000', '2019-02-08 08:34:28.000000', '2019-02-08 08:34:36.000000', 1, 7, 120, 514, ''),
(209, 1, '', '', '2019-02-08 09:09:07.000000', '2019-02-08 08:34:23.000000', '2019-02-08 08:34:28.000000', 1, 7, 120, 513, ''),
(210, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_sharing_world_view_screen.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_sharing_world_view_screenModified.png', '2019-02-08 09:09:07.000000', '2019-02-08 08:34:36.000000', '2019-02-08 08:34:43.000000', 1, 7, 120, 515, ''),
(211, 1, '', '', '2019-02-08 09:19:55.000000', '2019-02-08 08:48:18.000000', '2019-02-08 08:48:21.000000', 1, 8, 45, 199, ''),
(212, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat44.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat44Modified.png', '2019-02-08 09:19:55.000000', '2019-02-08 08:48:36.000000', '2019-02-08 08:48:48.000000', 1, 8, 45, 201, ''),
(213, 1, '', '', '2019-02-08 09:19:55.000000', '2019-02-08 08:48:21.000000', '2019-02-08 08:48:36.000000', 1, 8, 45, 200, ''),
(214, 1, '', '', '2019-02-08 09:19:55.000000', '2019-02-08 08:48:48.000000', '2019-02-08 08:48:53.000000', 1, 8, 45, 202, ''),
(215, 1, '', '', '2019-02-08 09:19:55.000000', '2019-02-08 08:41:18.000000', '2019-02-08 08:41:21.000000', 1, 8, 13, 41, ''),
(216, 1, '', '', '2019-02-08 09:19:55.000000', '2019-02-08 08:41:27.000000', '2019-02-08 08:41:32.000000', 1, 8, 13, 43, ''),
(217, 1, '', '', '2019-02-08 09:19:55.000000', '2019-02-08 08:41:21.000000', '2019-02-08 08:41:27.000000', 1, 8, 13, 42, ''),
(218, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_create_room_layout_dat13.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_create_room_layout_dat13Modified.png', '2019-02-08 09:19:55.000000', '2019-02-08 08:41:32.000000', '2019-02-08 08:41:36.000000', 1, 8, 13, 45, ''),
(219, 1, '', '', '2019-02-08 09:19:55.000000', '2019-02-08 08:41:27.000000', '2019-02-08 08:41:32.000000', 1, 8, 13, 44, ''),
(220, 1, '', '', '2019-02-08 09:19:55.000000', '2019-02-08 08:41:32.000000', '2019-02-08 08:41:36.000000', 1, 8, 13, 46, ''),
(221, 1, '', '', '2019-02-08 09:19:55.000000', '2019-02-08 08:41:36.000000', '2019-02-08 08:41:39.000000', 1, 8, 14, 47, ''),
(222, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_create_room_page_dat15.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_create_room_page_dat15Modified.png', '2019-02-08 09:19:55.000000', '2019-02-08 08:41:51.000000', '2019-02-08 08:41:57.000000', 1, 8, 14, 49, ''),
(223, 1, '', '', '2019-02-08 09:19:56.000000', '2019-02-08 08:41:39.000000', '2019-02-08 08:41:51.000000', 1, 8, 14, 48, ''),
(224, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_new_room_created_dat15.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_new_room_created_dat15Modified.png', '2019-02-08 09:19:56.000000', '2019-02-08 08:41:57.000000', '2019-02-08 08:42:12.000000', 1, 8, 14, 51, ''),
(225, 1, '', '', '2019-02-08 09:19:56.000000', '2019-02-08 08:41:51.000000', '2019-02-08 08:41:57.000000', 1, 8, 14, 50, ''),
(226, 1, '', '', '2019-02-08 09:19:56.000000', '2019-02-08 08:41:57.000000', '2019-02-08 08:42:12.000000', 1, 8, 14, 52, ''),
(227, 1, '', '', '2019-02-08 09:19:56.000000', '2019-02-08 08:43:32.000000', '2019-02-08 08:43:35.000000', 1, 8, 28, 117, ''),
(228, 1, '', '', '2019-02-08 09:19:56.000000', '2019-02-08 08:43:41.000000', '2019-02-08 08:43:55.000000', 1, 8, 28, 119, ''),
(229, 1, '', '', '2019-02-08 09:19:56.000000', '2019-02-08 08:43:35.000000', '2019-02-08 08:43:41.000000', 1, 8, 28, 118, ''),
(230, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/search_preview_layout_dat29.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/search_preview_layout_dat29Modified.png', '2019-02-08 09:19:56.000000', '2019-02-08 08:43:55.000000', '2019-02-08 08:43:58.000000', 1, 8, 28, 121, ''),
(231, 1, '', '', '2019-02-08 09:19:56.000000', '2019-02-08 08:43:41.000000', '2019-02-08 08:43:55.000000', 1, 8, 28, 120, ''),
(232, 1, '', '', '2019-02-08 09:19:56.000000', '2019-02-08 08:43:58.000000', '2019-02-08 08:44:04.000000', 1, 8, 28, 123, ''),
(233, 1, '', '', '2019-02-08 09:19:56.000000', '2019-02-08 08:43:55.000000', '2019-02-08 08:43:58.000000', 1, 8, 28, 122, ''),
(234, 1, '', '', '2019-02-08 09:19:56.000000', '2019-02-08 08:44:13.000000', '2019-02-08 08:44:19.000000', 1, 8, 28, 125, ''),
(235, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat29.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat29Modified.png', '2019-02-08 09:19:56.000000', '2019-02-08 08:44:04.000000', '2019-02-08 08:44:13.000000', 1, 8, 28, 124, ''),
(236, 1, '', '', '2019-02-08 09:19:56.000000', '2019-02-08 08:42:32.000000', '2019-02-08 08:42:35.000000', 1, 8, 16, 57, ''),
(237, 1, '', '', '2019-02-08 09:19:56.000000', '2019-02-08 08:42:52.000000', '2019-02-08 08:42:58.000000', 1, 8, 16, 59, ''),
(238, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_browse_rooms_layout_dat16.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_browse_rooms_layout_dat16Modified.png', '2019-02-08 09:19:56.000000', '2019-02-08 08:42:35.000000', '2019-02-08 08:42:52.000000', 1, 8, 16, 58, ''),
(239, 1, '', '', '2019-02-08 09:19:56.000000', '2019-02-08 08:42:58.000000', '2019-02-08 08:43:13.000000', 1, 8, 16, 61, ''),
(240, 1, '', '', '2019-02-08 09:19:56.000000', '2019-02-08 08:42:52.000000', '2019-02-08 08:42:58.000000', 1, 8, 16, 60, ''),
(241, 1, '', '', '2019-02-08 09:19:56.000000', '2019-02-08 08:43:13.000000', '2019-02-08 08:43:21.000000', 1, 8, 16, 63, ''),
(242, 1, '', '', '2019-02-08 09:19:56.000000', '2019-02-08 08:42:58.000000', '2019-02-08 08:43:13.000000', 1, 8, 16, 62, ''),
(243, 1, '', '', '2019-02-08 09:19:56.000000', '2019-02-08 08:39:57.000000', '2019-02-08 08:40:00.000000', 1, 8, 7, 20, ''),
(244, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/world_view_layout_dat7.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/world_view_layout_dat7Modified.png', '2019-02-08 09:19:56.000000', '2019-02-08 08:40:00.000000', '2019-02-08 08:40:04.000000', 1, 8, 7, 21, 'Badge count is not found'),
(245, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_flat_dm_page_screen.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_flat_dm_page_screenModified.png', '2019-02-08 09:19:57.000000', '2019-02-08 08:53:38.000000', '2019-02-08 08:54:04.000000', 1, 8, 146, 565, ''),
(246, 1, '', '', '2019-02-08 09:19:57.000000', '2019-02-08 08:54:04.000000', '2019-02-08 08:54:04.000000', 1, 8, 146, 567, ''),
(247, 1, '', '', '2019-02-08 09:19:57.000000', '2019-02-08 08:54:04.000000', '2019-02-08 08:54:04.000000', 1, 8, 146, 566, ''),
(248, 1, '', '', '2019-02-08 09:19:57.000000', '2019-02-08 08:54:04.000000', '2019-02-08 08:54:04.000000', 1, 8, 146, 569, ''),
(249, 1, '', '', '2019-02-08 09:19:57.000000', '2019-02-08 08:54:04.000000', '2019-02-08 08:54:04.000000', 1, 8, 146, 568, ''),
(250, 1, '', '', '2019-02-08 09:19:57.000000', '2019-02-08 08:54:04.000000', '2019-02-08 08:54:04.000000', 1, 8, 146, 570, ''),
(251, 1, '', '', '2019-02-08 09:19:57.000000', '2019-02-08 08:49:22.000000', '2019-02-08 08:49:25.000000', 1, 8, 66, 286, ''),
(252, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/topic_preview_layout_dat64.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/topic_preview_layout_dat64Modified.png', '2019-02-08 09:19:57.000000', '2019-02-08 08:49:40.000000', '2019-02-08 08:49:48.000000', 1, 8, 66, 288, ''),
(253, 1, '', '', '2019-02-08 09:19:57.000000', '2019-02-08 08:49:28.000000', '2019-02-08 08:49:40.000000', 1, 8, 66, 287, ''),
(254, 1, '', '', '2019-02-08 09:19:57.000000', '2019-02-08 08:49:48.000000', '2019-02-08 08:49:48.000000', 1, 8, 66, 289, ''),
(255, 1, '', '', '2019-02-08 09:19:58.000000', '2019-02-08 08:44:19.000000', '2019-02-08 08:44:22.000000', 1, 8, 39, 171, ''),
(256, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat38.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat38Modified.png', '2019-02-08 09:19:58.000000', '2019-02-08 08:44:46.000000', '2019-02-08 08:44:50.000000', 1, 8, 39, 173, ''),
(257, 1, '', '', '2019-02-08 09:19:58.000000', '2019-02-08 08:44:22.000000', '2019-02-08 08:44:46.000000', 1, 8, 39, 172, ''),
(258, 1, '', '', '2019-02-08 09:19:58.000000', '2019-02-08 08:44:55.000000', '2019-02-08 08:45:01.000000', 1, 8, 39, 175, ''),
(259, 1, '', '', '2019-02-08 09:19:58.000000', '2019-02-08 08:44:50.000000', '2019-02-08 08:44:55.000000', 1, 8, 39, 174, ''),
(260, 1, '', '', '2019-02-08 09:19:58.000000', '2019-02-08 08:45:09.000000', '2019-02-08 08:45:15.000000', 1, 8, 39, 177, ''),
(261, 1, '', '', '2019-02-08 09:19:58.000000', '2019-02-08 08:45:01.000000', '2019-02-08 08:45:07.000000', 1, 8, 39, 176, ''),
(262, 1, '', '', '2019-02-08 09:19:58.000000', '2019-02-08 08:45:15.000000', '2019-02-08 08:45:21.000000', 1, 8, 39, 178, ''),
(263, 1, '', '', '2019-02-08 09:19:58.000000', '2019-02-08 08:51:52.000000', '2019-02-08 08:51:55.000000', 1, 8, 115, 476, ''),
(264, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_find_people_screen.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_find_people_screenModified.png', '2019-02-08 09:19:58.000000', '2019-02-08 08:52:03.000000', '2019-02-08 08:52:08.000000', 1, 8, 115, 478, ''),
(265, 1, '', '', '2019-02-08 09:19:58.000000', '2019-02-08 08:51:55.000000', '2019-02-08 08:52:01.000000', 1, 8, 115, 477, ''),
(266, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_add_bot_screen.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_add_bot_screenModified.png', '2019-02-08 09:19:58.000000', '2019-02-08 08:52:08.000000', '2019-02-08 08:52:15.000000', 1, 8, 115, 479, ''),
(267, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/onboard_get_started_dat2.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/onboard_get_started_dat2Modified.png', '2019-02-08 09:19:58.000000', '2019-02-08 14:17:33.000000', '2019-02-08 14:17:38.000000', 1, 8, 2, 3, 'Choose account text is missing'),
(268, 1, '', '', '2019-02-08 09:19:58.000000', '2019-02-08 14:17:44.000000', '2019-02-08 14:18:17.000000', 1, 8, 2, 5, ''),
(269, 1, '', '', '2019-02-08 09:19:58.000000', '2019-02-08 14:17:38.000000', '2019-02-08 14:17:44.000000', 1, 8, 2, 4, ''),
(270, 1, '', '', '2019-02-08 09:19:58.000000', '2019-02-08 08:42:13.000000', '2019-02-08 08:42:16.000000', 1, 8, 15, 53, ''),
(271, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_browse_rooms_page_dat160.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_browse_rooms_page_dat160Modified.png', '2019-02-08 09:19:58.000000', '2019-02-08 08:42:22.000000', '2019-02-08 08:42:32.000000', 1, 8, 15, 55, ''),
(272, 1, '', '', '2019-02-08 09:19:58.000000', '2019-02-08 08:42:16.000000', '2019-02-08 08:42:22.000000', 1, 8, 15, 54, ''),
(273, 1, '', '', '2019-02-08 09:19:59.000000', '2019-02-08 08:42:32.000000', '2019-02-08 08:42:32.000000', 1, 8, 15, 56, ''),
(274, 1, '', '', '2019-02-08 09:19:59.000000', '2019-02-08 08:49:50.000000', '2019-02-08 08:50:12.000000', 1, 8, 82, 374, 'App is not launched'),
(275, 1, '', '', '2019-02-08 09:19:59.000000', '2019-02-08 08:50:21.000000', '2019-02-08 08:50:22.000000', 1, 8, 82, 376, ''),
(276, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/search_bar_layout_DAT78.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/search_bar_layout_DAT78Modified.png', '2019-02-08 09:19:59.000000', '2019-02-08 08:50:12.000000', '2019-02-08 08:50:21.000000', 1, 8, 82, 375, ''),
(277, 1, '', '', '2019-02-08 09:19:59.000000', '2019-02-08 08:50:22.000000', '2019-02-08 08:50:27.000000', 1, 8, 82, 377, ''),
(278, 1, '', '', '2019-02-08 09:19:59.000000', '2019-02-08 08:43:21.000000', '2019-02-08 08:43:24.000000', 1, 8, 27, 112, ''),
(279, 1, '', '', '2019-02-08 09:19:59.000000', '2019-02-08 08:43:30.000000', '2019-02-08 08:43:30.000000', 1, 8, 27, 114, ''),
(280, 1, '', '', '2019-02-08 09:19:59.000000', '2019-02-08 08:43:24.000000', '2019-02-08 08:43:30.000000', 1, 8, 27, 113, ''),
(281, 1, '', '', '2019-02-08 09:19:59.000000', '2019-02-08 08:43:30.000000', '2019-02-08 08:43:32.000000', 1, 8, 27, 116, ''),
(282, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat28.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat28Modified.png', '2019-02-08 09:19:59.000000', '2019-02-08 08:43:30.000000', '2019-02-08 08:43:32.000000', 1, 8, 27, 115, ''),
(283, 1, '', '', '2019-02-08 09:19:59.000000', '2019-02-08 08:40:04.000000', '2019-02-08 08:40:07.000000', 1, 8, 11, 33, ''),
(284, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_FAB_screen_dat11.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_FAB_screen_dat11Modified.png', '2019-02-08 09:19:59.000000', '2019-02-08 08:40:13.000000', '2019-02-08 08:40:16.000000', 1, 8, 11, 35, ''),
(285, 1, '', '', '2019-02-08 09:19:59.000000', '2019-02-08 08:40:07.000000', '2019-02-08 08:40:13.000000', 1, 8, 11, 34, ''),
(286, 1, '', '', '2019-02-08 09:19:59.000000', '2019-02-08 08:51:39.000000', '2019-02-08 08:51:51.000000', 1, 8, 109, 458, ''),
(287, 1, '', '', '2019-02-08 09:19:59.000000', '2019-02-08 08:50:28.000000', '2019-02-08 08:50:31.000000', 1, 8, 109, 449, ''),
(288, 1, '', '', '2019-02-08 09:19:59.000000', '2019-02-08 08:50:47.000000', '2019-02-08 08:51:10.000000', 1, 8, 109, 451, ''),
(289, 1, '', '', '2019-02-08 09:19:59.000000', '2019-02-08 08:50:33.000000', '2019-02-08 08:50:47.000000', 1, 8, 109, 450, ''),
(290, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/delete_press_layout_dat105.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/delete_press_layout_dat105Modified.png', '2019-02-08 09:19:59.000000', '2019-02-08 08:51:18.000000', '2019-02-08 08:51:26.000000', 1, 8, 109, 453, ''),
(291, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/long_press_layout_dat105.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/long_press_layout_dat105Modified.png', '2019-02-08 09:19:59.000000', '2019-02-08 08:51:10.000000', '2019-02-08 08:51:18.000000', 1, 8, 109, 452, ''),
(292, 1, '', '', '2019-02-08 09:19:59.000000', '2019-02-08 08:51:26.000000', '2019-02-08 08:51:34.000000', 1, 8, 109, 455, ''),
(293, 1, '', '', '2019-02-08 09:19:59.000000', '2019-02-08 08:51:18.000000', '2019-02-08 08:51:26.000000', 1, 8, 109, 454, ''),
(294, 1, '', '', '2019-02-08 09:19:59.000000', '2019-02-08 08:51:34.000000', '2019-02-08 08:51:39.000000', 1, 8, 109, 457, ''),
(295, 1, '', '', '2019-02-08 09:19:59.000000', '2019-02-08 08:51:34.000000', '2019-02-08 08:51:39.000000', 1, 8, 109, 456, ''),
(296, 1, '', '', '2019-02-08 09:19:59.000000', '2019-02-08 08:45:21.000000', '2019-02-08 08:46:09.000000', 1, 8, 42, 189, 'set_message_41 is missing'),
(297, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat41.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat41Modified.png', '2019-02-08 09:19:59.000000', '2019-02-08 08:46:56.000000', '2019-02-08 08:47:32.000000', 1, 8, 42, 191, ''),
(298, 1, '', '', '2019-02-08 09:20:00.000000', '2019-02-08 08:46:09.000000', '2019-02-08 08:46:56.000000', 1, 8, 42, 190, ''),
(299, 1, '', '', '2019-02-08 09:20:00.000000', '2019-02-08 08:40:16.000000', '2019-02-08 08:40:19.000000', 1, 8, 12, 36, ''),
(300, 1, '', '', '2019-02-08 09:20:00.000000', '2019-02-08 08:40:36.000000', '2019-02-08 08:40:41.000000', 1, 8, 12, 38, ''),
(301, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_test_keyword_filter_dat12.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_test_keyword_filter_dat12Modified.png', '2019-02-08 09:20:00.000000', '2019-02-08 08:40:19.000000', '2019-02-08 08:40:36.000000', 1, 8, 12, 37, '');
INSERT INTO `reports_testresults` (`id`, `test_result`, `screen_generated`, `screen_diff`, `executed`, `start_time`, `end_time`, `device_id`, `test_build_id`, `test_name_id`, `test_step_id`, `test_errors`) VALUES
(302, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_selected_room_screen_dat12.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_selected_room_screen_dat12Modified.png', '2019-02-08 09:20:00.000000', '2019-02-08 08:40:57.000000', '2019-02-08 08:41:18.000000', 1, 8, 12, 40, ''),
(303, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_room_keyword_filter_dat12.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_room_keyword_filter_dat12Modified.png', '2019-02-08 09:20:00.000000', '2019-02-08 08:40:41.000000', '2019-02-08 08:40:57.000000', 1, 8, 12, 39, ''),
(304, 1, '', '', '2019-02-08 09:20:00.000000', '2019-02-08 08:48:54.000000', '2019-02-08 08:48:57.000000', 1, 8, 63, 278, ''),
(305, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat61.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat61Modified.png', '2019-02-08 09:20:00.000000', '2019-02-08 08:49:12.000000', '2019-02-08 08:49:20.000000', 1, 8, 63, 280, ''),
(306, 1, '', '', '2019-02-08 09:20:00.000000', '2019-02-08 08:48:59.000000', '2019-02-08 08:49:12.000000', 1, 8, 63, 279, ''),
(307, 1, '', '', '2019-02-08 09:20:00.000000', '2019-02-08 08:49:20.000000', '2019-02-08 08:49:21.000000', 1, 8, 63, 281, ''),
(308, 1, '', '', '2019-02-08 09:20:00.000000', '2019-02-08 08:52:15.000000', '2019-02-08 08:52:18.000000', 1, 8, 120, 508, ''),
(309, 1, '', '', '2019-02-08 09:20:00.000000', '2019-02-08 08:52:47.000000', '2019-02-08 08:52:58.000000', 1, 8, 120, 510, ''),
(310, 1, '', '', '2019-02-08 09:20:00.000000', '2019-02-08 08:52:18.000000', '2019-02-08 08:52:47.000000', 1, 8, 120, 509, ''),
(311, 1, '', '', '2019-02-08 09:20:00.000000', '2019-02-08 08:53:09.000000', '2019-02-08 08:53:16.000000', 1, 8, 120, 512, ''),
(312, 1, '', '', '2019-02-08 09:20:00.000000', '2019-02-08 08:52:58.000000', '2019-02-08 08:53:09.000000', 1, 8, 120, 511, ''),
(313, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_sharing_screen.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_sharing_screenModified.png', '2019-02-08 09:20:00.000000', '2019-02-08 08:53:21.000000', '2019-02-08 08:53:29.000000', 1, 8, 120, 514, ''),
(314, 1, '', '', '2019-02-08 09:20:00.000000', '2019-02-08 08:53:16.000000', '2019-02-08 08:53:21.000000', 1, 8, 120, 513, ''),
(315, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_sharing_world_view_screen.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_sharing_world_view_screenModified.png', '2019-02-08 09:20:00.000000', '2019-02-08 08:53:29.000000', '2019-02-08 08:53:36.000000', 1, 8, 120, 515, ''),
(316, 1, '', '', '2019-02-08 09:30:44.000000', '2019-02-08 09:17:10.000000', '2019-02-08 09:17:13.000000', 1, 9, 45, 199, ''),
(317, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat44.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat44Modified.png', '2019-02-08 09:30:44.000000', '2019-02-08 09:17:28.000000', '2019-02-08 09:17:40.000000', 1, 9, 45, 201, ''),
(318, 1, '', '', '2019-02-08 09:30:44.000000', '2019-02-08 09:17:13.000000', '2019-02-08 09:17:28.000000', 1, 9, 45, 200, ''),
(319, 1, '', '', '2019-02-08 09:30:44.000000', '2019-02-08 09:17:40.000000', '2019-02-08 09:17:44.000000', 1, 9, 45, 202, ''),
(320, 1, '', '', '2019-02-08 09:30:44.000000', '2019-02-08 09:10:07.000000', '2019-02-08 09:10:10.000000', 1, 9, 13, 41, ''),
(321, 1, '', '', '2019-02-08 09:30:44.000000', '2019-02-08 09:10:15.000000', '2019-02-08 09:10:21.000000', 1, 9, 13, 43, ''),
(322, 1, '', '', '2019-02-08 09:30:44.000000', '2019-02-08 09:10:10.000000', '2019-02-08 09:10:15.000000', 1, 9, 13, 42, ''),
(323, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_create_room_layout_dat13.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_create_room_layout_dat13Modified.png', '2019-02-08 09:30:44.000000', '2019-02-08 09:10:21.000000', '2019-02-08 09:10:25.000000', 1, 9, 13, 45, ''),
(324, 1, '', '', '2019-02-08 09:30:45.000000', '2019-02-08 09:10:15.000000', '2019-02-08 09:10:21.000000', 1, 9, 13, 44, ''),
(325, 1, '', '', '2019-02-08 09:30:45.000000', '2019-02-08 09:10:21.000000', '2019-02-08 09:10:25.000000', 1, 9, 13, 46, ''),
(326, 1, '', '', '2019-02-08 09:30:45.000000', '2019-02-08 09:10:25.000000', '2019-02-08 09:10:28.000000', 1, 9, 14, 47, ''),
(327, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_create_room_page_dat15.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_create_room_page_dat15Modified.png', '2019-02-08 09:30:45.000000', '2019-02-08 09:10:40.000000', '2019-02-08 09:10:46.000000', 1, 9, 14, 49, ''),
(328, 1, '', '', '2019-02-08 09:30:45.000000', '2019-02-08 09:10:28.000000', '2019-02-08 09:10:40.000000', 1, 9, 14, 48, ''),
(329, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_new_room_created_dat15.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_new_room_created_dat15Modified.png', '2019-02-08 09:30:45.000000', '2019-02-08 09:10:46.000000', '2019-02-08 09:11:02.000000', 1, 9, 14, 51, ''),
(330, 1, '', '', '2019-02-08 09:30:45.000000', '2019-02-08 09:10:40.000000', '2019-02-08 09:10:46.000000', 1, 9, 14, 50, ''),
(331, 1, '', '', '2019-02-08 09:30:45.000000', '2019-02-08 09:10:46.000000', '2019-02-08 09:11:02.000000', 1, 9, 14, 52, ''),
(332, 1, '', '', '2019-02-08 09:30:45.000000', '2019-02-08 09:12:22.000000', '2019-02-08 09:12:25.000000', 1, 9, 28, 117, ''),
(333, 1, '', '', '2019-02-08 09:30:45.000000', '2019-02-08 09:12:31.000000', '2019-02-08 09:12:45.000000', 1, 9, 28, 119, ''),
(334, 1, '', '', '2019-02-08 09:30:45.000000', '2019-02-08 09:12:25.000000', '2019-02-08 09:12:31.000000', 1, 9, 28, 118, ''),
(335, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/search_preview_layout_dat29.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/search_preview_layout_dat29Modified.png', '2019-02-08 09:30:45.000000', '2019-02-08 09:12:45.000000', '2019-02-08 09:12:48.000000', 1, 9, 28, 121, ''),
(336, 1, '', '', '2019-02-08 09:30:45.000000', '2019-02-08 09:12:31.000000', '2019-02-08 09:12:45.000000', 1, 9, 28, 120, ''),
(337, 1, '', '', '2019-02-08 09:30:45.000000', '2019-02-08 09:12:48.000000', '2019-02-08 09:12:53.000000', 1, 9, 28, 123, ''),
(338, 1, '', '', '2019-02-08 09:30:45.000000', '2019-02-08 09:12:45.000000', '2019-02-08 09:12:48.000000', 1, 9, 28, 122, ''),
(339, 1, '', '', '2019-02-08 09:30:45.000000', '2019-02-08 09:13:03.000000', '2019-02-08 09:13:09.000000', 1, 9, 28, 125, ''),
(340, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat29.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat29Modified.png', '2019-02-08 09:30:45.000000', '2019-02-08 09:12:53.000000', '2019-02-08 09:13:03.000000', 1, 9, 28, 124, ''),
(341, 1, '', '', '2019-02-08 09:30:45.000000', '2019-02-08 09:11:21.000000', '2019-02-08 09:11:24.000000', 1, 9, 16, 57, ''),
(342, 1, '', '', '2019-02-08 09:30:45.000000', '2019-02-08 09:11:41.000000', '2019-02-08 09:11:47.000000', 1, 9, 16, 59, ''),
(343, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_browse_rooms_layout_dat16.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_browse_rooms_layout_dat16Modified.png', '2019-02-08 09:30:45.000000', '2019-02-08 09:11:24.000000', '2019-02-08 09:11:41.000000', 1, 9, 16, 58, ''),
(344, 1, '', '', '2019-02-08 09:30:45.000000', '2019-02-08 09:11:47.000000', '2019-02-08 09:12:03.000000', 1, 9, 16, 61, ''),
(345, 1, '', '', '2019-02-08 09:30:45.000000', '2019-02-08 09:11:41.000000', '2019-02-08 09:11:47.000000', 1, 9, 16, 60, ''),
(346, 1, '', '', '2019-02-08 09:30:46.000000', '2019-02-08 09:12:03.000000', '2019-02-08 09:12:10.000000', 1, 9, 16, 63, ''),
(347, 1, '', '', '2019-02-08 09:30:46.000000', '2019-02-08 09:11:47.000000', '2019-02-08 09:12:03.000000', 1, 9, 16, 62, ''),
(348, 1, '', '', '2019-02-08 09:30:46.000000', '2019-02-08 09:08:46.000000', '2019-02-08 09:08:50.000000', 1, 9, 7, 20, ''),
(349, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/world_view_layout_dat7.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/world_view_layout_dat7Modified.png', '2019-02-08 09:30:46.000000', '2019-02-08 09:08:50.000000', '2019-02-08 09:08:53.000000', 1, 9, 7, 21, ''),
(350, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_flat_dm_page_screen.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_flat_dm_page_screenModified.png', '2019-02-08 09:30:46.000000', '2019-02-08 09:22:33.000000', '2019-02-08 09:23:00.000000', 1, 9, 146, 565, ''),
(351, 1, '', '', '2019-02-08 09:30:46.000000', '2019-02-08 09:23:00.000000', '2019-02-08 09:23:00.000000', 1, 9, 146, 567, ''),
(352, 1, '', '', '2019-02-08 09:30:46.000000', '2019-02-08 09:23:00.000000', '2019-02-08 09:23:00.000000', 1, 9, 146, 566, ''),
(353, 1, '', '', '2019-02-08 09:30:46.000000', '2019-02-08 09:23:00.000000', '2019-02-08 09:23:00.000000', 1, 9, 146, 569, ''),
(354, 1, '', '', '2019-02-08 09:30:46.000000', '2019-02-08 09:23:00.000000', '2019-02-08 09:23:00.000000', 1, 9, 146, 568, ''),
(355, 1, '', '', '2019-02-08 09:30:46.000000', '2019-02-08 09:23:00.000000', '2019-02-08 09:23:00.000000', 1, 9, 146, 570, ''),
(356, 1, '', '', '2019-02-08 09:30:46.000000', '2019-02-08 09:18:14.000000', '2019-02-08 09:18:17.000000', 1, 9, 66, 286, ''),
(357, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/topic_preview_layout_dat64.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/topic_preview_layout_dat64Modified.png', '2019-02-08 09:30:46.000000', '2019-02-08 09:18:32.000000', '2019-02-08 09:18:39.000000', 1, 9, 66, 288, ''),
(358, 1, '', '', '2019-02-08 09:30:46.000000', '2019-02-08 09:18:19.000000', '2019-02-08 09:18:32.000000', 1, 9, 66, 287, ''),
(359, 1, '', '', '2019-02-08 09:30:46.000000', '2019-02-08 09:18:39.000000', '2019-02-08 09:18:40.000000', 1, 9, 66, 289, ''),
(360, 1, '', '', '2019-02-08 09:30:46.000000', '2019-02-08 09:13:09.000000', '2019-02-08 09:13:12.000000', 1, 9, 39, 171, ''),
(361, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat38.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat38Modified.png', '2019-02-08 09:30:46.000000', '2019-02-08 09:13:36.000000', '2019-02-08 09:13:40.000000', 1, 9, 39, 173, ''),
(362, 1, '', '', '2019-02-08 09:30:46.000000', '2019-02-08 09:13:12.000000', '2019-02-08 09:13:36.000000', 1, 9, 39, 172, ''),
(363, 1, '', '', '2019-02-08 09:30:46.000000', '2019-02-08 09:13:46.000000', '2019-02-08 09:13:53.000000', 1, 9, 39, 175, ''),
(364, 1, '', '', '2019-02-08 09:30:47.000000', '2019-02-08 09:13:40.000000', '2019-02-08 09:13:46.000000', 1, 9, 39, 174, ''),
(365, 1, '', '', '2019-02-08 09:30:47.000000', '2019-02-08 09:14:00.000000', '2019-02-08 09:14:06.000000', 1, 9, 39, 177, ''),
(366, 1, '', '', '2019-02-08 09:30:47.000000', '2019-02-08 09:13:53.000000', '2019-02-08 09:13:58.000000', 1, 9, 39, 176, ''),
(367, 1, '', '', '2019-02-08 09:30:47.000000', '2019-02-08 09:14:06.000000', '2019-02-08 09:14:12.000000', 1, 9, 39, 178, ''),
(368, 1, '', '', '2019-02-08 09:30:47.000000', '2019-02-08 09:20:47.000000', '2019-02-08 09:20:50.000000', 1, 9, 115, 476, ''),
(369, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_find_people_screen.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_find_people_screenModified.png', '2019-02-08 09:30:47.000000', '2019-02-08 09:20:58.000000', '2019-02-08 09:21:03.000000', 1, 9, 115, 478, ''),
(370, 1, '', '', '2019-02-08 09:30:47.000000', '2019-02-08 09:20:50.000000', '2019-02-08 09:20:56.000000', 1, 9, 115, 477, ''),
(371, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_add_bot_screen.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_add_bot_screenModified.png', '2019-02-08 09:30:47.000000', '2019-02-08 09:21:03.000000', '2019-02-08 09:21:10.000000', 1, 9, 115, 479, ''),
(372, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/onboard_get_started_dat2.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/onboard_get_started_dat2Modified.png', '2019-02-08 09:30:47.000000', '2019-02-08 14:46:25.000000', '2019-02-08 14:46:30.000000', 1, 9, 2, 3, 'Choose account text is missing'),
(373, 1, '', '', '2019-02-08 09:30:47.000000', '2019-02-08 14:46:35.000000', '2019-02-08 14:47:09.000000', 1, 9, 2, 5, ''),
(374, 1, '', '', '2019-02-08 09:30:47.000000', '2019-02-08 14:46:30.000000', '2019-02-08 14:46:35.000000', 1, 9, 2, 4, ''),
(375, 1, '', '', '2019-02-08 09:30:47.000000', '2019-02-08 09:11:02.000000', '2019-02-08 09:11:05.000000', 1, 9, 15, 53, ''),
(376, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_browse_rooms_page_dat160.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_browse_rooms_page_dat160Modified.png', '2019-02-08 09:30:47.000000', '2019-02-08 09:11:11.000000', '2019-02-08 09:11:21.000000', 1, 9, 15, 55, ''),
(377, 1, '', '', '2019-02-08 09:30:47.000000', '2019-02-08 09:11:05.000000', '2019-02-08 09:11:11.000000', 1, 9, 15, 54, ''),
(378, 1, '', '', '2019-02-08 09:30:47.000000', '2019-02-08 09:11:21.000000', '2019-02-08 09:11:21.000000', 1, 9, 15, 56, ''),
(379, 1, '', '', '2019-02-08 09:30:47.000000', '2019-02-08 09:18:41.000000', '2019-02-08 09:19:03.000000', 1, 9, 82, 374, 'App is not launched'),
(380, 1, '', '', '2019-02-08 09:30:47.000000', '2019-02-08 09:19:13.000000', '2019-02-08 09:19:13.000000', 1, 9, 82, 376, ''),
(381, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/search_bar_layout_DAT78.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/search_bar_layout_DAT78Modified.png', '2019-02-08 09:30:47.000000', '2019-02-08 09:19:03.000000', '2019-02-08 09:19:13.000000', 1, 9, 82, 375, ''),
(382, 1, '', '', '2019-02-08 09:30:47.000000', '2019-02-08 09:19:13.000000', '2019-02-08 09:19:18.000000', 1, 9, 82, 377, ''),
(383, 1, '', '', '2019-02-08 09:30:47.000000', '2019-02-08 09:12:10.000000', '2019-02-08 09:12:14.000000', 1, 9, 27, 112, ''),
(384, 1, '', '', '2019-02-08 09:30:47.000000', '2019-02-08 09:12:20.000000', '2019-02-08 09:12:20.000000', 1, 9, 27, 114, ''),
(385, 1, '', '', '2019-02-08 09:30:47.000000', '2019-02-08 09:12:14.000000', '2019-02-08 09:12:20.000000', 1, 9, 27, 113, ''),
(386, 1, '', '', '2019-02-08 09:30:47.000000', '2019-02-08 09:12:20.000000', '2019-02-08 09:12:22.000000', 1, 9, 27, 116, ''),
(387, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat28.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat28Modified.png', '2019-02-08 09:30:47.000000', '2019-02-08 09:12:20.000000', '2019-02-08 09:12:22.000000', 1, 9, 27, 115, ''),
(388, 1, '', '', '2019-02-08 09:30:47.000000', '2019-02-08 09:08:53.000000', '2019-02-08 09:08:56.000000', 1, 9, 11, 33, ''),
(389, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_FAB_screen_dat11.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_FAB_screen_dat11Modified.png', '2019-02-08 09:30:48.000000', '2019-02-08 09:09:02.000000', '2019-02-08 09:09:05.000000', 1, 9, 11, 35, ''),
(390, 1, '', '', '2019-02-08 09:30:48.000000', '2019-02-08 09:08:56.000000', '2019-02-08 09:09:02.000000', 1, 9, 11, 34, ''),
(391, 1, '', '', '2019-02-08 09:30:48.000000', '2019-02-08 09:20:32.000000', '2019-02-08 09:20:45.000000', 1, 9, 109, 458, ''),
(392, 1, '', '', '2019-02-08 09:30:48.000000', '2019-02-08 09:19:19.000000', '2019-02-08 09:19:22.000000', 1, 9, 109, 449, ''),
(393, 1, '', '', '2019-02-08 09:30:48.000000', '2019-02-08 09:19:39.000000', '2019-02-08 09:20:02.000000', 1, 9, 109, 451, ''),
(394, 1, '', '', '2019-02-08 09:30:48.000000', '2019-02-08 09:19:24.000000', '2019-02-08 09:19:39.000000', 1, 9, 109, 450, ''),
(395, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/delete_press_layout_dat105.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/delete_press_layout_dat105Modified.png', '2019-02-08 09:30:48.000000', '2019-02-08 09:20:11.000000', '2019-02-08 09:20:18.000000', 1, 9, 109, 453, ''),
(396, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/long_press_layout_dat105.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/long_press_layout_dat105Modified.png', '2019-02-08 09:30:48.000000', '2019-02-08 09:20:02.000000', '2019-02-08 09:20:11.000000', 1, 9, 109, 452, ''),
(397, 1, '', '', '2019-02-08 09:30:48.000000', '2019-02-08 09:20:18.000000', '2019-02-08 09:20:27.000000', 1, 9, 109, 455, ''),
(398, 1, '', '', '2019-02-08 09:30:48.000000', '2019-02-08 09:20:11.000000', '2019-02-08 09:20:18.000000', 1, 9, 109, 454, ''),
(399, 1, '', '', '2019-02-08 09:30:48.000000', '2019-02-08 09:20:27.000000', '2019-02-08 09:20:32.000000', 1, 9, 109, 457, ''),
(400, 1, '', '', '2019-02-08 09:30:48.000000', '2019-02-08 09:20:27.000000', '2019-02-08 09:20:32.000000', 1, 9, 109, 456, ''),
(401, 1, '', '', '2019-02-08 09:30:48.000000', '2019-02-08 09:14:12.000000', '2019-02-08 09:15:01.000000', 1, 9, 42, 189, 'set_message_41 is missing'),
(402, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat41.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat41Modified.png', '2019-02-08 09:30:48.000000', '2019-02-08 09:15:47.000000', '2019-02-08 09:16:24.000000', 1, 9, 42, 191, ''),
(403, 1, '', '', '2019-02-08 09:30:48.000000', '2019-02-08 09:15:01.000000', '2019-02-08 09:15:47.000000', 1, 9, 42, 190, ''),
(404, 1, '', '', '2019-02-08 09:30:48.000000', '2019-02-08 09:09:05.000000', '2019-02-08 09:09:08.000000', 1, 9, 12, 36, ''),
(405, 1, '', '', '2019-02-08 09:30:48.000000', '2019-02-08 09:09:25.000000', '2019-02-08 09:09:30.000000', 1, 9, 12, 38, ''),
(406, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_test_keyword_filter_dat12.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_test_keyword_filter_dat12Modified.png', '2019-02-08 09:30:48.000000', '2019-02-08 09:09:08.000000', '2019-02-08 09:09:25.000000', 1, 9, 12, 37, ''),
(407, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_selected_room_screen_dat12.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_selected_room_screen_dat12Modified.png', '2019-02-08 09:30:48.000000', '2019-02-08 09:09:46.000000', '2019-02-08 09:10:06.000000', 1, 9, 12, 40, ''),
(408, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_room_keyword_filter_dat12.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_room_keyword_filter_dat12Modified.png', '2019-02-08 09:30:48.000000', '2019-02-08 09:09:30.000000', '2019-02-08 09:09:46.000000', 1, 9, 12, 39, ''),
(409, 1, '', '', '2019-02-08 09:30:49.000000', '2019-02-08 09:17:46.000000', '2019-02-08 09:17:49.000000', 1, 9, 63, 278, ''),
(410, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat61.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/room_preview_layout_dat61Modified.png', '2019-02-08 09:30:49.000000', '2019-02-08 09:18:04.000000', '2019-02-08 09:18:11.000000', 1, 9, 63, 280, ''),
(411, 1, '', '', '2019-02-08 09:30:49.000000', '2019-02-08 09:17:51.000000', '2019-02-08 09:18:04.000000', 1, 9, 63, 279, ''),
(412, 1, '', '', '2019-02-08 09:30:49.000000', '2019-02-08 09:18:11.000000', '2019-02-08 09:18:12.000000', 1, 9, 63, 281, ''),
(413, 1, '', '', '2019-02-08 09:30:49.000000', '2019-02-08 09:21:10.000000', '2019-02-08 09:21:13.000000', 1, 9, 120, 508, ''),
(414, 1, '', '', '2019-02-08 09:30:49.000000', '2019-02-08 09:21:42.000000', '2019-02-08 09:21:53.000000', 1, 9, 120, 510, ''),
(415, 1, '', '', '2019-02-08 09:30:49.000000', '2019-02-08 09:21:13.000000', '2019-02-08 09:21:42.000000', 1, 9, 120, 509, ''),
(416, 1, '', '', '2019-02-08 09:30:49.000000', '2019-02-08 09:22:04.000000', '2019-02-08 09:22:11.000000', 1, 9, 120, 512, ''),
(417, 1, '', '', '2019-02-08 09:30:49.000000', '2019-02-08 09:21:53.000000', '2019-02-08 09:22:04.000000', 1, 9, 120, 511, ''),
(418, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_sharing_screen.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_sharing_screenModified.png', '2019-02-08 09:30:49.000000', '2019-02-08 09:22:17.000000', '2019-02-08 09:22:25.000000', 1, 9, 120, 514, ''),
(419, 1, '', '', '2019-02-08 09:30:49.000000', '2019-02-08 09:22:11.000000', '2019-02-08 09:22:17.000000', 1, 9, 120, 513, ''),
(420, 1, '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_sharing_world_view_screen.png', '2019-02-08_Pixel2_8.0.0_test_consolidation/verify_sharing_world_view_screenModified.png', '2019-02-08 09:30:49.000000', '2019-02-08 09:22:25.000000', '2019-02-08 09:22:32.000000', 1, 9, 120, 515, '');

-- --------------------------------------------------------

--
-- Table structure for table `reports_teststeps`
--

CREATE TABLE `reports_teststeps` (
  `id` int(11) NOT NULL,
  `test_step` longtext NOT NULL,
  `test_step_desc` longtext NOT NULL,
  `golden_screen` longtext NOT NULL,
  `created` datetime(6) NOT NULL,
  `test_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports_teststeps`
--

INSERT INTO `reports_teststeps` (`id`, `test_step`, `test_step_desc`, `golden_screen`, `created`, `test_id`) VALUES
(1, '1. App should be launched successfully.', '', '', '2019-02-05 19:24:42.717288', 1),
(2, '2. Onboarding message saying - "Welcome to the New Hangouts Chat - An Intelligent communication app built for teams" should be displayed with a "Get started" button', '', '', '2019-02-05 19:24:42.767490', 1),
(3, '1. Choose an account should be displayed with all accounts associated with device', '', '', '2019-02-05 19:24:42.879483', 2),
(4, '2. Add account option should also be available', '', '', '2019-02-05 19:24:43.044907', 2),
(5, '3. On choosing account "cancel" and "ok" buttons should be tappable"', '', '', '2019-02-05 19:24:43.089373', 2),
(6, '1. App should be launched successfully', '', '', '2019-02-05 19:24:43.167850', 3),
(7, '2. Choose an account should be displayed with all accounts associated with device', '', '', '2019-02-05 19:24:43.211222', 3),
(8, '3. "Allow chat to notify you of new messages" dialog should appear on first launch', '', '', '2019-02-05 19:24:43.255562', 3),
(9, '4. Verify whether World view page is displayed', '', '', '2019-02-05 19:24:43.299915', 3),
(10, '1. App should be launched successfully', '', '', '2019-02-05 19:24:43.345636', 4),
(11, '2. Choose an account should be displayed with all accounts associated with device', '', '', '2019-02-05 19:24:43.388978', 4),
(12, '3. "You don\'t have access to Hangouts Chat - Your account doesn\'t have access. To use Hangouts Chat during this early access period. - Exit and try a different account" Page should be displayed', '', '', '2019-02-05 19:24:43.433332', 4),
(13, '4. Upon tapping "Exit and try a different account" application should Exit.', '', '', '2019-02-05 19:24:43.477565', 4),
(14, '1. App should be launched successfully', '', '', '2019-02-05 19:24:43.523328', 5),
(15, '2. Choose an account should be displayed with all accounts associated with device', '', '', '2019-02-05 19:24:43.566347', 5),
(16, '3. Add White listed account and verify if application has launched', '', '', '2019-02-05 19:24:43.610645', 5),
(17, '4. Add non white listed account and verify if application displayed "You don\'t have access to Hangouts Chat - Your account doesn\'t have access to use Hangouts Chat during this early access period. - Exit and try a different account" page.', '', '', '2019-02-05 19:24:43.655176', 5),
(18, '1. No Network Connection - You need to be connected to the internet to log in. Please check your network connection and try again - Try Again', '', '', '2019-02-05 19:24:43.700015', 6),
(19, '2. Tap on Try again and Verify application should remain on "Welcome to the New Hangouts Chat - An Intelligent Communication app built for teams" should be displayed with a "Get started" button', '', '', '2019-02-05 19:24:43.743958', 6),
(20, '1. App must be launched successfully', '', '', '2019-02-05 19:24:43.789964', 7),
(21, '2. The following must be displayed properly without any UI issues. a) The header name - Username and active/DND status below it with presence indicator. b). FAB button c). Sections.- Unread, Starred, Recent d). Presence indicator - against member names for DM\'s e). Longer room names with ellipses. f). badge icons with count for unread messages. g). Mute icon if rooms are muted from room details screen. h). Timestamp as per last last activity of the room/DM.', '', '', '2019-02-05 19:24:43.832870', 7),
(22, '1. User Should be able to launch the app.', '', '', '2019-02-05 19:24:43.879960', 8),
(23, '2. World view must be displayed with three sections : \'Unread\', \'Starred\', \'Recent\'.', '', '', '2019-02-05 19:24:43.921447', 8),
(24, '3. Unread rooms must be displayed under "Unread" section that are sorted reverse chronologically .', '', '', '2019-02-05 19:24:43.965964', 8),
(25, '4. Rooms must have the star icon(for starred rooms), badge count (if unread topics) along with the timestamp a) Within last min: "Now" b) Within last hour: "X mins" c)Earlier today: "9:41 AM" Yesterday (older than last 24 hrs): "Yesterday" d) Last six days: "Tue" e) Last 11 months: "Apr 26" f) Older: "Apr 2015"', '', '', '2019-02-05 19:24:44.010339', 8),
(26, '1. User Should be able to launch the app.', '', '', '2019-02-05 19:24:44.056609', 9),
(27, '2. World view must be displayed with three sections : \'Unread\', \'Starred\', \'Recent\'.', '', '', '2019-02-05 19:24:44.099305', 9),
(28, '3. Recent rooms must be displayed under "Recent" section that are sorted reverse chronologically .', '', '', '2019-02-05 19:24:44.143516', 9),
(29, '4. Timestamp is displayed as per the below standards a) Within last min: "Now" b) Within last hour: "X mins" c) Earlier today: "9:41 AM" d) Yesterday (older than last 24 hrs): "Yesterday" e)Last six days: "Tue" f)Last 11 months: "Apr 26" g)Older: "Apr 2015" ', '', '', '2019-02-05 19:24:44.187782', 9),
(30, '1. Starred rooms are rendered in A-Z order (NOT timestamps).', '', '', '2019-02-05 19:24:44.233027', 10),
(31, '2. Uppercase and lowercase don\'t differentiate in Starred section.', '', '', '2019-02-05 19:24:44.321758', 10),
(32, '3. Star icon must be displayed while filtering rooms for unread and read rooms.', '', '', '2019-02-05 19:24:44.377747', 10),
(33, '1. User should be able to launch the app', '', '', '2019-02-05 19:24:44.435216', 11),
(34, '2. The FAB button must be present in the bottom right corner of the screen and always in the visible region(If the user has invite to rooms, the count will be displayed above the fab button)', '', '', '2019-02-05 19:24:44.488858', 11),
(35, '3. Conversation launcher must be displayed along with their corresponding options a) Group message b) Message a bot c) Create room d) Browse rooms (If the user has invite to rooms, the count will be displayed near the fab button which when tapped on FAB button shows up across \'Browse room\').', '', '', '2019-02-05 19:24:44.544269', 11),
(36, '1. User should be able to launch the app', '', '', '2019-02-05 19:24:44.605215', 12),
(37, '2. The filter rooms/DM search box along with the filter icon should be displayed with the hint text - "Find room or DM"', '', '', '2019-02-05 19:24:44.666148', 12),
(38, '3. Rooms/DM\'s related to the keyword must be displayed along with timestamp must be displayed', '', '', '2019-02-05 19:24:44.743971', 12),
(39, '4. The entered keyword must be cleared and the world view must be refreshed with the latest updates.', '', '', '2019-02-05 19:24:44.799533', 12),
(40, '5. user should be directed to the selected room/DM.', '', '', '2019-02-05 19:24:44.854765', 12),
(41, '1. App should be successfully launched', '', '', '2019-02-05 19:24:44.911749', 13),
(42, '2. Verify that Header should be "Create room"', '', '', '2019-02-05 19:24:44.965808', 13),
(43, '3. Verify close button is available for Add room at top left.', '', '', '2019-02-05 19:24:45.021317', 13),
(44, '4. Verify if user is able to enter text in \'Room name\' compose field', '', '', '2019-02-05 19:24:45.076587', 13),
(45, '5. Verify if keyboard pops up automatically upon tapping Create Room', '', '', '2019-02-05 19:24:45.132036', 13),
(46, '6. Verify if Tick mark enables on entering text, and gets disabled if no text in compose field.', '', '', '2019-02-05 19:24:45.187435', 13),
(47, '1. App should be successfully launched', '', '', '2019-02-05 19:24:45.244323', 14),
(48, '2. Verify if app opens "Create Room" page', '', '', '2019-02-05 19:24:45.298471', 14),
(49, '3. Verify if user able to enter text in "room name" compose field.', '', '', '2019-02-05 19:24:45.353920', 14),
(50, '4. Verify if you greyed out tickmark is enabled(teal color) on text input.', '', '', '2019-02-05 19:24:45.409443', 14),
(51, '5. Tapping on tick mark should create a room with name entered and the new room view is presented.', '', '', '2019-02-05 19:24:45.464971', 14),
(52, '6. Verify if "Give your room a name" banner appears upon typing text and clearing it.', '', '', '2019-02-05 19:24:45.520415', 14),
(53, '1. Title of the section: "Browse rooms"', '', '', '2019-02-05 19:24:45.576919', 15),
(54, '2. Text box should be available to filter rooms with place holder: Find a room.', '', '', '2019-02-05 19:24:45.631265', 15),
(55, '3. Invited rooms and rooms you have left previously should be listed below.', '', '', '2019-02-05 19:24:45.686795', 15),
(56, '4. "find a room" compose box should be visible.', '', '', '2019-02-05 19:24:45.742136', 15),
(57, '1. App should be successfully launched', '', '', '2019-02-05 19:24:45.799275', 16),
(58, '2. Verify if app opens \'Browse Rooms\' page', '', '', '2019-02-05 19:24:45.853444', 16),
(59, '3. On tapping "+" button, user should be able to join the room', '', '', '2019-02-05 19:24:45.908786', 16),
(60, '4. Toast message should be displayed at bottom upon joining', '', '', '2019-02-05 19:24:45.964020', 16),
(61, '5. On tapping tick mark button user should leave the Room', '', '', '2019-02-05 19:24:46.019459', 16),
(62, '6. Toast message should be displayed at bottom upon leaving', '', '', '2019-02-05 19:24:46.075210', 16),
(63, '7. Verify joining the room with "join" buttons displayed at the bottom of room preview and "room details" of room preview.', '', '', '2019-02-05 19:24:46.130522', 16),
(64, '1. App should be successfully launched', '', '', '2019-02-05 19:24:46.186251', 17),
(65, '2. Verify if app opens Browse rooms page', '', '', '2019-02-05 19:24:46.231455', 17),
(66, '3. Tapping on eye icon it should redirect to Room Preview.', '', '', '2019-02-05 19:24:46.275907', 17),
(67, '4. All the content of the room with \'JOIN" button at the bottom should be displayed.', '', '', '2019-02-05 19:24:46.320222', 17),
(68, '5. In room details, "join" button with MEMBERS list should be displayed.', '', '', '2019-02-05 19:24:46.364777', 17),
(69, '1. App should be successfully launched', '', '', '2019-02-05 19:24:46.409745', 18),
(70, '2. Verify if app opens Browse Rooms page', '', '', '2019-02-05 19:24:46.453518', 18),
(71, '3. Verify the Members count of Rooms is correct under rooms listed and header of room preview.', '', '', '2019-02-05 19:24:46.497846', 18),
(72, '1. App should be successfully launched', '', '', '2019-02-05 19:24:46.544015', 19),
(73, '2. Verify if app opens Create Room page', '', '', '2019-02-05 19:24:46.619648', 19),
(74, '3. Header should have "No Connection" banner.', '', '', '2019-02-05 19:24:46.664239', 19),
(75, '4. On creating room it should display a toast message: "This room couldn\'t be created. Try again in few minutes".', '', '', '2019-02-05 19:24:46.708675', 19),
(76, '1. App should be successfully launched', '', '', '2019-02-05 19:24:46.753759', 20),
(77, '2. Verify if New (red label) is available for Rooms you\'ve been invite to', '', '', '2019-02-05 19:24:46.797462', 20),
(78, '3. New (red label) should not be visible once add room window is closed', '', '', '2019-02-05 19:24:46.841795', 20),
(79, '1. App should be launched successfully', '', '', '2019-02-05 19:24:46.953961', 21),
(80, '2. Group message, Message a bot and Create room, Browse Rooms options should be displayed.', '', '', '2019-02-05 19:24:46.997156', 21),
(81, '3. Browse rooms screen must be displayed', '', '', '2019-02-05 19:24:47.041526', 21),
(82, '4. Room preview screen for the selected room must be displayed.', '', '', '2019-02-05 19:24:47.085960', 21),
(83, '5. User should be taken back to world view', '', '', '2019-02-05 19:24:47.130239', 21),
(84, '1. App should be launched successfully', '', '', '2019-02-05 19:24:47.176690', 22),
(85, '2. Group message, Message a bot and Create room, Browse Rooms options should be displayed.', '', '', '2019-02-05 19:24:47.219052', 22),
(86, '3. Add room screen must be displayed', '', '', '2019-02-05 19:24:47.263430', 22),
(87, '4. Room preview screen for the selected room must be displayed.Join button along with the room name should be displayed in the bottom of the screen.', '', '', '2019-02-05 19:24:47.307873', 22),
(88, '5. Room details screen should be displayed along with "Join" link (room name should be greyed out)', '', '', '2019-02-05 19:24:47.352105', 22),
(89, '6. User should join the room and user should be directed to space view with a toast message saying "You Joined <<RoomName>>"', '', '', '2019-02-05 19:24:47.396598', 22),
(90, '1. App should be launched successfully', '', '', '2019-02-05 19:24:47.442242', 23),
(91, '2. Group message, Message a bot,Create a Room, Browse rooms options should be displayed.', '', '', '2019-02-05 19:24:47.485350', 23),
(92, '3. Browse rooms screen must be displayed', '', '', '2019-02-05 19:24:47.529772', 23),
(93, '4. Room preview screen for the selected room must be displayed.', '', '', '2019-02-05 19:24:47.585285', 23),
(94, '5. User should join the room and user should be directed to space view with a toast message saying "You Joined <<roomname>>"', '', '', '2019-02-05 19:24:47.629918', 23),
(95, '1. App should be launched successfully', '', '', '2019-02-05 19:24:47.676680', 24),
(96, '2. Room, Direct message options should be displayed.', '', '', '2019-02-05 19:24:47.718847', 24),
(97, '3. Add room screen must be displayed', '', '', '2019-02-05 19:24:47.763221', 24),
(98, '4. Room preview screen for the selected room must be displayed.', '', '', '2019-02-05 19:24:47.807769', 24),
(99, '5. The collapsed messages summary in the Room Preview must be displayed with the following: a) Count of collapsed msgs (up to 99+) b) Summary of senders should not be displayed in room preview', '', '', '2019-02-05 19:24:47.852065', 24),
(100, '6. Collapsed summary should expand and show 10 messages at once and remaining messages should still be collapsed with collapsed count - 10 as the count ', '', '', '2019-02-05 19:24:47.896493', 24),
(101, '1. App should be launched successfully', '', '', '2019-02-05 19:24:47.942710', 25),
(102, '2. Room, Direct message options should be displayed.', '', '', '2019-02-05 19:24:47.985323', 25),
(103, '3. Browse Rooms screen must be displayed', '', '', '2019-02-05 19:24:48.029735', 25),
(104, '4. Room preview screen for the selected room must be displayed.', '', '', '2019-02-05 19:24:48.074116', 25),
(105, '5. The room should be rendered with data ( image, doc files, meet video chip, web chips/URL\'s, CML attachments) properly without any UI issues. a) The date dividers, timestamps, labels(eg: edited), must be displayed as in space view b) A faded"Join to reply" label should be displayed for each topic displayed in room preview', '', '', '2019-02-05 19:24:48.118513', 25),
(106, '6. Room preview should render the system messages and its appropriate icons properly without any UI issues.', '', '', '2019-02-05 19:24:48.162747', 25),
(107, '1. App should be launched successfully', '', '', '2019-02-05 19:24:48.207640', 26),
(108, '2. Room, Direct message options should be displayed.', '', '', '2019-02-05 19:24:48.251419', 26),
(109, '3. Add room screen must be displayed', '', '', '2019-02-05 19:24:48.295973', 26),
(110, '4. Room preview screen for the selected room must be displayed.Join button along with the room name should be displayed in the bottom of the screen.', '', '', '2019-02-05 19:24:48.340480', 26),
(111, '5. User should be displayed an overlay displaying two options: a) Copy text b) Send feedback on this message.', '', '', '2019-02-05 19:24:48.384655', 26),
(112, '1. User should be able to launch the app', '', '', '2019-02-05 19:24:48.430831', 27),
(113, '2. The FAB button must be present in the bottom right corner of the screen and always in the visible region.', '', '', '2019-02-05 19:24:48.484698', 27),
(114, '3. Conversation launcher screen should be displayed with focus in "Find people" search field', '', '', '2019-02-05 19:24:48.540134', 27),
(115, '4. "Group message", "Message a bot", "Create room", "Browse rooms" options should be present below "find people" text box.', '', '', '2019-02-05 19:24:48.595583', 27),
(116, '5. Suggest contacts under "frequent" header should be present below "browse rooms"', '', '', '2019-02-05 19:24:48.664746', 27),
(117, '1. User should be able to launch the app', '', '', '2019-02-05 19:24:48.731898', 28),
(118, '2. The FAB button must be present in the bottom right corner of the screen and always in the visible region.', '', '', '2019-02-05 19:24:48.784766', 28),
(119, '3. Conversation launcher screen must be displayed along with their corresponding icons.', '', '', '2019-02-05 19:24:48.840175', 28),
(120, '4. Conversation launcher screen should be displayed with focus in "Find people" field, close button on top left corner.', '', '', '2019-02-05 19:24:48.895721', 28),
(121, '5. Direct message contacts with avatars, presence indicator, Username and email id should be displayed (if no contacts available this will be empty)', '', '', '2019-02-05 19:24:48.951236', 28),
(122, '6. All the contact suggestions related on the keyword entered must be displayed.', '', '', '2019-02-05 19:24:49.006733', 28),
(123, '7. The contacts suggestions display must reset after clearing the keyword.', '', '', '2019-02-05 19:24:49.101466', 28),
(124, '8. Selected contact DM Space view should be displayed', '', '', '2019-02-05 19:24:49.161776', 28),
(125, '9. Application should return to World view', '', '', '2019-02-05 19:24:49.217312', 28),
(126, '1. User should be able to launch the app', '', '', '2019-02-05 19:24:49.275714', 30),
(127, '2. The FAB button must be present in the bottom right corner of the screen and always in the visible region.', '', '', '2019-02-05 19:24:49.328202', 30),
(128, '3. Conversation launcher screen must be displayed along with their corresponding icons.', '', '', '2019-02-05 19:24:49.383769', 30),
(129, '4. Conversation launcher screen should be displayed with focus in "Find people" field, close button on top left corner.', '', '', '2019-02-05 19:24:49.439207', 30),
(130, '5. Direct message contacts with avatars, presence indicator, Username and email id should be displayed (if no contacts available this will be empty)', '', '', '2019-02-05 19:24:49.494529', 30),
(131, '6. The selected contacts must be displayed as chip in the search field with avatar and username.(chip gets removed on tapping it twice)', '', '', '2019-02-05 19:24:49.550084', 30),
(132, '7. User must be taken to the new DM screen with header being the name of the contacts selected and a back button on top left corner.', '', '', '2019-02-05 19:24:49.605636', 30),
(133, '8. On posting the a message in new DM, the group DM should be established.', '', '', '2019-02-05 19:24:49.660971', 30),
(134, '1. User should be able to launch the app', '', '', '2019-02-05 19:24:49.718984', 31),
(135, '2. The FAB button must be present in the bottom right corner of the screen and always in the visible region.', '', '', '2019-02-05 19:24:49.771791', 31),
(136, '3. Room and Direct message options must be displayed along with their corresponding icons.', '', '', '2019-02-05 19:24:49.849320', 31),
(137, '4. Direct message screen should be displayed with focus in "Search for people to message" field and header being "Direct Message", close button on top right corner and faded tick mark button.', '', '', '2019-02-05 19:24:49.904874', 31),
(138, '5. Direct message contacts with avatars, presence indicator, Username and email id should be displayed (if no contacts available this will be empty)', '', '', '2019-02-05 19:24:49.960340', 31),
(139, '6. The selected contact must be displayed as chip in the search field with avatar and username and the DM preview should be loaded.', '', '', '2019-02-05 19:24:50.015845', 31),
(140, '7. User must be taken to the DM view screen with header being the name of the contact selected and a back button in the top left corner.', '', '', '2019-02-05 19:24:50.071456', 31),
(141, '8. User should be taken to the New conversation screen with educational promo saying "Off the record Messages in this conversation will be deleted after 24 hrs." and the OTR toggle button being highlighted.', '', '', '2019-02-05 19:24:50.127078', 31),
(142, '9. The label must now read: "New conversation" and toggle button should be set to off and the hint text must say - "Message <<username>>"', '', '', '2019-02-05 19:24:50.182329', 31),
(143, '10. The label must now read: "Off the record" and toggle button should be set to on and the hint text must say - "Message will be deleted after 24 hours"', '', '', '2019-02-05 19:24:50.238116', 31),
(144, '11. User should be able to successfully post the message and the posted message should be appended - [OFF-THE -RECORD] and the hint text in compose should still say - "Message will be deleted after 24 hours"', '', '', '2019-02-05 19:24:50.293528', 31),
(145, '1. App should be successfully launched', '', '', '2019-02-05 19:24:50.349880', 34),
(146, '2. Verify the header should be "DM details"', '', '', '2019-02-05 19:24:50.404441', 34),
(147, '3. Navigate back button should be available for DM details page', '', '', '2019-02-05 19:24:50.491532', 34),
(148, '4. Verify Star button.', '', '', '2019-02-05 19:24:50.548767', 34),
(149, '5. Verify Notifications toggle button', '', '', '2019-02-05 19:24:50.593157', 34),
(150, '6. Verify Hide button.', '', '', '2019-02-05 19:24:50.638633', 34),
(151, '1. App should be successfully launched', '', '', '2019-02-05 19:24:50.684164', 35),
(152, '2. On starring DM user conversation should be listed under Starred Section', '', '', '2019-02-05 19:24:50.727412', 35),
(153, '3. On Unstarring DM user conversation should not be listed under Starred Section', '', '', '2019-02-05 19:24:50.771948', 35),
(154, '4. Verify if icon color fills with green color upon starring', '', '', '2019-02-05 19:24:50.816316', 35),
(155, '5. Verify if icon color changes to white upon Unstarring', '', '', '2019-02-05 19:24:50.860838', 35),
(156, '1. App should be successfully launched', '', '', '2019-02-05 19:24:50.906427', 36),
(157, '2. Toggle off for Notifications for the DM should only receive @ mention Notifications', '', '', '2019-02-05 19:24:50.949400', 36),
(158, '3. Toggle On for Notifications for DM should receive all notifications', '', '', '2019-02-05 19:24:50.993814', 36),
(159, '4. Verify the Bell (mute) symbol on toggle off and on', '', '', '2019-02-05 19:24:51.038300', 36),
(160, '5. Verify the text "Only @mentions" on Notifications Toggle off', '', '', '2019-02-05 19:24:51.082582', 36),
(161, '6. Verify the text "General setting will apply" on Notifications Toggle on', '', '', '2019-02-05 19:24:51.127001', 36),
(162, '1. App should be successfully launched', '', '', '2019-02-05 19:24:51.172088', 37),
(163, '2. Verify if other is listed under this section along with you', '', '', '2019-02-05 19:24:51.237974', 37),
(164, '3. if any member is online, presence indicator should be ON with green color', '', '', '2019-02-05 19:24:51.283784', 37),
(165, '4. Verify both Members name and email\'s are mentioned ', '', '', '2019-02-05 19:24:51.326455', 37),
(166, '1. App should be successfully launched', '', '', '2019-02-05 19:24:51.372661', 38),
(167, '2. Toast message should display "Messages with \'username\' are hidden" with UNDO option and should Redirect to world view', '', '', '2019-02-05 19:24:51.415575', 38),
(168, '3. Type the same user name in "Find room or DM" from world view and verify if the DM conversation is not available', '', '', '2019-02-05 19:24:51.459852', 38),
(169, '4. Verify if hidden user sends a message you should receive notification and should Reappear in recent world view', '', '', '2019-02-05 19:24:51.504178', 38),
(170, '5. DM conversation should unhide when you search the same member from Search panel', '', '', '2019-02-05 19:24:51.548748', 38),
(171, '1. App should be launched successfully', '', '', '2019-02-05 19:24:51.595037', 39),
(172, '2. User should be directed to space view', '', '', '2019-02-05 19:24:51.637729', 39),
(173, '3. The nav bar should contain a) the room name with a carret symbol(long room name should be ellipsized properly) b)Nav bar should contain the member count eg: "2 members" c) Search icon d) Back button', '', '', '2019-02-05 19:24:51.681933', 39),
(174, '4. User should be taken to the room details screen.', '', '', '2019-02-05 19:24:51.726597', 39),
(175, '5. User should be taken back to the space view', '', '', '2019-02-05 19:24:51.771134', 39),
(176, '6. User should be taken to the space search screen', '', '', '2019-02-05 19:24:51.815721', 39),
(177, '7. User should be taken back to the space view', '', '', '2019-02-05 19:24:51.860090', 39),
(178, '8. User should be taken to the world view', '', '', '2019-02-05 19:24:51.904385', 39),
(179, '1. App should be launched successfully', '', '', '2019-02-05 19:24:51.950509', 40),
(180, '2. User should be directed to the space view', '', '', '2019-02-05 19:24:51.993081', 40),
(181, '3. The persistent header must be displayed in the primary app color with the room creation information a) The Room name should be displayed b) Subtitle explaining who created the room eg: "[person] created this room" - "You" should be displayed in case if creator and logged in user is same. Timestamp on subtitle must be :Today Yesterday Tuesday, Apr 24 Tuesday, Apr 24, 2015 c) Avatar of the user created the room d) "Invite people" link', '', '', '2019-02-05 19:24:52.051095', 40),
(182, '1. App should be launched successfully', '', '', '2019-02-05 19:24:52.105824', 41),
(183, '2. User should be taken to space view of the selected room', '', '', '2019-02-05 19:24:52.148711', 41),
(184, '3. User should be taken to topic view', '', '', '2019-02-05 19:24:52.193190', 41),
(185, '4. User should be able to successfully post the message', '', '', '2019-02-05 19:24:52.237526', 41),
(186, '5. User should return to space view', '', '', '2019-02-05 19:24:52.281973', 41),
(187, '6. Topic replied should still be the last but one topic', '', '', '2019-02-05 19:24:52.337374', 41),
(188, '7. The topic must be at the last position in the thread indicating the latest.', '', '', '2019-02-05 19:24:52.392937', 41),
(189, '1. App should be launched successfully', '', '', '2019-02-05 19:24:52.450618', 42),
(190, '2. User should be taken to the space view', '', '', '2019-02-05 19:24:52.503777', 42),
(191, '3. There must a divider labelled - "Unread" and the unread topics must be displayed below it with appropriate date dividers and timestamps. "New" label will be displayed and then be replaced with a blue dot for new messages which perishes in short time.Date Dividers should be displayed as below: a) Today: "Today" b) Yesterday: "Yesterday" c) Last 11 mos: "Tuesday, Apr 24"* d) Older: "Tuesday, Apr 24, 2015"* e)Timestamps should be displayed as below: f) Within last min:"Now" g) Within last hour:"X mins" h) Earlier today: "9:41 AM" i) Yesterday (older than last 24 hrs): "Yesterday" j) Last six days:"Tue" k) Last 11 months: "Apr 26" l) Older: "Apr 2015" ', '', '', '2019-02-05 19:24:52.595549', 42),
(192, '1. App should be launched successfully', '', '', '2019-02-05 19:24:52.650055', 43),
(193, '2. User should be navigated to space view', '', '', '2019-02-05 19:24:52.725880', 43),
(194, '3. The collapsed messages summary in the space view must be displayed with the following a) Count of collapsed msgs (up to 99+) b) Summary of senders of the last 10+ collapsed msgs', '', '', '2019-02-05 19:24:52.859322', 43),
(195, '4. Collapsed summary should expand and show 10 messages at once and remaining messages should still be collapsed with collapsed count - 10 as the count and the summary of the senders.', '', '', '2019-02-05 19:24:52.948111', 43),
(196, '1. App should be launched successfully', '', '', '2019-02-05 19:24:53.003848', 44),
(197, '2. User should be navigated to space view', '', '', '2019-02-05 19:24:53.080944', 44),
(198, '3. The summary of the sender names should follow the below convention: a) You" is used instead of the user\'s name, and is bolded b) Msgs from the same person are deduped to one entry c) If there are > 10 collapsed msgs, the list is appended with an ellipsis (to indicate that the sender list does not represent all collapsed msgs) d) If only one collapsed name, show their full name If >1, show first names only Ellipsize if they don\'t fit', '', '', '2019-02-05 19:24:53.314439', 44),
(199, '1. App should be launched successfully', '', '', '2019-02-05 19:24:53.436816', 45),
(200, '2. User should be directed to the space view', '', '', '2019-02-05 19:24:53.558209', 45),
(201, '3. New Conversation view should be opened.', '', '', '2019-02-05 19:24:53.679992', 45),
(202, '4. User should return to space view', '', '', '2019-02-05 19:24:53.768594', 45),
(203, '1. App should be launched successfully', '', '', '2019-02-05 19:24:53.858030', 46),
(204, '2. User should be directed to space view', '', '', '2019-02-05 19:24:53.968303', 46),
(205, '3. The file opens in its native app(sheets/slides/docs)', '', '', '2019-02-05 19:24:54.079218', 46),
(206, '4. User returns back to space view', '', '', '2019-02-05 19:24:54.134259', 46),
(207, '1. App should be launched successfully', '', '', '2019-02-05 19:24:54.212314', 47),
(208, '2. User should be directed to the space view', '', '', '2019-02-05 19:24:54.312168', 47),
(209, '3. In transition from space to topic, the topic summaries from space should disappear, and then the messages in the topic being rendered should fade in.', '', '', '2019-02-05 19:24:54.367607', 47),
(210, '4. In the transition back from the topic and space (both using the system back button and the back button in the action bar), the messages on screen should smoothly translate from their current position to their position in the space', '', '', '2019-02-05 19:24:54.423074', 47),
(211, '1. App should be launched successfully', '', '', '2019-02-05 19:24:54.479315', 48),
(212, '2. User should be directed to the space view', '', '', '2019-02-05 19:24:54.533895', 48),
(213, '3. The room should be rendered with data ( image, doc files, meet video chip, web chips/URL\'s, CML attachments) properly without any UI issues', '', '', '2019-02-05 19:24:54.589368', 48),
(214, '1. System messages should be displayed ', '', '', '2019-02-05 19:24:54.645133', 51),
(215, '1. App should be launched successfully', '', '', '2019-02-05 19:24:54.690650', 52),
(216, '2. User should be taken to space view of the selected room', '', '', '2019-02-05 19:24:54.733723', 52),
(217, '3. User should be shown an overlay with three options: a) Copy Text b) Edit', '', '', '2019-02-05 19:24:54.778081', 52),
(218, '4. Send Feedback on this message', '', '', '2019-02-05 19:24:54.822484', 52),
(219, '5. Focus should shift to Compose field with old message', '', '', '2019-02-05 19:24:54.867059', 52),
(220, '6. Edited / Empty / Same message should get posted upon tapping send button and message should have edited tag', '', '', '2019-02-05 19:24:54.911308', 52),
(221, '1. App should be launched successfully', '', '', '2019-02-05 19:24:54.959509', 53),
(222, '2. User should be directed to the space view', '', '', '2019-02-05 19:24:55.000133', 53),
(223, '3. The persistent header must be displayed in the primary app color with the room creation information a) The Room name should be displayed b) Subtitle explaining who created the roomeg: "[person] created this room" - "You" should be displayed in case if creator and logged in user is same. c) Timestamp on subtitle must be :Today Yesterday Tuesday, Apr 24 Tuesday, Apr 24, 2015 d) Avatar of the user created the room e) "Add People & Bots" link', '', '', '2019-02-05 19:24:55.044646', 53),
(224, '4. Add page should open with "search for people and groups" field. along with contact suggestions.', '', '', '2019-02-05 19:24:55.090041', 53),
(225, '5. Contact should get selected and header should change to "Add 1 person"', '', '', '2019-02-05 19:24:55.134570', 53),
(226, '6. Multiple contacts should get selected and header should match the no. of contacts selected', '', '', '2019-02-05 19:24:55.178707', 53),
(227, '7. Group contact should get selected and header should show the count of total contacts in group', '', '', '2019-02-05 19:24:55.223513', 53),
(228, '8. User should return to space view', '', '', '2019-02-05 19:24:55.267567', 53),
(229, '9. Corresponding system message should generate at the bottom of the space view', '', '', '2019-02-05 19:24:55.312176', 53),
(230, '1. App should be successfully launched', '', '', '2019-02-05 19:24:55.357148', 55),
(231, '2. Verify the header should be "Room details"', '', '', '2019-02-05 19:24:55.400528', 55),
(232, '3. Navigate back button should be available for Room details page', '', '', '2019-02-05 19:24:55.444911', 55),
(233, '4. Verify the Ui of Room details page', '', '', '2019-02-05 19:24:55.489501', 55),
(234, '1. App should be successfully launched', '', '', '2019-02-05 19:24:55.535566', 56),
(235, '2. Verify if Room name is editable', '', '', '2019-02-05 19:24:55.578387', 56),
(236, '3. On editing Right symbol should appear beside Room details', '', '', '2019-02-05 19:24:55.622681', 56),
(237, '4. Editing Room name should intake all special characters & emoji\'s', '', '', '2019-02-05 19:24:55.667202', 56),
(238, '5. On keeping Room name empty and same previous name Right Symbol should not appear', '', '', '2019-02-05 19:24:55.711554', 56),
(239, '6. On Editing room name and tapping on right symbol, Room name should change and toast should display like "Successfully changed room name to (new name)', '', '', '2019-02-05 19:24:55.756047', 56),
(240, '1. App should be successfully launched', '', '', '2019-02-05 19:24:55.801404', 57),
(241, '2. Verify if Add people page opens', '', '', '2019-02-05 19:24:55.844891', 57),
(242, '3. Verify the Ui of Add people page', '', '', '2019-02-05 19:24:55.889231', 57),
(243, '4. On closing Add people page app should stay on Room details page', '', '', '2019-02-05 19:24:55.933636', 57),
(244, '5. Verify if we can Add single or multiple users to the Room', '', '', '2019-02-05 19:24:55.978130', 57),
(245, '1. App should be successfully launched', '', '', '2019-02-05 19:24:56.023526', 58),
(246, '2. On starring Room should be listed under Starred Section', '', '', '2019-02-05 19:24:56.066849', 58),
(247, '3. On Unstarring Room should not be listed under Starred Section', '', '', '2019-02-05 19:24:56.111226', 58),
(248, '4. Verify if icon color fills with green color upon starring', '', '', '2019-02-05 19:24:56.155767', 58),
(249, '5. Verify if icon color changes to white upon Unstarring', '', '', '2019-02-05 19:24:56.200110', 58),
(250, '1. App should be successfully launched', '', '', '2019-02-05 19:24:56.245792', 59),
(251, '2. Toggle off for Notifications for the Room should only receive @ mention Notifications', '', '', '2019-02-05 19:24:56.289245', 59),
(252, '3. Toggle On for Notifications for the Room should receive all notifications', '', '', '2019-02-05 19:24:56.333098', 59),
(253, '4. Verify the Bell (mute) symbol on toggle off and on', '', '', '2019-02-05 19:24:56.377659', 59),
(254, '5. Verify the text "Only @mentions" on Notifications Toggle off', '', '', '2019-02-05 19:24:56.422073', 59),
(255, '6. Verify the text "General setting will apply" on Notifications Toggle on', '', '', '2019-02-05 19:24:56.466432', 59),
(256, '1. App should be successfully launched', '', '', '2019-02-05 19:24:56.512385', 60),
(257, '2. Verify the text "you will be able to return" under Leave option', '', '', '2019-02-05 19:24:56.566445', 60),
(258, '3. Tapping on leave should show a dialog message stating "Leave (room name) ? - You won\'t receive updates or be able to post messages Until you rejoin the Room, but you can rejoin at any time"', '', '', '2019-02-05 19:24:56.621730', 60),
(259, '4. Verify both cancel and leave room buttons are working', '', '', '2019-02-05 19:24:56.690155', 60),
(260, '5. Tapping on Leave room should redirect to World view with a toast message stating "You left (room name)"', '', '', '2019-02-05 19:24:56.765952', 60),
(261, '1. App should be successfully launched', '', '', '2019-02-05 19:24:56.868087', 61),
(262, '2. Verify if all the Room members are listed under this section', '', '', '2019-02-05 19:24:56.943996', 61),
(263, '3. if any member is online, presence indicator should be ON with green color', '', '', '2019-02-05 19:24:56.999358', 61),
(264, '4. Verify Members name and Members email\'s are mentioned', '', '', '2019-02-05 19:24:57.054913', 61),
(265, '5. Tapping on member menu, it should show Remove from room option', '', '', '2019-02-05 19:24:57.110385', 61),
(266, '6. Tapping on Remove from room, it should display a dialog message stating " Remove (username) from (room name) ? - (user name) will lose all access To this room and will not be able to join until invited again"', '', '', '2019-02-05 19:24:57.165788', 61),
(267, '7. Verify both cancel and remove buttons are working', '', '', '2019-02-05 19:24:57.221186', 61),
(268, '8. Tapping on remove should remove the user without any toast message', '', '', '2019-02-05 19:24:57.276833', 61),
(269, '1. App should be successfully launched', '', '', '2019-02-05 19:24:57.332481', 62),
(270, '2. Verify the text "These people can view and join this room at any Time , but will not receive updates unless mentioned" written under Can Join', '', '', '2019-02-05 19:24:57.387545', 62),
(271, '3. Verify if all invited members are listed under this section', '', '', '2019-02-05 19:24:57.443127', 62),
(272, '4. if any member is online, presence indicator should be ON with green color', '', '', '2019-02-05 19:24:57.498738', 62),
(273, '5. Verify Members name and Members email\'s are mentioned', '', '', '2019-02-05 19:24:57.553968', 62),
(274, '6. Tapping on member menu, it should show Remove from room option', '', '', '2019-02-05 19:24:57.609243', 62),
(275, '7. Tapping on Remove from room, it should display a dialog message stating "Remove (username) from (room name) ? - (user name) will lose all access To this room and will not be able to join until invited again"', '', '', '2019-02-05 19:24:57.665176', 62),
(276, '8. Verify both cancel and remove buttons are working', '', '', '2019-02-05 19:24:57.720422', 62),
(277, '9. Tapping on remove should remove the user without any toast message', '', '', '2019-02-05 19:24:57.776090', 62),
(278, '1. App should be launched successfully', '', '', '2019-02-05 19:24:57.833543', 63),
(279, '2. User should be taken to the space view', '', '', '2019-02-05 19:24:57.886789', 63),
(280, '3. User should be taken to the topic view of the selected topic.', '', '', '2019-02-05 19:24:57.942289', 63),
(281, '4. The following verifications should be successful a) All the messages posted should contain the Username, profile avatar and the timestamps displayed properly b) The edited messages should have the "edited" label displayed c) All the links, chips, files must be rendered properly without any UI issues. d) The compose bar should have the hint text: "Reply in <<RoomName>>"', '', '', '2019-02-05 19:24:57.997857', 63),
(282, '1. App should be launched successfully', '', '', '2019-02-05 19:24:58.054698', 64),
(283, '2. User should be taken to the space view', '', '', '2019-02-05 19:24:58.108718', 64),
(284, '3. User should be taken to the new topic view', '', '', '2019-02-05 19:24:58.164211', 64),
(285, '4. The following verifications should be successful:a) A label Saying "New Conversation" should be displayed in primary app color b) The compose bar should have the hint text: "Message <<RoomName>>?"', '', '', '2019-02-05 19:24:58.219812', 64),
(286, '1. App should be launched successfully', '', '', '2019-02-05 19:24:58.276806', 66),
(287, '2. User should be taken to the space view', '', '', '2019-02-05 19:24:58.330654', 66),
(288, '3. User should be taken to the topic view of the selected topic.', '', '', '2019-02-05 19:24:58.386333', 66),
(289, '4. The compose field should contain the following components a) Compose text field with hint text b) Upload icon c) Camera icon e) Thor icon(Hangouts meet) f ) Send button', '', '', '2019-02-05 19:24:58.441708', 66),
(290, '1. App should be launched successfully', '', '', '2019-02-05 19:24:58.498859', 67),
(291, '2. User should be taken to the space view', '', '', '2019-02-05 19:24:58.552690', 67),
(292, '3. User should be taken to the topic view of the selected topic.', '', '', '2019-02-05 19:24:58.607856', 67),
(293, '4. The send button must be disabled and should be in grey color.', '', '', '2019-02-05 19:24:58.663641', 67),
(294, '5. Send button should be enabled and in primary app color when enabled.', '', '', '2019-02-05 19:24:58.708116', 67),
(295, '6. The send button must be disabled and should be in grey color.', '', '', '2019-02-05 19:24:58.752455', 67),
(296, '7. Send button should be enabled and in primary app color when enabled', '', '', '2019-02-05 19:24:58.796709', 67),
(297, '8. Send button should be enabled mode only.', '', '', '2019-02-05 19:24:58.841095', 67),
(298, '9. Message with the attachment should be sent successfully', '', '', '2019-02-05 19:24:58.885763', 67),
(299, '1. App should be launched successfully', '', '', '2019-02-05 19:24:58.931250', 68),
(300, '2. User should be taken to the space view', '', '', '2019-02-05 19:24:58.974310', 68),
(301, '3. User should be taken to the topic view of the selected topic.', '', '', '2019-02-05 19:24:59.018683', 68),
(302, '4. User should be directed to "Select a Photo" screen', '', '', '2019-02-05 19:24:59.063149', 68),
(303, '5. User selected file should be shown as attachment with a cancel button on the chip generated', '', '', '2019-02-05 19:24:59.107484', 68),
(304, '6. User should be able to post the message successfully.', '', '', '2019-02-05 19:24:59.151970', 68),
(305, '1. App should be launched successfully', '', '', '2019-02-05 19:24:59.198215', 69),
(306, '2. User should be taken to the space view', '', '', '2019-02-05 19:24:59.240841', 69),
(307, '3. User should be taken to the topic view of the selected topic.', '', '', '2019-02-05 19:24:59.285296', 69),
(308, '4. Capture screen should be displayed (close button on top left )', '', '', '2019-02-05 19:24:59.329556', 69),
(309, '5. image captured must be displayed with a return button and a tick mark button', '', '', '2019-02-05 19:24:59.374043', 69),
(310, '6. Capture screen should be displayed (close button on top left )', '', '', '2019-02-05 19:24:59.418362', 69),
(311, '7. image captured must be displayed with a return button and a tick mark button', '', '', '2019-02-05 19:24:59.462658', 69),
(312, '8. User should return to topic view with the captured image displayed as chip in compose with a cancel button on top right.', '', '', '2019-02-05 19:24:59.508286', 69),
(313, '9. User should be able to post the captured image successfully.', '', '', '2019-02-05 19:24:59.552703', 69),
(314, '1. App should be launched successfully', '', '', '2019-02-05 19:24:59.598501', 70),
(315, '2. User should be taken to the space view', '', '', '2019-02-05 19:24:59.641457', 70),
(316, '3. User should be taken to the topic view of the selected topic.', '', '', '2019-02-05 19:24:59.686042', 70),
(317, '4. Video meeting chip should be generated', '', '', '2019-02-05 19:24:59.730336', 70),
(318, '5. The video meeting chip should have the label - "Video meeting" with a subtitle - "Hangouts Meet" and cancel button on top right with another label - "Join Video Meeting" with meet icon.', '', '', '2019-02-05 19:24:59.774645', 70),
(319, '6. User should be able to post the video meet chip successfully', '', '', '2019-02-05 19:24:59.886264', 70),
(320, '1. App should be launched successfully', '', '', '2019-02-05 19:24:59.934893', 71),
(321, '2. User should be taken to the space view', '', '', '2019-02-05 19:24:59.975611', 71),
(322, '3. User should be taken to the topic view of the selected topic.', '', '', '2019-02-05 19:25:00.019945', 71),
(323, '4. Web chip for the typed URL should be displayed', '', '', '2019-02-05 19:25:00.064514', 71),
(324, '5. Replacing current item?" alert should be displayed', '', '', '2019-02-05 19:25:00.108887', 71),
(325, '6. User should be directed to "Select a Photo" screen', '', '', '2019-02-05 19:25:00.186377', 71),
(326, '7. User selected file should be shown as attachment with a cancel button on the chip generated', '', '', '2019-02-05 19:25:00.264231', 71),
(327, '8. "Replacing current item?" alert should be displayed', '', '', '2019-02-05 19:25:00.319399', 71),
(328, '9. "Replacing current item?" alert displayed should contain the text: "Messages can only have one image, file or website preview attached. The new item will replace the current one" with "cancel" and "replace" buttons.', '', '', '2019-02-05 19:25:00.375236', 71),
(329, '10. The dialog should be dismissed and the existing attachment should be retained.', '', '', '2019-02-05 19:25:00.430549', 71),
(330, '11. "Replacing current item?" alert should be displayed', '', '', '2019-02-05 19:25:00.486000', 71),
(331, '12. Image capture screen should be displayed', '', '', '2019-02-05 19:25:00.541309', 71),
(332, '13. Capture image chip should be generated and displayed in the compose', '', '', '2019-02-05 19:25:00.596824', 71),
(333, '14. The attachment should be replaced with the camera captured image chip.', '', '', '2019-02-05 19:25:00.652034', 71),
(334, '15. "Replacing current item?" alert should be displayed', '', '', '2019-02-05 19:25:00.707706', 71),
(335, '16. Dialog should be replaced with the Video meet chip', '', '', '2019-02-05 19:25:00.763357', 71),
(336, '17. Verify the chip generated', '', '', '2019-02-05 19:25:00.818723', 71),
(337, '18. Tap on send button', '', '', '2019-02-05 19:25:00.874147', 71),
(338, '1. App should be launched successfully.', '', '', '2019-02-05 19:25:00.931047', 73),
(339, '2. User should be taken to space view', '', '', '2019-02-05 19:25:00.984997', 73),
(340, '3. User should be taken to topic view', '', '', '2019-02-05 19:25:01.040645', 73),
(341, '4. User should be able to post the message successfully.', '', '', '2019-02-05 19:25:01.096115', 73),
(342, '5. All the messages posted under 1 min interval must be coalesced and displayed under one avatar and username and one timestamp.', '', '', '2019-02-05 19:25:01.151561', 73),
(343, '6. the message should posted after 1 min interval should be separately displayed under avatar and username', '', '', '2019-02-05 19:25:01.207038', 73),
(344, '1. App should be launched successfully', '', '', '2019-02-05 19:25:01.263915', 74),
(345, '2. User should be taken to space view of the selected room', '', '', '2019-02-05 19:25:01.317875', 74),
(346, '3. User should navigate to topic view', '', '', '2019-02-05 19:25:01.373438', 74),
(347, '4. User should be shown an overlay with three options Copy Text, Edit, Send Feedback on this message', '', '', '2019-02-05 19:25:01.428764', 74),
(348, '5. Focus should shift to Compose field with old message', '', '', '2019-02-05 19:25:01.484315', 74),
(349, '6. Edited / Empty / Same message should get posted upon tapping send button and message should have edited tag', '', '', '2019-02-05 19:25:01.539771', 74),
(350, '7. Edited message should reflect back in space view with edited tag.', '', '', '2019-02-05 19:25:01.595243', 74),
(351, '1. User should be able to see the message entered in step 3 in the compose bar.', '', '', '2019-02-05 19:25:01.773095', 76),
(352, '2. Cursor position should be at the end of the message with no white spaces preserved', '', '', '2019-02-05 19:25:01.850135', 76),
(353, '1. User should be able to see the message entered in step 3 in the compose bar.', '', '', '2019-02-05 19:25:01.906467', 77),
(354, '2. Cursor position should be at the end of the message with no white spaces preserved', '', '', '2019-02-05 19:25:02.027427', 77),
(355, '1. User should be able to see the attached file in drafts, this should work for both existing and new topics', '', '', '2019-02-05 19:25:02.150417', 78),
(356, '1.User should be taken to space view', '', '', '2019-02-05 19:25:02.207585', 79),
(357, '2. User should be taken to topic view', '', '', '2019-02-05 19:25:02.260696', 79),
(358, '3. No connection grey bar should be displayed', '', '', '2019-02-05 19:25:02.316106', 79),
(359, '4. Message should be posted in grey color and with "failed to send" message with \'i\' icon should be displayed', '', '', '2019-02-05 19:25:02.371693', 79),
(360, '5. Overlay displaying - "Copy text, Resend, delete" options must be displayed', '', '', '2019-02-05 19:25:02.427167', 79),
(361, '6. failed message should be deleted.', '', '', '2019-02-05 19:25:02.482434', 79),
(362, '7. Message should be posted in grey color and with "failed to send" message with \'i\' icon should be displayed', '', '', '2019-02-05 19:25:02.538040', 79),
(363, '8. No connection grey bar should be removed.', '', '', '2019-02-05 19:25:02.593427', 79),
(364, '9. Message should be posted in grey color and with "failed to send" message with \'i\' icon should be displayed', '', '', '2019-02-05 19:25:02.648793', 79),
(365, '10. Message should be sent successfully and the failed to send message should be disappeared.', '', '', '2019-02-05 19:25:02.704454', 79),
(366, '1. Every topic should have the notification icon in the top right.', '', '', '2019-02-05 19:25:02.749720', 80),
(367, '2. When pressed, the type of icon should toggle.', '', '', '2019-02-05 19:25:02.826585', 80),
(368, '3. If offline, the icon will revert to the original.', '', '', '2019-02-05 19:25:02.915390', 80),
(369, '4. By default it should be off unless the user has replied to the topic (or started it).', '', '', '2019-02-05 19:25:03.070866', 80),
(370, '1. App should be launched successfully', '', '', '2019-02-05 19:25:03.116499', 81),
(371, '2. User should be navigated to Topic view', '', '', '2019-02-05 19:25:03.159642', 81),
(372, '3. Verify upon Scrolling Up/Down the previous Messages should be loaded accordingly.', '', '', '2019-02-05 19:25:03.203923', 81),
(373, '4. Verify upon Scrolling Up/Down the previous Messages should be loaded accordingly when entered from Search View.', '', '', '2019-02-05 19:25:03.248394', 81),
(374, '1. User should be navigated to the selected room', '', '', '2019-02-05 19:25:03.294603', 82),
(375, '2. Search icon should be available on the header at the top right corner.', '', '', '2019-02-05 19:25:03.336991', 82),
(376, '3. User should be navigated to space search', '', '', '2019-02-05 19:25:03.426056', 82);
INSERT INTO `reports_teststeps` (`id`, `test_step`, `test_step_desc`, `golden_screen`, `created`, `test_id`) VALUES
(377, '4. There should be Back\' button/arrow appears in the upper left, to cancel out of Search, The search box, Filter panel - room members and the contacts based on affinity sections with a separator in between, other content filters - "@me", "videos", "Links", "Docs", "Slides", "Sheets", "PDFs", "All Files", Two tabs - "current room name " and "all"', '', '', '2019-02-05 19:25:03.470464', 82),
(378, '1. User should be navigated to the selected room', '', '', '2019-02-05 19:25:03.517796', 83),
(379, '2. Search icon should be available on the header at the top right corner.', '', '', '2019-02-05 19:25:03.559074', 83),
(380, '3. User should be navigated to space search', '', '', '2019-02-05 19:25:03.605449', 83),
(381, '4. "No results in <<current room="">> text along with "Search all" button must be displayed and a blue spinner must be displayed in place of black button while loading', '', '', '2019-02-05 19:25:03.647844', 83),
(382, '5. Search results pertaining to that keyword must be displayed and any collapsed messages should be displayed just with the count(99+ for more than 99) and the results must be in chronological order', '', '', '2019-02-05 19:25:03.692097', 83),
(383, '6. User should navigate to topic page(topic header and mute functionalities doesn\'t work currently)', '', '', '2019-02-05 19:25:03.747820', 83),
(384, '7. Search results must be retained and also the search keyword with clear search button', '', '', '2019-02-05 19:25:03.792116', 83),
(385, '8. Search results from all rooms must be fetched and displayed with proper room name and the date dividers appropriately', '', '', '2019-02-05 19:25:03.836537', 83),
(386, '1. User should be navigated to the selected room', '', '', '2019-02-05 19:25:03.883677', 84),
(387, '2. Search icon should be available on the header at the top right corner.', '', '', '2019-02-05 19:25:03.926513', 84),
(388, '3. User should be navigated to space search', '', '', '2019-02-05 19:25:03.971022', 84),
(389, '4. The results pertaining the member selected must be displayed', '', '', '2019-02-05 19:25:04.015301', 84),
(390, '5. Search results pertaining to that member selected must be fetched from all the rooms and displayed', '', '', '2019-02-05 19:25:04.059793', 84),
(391, '6. Search results pertaining to that members selected must be fetched from all the rooms and displayed', '', '', '2019-02-05 19:25:04.104043', 84),
(392, '1. User should be navigated to the selected room', '', '', '2019-02-05 19:25:04.150030', 85),
(393, '2. Search icon should be available on the header at the top right corner.', '', '', '2019-02-05 19:25:04.192813', 85),
(394, '3. User should be navigated to space search', '', '', '2019-02-05 19:25:04.237225', 85),
(395, '4. The results pertaining the member selected must be displayed', '', '', '2019-02-05 19:25:04.281628', 85),
(396, '5. Search results pertaining to that content type selected must be fetched from all the rooms and displayed', '', '', '2019-02-05 19:25:04.326088', 85),
(397, '6. Search results pertaining to that contents selected must be fetched from all the rooms and displayed', '', '', '2019-02-05 19:25:04.370563', 85),
(398, '1. Launch the app', '', '', '2019-02-05 19:25:04.488116', 86),
(399, '2. Mobile Notifications page should be shown', '', '', '2019-02-05 19:25:04.536966', 86),
(400, '3. The overlay should contain the following: The heading label: "Mobile Notifications", All Messages, New conversations and those I\'ve replied to, Conversations I\'ve replied to, Only @mentions & direct messages, Off', '', '', '2019-02-05 19:25:04.581374', 86),
(401, '1. User should be able to navigate to settings of the device', '', '', '2019-02-05 19:25:04.638413', 87),
(402, '2. User should be able to navigate to app settings screen', '', '', '2019-02-05 19:25:04.692302', 87),
(403, '3. User should be able to disable the notifications for the app', '', '', '2019-02-05 19:25:04.747799', 87),
(404, '4. App should be launched successfully.', '', '', '2019-02-05 19:25:04.803207', 87),
(405, '5. off toggle should be enabled.', '', '', '2019-02-05 19:25:04.858912', 87),
(406, '6. verify the top blue banner', '', '', '2019-02-05 19:25:04.947471', 87),
(407, '7. A message saying "Turn on notifications for chat in Androidsystem settings" should be displayed', '', '', '2019-02-05 19:25:05.180186', 87),
(408, '8. User should be taken to the Chat app notification settings screen', '', '', '2019-02-05 19:25:05.235756', 87),
(409, '1. App should be launched successfully', '', '', '2019-02-05 19:25:05.293666', 92),
(410, '2. User should be taken to space view of the selected room', '', '', '2019-02-05 19:25:05.346624', 92),
(411, '3. User should be taken to topic view of the selected topic', '', '', '2019-02-05 19:25:05.401869', 92),
(412, '4. Autocomplete pop up should be rendered and below things should be verified successfully:a) all and existing members should be displayed in the bottom most section of the popup with their profile avatars rendered properly, b) Next to the existing members section should be the "Add to room" members section displaying the contacts who can be added to the space with text "Add to room" against the user names with profile avatars rendered properly. c)User should be able to scroll through the contacts list in the autocomplete pop up.', '', '', '2019-02-05 19:25:05.457256', 92),
(413, '1. App should be launched successfully', '', '', '2019-02-05 19:25:05.513187', 93),
(414, '2. User should be taken to space view of the selected room', '', '', '2019-02-05 19:25:05.568333', 93),
(415, '3. User should be taken to topic view of the selected topic', '', '', '2019-02-05 19:25:05.623765', 93),
(416, '4. Autocomplete pop up should be rendered and below things should be verified successfully a) \'all\' alone should be displayed in the bottom most section of the popup with corresponding profile avatar rendered properly b) Next to that section should be the "Add to room" members section displaying the contacts who can be added to the space with text "Add to room" against the user names with profile avatars rendered properly. c) User should be able to scroll through the contacts list in the autocomplete pop up.', '', '', '2019-02-05 19:25:05.679364', 93),
(417, '1. App should be launched successfully', '', '', '2019-02-05 19:25:05.735222', 94),
(418, '2. User should be taken to space view of the selected room', '', '', '2019-02-05 19:25:05.790823', 94),
(419, '3. User should be taken to topic view of the selected topic', '', '', '2019-02-05 19:25:05.846206', 94),
(420, '4. Autocomplete pop up should contain only one other contact of the DM with their profile avatar and username rendered without any UI issues.', '', '', '2019-02-05 19:25:05.901503', 94),
(421, '1. App should be launched successfully', '', '', '2019-02-05 19:25:05.958131', 95),
(422, '2. User should be taken to space view of the selected room', '', '', '2019-02-05 19:25:06.012629', 95),
(423, '3. User should be taken to topic view of the selected topic', '', '', '2019-02-05 19:25:06.067919', 95),
(424, '4. Autocomplete pop up should contain "all" and other existing members username with their profile avatars rendered properly without any UI issues.', '', '', '2019-02-05 19:25:06.123463', 95),
(425, '1. Selected user token is generated', '', '', '2019-02-05 19:25:06.179559', 96),
(426, '2. @mention for the selected user is posted and the notification is sent for the user.', '', '', '2019-02-05 19:25:06.234321', 96),
(427, '1. @all is tokenised in primary app color', '', '', '2019-02-05 19:25:06.290177', 97),
(428, '2. @all mention is posted and the notification is sent for the user.', '', '', '2019-02-05 19:25:06.345355', 97),
(429, '1. User should be navigated to space view', '', '', '2019-02-05 19:25:06.402826', 98),
(430, '2. User should be navigated to topic view', '', '', '2019-02-05 19:25:06.456257', 98),
(431, '3. The user selected contact token should be generated in compose', '', '', '2019-02-05 19:25:06.511675', 98),
(432, '4. "Send message and add to <<roomname>>" alert must be displayed with Cancel and Add to room buttons displayed', '', '', '2019-02-05 19:25:06.567157', 98),
(433, '5. Dialog should be dismissed, but the token must persist in compose.', '', '', '2019-02-05 19:25:06.633605', 98),
(434, '6. "Send message and add to <<roomname>>" alert must be displayed with Cancel and Add to room buttons displayed', '', '', '2019-02-05 19:25:06.689055', 98),
(435, '7. The "@<<username>>" should be posted', '', '', '2019-02-05 19:25:06.744522', 98),
(436, '8. System message saying : "<<User>>has added <<User>>" with appropriate icons and timestamp must be displayed. nav bar should have the added count.', '', '', '2019-02-05 19:25:06.800022', 98),
(437, '1. App should be launched successfully.', '', '', '2019-02-05 19:25:06.844645', 99),
(438, '2. The presence indicator should be available in below locations either active (green circle) or inactive (grey circle) All dots ( Active & Inactive ) are located to the left of the username and vertically-centered with the name a. World view against 1:1 DM\'s b. Autocomplete list c. Room details page - Members list and Can join list d. Invite people screen e. DM contacts screen.', '', '', '2019-02-05 19:25:06.888788', 99),
(439, '1. App should be launched successfully.', '', '', '2019-02-05 19:25:06.967928', 100),
(440, '2. The presence indicator should be verified successfully in below locations with green dot indicating active presence status a) World view against 1:1 DM\'s, b) Autocomplete list c) Room details page - Members list and Can join list d) Invite people screen e)DM contacts screen.', '', '', '2019-02-05 19:25:07.011219', 100),
(441, '1. App should be launched successfully.', '', '', '2019-02-05 19:25:07.056566', 101),
(442, '2. The presence indicator should be verified successfully in below locations with grey dot indicating inactive presence status a) World view against 1:1 DM\'s b) Autocomplete list c) Room details page - Members list and Can join list d) Invite people screen e)DM contacts screen.', '', '', '2019-02-05 19:25:07.100011', 101),
(443, 'The app should rendered all the strings in all screens without any UI issues (padding, overlapping etc).Ellipsis will be displayed for strings which doesn\'t fit the screen', '', '', '2019-02-05 19:25:07.189263', 103),
(444, '1. App should be launched successfully', '', '', '2019-02-05 19:25:07.234030', 105),
(445, '2. User should be displayed a overlay with "Help&feedback" option', '', '', '2019-02-05 19:25:07.277632', 105),
(446, '3. User should be taken to Help screen', '', '', '2019-02-05 19:25:07.322124', 105),
(447, '4. User should be taken to Feedback screen', '', '', '2019-02-05 19:25:07.366398', 105),
(448, '5. User should be able to successfully post the feedback', '', '', '2019-02-05 19:25:07.410664', 105),
(449, '1. App should be launched successfully', '', '', '2019-02-05 19:25:07.457468', 109),
(450, '2. User should be taken to space view of the selected room', '', '', '2019-02-05 19:25:07.499645', 109),
(451, '3. User should navigate to topic view', '', '', '2019-02-05 19:25:07.544054', 109),
(452, '4. User should be shown an overlay with three options: a) Copy Text b) Edit c) Delete d) Send Feedback on this message', '', '', '2019-02-05 19:25:07.588229', 109),
(453, '5. Delete message alert should be displayed', '', '', '2019-02-05 19:25:07.632665', 109),
(454, '6. Delete message alert should be displayed with message saying: "Delete this message permanently <<username>>: "<<Message>>"" with "Cancel" and "Delete" buttons.', '', '', '2019-02-05 19:25:07.677119', 109),
(455, '7. Alert must be dismissed.', '', '', '2019-02-05 19:25:07.721521', 109),
(456, '8. User should be shown an overlay with three options: a) Copy Text b) Edit c) Delete e) Send Feedback on this message', '', '', '2019-02-05 19:25:07.765984', 109),
(457, '9. Delete message alert should be displayed', '', '', '2019-02-05 19:25:07.810241', 109),
(458, '10. Message selected should be deleted and the deleted message should not be seen by other users in the room/dm', '', '', '2019-02-05 19:25:07.854796', 109),
(459, '1. App should be launched successfully', '', '', '2019-02-05 19:25:07.901108', 110),
(460, '2. User should be taken to space view of the selected room/DM', '', '', '2019-02-05 19:25:07.943504', 110),
(461, '3. User should navigate to new conversation screen', '', '', '2019-02-05 19:25:07.987932', 110),
(462, '4. User should be able to post the message successfully.', '', '', '2019-02-05 19:25:08.032344', 110),
(463, '5. User should be shown an overlay with three options: a) Copy Text b) Edit c) Delete d) Send Feedback on this message', '', '', '2019-02-05 19:25:08.076611', 110),
(464, '6. Delete message alert should be displayed', '', '', '2019-02-05 19:25:08.121040', 110),
(465, '7. Message selected should be deleted and user should be taken back to space view with a toast saying - "oops this topic has been deleted"', '', '', '2019-02-05 19:25:08.165546', 110),
(466, '1. App should be launched successfully', '', '', '2019-02-05 19:25:08.211425', 111),
(467, '2. User should be taken to space view of the selected room/DM', '', '', '2019-02-05 19:25:08.254336', 111),
(468, '3. User should be shown an overlay with three options: a) Copy Text b) Edit c) Delete d) Send Feedback on this message', '', '', '2019-02-05 19:25:08.298600', 111),
(469, '4. Delete message alert should be displayed', '', '', '2019-02-05 19:25:08.343259', 111),
(470, '5. Message selected should be deleted and the date dividers and the timestamps for the other messages should not be affected. Other users in the room/dm should not be able to view the deleted message.', '', '', '2019-02-05 19:25:08.387487', 111),
(471, '1. App should be launched successfully', '', '', '2019-02-05 19:25:08.433223', 114),
(472, '2. User should be taken to space view of the selected room/DM', '', '', '2019-02-05 19:25:08.477368', 114),
(473, '3. User should be shown an overlay with three options: a) Copy Text b) Edit c) Delete d) Send Feedback on this message', '', '', '2019-02-05 19:25:08.521753', 114),
(474, '4. Delete message alert should be displayed', '', '', '2019-02-05 19:25:08.566221', 114),
(475, '5. "Message couldn\'t be deleted, Please try again in few minutes" message should be displayed', '', '', '2019-02-05 19:25:08.610399', 114),
(476, '1. User should be able to launch the app', '', '', '2019-02-05 19:25:08.655426', 115),
(477, '2. The FAB button must be present in the bottom right corner of the screen and always in the visible region.', '', '', '2019-02-05 19:25:08.699413', 115),
(478, '3. Room, Bot and Direct message options must be displayed along with their corresponding icons.(If the user has invite to rooms, the count will be displayed near the fab button which when tapped on FAB button shows up in place of Room icon)', '', '', '2019-02-05 19:25:08.743718', 115),
(479, '4. User should be taken to "Add bot" screen with a close button on top left corner and focus in input field and the available bots for that user listed ', '', '', '2019-02-05 19:25:08.788308', 115),
(480, '1. User should be able to launch the app', '', '', '2019-02-05 19:25:08.845982', 116),
(481, '2. The FAB button must be present in the bottom right corner of the screen and always in the visible region.', '', '', '2019-02-05 19:25:08.899191', 116),
(482, '3. Room, Bot and Direct message options must be displayed along with their corresponding icons.(If the user has invite to rooms, the count will be displayed near the fab button which when tapped on FAB button shows up in place of Room icon)', '', '', '2019-02-05 19:25:08.954368', 116),
(483, '4. User should be taken to "Add bot" screen with a close button on top left corner and focus in input field and the available bots for that user listed', '', '', '2019-02-05 19:25:09.010197', 116),
(484, '5. User should be taken to BOT DM screen', '', '', '2019-02-05 19:25:09.065668', 116),
(485, '6. There should be appropriate Response from the BOT for all the content types (as per the BOT selected) with BOT avatar and timestamps properly displayed in space and topic views.', '', '', '2019-02-05 19:25:09.121220', 116),
(486, '1. User should be able to launch the app', '', '', '2019-02-05 19:25:09.178755', 117),
(487, '2. The FAB button must be present in the bottom right corner of the screen and always in the visible region.', '', '', '2019-02-05 19:25:09.231812', 117),
(488, '3. Room, Bot and Direct message options must be displayed along with their corresponding icons.(If the user has invite to rooms, the count will be displayed near the fab button which when tapped on FAB button shows up in place of Room icon)', '', '', '2019-02-05 19:25:09.287171', 117),
(489, '4. User should be taken to "Add bot" screen with a close button on top left corner and focus in input field and the available bots for that user listed', '', '', '2019-02-05 19:25:09.342918', 117),
(490, '5. User should be taken to BOT DM screen', '', '', '2019-02-05 19:25:09.398194', 117),
(491, '6. User should be taken to DM details screen.', '', '', '2019-02-05 19:25:09.453807', 117),
(492, '7. The DM details screen should contain a) Star/Unstar b) Notifications toggle button c) Remove d) Members section - listing the bot and the user', '', '', '2019-02-05 19:25:09.509340', 117),
(493, '1. User should be able to launch the app', '', '', '2019-02-05 19:25:09.833725', 118),
(494, '2. The FAB button must be present in the bottom right corner of the screen and always in the visible region.', '', '', '2019-02-05 19:25:09.885947', 118),
(495, '3. Room, Bot and Direct message options must be displayed along with their corresponding icons.(If the user has invite to rooms, the count will be displayed near the fab button which when tapped on FAB button shows up in place of Room icon)', '', '', '2019-02-05 19:25:09.941421', 118),
(496, '4. User should be taken to "Add bot" screen with a close button on top left corner and focus in input field and the available bots for that user listed', '', '', '2019-02-05 19:25:09.996837', 118),
(497, '5. User should be taken to BOT DM screen', '', '', '2019-02-05 19:25:10.052308', 118),
(498, '6. User should be taken to DM details screen.', '', '', '2019-02-05 19:25:10.107914', 118),
(499, '7. The DM details screen should contain a) Star/Unstar b) Notifications toggle button c) Remove d) Members section - listing the bot and the user', '', '', '2019-02-05 19:25:10.163427', 118),
(500, '8. User should be taken to world view with a message - "Bot removed" and world view should not contain the removed bot', '', '', '2019-02-05 19:25:10.218843', 118),
(501, '9. User should be able to select the removed bot', '', '', '2019-02-05 19:25:10.274196', 118),
(502, '10. User should be able to post the message in bot dm and the selected bot should be displayed back in world view.', '', '', '2019-02-05 19:25:10.329610', 118),
(503, '1. 3rd party app should be launched', '', '', '2019-02-05 19:25:10.386520', 119),
(504, '2. On taping share all sharing apps should be available', '', '', '2019-02-05 19:25:10.440623', 119),
(505, '3. a) App should be successfully launched with "Get Started" button b) "Sharing to chat" toast should be visible', '', '', '2019-02-05 19:25:10.496000', 119),
(506, '4. Choose an account pop up should appear with all device accounts', '', '', '2019-02-05 19:25:10.551617', 119),
(507, '5. a) Verify if app gets login and stays on World View b) Close the app and try logging with a different account should get displayed', '', '', '2019-02-05 19:25:10.607011', 119),
(508, '1. 3rd party app should be launched', '', '', '2019-02-05 19:25:10.664303', 120),
(509, '2. On taping share all sharing apps should be available', '', '', '2019-02-05 19:25:10.717873', 120),
(510, '3. "Send to" screen should be displayed with all Rooms, Dm\'s & Group Dm\'s', '', '', '2019-02-05 19:25:10.784530', 120),
(511, '4. Verify if New Conversation window gets opened with shared file attachment in compose field', '', '', '2019-02-05 19:25:10.851072', 120),
(512, '5. Verify if shared file attachment visible in compose field with Off the record enabled.', '', '', '2019-02-05 19:25:10.917699', 120),
(513, '6. attached file should be shared to respective Room/DM/Group DM', '', '', '2019-02-05 19:25:10.984117', 120),
(514, '7. Upon tapping close button it should redirect to World view', '', '', '2019-02-05 19:25:11.039647', 120),
(515, '8. Verify if the shared file has time stamp, username with avatar and today header', '', '', '2019-02-05 19:25:11.095199', 120),
(516, '1. Verify if content is being shared from apps like youtube and drive apps', '', '', '2019-02-05 19:25:11.151594', 122),
(517, '2. "Send to" screen should be displayed with all Rooms, Dm\'s & Group Dm\'s', '', '', '2019-02-05 19:25:11.206080', 122),
(518, '3. Sharing for these type of files are not yet supported in chat application.', '', '', '2019-02-05 19:25:11.261426', 122),
(519, '1. photos/gallery should get launched', '', '', '2019-02-05 19:25:11.317797', 123),
(520, '2. N/A', '', '', '2019-02-05 19:25:11.372298', 123),
(521, '3. "Send to" screen should be displayed with all Rooms, Dm\'s & Group Dm\'s', '', '', '2019-02-05 19:25:11.427886', 123),
(522, '4. File couldn\'t be uploaded . Check your internet connection should be displayed on image, send button should be disable by default', '', '', '2019-02-05 19:25:11.483199', 123),
(523, '1. Chat application must be launched successfully.', '', '', '2019-02-05 19:25:11.540544', 129),
(524, '2. User should be able to navigate to the selected room successfully.', '', '', '2019-02-05 19:25:11.594246', 129),
(525, '3. Autocomplete must be invoked and the BOTs which are not in the room must be listed after all the members section with "Add to room" text', '', '', '2019-02-05 19:25:11.716160', 129),
(526, '4. User should be able to @mention the bot and the "Add to room" dialog must be displayed with the text related to that of BOT.', '', '', '2019-02-05 19:25:11.762693', 129),
(527, '5. Selected BOT must be added to the room and the room members count should increase and the system message should also get generated', '', '', '2019-02-05 19:25:11.806832', 129),
(528, '1. Chat application must be launched successfully.', '', '', '2019-02-05 19:25:11.852568', 130),
(529, '2. User should be able to navigate to the selected room successfully.', '', '', '2019-02-05 19:25:11.895906', 130),
(530, '3. User should navigate to the Invite screen', '', '', '2019-02-05 19:25:11.940199', 130),
(531, '4. Bots suggestions must be displayed at the end of the screen', '', '', '2019-02-05 19:25:11.984494', 130),
(532, '5. Selected BOT must be added to the room automatically and the room members count should increase and the system message should also get generated', '', '', '2019-02-05 19:25:12.044633', 130),
(533, '1. Chat application must be launched successfully.', '', '', '2019-02-05 19:25:12.097404', 131),
(534, '2. User should be able to navigate to the selected room successfully.', '', '', '2019-02-05 19:25:12.140373', 131),
(535, '3. User should navigate to the Invite screen', '', '', '2019-02-05 19:25:12.184606', 131),
(536, '4. Bots suggestions must be displayed at the end of the screen', '', '', '2019-02-05 19:25:12.229026', 131),
(537, '5. Selected BOT must be added to the room automatically and the room members count should increase and the system message should also get generated', '', '', '2019-02-05 19:25:12.273287', 131),
(538, '1. App should be launched successfully', '', '', '2019-02-05 19:25:12.318702', 139),
(539, '2. User should be taken to space view of the selected room', '', '', '2019-02-05 19:25:12.362186', 139),
(540, '3. User should be taken to topic view', '', '', '2019-02-05 19:25:12.406541', 139),
(541, '4. User should be able to post the Inline code successfully', '', '', '2019-02-05 19:25:12.451048', 139),
(542, '5. User should return to space view', '', '', '2019-02-05 19:25:12.495242', 139),
(543, '6. Posted Inline code should be displayed in Space view', '', '', '2019-02-05 19:25:12.539694', 139),
(544, '1. App should be launched successfully', '', '', '2019-02-05 19:25:12.585013', 140),
(545, '2. User should be taken to space view of the selected room', '', '', '2019-02-05 19:25:12.628653', 140),
(546, '3. User should be taken to topic view', '', '', '2019-02-05 19:25:12.672924', 140),
(547, '4. User should be able to post the Code Block successfully', '', '', '2019-02-05 19:25:12.717259', 140),
(548, '5. User should return to space view', '', '', '2019-02-05 19:25:12.774082', 140),
(549, '6. Posted Code Block should be displayed in Space view', '', '', '2019-02-05 19:25:12.862278', 140),
(550, '1. App should be launched successfully', '', '', '2019-02-05 19:25:12.918905', 141),
(551, '2. User should be taken to space view of the selected room', '', '', '2019-02-05 19:25:12.973529', 141),
(552, '3. User should be taken to the room details screen', '', '', '2019-02-05 19:25:13.073540', 141),
(553, '4. User should be taken to the Invite screen', '', '', '2019-02-05 19:25:13.157059', 141),
(554, '5. Invite modal should be launched containing the below Notify people via email heading with a toggle button,Invitations will not be emailed to groups larger than 100 members,Settings, Help & Feedback', '', '', '2019-02-05 19:25:13.306329', 141),
(555, '1. App should be launched successfully', '', '', '2019-02-05 19:25:13.429117', 142),
(556, '2. User should be taken to space view of the selected room', '', '', '2019-02-05 19:25:13.517327', 142),
(557, '3. User should be taken to the room details screen', '', '', '2019-02-05 19:25:13.572837', 142),
(558, '4. User should be taken to the Invite screen', '', '', '2019-02-05 19:25:13.628047', 142),
(559, '5.Invite modal should be launched containing the below Notify people via email heading with a toggle button - default on position,Invitations will not be emailed to groups larger than 100 members, Settings , Help & Feedback b)Modal should be dismissed c)Selected group must be chipified and shown d) User should be redirected to the Room details screen e) The invited members of the group should receive the room invite and also an email containing the invite to the room,with the subject: [RoomName] room - Invitation to join', '', '', '2019-02-05 19:25:13.683689', 142),
(560, '1. App should be launched successfully', '', '', '2019-02-05 19:25:13.741274', 143),
(561, '2. User should be taken to space view of the selected room', '', '', '2019-02-05 19:25:13.794639', 143),
(562, '3. User should be taken to the room details screen', '', '', '2019-02-05 19:25:13.849976', 143),
(563, '4. User should be taken to the Invite screen', '', '', '2019-02-05 19:25:13.905516', 143),
(564, '5. Invite modal should be launched containing the below Notify people via email heading with a toggle button - default on position,Invitations will not be emailed to groups larger than 100 members, Settings , Help & Feedback b)Modal should be dismissed c)Selected group must be chipified and shown d) User should be redirected to the Room details screen e) The invited members of the group should receive the room invite and also an email containing the invite to the room,with the subject: [RoomName] room - Invitation to join', '', '', '2019-02-05 19:25:13.961043', 143),
(565, '1. Ensure no space (padding) is getting displayed between Threads in the DM', '', '', '2019-02-05 19:25:14.062390', 146),
(566, '2. DM background should be white in color', '', '', '2019-02-05 19:25:14.116332', 146),
(567, '3. Bell notification icon per conversation shouldn\'t be displayed', '', '', '2019-02-05 19:25:14.238797', 146),
(568, '4. New Conversation button for DM should not be displayed', '', '', '2019-02-05 19:25:14.317112', 146),
(569, '5. Reply button for existing threads should not be displayed', '', '', '2019-02-05 19:25:14.361830', 146),
(570, '6. Make sure input box is displaying at the end of DM', '', '', '2019-02-05 19:25:14.406317', 146),
(571, '1. Should be able to launch the DM.', '', '', '2019-02-05 19:25:14.453088', 148),
(572, '2. Should be able to post a message with history ON.', '', '', '2019-02-05 19:25:14.495056', 148),
(573, '3. Mobile blocker(banner in compose box) in TEAL with your username should be displayed at the other member\'s compose box.', '', '', '2019-02-05 19:25:14.539215', 148),
(574, '4. Should be able to post a message with history OFF.', '', '', '2019-02-05 19:25:14.583614', 148),
(575, '5. Mobile blocker(banner in compose box) in BLUE with your username should be displayed at the other member\'s compose box.', '', '', '2019-02-05 19:25:14.628004', 148),
(576, '6. \'New\' badge in light blue should be displayed for history ON messages, at send and receiver.', '', '', '2019-02-05 19:25:14.727977', 148),
(577, '7. \'New\' badge with trimer icon in dark blue should be displayed for history OFF messages, at send and receiver.', '', '', '2019-02-05 19:25:14.772162', 148),
(578, '8. On entering the READ DM, the timer icon without "new" should be displayed for history OFF message.', '', '', '2019-02-05 19:25:14.816745', 148),
(579, '1 a) Should be able to locate a history ON message. b) Should be able to select message by TAP and "edit". c) Should be able to successfully post the edited message. d) Editing and posting a message to be empty, triggers delete flow. e) Edited content should append "Edited" text to the time stamp. f) Repeat steps 1-5 above, for History OFF messages.', '', '', '2019-02-05 19:25:14.863235', 149),
(580, '2 a) Repeat verification case #1 via search view.', '', '', '2019-02-05 19:25:14.905510', 149),
(581, '3 a) Should be able to locate and delete a history ON message b) Should be able to locate and delete a history OFF message', '', '', '2019-02-05 19:25:14.949914', 149),
(582, '4 a) Should be able to locate and delete a history ON message via search view. b)Should be able to locate and delete a history OFF message via search view.', '', '', '2019-02-05 19:25:14.994151', 149),
(583, '1. a)Should be able to post message in offline mode with messages should be posted in greyed out color indicating pending state. b)Pending state messages should be delivered without user intervention after connecting back to network.', '', '', '2019-02-05 19:25:15.040613', 151),
(584, '2 a) Should be able to post messages in offline mode. b) Offline messages should convert to "Failed to send" state from Pending state after cold starting the app. c) Should be able to send/delete the "failed to send" messages by selecting the respective option from MODAL when connected back online.', '', '', '2019-02-05 19:25:15.083224', 151),
(585, '3 a) Should be to an existing DM in offline mode. b) Content available when online should be available in offline mode.', '', '', '2019-02-05 19:25:15.127417', 151),
(586, 'Verify @mentioned bot requires configuration', '', '', '2019-02-05 19:25:15.283233', 152),
(587, '1. Should be able to visit a room and messages should load.', '', '', '2019-02-05 19:25:15.327865', 159),
(588, '2. On revisiting the room/DM while offline, already fetched messages should be displayed.', '', '', '2019-02-05 19:25:15.371466', 159),
(589, '1. a) Should be able to scroll to the top and J2B FAB should reveal. b)Tapping on J2B FAB, space view scroll position should reach to the bottom and new topic FAB should be revealed.(j2b FAB should disappear)', '', '', '2019-02-05 19:25:15.417385', 160),
(590, '2. a) Should be able to create a new room, post new topics, scroll to top and J2B FAB should reveal. b) Tapping on J2B FAB, space view scroll position should reach to the bottom and new topic FAB should be revealed.(j2b FAB should disappear)', '', '', '2019-02-05 19:25:15.460464', 160),
(591, '1. a)Should be able to go to 1:1 DM .b) On scrolling to the top, J2B FAB should NOT be revealed in DM.', '', '', '2019-02-05 19:25:15.505283', 162),
(592, '2. a) Should be able to go to Group DM .b) On scrolling to the top, J2B FAB should NOT be revealed in DM.', '', '', '2019-02-05 19:25:15.549093', 162),
(593, 'The Navigation Panel should open and display user profile section and menu options.', '', '', '2019-02-05 19:25:15.593402', 163),
(594, '1. Under Profile section the user name, profile image and the user email id should be displayed.', '', '', '2019-02-05 19:25:15.638149', 164),
(595, '2. below the profile section there should be turn off do not disturb (only if dnd is set), set do not disturb, Settings, Help & Feedback , Logout ', '', '', '2019-02-05 19:25:15.682360', 164),
(596, '1. Should be able to launch the app.', '', '', '2019-02-05 19:25:15.727595', 165),
(597, '2. World view header should be updated with logged-in user\'s username and "Active" status below it with online(green) indicator.', '', '', '2019-02-05 19:25:15.770942', 165),
(598, '1. Should be able to launch the app.', '', '', '2019-02-05 19:25:15.817243', 166),
(599, '2. Should be able to set DND.', '', '', '2019-02-05 19:25:15.859773', 166),
(600, '3. DND status(on logged-in device) should update on world view header below username as "Do not disturb until <timestamp>".', '', '', '2019-02-05 19:25:15.904353', 166),
(601, '4. User\'s DND status(on logged-in device) should be displayed with "filled half moon" indicator in Room/DM details view (in rooms/dms where user is part of).', '', '', '2019-02-05 19:25:15.948721', 166),
(602, '5. a) user\'s DND status should be displayed with the "filled half moon" indicator on World view(unread/read/starred sections) b) "Frequent listc c) Room/DM details view d) @mention auto complete list, (e) Conversation launcher --> "Group Message" auto complete list.', '', '', '2019-02-05 19:25:15.993077', 166),
(603, '1. Should be able to go to "mobile notifications" from "settings"', '', '', '2019-02-05 19:25:16.038902', 167),
(604, '2. The mobile notifications view should contain the heading label: "Mobile Notifications", "New conversations and those I\'ve replied to" , "Conversations I\'ve replied to", "Only @mentions & direct messages", Off options.', '', '', '2019-02-05 19:25:16.081904', 167),
(605, '3. Toggle Button be displayed across the selected option.', '', '', '2019-02-05 19:25:16.159414', 167),
(606, '4. Incoming notifications should work according to the notification settings.', '', '', '2019-02-05 19:25:16.248246', 167),
(607, '1. Should be able to go to a room/dm/bot-dm/interoped-room.', '', '', '2019-02-05 19:25:16.354996', 168),
(608, '2. Action sheet should display "Add Reaction" option on long pressing the message.("Add reaction" should be verified from topic view as well)', '', '', '2019-02-05 19:25:16.425667', 168),
(609, '3. Emoji picker should show up on selecting "add reaction" from action sheet.', '', '', '2019-02-05 19:25:16.470271', 168),
(610, '4. Emoji picker should contain two sections: \'Quick Reactions\', \'Smileys & Emojis\'.', '', '', '2019-02-05 19:25:16.514572', 168),
(611, '5. Tapping any emoji dismisses the picker and adds the selected emoji as reaction below the long pressed message.(No action takes place on re-adding a preselected reaction)', '', '', '2019-02-05 19:25:16.559079', 168),
(612, '6. Adding first reaction to a message adds emoji picker icon next to the reaction.', '', '', '2019-02-05 19:25:16.603278', 168),
(613, '7. Count on emoji should be displayed in TEAL color if the logged is user is reacted.', '', '', '2019-02-05 19:25:16.662211', 168),
(614, '8. Count on emoji should be displayed in GREY color if the logged-in user is not reacted but reacted by other members.', '', '', '2019-02-05 19:25:16.714685', 168),
(615, '1 a) Should be able to go to a room b)Should be able to long press a message and select "add reaction" c)Should be able to post a reaction d) Other member should be able to react e)Count should increment by +1 for every reaction by other members.', '', '', '2019-02-05 19:25:16.761088', 169),
(616, '2 a) Should be able to go to a room b) Should be able to long press a message and select "add reaction" c) Should be able to post a reaction. d) Other member should be able to react e)Count on emoji should increment by +1 for every reaction by other members. f) Should be post a reaction from logged-in user and tapping the same reaction should decrement the count to "0" and the emoji should drop.', '', '', '2019-02-05 19:25:16.803499', 169),
(617, '1. Should be able to go to a room and located an emoji which is reacted by more users.', '', '', '2019-02-05 19:25:16.862186', 170),
(618, '2. Long pressing the emoji reaction should present list of reactors.', '', '', '2019-02-05 19:25:16.915356', 170),
(619, '3. Order of usernames should be based on timestamp(recent ones on top) and logged-in username should be the FIRST.', '', '', '2019-02-05 19:25:16.970817', 170),
(620, '4. List should show recent 10 reactors names and option to load more for more than 10 reactors.', '', '', '2019-02-05 19:25:17.026464', 170),
(621, '5. Title of the reactors list should be <n people="" reacted="" with="" emoji=""> Note: N= Count of total no. of reactors and emoji = emoji icon posted as reaction', '', '', '2019-02-05 19:25:17.081878', 170),
(622, '6. Reactions should be in "READ-Only" mode in search results.(Long pressing on reaction for reactors list should not work)', '', '', '2019-02-05 19:25:17.170409', 170);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indexes for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indexes for table `reports_builds`
--
ALTER TABLE `reports_builds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_builds_project_id_17fe8f51_fk_reports_projects_id` (`project_id`);

--
-- Indexes for table `reports_devices`
--
ALTER TABLE `reports_devices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_devices_project_id_30c9dc68_fk_reports_projects_id` (`project_id`);

--
-- Indexes for table `reports_projects`
--
ALTER TABLE `reports_projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reports_testcases`
--
ALTER TABLE `reports_testcases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_testcases_project_id_c210f3a3_fk_reports_projects_id` (`project_id`);

--
-- Indexes for table `reports_testresultconsolidation`
--
ALTER TABLE `reports_testresultconsolidation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_testresultco_test_build_id_625401e3_fk_reports_b` (`test_build_id`),
  ADD KEY `reports_testresultco_test_case_id_2426cbb0_fk_reports_t` (`test_case_id`),
  ADD KEY `reports_testresultco_test_device_id_4274a34e_fk_reports_d` (`test_device_id`);

--
-- Indexes for table `reports_testresults`
--
ALTER TABLE `reports_testresults`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_testresults_device_id_e87ffe02_fk_reports_devices_id` (`device_id`),
  ADD KEY `reports_testresults_test_build_id_5ec50097_fk_reports_builds_id` (`test_build_id`),
  ADD KEY `reports_testresults_test_name_id_b7280f5d_fk_reports_t` (`test_name_id`),
  ADD KEY `reports_testresults_test_step_id_3de6faeb_fk_reports_t` (`test_step_id`);

--
-- Indexes for table `reports_teststeps`
--
ALTER TABLE `reports_teststeps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_teststeps_test_id_b4702c33_fk_reports_testcases_id` (`test_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `reports_builds`
--
ALTER TABLE `reports_builds`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `reports_devices`
--
ALTER TABLE `reports_devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `reports_projects`
--
ALTER TABLE `reports_projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `reports_testcases`
--
ALTER TABLE `reports_testcases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=173;
--
-- AUTO_INCREMENT for table `reports_testresultconsolidation`
--
ALTER TABLE `reports_testresultconsolidation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT for table `reports_testresults`
--
ALTER TABLE `reports_testresults`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=421;
--
-- AUTO_INCREMENT for table `reports_teststeps`
--
ALTER TABLE `reports_teststeps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=623;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `reports_builds`
--
ALTER TABLE `reports_builds`
  ADD CONSTRAINT `reports_builds_project_id_17fe8f51_fk_reports_projects_id` FOREIGN KEY (`project_id`) REFERENCES `reports_projects` (`id`);

--
-- Constraints for table `reports_devices`
--
ALTER TABLE `reports_devices`
  ADD CONSTRAINT `reports_devices_project_id_30c9dc68_fk_reports_projects_id` FOREIGN KEY (`project_id`) REFERENCES `reports_projects` (`id`);

--
-- Constraints for table `reports_testcases`
--
ALTER TABLE `reports_testcases`
  ADD CONSTRAINT `reports_testcases_project_id_c210f3a3_fk_reports_projects_id` FOREIGN KEY (`project_id`) REFERENCES `reports_projects` (`id`);

--
-- Constraints for table `reports_testresultconsolidation`
--
ALTER TABLE `reports_testresultconsolidation`
  ADD CONSTRAINT `reports_testresultco_test_build_id_625401e3_fk_reports_b` FOREIGN KEY (`test_build_id`) REFERENCES `reports_builds` (`id`),
  ADD CONSTRAINT `reports_testresultco_test_case_id_2426cbb0_fk_reports_t` FOREIGN KEY (`test_case_id`) REFERENCES `reports_testcases` (`id`),
  ADD CONSTRAINT `reports_testresultco_test_device_id_4274a34e_fk_reports_d` FOREIGN KEY (`test_device_id`) REFERENCES `reports_devices` (`id`);

--
-- Constraints for table `reports_testresults`
--
ALTER TABLE `reports_testresults`
  ADD CONSTRAINT `reports_testresults_device_id_e87ffe02_fk_reports_devices_id` FOREIGN KEY (`device_id`) REFERENCES `reports_devices` (`id`),
  ADD CONSTRAINT `reports_testresults_test_build_id_5ec50097_fk_reports_builds_id` FOREIGN KEY (`test_build_id`) REFERENCES `reports_builds` (`id`),
  ADD CONSTRAINT `reports_testresults_test_name_id_b7280f5d_fk_reports_t` FOREIGN KEY (`test_name_id`) REFERENCES `reports_testcases` (`id`),
  ADD CONSTRAINT `reports_testresults_test_step_id_3de6faeb_fk_reports_t` FOREIGN KEY (`test_step_id`) REFERENCES `reports_teststeps` (`id`);

--
-- Constraints for table `reports_teststeps`
--
ALTER TABLE `reports_teststeps`
  ADD CONSTRAINT `reports_teststeps_test_id_b4702c33_fk_reports_testcases_id` FOREIGN KEY (`test_id`) REFERENCES `reports_testcases` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
