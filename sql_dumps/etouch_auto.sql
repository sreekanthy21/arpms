-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 22, 2019 at 10:04 AM
-- Server version: 5.7.24-0ubuntu0.16.04.1
-- PHP Version: 7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `etouch_auto`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add test steps', 1, 'add_teststeps'),
(2, 'Can change test steps', 1, 'change_teststeps'),
(3, 'Can delete test steps', 1, 'delete_teststeps'),
(4, 'Can add test cases', 2, 'add_testcases'),
(5, 'Can change test cases', 2, 'change_testcases'),
(6, 'Can delete test cases', 2, 'delete_testcases'),
(7, 'Can add test results', 3, 'add_testresults'),
(8, 'Can change test results', 3, 'change_testresults'),
(9, 'Can delete test results', 3, 'delete_testresults'),
(10, 'Can add builds', 4, 'add_builds'),
(11, 'Can change builds', 4, 'change_builds'),
(12, 'Can delete builds', 4, 'delete_builds'),
(13, 'Can add projects', 5, 'add_projects'),
(14, 'Can change projects', 5, 'change_projects'),
(15, 'Can delete projects', 5, 'delete_projects'),
(16, 'Can add devices', 6, 'add_devices'),
(17, 'Can change devices', 6, 'change_devices'),
(18, 'Can delete devices', 6, 'delete_devices'),
(19, 'Can add test result consolidation', 7, 'add_testresultconsolidation'),
(20, 'Can change test result consolidation', 7, 'change_testresultconsolidation'),
(21, 'Can delete test result consolidation', 7, 'delete_testresultconsolidation'),
(22, 'Can add log entry', 8, 'add_logentry'),
(23, 'Can change log entry', 8, 'change_logentry'),
(24, 'Can delete log entry', 8, 'delete_logentry'),
(25, 'Can add group', 9, 'add_group'),
(26, 'Can change group', 9, 'change_group'),
(27, 'Can delete group', 9, 'delete_group'),
(28, 'Can add permission', 10, 'add_permission'),
(29, 'Can change permission', 10, 'change_permission'),
(30, 'Can delete permission', 10, 'delete_permission'),
(31, 'Can add user', 11, 'add_user'),
(32, 'Can change user', 11, 'change_user'),
(33, 'Can delete user', 11, 'delete_user'),
(34, 'Can add content type', 12, 'add_contenttype'),
(35, 'Can change content type', 12, 'change_contenttype'),
(36, 'Can delete content type', 12, 'delete_contenttype'),
(37, 'Can add session', 13, 'add_session'),
(38, 'Can change session', 13, 'change_session'),
(39, 'Can delete session', 13, 'delete_session');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(8, 'admin', 'logentry'),
(9, 'auth', 'group'),
(10, 'auth', 'permission'),
(11, 'auth', 'user'),
(12, 'contenttypes', 'contenttype'),
(4, 'reports', 'builds'),
(6, 'reports', 'devices'),
(5, 'reports', 'projects'),
(2, 'reports', 'testcases'),
(7, 'reports', 'testresultconsolidation'),
(3, 'reports', 'testresults'),
(1, 'reports', 'teststeps'),
(13, 'sessions', 'session');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2019-01-17 16:01:07.899840'),
(2, 'auth', '0001_initial', '2019-01-17 16:01:22.092309'),
(3, 'admin', '0001_initial', '2019-01-17 16:01:25.878814'),
(4, 'admin', '0002_logentry_remove_auto_add', '2019-01-17 16:01:26.539722'),
(5, 'contenttypes', '0002_remove_content_type_name', '2019-01-17 16:01:29.618670'),
(6, 'auth', '0002_alter_permission_name_max_length', '2019-01-17 16:01:29.887302'),
(7, 'auth', '0003_alter_user_email_max_length', '2019-01-17 16:01:30.285705'),
(8, 'auth', '0004_alter_user_username_opts', '2019-01-17 16:01:30.541932'),
(9, 'auth', '0005_alter_user_last_login_null', '2019-01-17 16:01:31.481236'),
(10, 'auth', '0006_require_contenttypes_0002', '2019-01-17 16:01:31.574413'),
(11, 'auth', '0007_alter_validators_add_error_messages', '2019-01-17 16:01:31.667615'),
(12, 'auth', '0008_alter_user_username_max_length', '2019-01-17 16:01:31.854647'),
(13, 'reports', '0001_initial', '2019-01-17 16:01:49.653943'),
(14, 'sessions', '0001_initial', '2019-01-17 16:01:50.533250'),
(15, 'reports', '0002_auto_20190118_1231', '2019-01-18 12:31:40.425529'),
(16, 'reports', '0003_testresults_test_errors', '2019-01-21 08:43:34.319535');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reports_builds`
--

CREATE TABLE `reports_builds` (
  `id` int(11) NOT NULL,
  `build` varchar(300) NOT NULL,
  `executed_date` date NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports_builds`
--

INSERT INTO `reports_builds` (`id`, `build`, `executed_date`, `project_id`) VALUES
(1, 'dynamite-android_20190104-09_RC04', '2019-01-16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `reports_devices`
--

CREATE TABLE `reports_devices` (
  `id` int(11) NOT NULL,
  `device_name` longtext NOT NULL,
  `device_os` longtext NOT NULL,
  `device_udid` longtext NOT NULL,
  `created` datetime(6) NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports_devices`
--

INSERT INTO `reports_devices` (`id`, `device_name`, `device_os`, `device_udid`, `created`, `project_id`) VALUES
(1, 'Pixel 2', '8.1.0', 'FA6A40307885', '2019-01-16 00:00:00.000000', 1);

-- --------------------------------------------------------

--
-- Table structure for table `reports_projects`
--

CREATE TABLE `reports_projects` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` longtext NOT NULL,
  `created` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports_projects`
--

INSERT INTO `reports_projects` (`id`, `name`, `description`, `created`) VALUES
(1, 'Dynamite Automation', 'Dynamite Automation', '2019-01-17 00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `reports_testcases`
--

CREATE TABLE `reports_testcases` (
  `id` int(11) NOT NULL,
  `test_feature` longtext NOT NULL,
  `test_title` longtext NOT NULL,
  `test_name` longtext NOT NULL,
  `test_suite_type` longtext NOT NULL,
  `test_priority` int(11) NOT NULL,
  `created` datetime(6) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports_testcases`
--

INSERT INTO `reports_testcases` (`id`, `test_feature`, `test_title`, `test_name`, `test_suite_type`, `test_priority`, `created`, `status`, `project_id`) VALUES
(1, 'App Launch', 'Verify the Onboarding message upon app launch', 'Onboarding message should be displayed upon app launch', 'Regression', 2, '2019-01-16 00:00:00.000000', 1, 1),
(2, 'App Launch', 'Verify Tapping on "Get started" Button', 'Choose an Account should be displayed on tapping Get Started', 'Regression', 0, '2019-01-16 00:00:00.000000', 1, 1),
(3, 'App Launch', 'Verify login with White listed Accounts', 'To Successfully login to application', 'Regression', 2, '2019-01-16 00:00:00.000000', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `reports_testresultconsolidation`
--

CREATE TABLE `reports_testresultconsolidation` (
  `id` int(11) NOT NULL,
  `test_status` int(11) NOT NULL,
  `bugid` varchar(50) NOT NULL,
  `bug_status` varchar(50) NOT NULL,
  `execution_time` double NOT NULL,
  `executed_date` date NOT NULL,
  `test_build_id` int(11) NOT NULL,
  `test_case_id` int(11) NOT NULL,
  `test_device_id` int(11) NOT NULL,
  `test_device_logs` longtext,
  `test_server_logs` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports_testresultconsolidation`
--

INSERT INTO `reports_testresultconsolidation` (`id`, `test_status`, `bugid`, `bug_status`, `execution_time`, `executed_date`, `test_build_id`, `test_case_id`, `test_device_id`, `test_device_logs`, `test_server_logs`) VALUES
(1, 1, '', '', 2, '2019-01-16', 1, 1, 1, NULL, NULL),
(2, 1, '', '', 2, '2019-01-16', 1, 2, 1, NULL, NULL),
(3, 0, 'b/123456', '', 1, '2019-01-16', 1, 3, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reports_testresults`
--

CREATE TABLE `reports_testresults` (
  `id` int(11) NOT NULL,
  `test_result` tinyint(1) NOT NULL,
  `screen_generated` longtext NOT NULL,
  `screen_diff` longtext NOT NULL,
  `executed` datetime(6) NOT NULL,
  `start_time` datetime(6) NOT NULL,
  `end_time` datetime(6) NOT NULL,
  `device_id` int(11) NOT NULL,
  `test_build_id` int(11) DEFAULT NULL,
  `test_name_id` int(11) NOT NULL,
  `test_step_id` int(11) NOT NULL,
  `test_errors` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports_testresults`
--

INSERT INTO `reports_testresults` (`id`, `test_result`, `screen_generated`, `screen_diff`, `executed`, `start_time`, `end_time`, `device_id`, `test_build_id`, `test_name_id`, `test_step_id`, `test_errors`) VALUES
(1, 1, '', '', '2019-01-16 00:00:00.000000', '2019-01-16 00:00:00.000000', '2019-01-16 00:00:00.000000', 1, 1, 1, 1, NULL),
(2, 1, '', '', '2019-01-16 00:00:00.000000', '2019-01-16 00:00:00.000000', '2019-01-16 00:00:00.000000', 1, 1, 1, 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reports_teststeps`
--

CREATE TABLE `reports_teststeps` (
  `id` int(11) NOT NULL,
  `test_step` longtext NOT NULL,
  `test_step_desc` longtext NOT NULL,
  `golden_screen` longtext NOT NULL,
  `created` datetime(6) NOT NULL,
  `test_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports_teststeps`
--

INSERT INTO `reports_teststeps` (`id`, `test_step`, `test_step_desc`, `golden_screen`, `created`, `test_id`) VALUES
(1, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-16 00:00:00.000000', 1),
(2, 'Onboarding message saying - "Welcome to the New Hangouts Chat - An Intelligent communication app built for teams" should be displayed with a "Get started" button', 'Onboarding message saying - "Welcome to the New Hangouts Chat - An Intelligent communication app built for teams" should be displayed with a "Get started" button', '', '2019-01-17 00:00:00.000000', 1),
(3, 'Choose an account should be displayed with all accounts associated with device', 'Choose an account should be displayed with all accounts associated with device', '', '2019-01-16 00:00:00.000000', 2),
(4, 'Add account option should also be available', 'Add account option should also be available', '', '2019-01-16 00:00:00.000000', 2),
(5, 'On choosing account "cancel" and "ok" buttons should be tappable"', 'On choosing account "cancel" and "ok" buttons should be tappable"', '', '2019-01-16 00:00:00.000000', 2),
(6, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-16 00:00:00.000000', 3),
(7, 'Choose an account should be displayed with all accounts associated with device', 'Choose an account should be displayed with all accounts associated with device', '', '2019-01-16 00:00:00.000000', 3),
(8, '"Allow chat to notify you of new messages" dialog should appear on first launch', '"Allow chat to notify you of new messages" dialog should appear on first launch', '', '2019-01-16 00:00:00.000000', 3),
(9, 'Verify whether World view page is displayed', 'Verify whether World view page is displayed', '', '2019-01-16 00:00:00.000000', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indexes for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indexes for table `reports_builds`
--
ALTER TABLE `reports_builds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_builds_project_id_17fe8f51_fk_reports_projects_id` (`project_id`);

--
-- Indexes for table `reports_devices`
--
ALTER TABLE `reports_devices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_devices_project_id_30c9dc68_fk_reports_projects_id` (`project_id`);

--
-- Indexes for table `reports_projects`
--
ALTER TABLE `reports_projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reports_testcases`
--
ALTER TABLE `reports_testcases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_testcases_project_id_c210f3a3_fk_reports_projects_id` (`project_id`);

--
-- Indexes for table `reports_testresultconsolidation`
--
ALTER TABLE `reports_testresultconsolidation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_testresultco_test_build_id_625401e3_fk_reports_b` (`test_build_id`),
  ADD KEY `reports_testresultco_test_case_id_2426cbb0_fk_reports_t` (`test_case_id`),
  ADD KEY `reports_testresultco_test_device_id_4274a34e_fk_reports_d` (`test_device_id`);

--
-- Indexes for table `reports_testresults`
--
ALTER TABLE `reports_testresults`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_testresults_device_id_e87ffe02_fk_reports_devices_id` (`device_id`),
  ADD KEY `reports_testresults_test_build_id_5ec50097_fk_reports_builds_id` (`test_build_id`),
  ADD KEY `reports_testresults_test_name_id_b7280f5d_fk_reports_t` (`test_name_id`),
  ADD KEY `reports_testresults_test_step_id_3de6faeb_fk_reports_t` (`test_step_id`);

--
-- Indexes for table `reports_teststeps`
--
ALTER TABLE `reports_teststeps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_teststeps_test_id_b4702c33_fk_reports_testcases_id` (`test_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `reports_builds`
--
ALTER TABLE `reports_builds`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `reports_devices`
--
ALTER TABLE `reports_devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `reports_projects`
--
ALTER TABLE `reports_projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `reports_testcases`
--
ALTER TABLE `reports_testcases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `reports_testresultconsolidation`
--
ALTER TABLE `reports_testresultconsolidation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `reports_testresults`
--
ALTER TABLE `reports_testresults`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `reports_teststeps`
--
ALTER TABLE `reports_teststeps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `reports_builds`
--
ALTER TABLE `reports_builds`
  ADD CONSTRAINT `reports_builds_project_id_17fe8f51_fk_reports_projects_id` FOREIGN KEY (`project_id`) REFERENCES `reports_projects` (`id`);

--
-- Constraints for table `reports_devices`
--
ALTER TABLE `reports_devices`
  ADD CONSTRAINT `reports_devices_project_id_30c9dc68_fk_reports_projects_id` FOREIGN KEY (`project_id`) REFERENCES `reports_projects` (`id`);

--
-- Constraints for table `reports_testcases`
--
ALTER TABLE `reports_testcases`
  ADD CONSTRAINT `reports_testcases_project_id_c210f3a3_fk_reports_projects_id` FOREIGN KEY (`project_id`) REFERENCES `reports_projects` (`id`);

--
-- Constraints for table `reports_testresultconsolidation`
--
ALTER TABLE `reports_testresultconsolidation`
  ADD CONSTRAINT `reports_testresultco_test_build_id_625401e3_fk_reports_b` FOREIGN KEY (`test_build_id`) REFERENCES `reports_builds` (`id`),
  ADD CONSTRAINT `reports_testresultco_test_case_id_2426cbb0_fk_reports_t` FOREIGN KEY (`test_case_id`) REFERENCES `reports_testcases` (`id`),
  ADD CONSTRAINT `reports_testresultco_test_device_id_4274a34e_fk_reports_d` FOREIGN KEY (`test_device_id`) REFERENCES `reports_devices` (`id`);

--
-- Constraints for table `reports_testresults`
--
ALTER TABLE `reports_testresults`
  ADD CONSTRAINT `reports_testresults_device_id_e87ffe02_fk_reports_devices_id` FOREIGN KEY (`device_id`) REFERENCES `reports_devices` (`id`),
  ADD CONSTRAINT `reports_testresults_test_build_id_5ec50097_fk_reports_builds_id` FOREIGN KEY (`test_build_id`) REFERENCES `reports_builds` (`id`),
  ADD CONSTRAINT `reports_testresults_test_name_id_b7280f5d_fk_reports_t` FOREIGN KEY (`test_name_id`) REFERENCES `reports_testcases` (`id`),
  ADD CONSTRAINT `reports_testresults_test_step_id_3de6faeb_fk_reports_t` FOREIGN KEY (`test_step_id`) REFERENCES `reports_teststeps` (`id`);

--
-- Constraints for table `reports_teststeps`
--
ALTER TABLE `reports_teststeps`
  ADD CONSTRAINT `reports_teststeps_test_id_b4702c33_fk_reports_testcases_id` FOREIGN KEY (`test_id`) REFERENCES `reports_testcases` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
