-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 30, 2019 at 05:51 PM
-- Server version: 5.7.24-0ubuntu0.16.04.1
-- PHP Version: 7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `etouch_auto`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add test steps', 1, 'add_teststeps'),
(2, 'Can change test steps', 1, 'change_teststeps'),
(3, 'Can delete test steps', 1, 'delete_teststeps'),
(4, 'Can add test cases', 2, 'add_testcases'),
(5, 'Can change test cases', 2, 'change_testcases'),
(6, 'Can delete test cases', 2, 'delete_testcases'),
(7, 'Can add test results', 3, 'add_testresults'),
(8, 'Can change test results', 3, 'change_testresults'),
(9, 'Can delete test results', 3, 'delete_testresults'),
(10, 'Can add builds', 4, 'add_builds'),
(11, 'Can change builds', 4, 'change_builds'),
(12, 'Can delete builds', 4, 'delete_builds'),
(13, 'Can add projects', 5, 'add_projects'),
(14, 'Can change projects', 5, 'change_projects'),
(15, 'Can delete projects', 5, 'delete_projects'),
(16, 'Can add devices', 6, 'add_devices'),
(17, 'Can change devices', 6, 'change_devices'),
(18, 'Can delete devices', 6, 'delete_devices'),
(19, 'Can add test result consolidation', 7, 'add_testresultconsolidation'),
(20, 'Can change test result consolidation', 7, 'change_testresultconsolidation'),
(21, 'Can delete test result consolidation', 7, 'delete_testresultconsolidation'),
(22, 'Can add log entry', 8, 'add_logentry'),
(23, 'Can change log entry', 8, 'change_logentry'),
(24, 'Can delete log entry', 8, 'delete_logentry'),
(25, 'Can add group', 9, 'add_group'),
(26, 'Can change group', 9, 'change_group'),
(27, 'Can delete group', 9, 'delete_group'),
(28, 'Can add permission', 10, 'add_permission'),
(29, 'Can change permission', 10, 'change_permission'),
(30, 'Can delete permission', 10, 'delete_permission'),
(31, 'Can add user', 11, 'add_user'),
(32, 'Can change user', 11, 'change_user'),
(33, 'Can delete user', 11, 'delete_user'),
(34, 'Can add content type', 12, 'add_contenttype'),
(35, 'Can change content type', 12, 'change_contenttype'),
(36, 'Can delete content type', 12, 'delete_contenttype'),
(37, 'Can add session', 13, 'add_session'),
(38, 'Can change session', 13, 'change_session'),
(39, 'Can delete session', 13, 'delete_session');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(8, 'admin', 'logentry'),
(9, 'auth', 'group'),
(10, 'auth', 'permission'),
(11, 'auth', 'user'),
(12, 'contenttypes', 'contenttype'),
(4, 'reports', 'builds'),
(6, 'reports', 'devices'),
(5, 'reports', 'projects'),
(2, 'reports', 'testcases'),
(7, 'reports', 'testresultconsolidation'),
(3, 'reports', 'testresults'),
(1, 'reports', 'teststeps'),
(13, 'sessions', 'session');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2019-01-17 16:01:07.899840'),
(2, 'auth', '0001_initial', '2019-01-17 16:01:22.092309'),
(3, 'admin', '0001_initial', '2019-01-17 16:01:25.878814'),
(4, 'admin', '0002_logentry_remove_auto_add', '2019-01-17 16:01:26.539722'),
(5, 'contenttypes', '0002_remove_content_type_name', '2019-01-17 16:01:29.618670'),
(6, 'auth', '0002_alter_permission_name_max_length', '2019-01-17 16:01:29.887302'),
(7, 'auth', '0003_alter_user_email_max_length', '2019-01-17 16:01:30.285705'),
(8, 'auth', '0004_alter_user_username_opts', '2019-01-17 16:01:30.541932'),
(9, 'auth', '0005_alter_user_last_login_null', '2019-01-17 16:01:31.481236'),
(10, 'auth', '0006_require_contenttypes_0002', '2019-01-17 16:01:31.574413'),
(11, 'auth', '0007_alter_validators_add_error_messages', '2019-01-17 16:01:31.667615'),
(12, 'auth', '0008_alter_user_username_max_length', '2019-01-17 16:01:31.854647'),
(13, 'reports', '0001_initial', '2019-01-17 16:01:49.653943'),
(14, 'sessions', '0001_initial', '2019-01-17 16:01:50.533250'),
(15, 'reports', '0002_auto_20190118_1231', '2019-01-18 12:31:40.425529'),
(16, 'reports', '0003_testresults_test_errors', '2019-01-21 08:43:34.319535');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reports_builds`
--

CREATE TABLE `reports_builds` (
  `id` int(11) NOT NULL,
  `build` varchar(300) NOT NULL,
  `executed_date` date NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports_builds`
--

INSERT INTO `reports_builds` (`id`, `build`, `executed_date`, `project_id`) VALUES
(1, 'dynamite-android_20190104-09_RC04', '2019-01-16', 1),
(2, 'dynamite-android_20190111-03_RC03', '2019-01-17', 1),
(3, 'dynamite-android_20190116-01_RC00', '2019-01-17', 1);

-- --------------------------------------------------------

--
-- Table structure for table `reports_devices`
--

CREATE TABLE `reports_devices` (
  `id` int(11) NOT NULL,
  `device_name` longtext NOT NULL,
  `device_os` longtext NOT NULL,
  `device_udid` longtext NOT NULL,
  `created` datetime(6) NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports_devices`
--

INSERT INTO `reports_devices` (`id`, `device_name`, `device_os`, `device_udid`, `created`, `project_id`) VALUES
(1, 'Pixel 2', '8.1.0', 'FA6A40307885', '2019-01-16 00:00:00.000000', 1),
(2, 'Samsung', '8.0.0', 'JKISDSK24923', '2019-01-24 00:00:00.000000', 1);

-- --------------------------------------------------------

--
-- Table structure for table `reports_projects`
--

CREATE TABLE `reports_projects` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` longtext NOT NULL,
  `created` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports_projects`
--

INSERT INTO `reports_projects` (`id`, `name`, `description`, `created`) VALUES
(1, 'Dynamite Automation', 'Dynamite Automation', '2019-01-17 00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `reports_testcases`
--

CREATE TABLE `reports_testcases` (
  `id` int(11) NOT NULL,
  `test_feature` longtext NOT NULL,
  `test_title` longtext NOT NULL,
  `test_name` longtext NOT NULL,
  `test_suite_type` longtext NOT NULL,
  `test_priority` int(11) NOT NULL,
  `created` datetime(6) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports_testcases`
--

INSERT INTO `reports_testcases` (`id`, `test_feature`, `test_title`, `test_name`, `test_suite_type`, `test_priority`, `created`, `status`, `project_id`) VALUES
(1, 'App Launch', 'Verify the Onboarding message upon app launch', 'Onboarding message should be displayed upon app launch', 'Regression', 2, '2019-01-16 00:00:00.000000', 1, 1),
(2, 'App Launch', 'Verify Tapping on "Get started" Button', 'Choose an Account should be displayed on tapping Get Started', 'Regression', 0, '2019-01-16 00:00:00.000000', 1, 1),
(3, 'App Launch', 'Verify login with White listed Accounts', 'To Successfully login to application', 'Regression', 2, '2019-01-16 00:00:00.000000', 1, 1),
(4, 'App Launch', 'Verify login with Non White listed Accounts', 'No Access for Non White listed Accounts', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(5, 'App Launch', 'Verify adding account', 'Adding account upon launching Application', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(6, 'App Launch', 'Verify launching on Offline Mode', 'Verify launching on Offline Mode', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(7, 'World View', 'Verify the UI Sections/Elements displayed in world view', 'All the UI sections/elements must be verified in world view', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(8, 'World View', 'Verify the Unread section rooms rendering in world view', 'Verify the Unread section rooms rendering in world view', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(9, 'World View', 'Verify the Recent section rooms rendering in world view', 'Verify the recent section rooms rendering in world view', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(10, 'World View', 'Verify the starred section rooms rendering in world view', 'Verify the starred section rooms rendering in world view', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(11, 'World View', 'Verify the FAB button functionality displayed in World view', 'Verify the FAB button functionality displayed in World view', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(12, 'World View', 'Verify the FAB button functionality displayed in World view', 'Verify the Filter rooms/DM functionality from World view', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(13, 'Create & Browse Rooms', 'Verify Create Room Page', 'To Verify all details of Create Room Page', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(14, 'Create & Browse Rooms', 'Verify Creating a Room', 'To Create a new room', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(15, 'Create & Browse Rooms', 'Verify Browse Rooms page', 'To verify \'Browse Rooms\' page', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(16, 'Create & Browse Rooms', 'Verify Joining & Leaving Room from Browse rooms', 'Joining and Leaving Room', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(17, 'Create & Browse Rooms', 'Verify Room Preview from Browse rooms', 'Room Preview Page', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(18, 'Create & Browse Rooms', 'Verify Members Count of Rooms in Rooms You\'ve been invited to', 'Verify the count in rooms', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(19, 'Create & Browse Rooms', 'Verify Add room in offline mode', 'Behaviour of add room page in offline mode', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(20, 'Create & Browse Rooms', 'Verify New (red label) for Rooms you\'ve been invited to', 'Verify New (red label) for Rooms you\'ve been invited to', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(21, 'Room Preview', 'Verify the user navigation to room preview screens', 'Verify the user navigation to room preview screens', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(22, 'Room Preview', 'Verify the Join option from room details through room preview', 'Verify the Join option from room details through room preview', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(23, 'Room Preview', 'Verify the Join option from room preview', 'Verify the Join option from room preview', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(24, 'Room Preview', 'Verify the collapsed summary functionality in Room preview', 'Verify the collapsed summary functionality in Room preview', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(25, 'Room Preview', 'Verify the UI elements/sections rendering in Room preview', 'Verify the UI elements/sections rendering in Room preview', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(26, 'Room Preview', 'Verify the Long press message options in Room preview', 'Verify the Long press message options in Room preview', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(27, 'Faster DM & Group DM', 'Verify the navigation to Conversation launcher from world view', 'Verify the navigation to Direct message screen from world view', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(28, 'Faster DM & Group DM', 'Verify the Faster DM screen functionality', 'Verify the Direct Message contacts screen functionality', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(29, 'Faster DM & Group DM', 'Verify if the user is able to create a group DM', 'Verify if the user is able to create a group DM', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(30, 'Faster DM & Group DM', 'Verify the OTR functionality', 'Verify the OTR functionality', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(31, 'DM Details', 'Verify DM Details Page', 'To Verify DM Details Page', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(32, 'DM Details', 'Verify Star & Unstar', 'To Verify Star & Unstar', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(33, 'DM Details', 'Verify Notifications setting for Room Details', 'To Verify Notifications setting for Room Details', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(34, 'DM Details', 'Verify the Members list', 'To Verify the Members list', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(35, 'DM Details', 'Verify Hide functionality from DM', 'To Verify Hide functionality from DM', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(36, 'Space View', 'Verify the Nav bar functionality in space view', 'Verify the functionality of the nav bar in space view', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(37, 'Space View', 'Verify the persistent header in space view', 'Verify the persistent header in space view', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(38, 'Space View', 'Verify topics pinning in Space view', 'Verify topics pinning in Space view', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(39, 'Space View', 'Verify timestamps, avatars, date dividers and unread labels in space view', 'Verify timestamps, date dividers and unread labels in space view', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(40, 'Space View', 'Verify timestamps, avatars, date dividers and unread labels in space view', 'Verify the collapsed summary functionality in space view', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(41, 'Space View', 'Verify the senders summary naming in Collapsed messages summary', 'Verify the senders summary naming in Collapsed messages summary', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(42, 'Space View', 'Verify the new conversation FAB in space view', 'Verify the new conversation FAB in space view', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(43, 'Space View', 'Verify if the files are doc files are launched in native apps from space view', 'Verify if the files are doc files are launched in native apps from space view', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(44, 'Space View', 'Verify the space-topic view transitions', 'Verify the space-topic view transitions', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(45, 'Space View', 'Verify the space view rendering with topics containing different types of data', 'Verify the space view rendering with topics containing different types of data', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(46, 'Space View', 'Verify the System messages generated in space view', 'Verify the System messages generated in space view', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(47, 'Space View', 'Verify editing messages functionality from Space view', 'Verify editing messages functionality from Space view', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(48, 'Space View', 'Verify Add people functionality from persistent header in space view', 'Verify Add people functionality from persistent header in space view', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(49, 'Room Details', 'Verify Room Details Page', 'Verify Room Details Page', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(50, 'Room Details', 'Verify Edit Room name', 'Verify Edit Room name', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(51, 'Room Details', 'Verify adding People & Bots', 'Verify adding People & Bots', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(52, 'Room Details', 'Verify Star & Unstar', 'Verify Star & Unstar', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(53, 'Room Details', 'Verify Notifications for Room Details', 'Verify Notifications for Room Details', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(54, 'Room Details', 'Verify Leave Room', 'Verify Leave Room', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(55, 'Room Details', 'Verify the Members list', 'Verify the Members list', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(56, 'Room Details', 'Verify the Can join list', 'Verify the Can join list', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(57, 'Topic View', 'Verify the existing Topic view rendering of UI elements/Sections', 'Verify the existing Topic view rendering of UI elements/Sections', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(58, 'Topic View', 'Verify the New Conversation view rendering of UI elements/Sections', 'Verify the New Conversation view rendering of UI elements/Sections', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(59, 'Topic View', 'Verify the Compose field components in topic view', 'Verify the Compose field components in topic view', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(60, 'Topic View', 'Verify the Send button functionality in compose view', 'Verify the Send button functionality in compose view', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(61, 'Topic View', 'Verify the Upload icon functionality in compose view', 'Verify the Upload icon functionality in compose view', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(62, 'Topic View', 'Verify the Camera icon functionality in compose', 'Verify the Camera icon functionality in compose', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(63, 'Topic View', 'Verify the Thor icon functionality in compose', 'Verify the Thor icon functionality in compose', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(64, 'Topic View', 'Verify the Attachment replace functionality in compose view', 'Verify the Attachment replace functionality in compose view', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(65, 'Topic View', 'Verify the edit message functionality in topic view', 'Verify the edit message functionality in topic view', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(66, 'Topic View', 'Verify the failed message options in offline mode', 'Verify the failed message options in offline mode', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(67, 'Topic View', 'Verify notification settings in Topic view.', 'Verify notification settings in Topic view.', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(68, 'Topic View', 'Verify pagination in Topic view', 'Verify pagination in Topic view', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(69, 'Space Search', 'Verify notification settings in Topic view.', 'Verify notification settings in Topic view.', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(70, 'Space Search', 'Verify pagination in Topic view', 'Verify pagination in Topic view', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(71, 'Space Search', 'Verify the Space search navigation and UI of space search in Zero state', 'Verify the Space search navigation and UI of space search in Zero state', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(72, 'Space Search', 'Verify the Space search functionality based on keyword', 'Verify the Space search functionality based on keyword', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(73, 'Space Search', 'Verify the Space search functionality based on member selection from the filter panel', 'Verify the Space search functionality based on member selection from the filter panel', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(74, 'Space Search', 'Verify the Space search functionality based on Content type selection from the filter panel', 'Verify the Space search functionality based on Content type selection from the filter panel', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(75, 'Notifications', 'Verify the notifications options menu from Settings', 'Verify the notifications options menu from Settings', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(76, 'Notifications', 'Verify notifications disabling from device settings', 'Verify notifications disabling from device settings', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(77, 'Autocomplete', 'Verify the Autocomplete rendering in Room_Existing Topic view', 'Verify the Autocomplete rendering in Room_Existing Topic view', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(78, 'Autocomplete', 'Verify the Autocomplete rendering and functionality in New room', 'Verify the Autocomplete rendering and functionality in New room', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(79, 'Autocomplete', 'Verify the Autocomplete rendering and functionality in 1:1 DM', 'Verify the Autocomplete rendering and functionality in 1:1 DM', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(80, 'Autocomplete', 'Verify the Autocomplete rendering and functionality in group DM', 'Verify the Autocomplete rendering and functionality in group DM', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(81, '@Mentions', 'Verify @mention functionality', 'Verify @mention functionality', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(82, '@Mentions', 'Verify @all mentions functionality', 'Verify @all mentions functionality', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(83, '@Mentions', 'Verify adding people functionality using @mention', 'Verify adding people functionality using @mention', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(84, 'Presence Indicators', 'Verify the presence indicators display across the app', 'Verify the presence indicators display across the app', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(85, 'Miscellaneous', 'Verify the app behaviour and UI upon font settings change', 'Verify the app behaviour and UI upon font settings change', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(86, 'Miscellaneous', 'Verify feedback functionality in app', 'Verify feedback functionality in app', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(87, 'Message Delete', 'Verify the Delete message functionality in topic view', 'Verify the Delete message functionality in topic view', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(88, 'Message Delete', 'Verify the Delete message functionality in topic view', 'Verify the Delete message functionality in topic view', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(89, 'Message Delete', 'Verify the Delete message functionality in space view', 'Verify the Delete message functionality in space view', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(90, 'Message Delete', 'Verify the Delete message functionality in Offline', 'Verify the Delete message functionality in Offline', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(91, 'Bots', 'Verify the Bot option displayed in FAB of World view', 'Verify the Bot option displayed in FAB of World view', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(92, 'Bots', 'Verify the Bot DM functionality from the "Add Bot" screen', 'Verify the Bot DM functionality from the "Add Bot" screen', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(93, 'Bots', 'Verify the Bot DM Details screen', 'Verify the Bot DM Details screen', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(94, 'Bots', 'Verify the remove option functionality for Bot DM', 'Verify the remove option functionality for Bot DM', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(95, 'Content Sharing', 'Verify Content sharing without app login', 'Verify Content sharing without app login', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(96, 'Content Sharing', 'Verify Content sharing with app login', 'Verify Content sharing with app login', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(97, 'Content Sharing', 'Verify content sharing in offline mode', 'Verify content sharing in offline mode', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(98, 'Bots in Rooms', 'Verify if the user is able to add a bot to the room by @mentioning', 'Verify if the user is able to add a bot to the room by @mentioning', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(99, 'Bots in Rooms', 'Verify if the user is able to add a bot to the room through invite flow', 'Verify if the user is able to add a bot to the room through invite flow', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(100, 'Bots in Rooms', 'Verify if the user is able to remove a bot from the room', 'Verify if the user is able to remove a bot from the room', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(101, 'Enhanced Invite Emails', 'Verify if the invite email is sent to the user on invitation to room', 'Verify if the invite email is sent to the user on invitation to room', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(102, 'Enhanced Invite Emails', 'Verify if the invite email is sent to all the users of group on invitation to room', 'Verify if the invite email is sent to all the users of group on invitation to room', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(103, 'Flat DM', 'Verify UI changes of DM', 'Observe UI changes of DM', 'Regression', 0, '2019-01-30 00:00:00.000000', 1, 1),
(104, 'Flat DM', 'Verify History ON/OFF functionality', 'Verify History ON/OFF functionality', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(105, 'Flat DM', 'Verify edit/delete message functionality in flat DMs.', 'Verify edit/delete message functionality in flat DMs.', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(106, 'Flat DM', 'Verify offline handling in flat DMs', 'Verify offline handling in flat DMs', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(107, 'Both Auth & Disabled bots', 'Verify @mentioned bot successfully respond.', 'Verify @mentioned bot successfully respond.', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(108, 'Local stream Data', 'Verify messages loading in Offline/Slow-network loading', 'Verify messages loading in Offline/Slow-network loading', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(109, 'Jump to Bottom', 'Verify jump to bottom FAB in READ rooms', 'Verify jump to bottom FAB in READ rooms', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(110, 'Jump to Bottom', 'Verify that jump to bottom FAB is not displayed in DM\'s', 'Verify that jump to bottom FAB is not displayed in DM\'s', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(111, 'Navigation Drawer', 'Verify Navigation Panel available after Login', 'Verify Navigation Panel available after Login', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(112, 'Navigation Drawer', 'Verify menu contents in Navigation', 'Verify menu contents in Navigation', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(113, 'Do Not Disturb & Dedicated Settings', 'Verify user status on world view header', 'Verify user status on world view header', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(114, 'Do Not Disturb & Dedicated Settings', 'Verify Do Not Disturb feature from navigation drawer', 'Verify Do Not Disturb feature from navigation drawer', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(115, 'Do Not Disturb & Dedicated Settings', 'Verify (Dedicated Settings) Settings -> Mobile Notifications from nav drawer', 'Verify (Dedicated Settings) Settings -> Mobile Notifications from nav drawer', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(116, 'Reactions', 'Verify adding reaction on self/other member/bot message in rooms/dms', 'Verify adding reaction on self/other member/bot message in rooms/dms', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(117, 'Topic View', 'Verify adding reaction on self/other member/bot message in rooms/dms', 'Verify if the user is able to successfully create the draft in topic view', 'Regression', 1, '2019-01-30 00:00:00.000000', 1, 1),
(118, 'Topic View', 'Verify if the user is able to successfully create a draft with attachment', 'Verify if the user is able to successfully create a draft with attachment', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(119, 'Presence Indicators', 'Verify the presence indicator for active status', 'Verify the presence indicator for active status', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1),
(120, 'Presence Indicators', 'Verify the presence indicator for inactive status', 'Verify the presence indicator for inactive status', 'Regression', 2, '2019-01-30 00:00:00.000000', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `reports_testresultconsolidation`
--

CREATE TABLE `reports_testresultconsolidation` (
  `id` int(11) NOT NULL,
  `test_status` int(11) NOT NULL,
  `bugid` varchar(50) NOT NULL,
  `bug_status` varchar(50) NOT NULL,
  `execution_time` double NOT NULL,
  `executed_date` date NOT NULL,
  `test_build_id` int(11) NOT NULL,
  `test_case_id` int(11) NOT NULL,
  `test_device_id` int(11) NOT NULL,
  `test_device_logs` longtext,
  `test_server_logs` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports_testresultconsolidation`
--

INSERT INTO `reports_testresultconsolidation` (`id`, `test_status`, `bugid`, `bug_status`, `execution_time`, `executed_date`, `test_build_id`, `test_case_id`, `test_device_id`, `test_device_logs`, `test_server_logs`) VALUES
(1, 1, '', '', 2, '2019-01-16', 1, 1, 1, NULL, NULL),
(2, 1, '', '', 2, '2019-01-16', 1, 2, 1, NULL, NULL),
(3, 0, 'b/123456', '', 1, '2019-01-16', 1, 3, 1, NULL, NULL),
(4, 1, '', '', 2, '2019-01-17', 2, 1, 1, NULL, NULL),
(5, 0, 'b/123456', '', 1, '2019-01-17', 2, 2, 1, NULL, NULL),
(6, 1, '', '', 2, '2019-01-17', 3, 1, 1, NULL, NULL),
(7, 1, '', '', 2, '2019-01-17', 3, 2, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reports_testresults`
--

CREATE TABLE `reports_testresults` (
  `id` int(11) NOT NULL,
  `test_result` tinyint(1) NOT NULL COMMENT '1:pass,0:fail',
  `screen_generated` longtext NOT NULL,
  `screen_diff` longtext NOT NULL,
  `executed` datetime(6) NOT NULL,
  `start_time` datetime(6) NOT NULL,
  `end_time` datetime(6) NOT NULL,
  `device_id` int(11) NOT NULL,
  `test_build_id` int(11) DEFAULT NULL,
  `test_name_id` int(11) NOT NULL,
  `test_step_id` int(11) NOT NULL,
  `test_errors` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports_testresults`
--

INSERT INTO `reports_testresults` (`id`, `test_result`, `screen_generated`, `screen_diff`, `executed`, `start_time`, `end_time`, `device_id`, `test_build_id`, `test_name_id`, `test_step_id`, `test_errors`) VALUES
(1, 1, '', '', '2019-01-16 00:00:00.000000', '2019-01-16 00:00:00.000000', '2019-01-16 00:00:00.000000', 1, 1, 1, 1, NULL),
(2, 1, '', '', '2019-01-16 00:00:00.000000', '2019-01-16 00:00:00.000000', '2019-01-16 00:00:00.000000', 1, 1, 1, 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reports_teststeps`
--

CREATE TABLE `reports_teststeps` (
  `id` int(11) NOT NULL,
  `test_step` longtext NOT NULL,
  `test_step_desc` longtext NOT NULL,
  `golden_screen` longtext NOT NULL,
  `created` datetime(6) NOT NULL,
  `test_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports_teststeps`
--

INSERT INTO `reports_teststeps` (`id`, `test_step`, `test_step_desc`, `golden_screen`, `created`, `test_id`) VALUES
(1, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-16 00:00:00.000000', 1),
(2, 'Onboarding message saying - "Welcome to the New Hangouts Chat - An Intelligent communication app built for teams" should be displayed with a "Get started" button', 'Onboarding message saying - "Welcome to the New Hangouts Chat - An Intelligent communication app built for teams" should be displayed with a "Get started" button', '', '2019-01-17 00:00:00.000000', 1),
(3, 'Choose an account should be displayed with all accounts associated with device', 'Choose an account should be displayed with all accounts associated with device', '', '2019-01-16 00:00:00.000000', 2),
(4, 'Add account option should also be available', 'Add account option should also be available', '', '2019-01-16 00:00:00.000000', 2),
(5, 'On choosing account "cancel" and "ok" buttons should be tappable"', 'On choosing account "cancel" and "ok" buttons should be tappable"', '', '2019-01-16 00:00:00.000000', 2),
(6, 'App should be launched successfully', 'App should be launched successfully', '', '2019-01-16 00:00:00.000000', 3),
(7, 'Choose an account should be displayed with all accounts associated with device', 'Choose an account should be displayed with all accounts associated with device', '', '2019-01-16 00:00:00.000000', 3),
(8, '"Allow chat to notify you of new messages" dialog should appear on first launch', '"Allow chat to notify you of new messages" dialog should appear on first launch', '', '2019-01-16 00:00:00.000000', 3),
(9, 'Verify whether World view page is displayed', 'Verify whether World view page is displayed', '', '2019-01-16 00:00:00.000000', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indexes for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indexes for table `reports_builds`
--
ALTER TABLE `reports_builds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_builds_project_id_17fe8f51_fk_reports_projects_id` (`project_id`);

--
-- Indexes for table `reports_devices`
--
ALTER TABLE `reports_devices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_devices_project_id_30c9dc68_fk_reports_projects_id` (`project_id`);

--
-- Indexes for table `reports_projects`
--
ALTER TABLE `reports_projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reports_testcases`
--
ALTER TABLE `reports_testcases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_testcases_project_id_c210f3a3_fk_reports_projects_id` (`project_id`);

--
-- Indexes for table `reports_testresultconsolidation`
--
ALTER TABLE `reports_testresultconsolidation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_testresultco_test_build_id_625401e3_fk_reports_b` (`test_build_id`),
  ADD KEY `reports_testresultco_test_case_id_2426cbb0_fk_reports_t` (`test_case_id`),
  ADD KEY `reports_testresultco_test_device_id_4274a34e_fk_reports_d` (`test_device_id`);

--
-- Indexes for table `reports_testresults`
--
ALTER TABLE `reports_testresults`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_testresults_device_id_e87ffe02_fk_reports_devices_id` (`device_id`),
  ADD KEY `reports_testresults_test_build_id_5ec50097_fk_reports_builds_id` (`test_build_id`),
  ADD KEY `reports_testresults_test_name_id_b7280f5d_fk_reports_t` (`test_name_id`),
  ADD KEY `reports_testresults_test_step_id_3de6faeb_fk_reports_t` (`test_step_id`);

--
-- Indexes for table `reports_teststeps`
--
ALTER TABLE `reports_teststeps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_teststeps_test_id_b4702c33_fk_reports_testcases_id` (`test_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `reports_builds`
--
ALTER TABLE `reports_builds`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `reports_devices`
--
ALTER TABLE `reports_devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `reports_projects`
--
ALTER TABLE `reports_projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `reports_testcases`
--
ALTER TABLE `reports_testcases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;
--
-- AUTO_INCREMENT for table `reports_testresultconsolidation`
--
ALTER TABLE `reports_testresultconsolidation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `reports_testresults`
--
ALTER TABLE `reports_testresults`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `reports_teststeps`
--
ALTER TABLE `reports_teststeps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `reports_builds`
--
ALTER TABLE `reports_builds`
  ADD CONSTRAINT `reports_builds_project_id_17fe8f51_fk_reports_projects_id` FOREIGN KEY (`project_id`) REFERENCES `reports_projects` (`id`);

--
-- Constraints for table `reports_devices`
--
ALTER TABLE `reports_devices`
  ADD CONSTRAINT `reports_devices_project_id_30c9dc68_fk_reports_projects_id` FOREIGN KEY (`project_id`) REFERENCES `reports_projects` (`id`);

--
-- Constraints for table `reports_testcases`
--
ALTER TABLE `reports_testcases`
  ADD CONSTRAINT `reports_testcases_project_id_c210f3a3_fk_reports_projects_id` FOREIGN KEY (`project_id`) REFERENCES `reports_projects` (`id`);

--
-- Constraints for table `reports_testresultconsolidation`
--
ALTER TABLE `reports_testresultconsolidation`
  ADD CONSTRAINT `reports_testresultco_test_build_id_625401e3_fk_reports_b` FOREIGN KEY (`test_build_id`) REFERENCES `reports_builds` (`id`),
  ADD CONSTRAINT `reports_testresultco_test_case_id_2426cbb0_fk_reports_t` FOREIGN KEY (`test_case_id`) REFERENCES `reports_testcases` (`id`),
  ADD CONSTRAINT `reports_testresultco_test_device_id_4274a34e_fk_reports_d` FOREIGN KEY (`test_device_id`) REFERENCES `reports_devices` (`id`);

--
-- Constraints for table `reports_testresults`
--
ALTER TABLE `reports_testresults`
  ADD CONSTRAINT `reports_testresults_device_id_e87ffe02_fk_reports_devices_id` FOREIGN KEY (`device_id`) REFERENCES `reports_devices` (`id`),
  ADD CONSTRAINT `reports_testresults_test_build_id_5ec50097_fk_reports_builds_id` FOREIGN KEY (`test_build_id`) REFERENCES `reports_builds` (`id`),
  ADD CONSTRAINT `reports_testresults_test_name_id_b7280f5d_fk_reports_t` FOREIGN KEY (`test_name_id`) REFERENCES `reports_testcases` (`id`),
  ADD CONSTRAINT `reports_testresults_test_step_id_3de6faeb_fk_reports_t` FOREIGN KEY (`test_step_id`) REFERENCES `reports_teststeps` (`id`);

--
-- Constraints for table `reports_teststeps`
--
ALTER TABLE `reports_teststeps`
  ADD CONSTRAINT `reports_teststeps_test_id_b4702c33_fk_reports_testcases_id` FOREIGN KEY (`test_id`) REFERENCES `reports_testcases` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
