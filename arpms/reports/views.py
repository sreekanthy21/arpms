# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime, time
from django.core.paginator import Paginator
from django.template.response import TemplateResponse
from django.http import HttpResponse
from django.shortcuts import  render, redirect
import json
from models import TestResultConsolidation
from models import TestCases
from models import TestResults
from models import Builds
from serializers import *
from datetime import datetime, timedelta
from time import gmtime, strftime
import os
import zipfile
from django.contrib import messages
from reports.generate_report import *
from PIL import Image




def index(request):
    test_results, recent, test_cases, test_details = {}, None, {}, {}
    recentbuild = Builds.objects.all().order_by('-executed_date')
    recent_date = recentbuild[0].executed_date
    previous_date = recentbuild[0].executed_date - timedelta(days=1)

    recent = Builds.objects.filter(executed_date__in=[recent_date,previous_date]).order_by('-id')
    buildscount = recent.count()

    build_details = []

    pass_count, fail_count, flaky_count = 0, 0, 0
    for builds in recent:
        testCases = []
        test_cases = TestResultConsolidation.objects.filter(test_build=builds.id,executed_date__startswith=builds.executed_date)
        success, fail, bugs = 0, 0, 0
        testcases_for_builds = []
        for testDetails in test_cases:
            if testDetails.test_status == 1:
                success += 1
            else:
                fail += 1
            if testDetails.bugid != '':
                bugs += 1
            total = success + fail
            flaky = fail - bugs if fail - bugs > 0 else 0
            device_name = testDetails.test_device.device_name
            device_os = testDetails.test_device.device_os

            # Checking whether data is present for build or not
            testcases_for_builds.append({'build': testDetails.test_build.build})
            testCases.append({'build': testDetails.test_build.build, 'feature': testDetails.test_case.test_feature, 'testname': testDetails.test_case.test_name, 'execution_time': testDetails.execution_time, 'priority': testDetails.test_case.test_priority, 'status': testDetails.test_status, 'bugid': testDetails.bugid, 'test_case_id':testDetails.test_case, 'executed_date': testDetails.executed_date})

        if len(testcases_for_builds) > 0:
            pass_count += success
            fail_count += fail
            flaky_count += flaky
            build_details.append({'id': builds.id, 'build': builds.build, 'executed_date': builds.executed_date, 'total': total, 'pass': success, 'fail': fail, 'flaky': flaky, 'bugs': bugs, 'device_name': device_name, 'device_os': device_os})
    return TemplateResponse(request, 'dashboard.html', {'buildscount': buildscount, 'builds': build_details,'test_details': testCases, 'pass_count': pass_count, 'fail_count': fail_count, 'flaky_count': flaky_count})


def searchbuild(request):
    build_date = request.POST['build_date']
    build_name = request.POST['build_name']

    if build_date != "":
        previous_date = datetime.strptime(build_date, '%Y-%m-%d') - timedelta(days=1)

    if build_date != "" and build_name != "":
        builds = BuildsSerializer(Builds.objects.filter(executed_date__in=[build_date,previous_date],build__startswith=build_name).order_by('-id'), many=True)
    elif build_name != "":
        builds = BuildsSerializer(Builds.objects.filter(build__startswith=build_name).order_by('-id'), many=True)
    elif build_date != "":
        builds = BuildsSerializer(Builds.objects.filter(executed_date__in=[build_date,previous_date]).order_by('-id'), many=True)

    buildscount = 0
    for build_details in builds.data:
        buildscount += 1
        test_details = TestResultConsolidation.objects.filter(test_build=build_details['id']).order_by('-test_case_id')
        alist = []
        success, fail, bugs = 0, 0, 0
        testcases_for_builds = []
        for testDetails in test_details:
            if testDetails.test_status == 1:
                success += 1
            else:
                fail += 1
            if testDetails.bugid != '':
                bugs += 1
            flaky = fail - bugs if fail - bugs > 0 else 0
            build_details['device_name'] = testDetails.test_device.device_name
            build_details['device_os'] = testDetails.test_device.device_os
            build_details['success'] = success
            build_details['fail'] = fail
            build_details['total'] = success + fail
            build_details['bugs'] = bugs
            build_details['flaky'] = flaky


            testcases_for_builds.append({'id': testDetails.test_build.build})
            if len(testcases_for_builds) > 0:
                # To get testresults based on build id and test case id
                test_results = TestResults.objects.filter(test_build_id = build_details['id'], test_name = testDetails.test_case.id)
                testresults = []
                for testResults in test_results:
                    testresults.append({'test_step': str(testResults.test_step.test_step), 'screen_diff': testResults.screen_diff,  'test_step_id': str(testResults.test_step.id)})

                alist.append({'build': testDetails.test_build.build, 'feature': testDetails.test_case.test_feature, 'testname': testDetails.test_case.test_name, 'execution_time': testDetails.execution_time, 'priority': testDetails.test_case.test_priority, 'status': testDetails.test_status, 'bugid': testDetails.bugid, 'devicelogs': testDetails.test_device_logs, 'serverlogs': testDetails.test_server_logs, 'testResults': testresults})


            build_details['tcases'] = alist
    result = json.dumps(builds.data)
    response = {'buildscount': buildscount, 'builds_data': json.loads(result)}
    return HttpResponse(json.dumps(response))
    # return TemplateResponse(request, 'builds_data.html', response)


def buildtestcases(request):
    build_id = request.GET.get('build_id')
    test_cases = TestResultConsolidation.objects.filter(test_build__build=build_id).order_by('test_case_id')
    testCases = []
    for testDetails in test_cases:
        #To Get testresults based on test case id and build id
        test_results = TestResults.objects.filter(test_build_id = testDetails.test_build.id, test_name = testDetails.test_case.id)
        testresults = []
        for testResults in test_results:
            testresults.append({'test_step': str(testResults.test_step.test_step), 'screen_diff': testResults.screen_diff,  'test_step_id': str(testResults.test_step.id)})

        testCases.append({'build_id': testDetails.test_build.id, 'build': testDetails.test_build.build, 'feature': testDetails.test_case.test_feature, 'testname': testDetails.test_case.test_name, 'execution_time': testDetails.execution_time, 'priority': testDetails.test_case.test_priority, 'status': testDetails.test_status, 'bugid': testDetails.bugid, 'test_case_id': testDetails.test_case.id, 'executed_date': str(testDetails.executed_date), 'test_device_id': testDetails.test_device_id, 'devicelogs': testDetails.test_device_logs, 'serverlogs': testDetails.test_server_logs, 'testResults': testresults})

    response = {'testCases': testCases}
    return HttpResponse(json.dumps(response))



def buildcharts(request):
    build_date = request.GET.get('build_date')
    build_name = request.GET.get('build_name')

    if build_date != "":
        previous_date = datetime.strptime(request.GET.get('build_date'), '%Y-%m-%d') - timedelta(days=1)

    if build_date != "" and build_name != "":
        recent = Builds.objects.filter(executed_date__in=[build_date,previous_date],build__startswith=build_name).order_by('-id')
    elif build_name != "":
        recent = Builds.objects.filter(build__startswith=build_name).order_by('-id')
    elif build_date != "":
        recent = Builds.objects.filter(executed_date__in=[build_date,previous_date]).order_by('-id')

    buildscount = recent.count()
    build_details = []
    pass_count, fail_count, flaky_count = 0, 0, 0
    for builds in recent:
        testCases = []
        test_cases = TestResultConsolidation.objects.filter(test_build=builds.id,executed_date__startswith=builds.executed_date)
        success, fail, bugs = 0, 0, 0
        testcases_for_builds = []
        for testDetails in test_cases:
            if testDetails.test_status == 1:
                success += 1
            else:
                fail += 1
            if testDetails.bugid != '':
                bugs += 1
            total = success + fail
            flaky = fail - bugs if fail - bugs > 0 else 0
            device_name = testDetails.test_device.device_name
            device_os = testDetails.test_device.device_os

            # Checking whether data is present for build or not
            testcases_for_builds.append({'build': testDetails.test_build.build})
            testCases.append({'build': testDetails.test_build.build, 'feature': testDetails.test_case.test_feature, 'testname': testDetails.test_case.test_name, 'execution_time': testDetails.execution_time, 'priority': testDetails.test_case.test_priority, 'status': testDetails.test_status, 'bugid': testDetails.bugid, 'test_case_id':testDetails.test_case, 'executed_date': testDetails.executed_date})

        if len(testcases_for_builds) > 0:
            pass_count += success
            fail_count += fail
            flaky_count += flaky
            build_details.append({'id': builds.id, 'build': builds.build, 'executed_date': builds.executed_date, 'total': total, 'pass': success, 'fail': fail, 'flaky': flaky, 'bugs': bugs, 'device_name': device_name, 'device_os': device_os})
    return TemplateResponse(request, 'buildcharts.html', {'buildscount': buildscount, 'builds': build_details,'test_details': testCases, 'pass_count': pass_count, 'fail_count': fail_count, 'flaky_count': flaky_count})



def testresults(request):
    test_case_id = request.GET.get('test_case_id')
    # executed_date = request.GET.get('executed_date') executed__startswith = executed_date
    build_id = request.GET.get('build_id')
    device_id = request.GET.get('device_id')
    test_results = TestResults.objects.filter(test_build_id = build_id, device = device_id, test_name_id = test_case_id).order_by('-test_name_id')
    testresults = []
    for testDetails in test_results:
        if testDetails.test_result == 1:
            status = 'Pass'
        else:
            status = 'Fail'
        testresults.append({'test_step': str(testDetails.test_step.test_step), 'test_result': status, 'screen_diff': testDetails.screen_diff, 'screen_generated': testDetails.screen_generated, 'test_step_id': str(testDetails.test_step.id), 'error_msg': testDetails.test_errors})
    response = {'testresults': testresults}
    return HttpResponse(json.dumps(response))


def updatestatus(request):
    return TemplateResponse(request, 'updatestatus.html')

def updatetestcase(request):
    build_id = request.GET.get('build_id')
    test_case_id = request.GET.get('test_case_id')
    bugid = request.GET.get('bugid')
    bugstatus = request.GET.get('bugstatus')
    testcase_status = request.GET.get('testcase_status')
    feature = request.GET.get('feature')
    testcase = request.GET.get('testcase')
    buildname = request.GET.get('buildname')
    testcase_sno = request.GET.get('testcase_sno')
    device_id = request.GET.get('device_id')
    test_step_id = request.GET.get('test_step_id')

    if test_step_id != "":
        TestResultConsolidation.objects.filter(test_build=build_id,test_case=test_case_id).update(bugid=bugid,bug_status=bugstatus,test_status=testcase_status)
        TestResults.objects.filter(test_build=build_id,test_name=test_case_id,test_step=test_step_id).update(test_result=testcase_status)
    else:
        TestResultConsolidation.objects.filter(test_build=build_id,test_case=test_case_id).update(bugid=bugid,bug_status=bugstatus,test_status=testcase_status)
        TestResults.objects.filter(test_build=build_id,test_name=test_case_id).update(test_result=testcase_status)
    if testcase_status == '1':
        status = 'Pass'
    else:
        status = 'Fail'
    rowDetails = {'device_id':device_id,'testcase_sno':testcase_sno,'build':build_id,'feature':feature,'title':testcase,'status':status,'buildname':buildname,'test_case_id':test_case_id,'build_id':build_id,'bugid':bugid}
    response = {"message": "Test case updated successfully","rowDetails":rowDetails}
    # response = {'build_id':build_id,'test_case_id':test_case_id,'bugid':bugid,'bugstatus':bugstatus,'testcase_status':testcase_status}
    return HttpResponse(json.dumps(response))


def buildaction(request):
    builds = Builds.objects.all().order_by('-id')
    totalbuilds = []
    for buildsdata in builds:
        totalbuilds.append({'buildid': buildsdata.id, 'build': buildsdata.build, 'executed_date': buildsdata.executed_date, 'type': buildsdata.type})
    return TemplateResponse(request, 'builds.html', {'builds': totalbuilds})

def deletebuild(request):
    build_id = request.GET.get('build_id')
    Builds.objects.filter(id=build_id).delete()
    TestResultConsolidation.objects.filter(test_build=build_id).delete()
    TestResults.objects.filter(test_build=build_id).delete()
    return HttpResponse(build_id)


def createbuild(request):
    build_id = request.GET.get('build_id')
    type = request.GET.get('type')
    date = strftime("%Y-%m-%d", gmtime())
    project = Projects.objects.get(id=1)
    build = Builds(project=project, build=build_id, executed_date=date, type=type)
    build.save()
    return HttpResponse(build.id)


def upload(request):
    if request.method == 'POST':
        handle_uploaded_file(request.FILES['file'], str(request.FILES['file']))
        messages.add_message(request, messages.SUCCESS, 'File uploaded successfully')
        return redirect('/')
    messages.add_message(request, messages.ERROR, 'File upload failed')
    return redirect('/')

def handle_uploaded_file(file, filename):
    with open('static/' + filename, 'wb+') as destination:
        for chunk in file.chunks():
            destination.write(chunk)
    zip_ref = zipfile.ZipFile('static/' + filename, 'r')
    zip_ref.extractall('static/')
    images = []
    foldername = os.path.splitext(filename)[0]
    files = os.listdir("static/" + foldername)
    for filename in files:
        if filename.endswith(('.jpg', '.jpeg', '.png')):
            images.append(filename)
    for imagename in images:
        image = Image.open("static/" + foldername + "/" + imagename)
        resizedimage = image.resize((1440,2560),Image.ANTIALIAS)
        os.remove("static/" + foldername + "/" + imagename)
        resizedimage.save("static/" + foldername + "/" + imagename, quality=95)
    zip_ref.close()


def uploadfile(request):
    return render(request, 'upload.html')

def importjson(request):
    builds = Builds.objects.all().order_by('-id')
    projects = Projects.objects.all().order_by('-id')
    devices = Devices.objects.all().order_by('-id')
    return render(request, 'importjsons.html', {'builds': builds, 'projects': projects, 'devices': devices})

def importjsonfiles(request):
    if request.method == 'POST':
        project = request.POST['project']
        build = request.POST['build']
        device_id = request.POST['device']
        directory = request.POST['directory']
        gvo = GenerateReportData(device_id, build, directory)
        data = gvo.insert_test_results(device_id, build, directory)
        if data == 'All test cases import is done successfully!':
            messages.add_message(request, messages.SUCCESS, data)
        else:
            messages.add_message(request, messages.ERROR, data)
        return redirect('/')
