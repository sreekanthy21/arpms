from django.contrib.auth.models import User, Group
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.db import models
from django.template.context_processors import csrf
from django.db.models import Q
from rest_framework.validators import UniqueTogetherValidator
from django.http import JsonResponse
from django.contrib.auth.hashers import make_password, check_password, is_password_usable
from Crypto.Cipher import AES

import json
import base64
from models import *
import datetime
from rest_framework import status,serializers


class BuildsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Builds
        fields = ('id', 'build', 'executed_date', 'project_id')
