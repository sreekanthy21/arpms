# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-01-17 16:01
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Builds',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('build', models.CharField(max_length=300)),
                ('executed_date', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='Devices',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('device_name', models.TextField()),
                ('device_os', models.TextField()),
                ('device_udid', models.TextField()),
                ('created', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Projects',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('description', models.TextField()),
                ('created', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='TestCases',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('test_feature', models.TextField()),
                ('test_title', models.TextField()),
                ('test_name', models.TextField()),
                ('test_suite_type', models.TextField()),
                ('test_priority', models.IntegerField()),
                ('created', models.DateTimeField()),
                ('status', models.BooleanField(default=True)),
                ('project', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='reports.Projects')),
            ],
        ),
        migrations.CreateModel(
            name='TestResultConsolidation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('test_status', models.IntegerField()),
                ('bugid', models.CharField(max_length=50)),
                ('bug_status', models.CharField(max_length=50)),
                ('execution_time', models.FloatField()),
                ('executed_date', models.DateField()),
                ('test_build', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='reports.Builds')),
                ('test_case', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='reports.TestCases')),
                ('test_device', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='reports.Devices')),
            ],
        ),
        migrations.CreateModel(
            name='TestResults',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('test_result', models.BooleanField(default=0)),
                ('screen_generated', models.TextField()),
                ('screen_diff', models.TextField()),
                ('executed', models.DateTimeField()),
                ('start_time', models.DateTimeField(blank=True, verbose_name=datetime.datetime.now)),
                ('end_time', models.DateTimeField(blank=True, verbose_name=datetime.datetime.now)),
                ('device', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='reports.Devices')),
                ('test_build', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='reports.Builds')),
                ('test_name', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='reports.TestCases')),
            ],
        ),
        migrations.CreateModel(
            name='TestSteps',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('test_step', models.TextField()),
                ('test_step_desc', models.TextField()),
                ('golden_screen', models.TextField()),
                ('created', models.DateTimeField()),
                ('test', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='reports.TestCases')),
            ],
        ),
        migrations.AddField(
            model_name='testresults',
            name='test_step',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='reports.TestSteps'),
        ),
        migrations.AddField(
            model_name='devices',
            name='project',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='reports.Projects'),
        ),
        migrations.AddField(
            model_name='builds',
            name='project',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='reports.Projects'),
        ),
    ]
