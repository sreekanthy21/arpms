# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from datetime import datetime

class Projects(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField()
    created = models.DateTimeField(auto_now=True)


class TestCases(models.Model):
    project = models.ForeignKey(Projects)
    test_feature = models.TextField()
    test_title = models.TextField()
    test_name = models.TextField()
    test_suite_type = models.TextField()
    test_priority = models.IntegerField()
    created = models.DateTimeField()
    status = models.BooleanField(default=True)


class TestSteps(models.Model):
    test = models.ForeignKey(TestCases)
    test_step = models.TextField()
    test_step_desc = models.TextField()
    golden_screen = models.TextField()
    created = models.DateTimeField()


class Devices(models.Model):
    device_name = models.TextField()
    device_os = models.TextField()
    device_udid = models.TextField()
    project = models.ForeignKey(Projects)
    created = models.DateTimeField()


class Builds(models.Model):
    project = models.ForeignKey(Projects)
    build = models.CharField(max_length=300)
    executed_date = models.DateField()
    type = models.CharField(max_length=300, default="Prod")


class TestResults(models.Model):
    test_name = models.ForeignKey(TestCases)
    test_step = models.ForeignKey(TestSteps)
    test_result = models.BooleanField(default=0)
    test_build = models.ForeignKey(Builds, null=True)
    device = models.ForeignKey(Devices)
    screen_generated = models.TextField()
    screen_diff = models.TextField()
    executed = models.DateTimeField()
    start_time = models.DateTimeField(datetime.now, blank=True)
    end_time = models.DateTimeField(datetime.now, blank=True)
    test_errors = models.TextField(default=None, null=True)


class TestResultConsolidation(models.Model):
    test_case = models.ForeignKey(TestCases)
    test_build = models.ForeignKey(Builds)
    test_device = models.ForeignKey(Devices)
    test_status = models.IntegerField()
    bugid = models.CharField(max_length=50)
    bug_status = models.CharField(max_length=50)
    execution_time = models.FloatField()
    executed_date = models.DateField()
    test_server_logs = models.TextField(default=None, null=True)
    test_device_logs = models.TextField(default=None, null=True)
