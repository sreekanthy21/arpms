//Script for search functionality
function searchbuild(){
  var base_url = window.location.origin;
  var build_date = $("#builddate").val();
  var build_name = $("#buildname").val();
  var csrftoken = $('input[name="csrfmiddlewaretoken"]').val()
  if(build_date == "" && build_name == ""){
    alert('Please enter either date or build name')
  }else{
      jQuery.ajax({
        type:'POST',
        data:{'build_date':build_date,'build_name':build_name,'csrfmiddlewaretoken': csrftoken},
        url: 'searchbuild/',
        beforeSend: function(){
            $("#testCasesdata").html('<tr><td colspan="9" align="center"><i class="fa fa-circle-o-notch fa-spin" style="font-size:40px"></i></td></tr>');
            $("#search_build_data").html('<tr><td colspan="9" align="center"><i class="fa fa-circle-o-notch fa-spin" style="font-size:40px"></i></td></tr>');
        },
        success:function(data){
          var searchresults = JSON.parse(data)
          var buildsData = ''
          var testCasesdata = ''
          var buildsDropdown = "<option value=''>Select Build</option>"
          var imagepath = '';
          for(i=0;i<searchresults['builds_data'].length;i++){
            if("tcases" in searchresults['builds_data'][i]){
                buildsData += "<tr><td>"+searchresults['builds_data'][i]['build']+"</td><td>"+searchresults['builds_data'][i]['executed_date']+"</td><td>"+searchresults['builds_data'][i]['total']+"</td><td>"+searchresults['builds_data'][i]['success']+"</td><td>"+searchresults['builds_data'][i]['fail']+"</td><td>"+searchresults['builds_data'][i]['flaky']+"</td><td>"+searchresults['builds_data'][i]['bugs']+"</td><td>"+searchresults['builds_data'][i]['device_name']+"/"+searchresults['builds_data'][i]['device_os']+"</td></tr>"

                buildsDropdown += "<option value="+searchresults['builds_data'][i]['id']+" data-id="+searchresults['builds_data'][i]['build']+">"+searchresults['builds_data'][i]['build']+"</option>"

                for(j=0;j<searchresults['builds_data'][i]['tcases'].length;j++){
                  if(searchresults['builds_data'][i]['tcases'][j]['status'] == '1'){
                    status = 'Pass'
                    backgroundcolor = '#86ff84'
                  }else{
                    status = 'Fail'
                    backgroundcolor = '#ff9b9b'
                  }


                  for(k=0;k<searchresults['builds_data'][i]['tcases'][j]['testResults'].length;k++){
                      if(searchresults['builds_data'][i]['tcases'][j]['testResults'][k]['screen_diff'] != ""){
                          var imgpath = searchresults['builds_data'][i]['tcases'][j]['testResults'][k]['screen_diff'].split('_test_')
                          var imagepath = imgpath[0]
                      }
                  }

  testCasesdata += "<tr style='background:"+backgroundcolor+"'><td>"+(i+1)+"</td><td>"+searchresults['builds_data'][i]['tcases'][j]['feature']+"</td><td>"+searchresults['builds_data'][i]['tcases'][j]['testname']+"</td><td>"+status+"</td><td>"+searchresults['builds_data'][i]['tcases'][j]['priority']+"</td><td>"+searchresults['builds_data'][i]['tcases'][j]['execution_time']+"</td><td>"+searchresults['builds_data'][i]['tcases'][j]['bugid']+"</td><td><a href="+base_url+"/static/"+imagepath+"/"+searchresults['builds_data'][i]['tcases'][j]['devicelogs']+" download>Download</a></td><td><a href="+base_url+"/static/"+imagepath+"/"+searchresults['builds_data'][i]['tcases'][j]['serverlogs']+" download>Download</a></td></tr>"
                }
            }
          }
          //Builds Data
          if(buildsData != ""){
              $("#search_build_data").html(buildsData)
              $("#piecharts_div").show()
              $("#build_beside_search").html(build_name)
          }else{
              $("#nav").html('')
              $("#piecharts_div").hide()
              $("#build_beside_search").html('')
              $("#search_build_data").html('<tr><td colspan="9" style="text-align:center;color: rgba(0,0,0,.54) !important;"><h4>No Data Found</h4></td></tr>')
          }

          //Test Cases
          if(testCasesdata != ""){
              $("#testCasesdata").html(testCasesdata)
              $("#data-table-basic_info").hide()
              $("#piecharts_div").show()
          }else{
              $("#testCasesdata").html('<tr><td colspan="8" style="text-align:center"><h4>No Data Found</h4></td></tr>')
              $("#data-table-basic_info").hide()
              $("#piecharts_div").hide()
          }

          if(buildsDropdown != "<option value=''>Select Build</option>"){
              $("#duilds_dropdown_div").show()
              $("#build_dropdown").html(buildsDropdown)
              $("#piecharts_div").show()
          }else{
              $("#build_dropdown").html('')
              $("#duilds_dropdown_div").hide()
              $("#piecharts_div").hide()
          }

          document.getElementById("build_dropdown").selectedIndex = "1"
          testcases_build()

        }
      })
        dynamic_charts()
  }
}


//Script to get test cases based on build
function testcases_build(){
  var base_url = window.location.origin;
  var buildname = $("#build_dropdown").val()
  var bname = $("#build_dropdown option:selected").attr('data-id');
  $("#build_beside_search").html(bname)
  jQuery.ajax({
    url: 'buildtestcases',
    data:{'build_id': bname},
    beforeSend: function(){
        $("#testCasesdata").html('<tr><td colspan="9" align="center"><i class="fa fa-circle-o-notch fa-spin" style="font-size:40px"></i></td></tr>');
    },
    success:function(data){
      var searchresults = JSON.parse(data)
      var testCasesdata = ''
      var imagepath = ''
      for(i=0;i<searchresults['testCases'].length;i++){
          if(searchresults['testCases'][i]['status'] == '1'){
            status = 'Pass'
            backgroudcolor = '#86ff84'
          }else{
            status = 'Fail'
            backgroudcolor = '#ff9b9b'
          }

          var cases_testResults = '';
          for(j=0;j<searchresults['testCases'][i]['testResults'].length;j++){
              cases_testResults += searchresults['testCases'][i]['testResults'][j]['test_step']+'</br>';
              if(searchresults['testCases'][i]['testResults'][j]['screen_diff'] != ""){
                  var imgpath = searchresults['testCases'][i]['testResults'][j]['screen_diff'].split('_test_')
                  var imagepath = imgpath[0]
              }
          }
          testCasesdata += "<tr style='background:"+backgroudcolor+"'><td>"+(i+1)+"</td><td>"+searchresults['testCases'][i]['feature']+"</td><td><a href='javascript:' onclick='get_test_steps("+searchresults['testCases'][i]['test_case_id']+","+searchresults['testCases'][i]['build_id']+","+searchresults['testCases'][i]['test_device_id']+")'>"+searchresults['testCases'][i]['testname']+"</td><td>"+status+"</td><td>"+searchresults['testCases'][i]['priority']+"</td><td>"+searchresults['testCases'][i]['execution_time']+"</td><td>"+searchresults['testCases'][i]['bugid']+"</td><td><a href="+base_url+"/static/"+imagepath+"/"+searchresults['testCases'][i]['devicelogs']+" download>Download</a></td><td><a href="+base_url+"/static/"+imagepath+"/"+searchresults['testCases'][i]['serverlogs']+" download>Download</a></td></tr>"
      }
      if(testCasesdata != ""){
          $("#testCasesdata").html(testCasesdata)
          pagination()
      }else{
          $("#testCasesdata").html('<tr><td colspan="8" style="text-align:center"><h4>No Data Found</h4></td></tr>')
      }

    }
  })
}


//To get test steps based on test case id, build id and device id
function get_test_steps(test_case_id,build_id,device_id){
    var base_url = window.location.origin;
    $("#testresults_data").html('')
    $("#trload").html('<i class="fa fa-circle-o-notch fa-spin" style="font-size:40px"></i>')
    jQuery.ajax({
      url: base_url+'/'+'testresults',
      data:{'test_case_id': test_case_id,'build_id':build_id, 'device_id': device_id},
      success:function(data){
          var testResults = ''
          var jsondata = JSON.parse(data)
          for(i=0;i<jsondata['testresults'].length;i++){
              if(jsondata['testresults'][i]['test_result'] == 'Pass'){
                backgroundcolor = '#70DB93'
              }else{
                backgroundcolor = '#EE6363'
              }

              if(jsondata['testresults'][i]['screen_diff'] != ""){
                  var imgpath = jsondata['testresults'][i]['screen_diff'].split('_test_')
                  var imagepath = imgpath[0]
                  var screendiff = jsondata['testresults'][i]['screen_diff'].split('/')
                  var screendifference = screendiff[1]
              }else{
                  var imagepath = ''
                  var screendifference = ''
              }

              if(jsondata['testresults'][i]['screen_generated'] != ""){
                  var screengenerated = jsondata['testresults'][i]['screen_generated'].split('/')
                  var screen_gene  = screengenerated[1]
              }else{
                  var screen_gene = ''
              }

              testResults += "<tr style='background:"+backgroundcolor+"'><td style='width:40%'>"+jsondata['testresults'][i]['test_step']+"</td><td>"+jsondata['testresults'][i]['test_result']+"</td><td><img src="+base_url+"/static/"+imagepath+"/"+screen_gene+" alt='' style='width:150px;height:auto'></td><td><img src="+base_url+"/static/"+imagepath+"/"+screendifference+" alt='' style='width:150px;height:auto'></td><td>"+jsondata['testresults'][i]['error_msg']+"</td></tr>"
              // <td>"+jsondata['testresults'][i]['screen_diff']+"</td><td>"+jsondata['testresults'][i]['screen_generated']+"</td>
          }


          if(testResults != ""){
              $("#trload").html('')
              $("#testresults_data").html(testResults)
          }else{
              $("#testresults_data").html('<tr><td colspan="2">No Data Found</td></tr>')
          }
      }
    })
    $('#myModalthree').modal('toggle');
}


//Script for pagination
function pagination(){
    $("#nav").html('')
    $('#data-table-basic').after('<div id="nav" style="float:right"></div>');
    var rowsShown = 20;
    var rowsTotal = $('#data-table-basic >tbody >tr').length;
    var numPages = rowsTotal/rowsShown;
    for(i = 0;i < numPages;i++) {
        var pageNum = i + 1;
        $('#nav').append('<a class="stylepaginate current" href="javascript:" rel="'+i+'">'+pageNum+'</a> ');

    }
    $('#data-table-basic tbody tr').hide();
    $('#data-table-basic tbody tr').slice(0, rowsShown).show();
    $('#nav a:first').addClass('active');
    $('#nav a').bind('click', function(){

        $('#nav a').removeClass('active');
        $(this).addClass('active');
        var currPage = $(this).attr('rel');
        var startItem = currPage * rowsShown;
        var endItem = startItem + rowsShown;
        $('#data-table-basic tbody tr').css('opacity','0.0').hide().slice(startItem, endItem).
        css('display','table-row').animate({opacity:1}, 300);
    });
}


//To Refresh Fields
function refreshfields(){
  $("#builddate").val('')
  $("#buildname").val('')
}

//To get charts after searching
function dynamic_charts(){
var build_date = $("#builddate").val();
var build_name = $("#buildname").val();
jQuery.ajax({
  data:{'build_date':build_date,'build_name':build_name},
  url: 'buildcharts/',
  success:function(data){
    $("#piecharts_div").html(data)
  }
  })
}



//To get test cases based on build id
function build_testcases(){
  var build_id = $("#buildname").val();
  if(build_id == ""){
    alert('Please enter build id')
  }else{
    var base_url = window.location.origin;
    jQuery.ajax({
      url: base_url+'/'+'buildtestcases',
      data:{'build_id': build_id},
      beforeSend: function(){
          $("#testCasesdata").html('<tr><td colspan="7" align="center"><i class="fa fa-circle-o-notch fa-spin" style="font-size:40px"></i></td></tr>');
      },
      success:function(data){
        var searchresults = JSON.parse(data)
        var testCasesdata = ''
        for(i=0;i<searchresults['testCases'].length;i++){
            if(searchresults['testCases'][i]['status'] == '1'){
              status = 'Pass'
              backgroudcolor = '#86ff84'
            }else{
              status = 'Fail'
              backgroudcolor = '#ff9b9b'
            }
            testCasesdata += "<tr style='background:"+backgroudcolor+"' id='rowid"+searchresults['testCases'][i]['test_case_id']+"'><td id='sno"+searchresults['testCases'][i]['test_case_id']+"'>"+(i+1)+"</td><td id='build"+searchresults['testCases'][i]['test_case_id']+"'>"+searchresults['testCases'][i]['build']+"</td><td id='feature"+searchresults['testCases'][i]['test_case_id']+"'>"+searchresults['testCases'][i]['feature']+"</td><td><a href='javascript:' onclick='get_test_steps("+searchresults['testCases'][i]['test_case_id']+","+searchresults['testCases'][i]['build_id']+","+searchresults['testCases'][i]['test_device_id']+")' id='testcase"+searchresults['testCases'][i]['test_case_id']+"'>"+searchresults['testCases'][i]['testname']+"</td><td id='status"+searchresults['testCases'][i]['test_case_id']+"'>"+status+"</td><td id='bugid"+searchresults['testCases'][i]['test_case_id']+"'>"+searchresults['testCases'][i]['bugid']+"</td><td><a href='javascript:editcase("+searchresults['testCases'][i]['build_id']+","+searchresults['testCases'][i]['test_case_id']+","+searchresults['testCases'][i]['test_device_id']+")'>Edit</a></td></tr>"
        }
        if(testCasesdata != ""){
            $("#testCasesdata").html(testCasesdata)
            pagination()
        }else{
            $("#testCasesdata").html('<tr><td colspan="8" style="text-align:center"><h4>No Data Found</h4></td></tr>')
        }
      }
    })
  }
}


function editcase(build_id,test_case_id,device_id){
  $("#bugid").val('');
  $("#bugstatus").val('');
  var build = $("#build"+test_case_id).html();
  var testcase = $("#testcase"+test_case_id).html();
  var status = $("#status"+test_case_id).html();
  var feature = $("#feature"+test_case_id).html();
  var bugid = $("#bugid"+test_case_id).html();
  var sno = $("#sno"+test_case_id).html();

  if(status == 'Pass'){
    var tcstatus = '<option value="1" selected>Pass</option><option value="0">Fail</option>'
  }else{
    var tcstatus = '<option value="1">Pass</option><option value="0" selected>Fail</option>'
  }
  $("#build_id").val(build_id);
  $("#test_case_id").val(test_case_id);
  $("#display_buildname").html(build);
  $("#display_testcase").html(testcase);
  $("#testcase_status").html(tcstatus);
  $("#feature").val(feature);
  $("#bugid").val(bugid);
  $("#testcase_sno").val(sno);
  $("#device_id").val(device_id);
  $("#ustatusmodal").modal('toggle');
}


function updateteststatus(){
    var testcase_status = $("#testcase_status").val();
    if(testcase_status == '1'){
        updateteststatus_pass();
    }else{
        updateteststatus_fail();
    }
}


//To update status of test case as Pass
function updateteststatus_pass(){
  var build_id = $("#build_id").val();
  var test_case_id = $("#test_case_id").val();
  var bug_id = $("#bugid").val();
  var bug_status = $("#bugstatus").val();
  var testcase_status = $("#testcase_status").val();
  var feature = $("#feature").val();
  var testcase = $("#display_testcase").html();
  var buildname = $("#display_buildname").html();
  var sno = $("#testcase_sno").val();
  var device_id = $("#device_id").val();
  var test_step_id = "";
  var base_url = window.location.origin;
  jQuery.ajax({
      data:{'test_step_id':test_step_id, 'device_id': device_id,'testcase_sno':sno,'build_id':build_id, 'test_case_id': test_case_id, 'bugid': bug_id, 'bugstatus': bug_status, 'testcase_status': testcase_status, 'feature': feature, 'testcase': testcase, 'buildname': buildname},
      url : base_url+'/'+'updatetestcase',
      beforeSend: function(){
          $("#testcasesload").html('<i class="fa fa-circle-o-notch fa-spin" style="font-size:40px"></i>');
      },
      success:function(data){
        var parsedata = JSON.parse(data);
        var msg = "<td id='sno"+parsedata['rowDetails']['test_case_id']+"'>"+parsedata['rowDetails']['testcase_sno']+"</td><td id='build"+parsedata['rowDetails']['test_case_id']+"'>"+parsedata['rowDetails']['buildname']+"</td><td id='feature"+parsedata['rowDetails']['test_case_id']+"'>"+parsedata['rowDetails']['feature']+"</td><td><a href='javascript:' onclick='get_test_steps("+parsedata['rowDetails']['test_case_id']+","+parsedata['rowDetails']['build_id']+","+parsedata['rowDetails']['device_id']+")' id='testcase"+parsedata['rowDetails']['test_case_id']+"'>"+parsedata['rowDetails']['title']+"</a></td><td id='status"+parsedata['rowDetails']['test_case_id']+"'>"+parsedata['rowDetails']['status']+"</td><td id='bugid"+parsedata['rowDetails']['test_case_id']+"'>"+parsedata['rowDetails']['bugid']+"</td><td><a href='javascript:editcase("+parsedata['rowDetails']['build_id']+","+parsedata['rowDetails']['test_case_id']+","+parsedata['rowDetails']['device_id']+")'>Edit</a></td>";
        $("#rowid"+test_case_id).html(msg)
        if(parsedata['rowDetails']['status'] == 'Pass'){
            document.getElementById("rowid"+test_case_id).style.backgroundColor = "rgb(134, 255, 132)";
        }else{
            document.getElementById("rowid"+test_case_id).style.backgroundColor = "rgb(255, 155, 155)";
        }
        if(parsedata['message'] == 'Test case updated successfully'){
            iziToast.success({title: 'Success',message: 'Test case updated successfully',position: 'topRight'});
        }else{
            iziToast.error({title: 'Error',message: 'Please try later',position: 'topRight'});
        }
        $("#ustatusmodal").modal('toggle');
        // location.reload();
      },
      complete:function(data){
          $("#testcasesload").html('')
      }
  })
}

//To update status of test case as Fail
function updateteststatus_fail(){
  var build_id = $("#build_id").val();
  var test_case_id = $("#test_case_id").val();
  var bug_id = $("#bugid").val();
  var bug_status = $("#bugstatus").val();
  var testcase_status = $("#testcase_status").val();
  var feature = $("#feature").val();
  var testcase = $("#display_testcase").html();
  var buildname = $("#display_buildname").html();
  var sno = $("#testcase_sno").val();
  var device_id = $("#device_id").val();
  var base_url = window.location.origin;

    var teststeps = [];
    $.each($("input[name='teststep_value[]']:checked"), function(){
    teststeps.push($(this).val());
    });
    for(i=0;i<teststeps.length;i++){
        jQuery.ajax({
            data:{'test_step_id':teststeps[i],'device_id': device_id,'testcase_sno':sno,'build_id':build_id, 'test_case_id': test_case_id, 'bugid': bug_id, 'bugstatus': bug_status, 'testcase_status': testcase_status, 'feature': feature, 'testcase': testcase, 'buildname': buildname},
            url : base_url+'/'+'updatetestcase',
            beforeSend: function(){
                $("#testcasesload").html('<i class="fa fa-circle-o-notch fa-spin" style="font-size:40px"></i>');
            },
            success:function(data){
              var parsedata = JSON.parse(data);
              var msg = "<td id='sno"+parsedata['rowDetails']['test_case_id']+"'>"+parsedata['rowDetails']['testcase_sno']+"</td><td id='build"+parsedata['rowDetails']['test_case_id']+"'>"+parsedata['rowDetails']['buildname']+"</td><td id='feature"+parsedata['rowDetails']['test_case_id']+"'>"+parsedata['rowDetails']['feature']+"</td><td><a href='javascript:' onclick='get_test_steps("+parsedata['rowDetails']['test_case_id']+","+parsedata['rowDetails']['build_id']+","+parsedata['rowDetails']['device_id']+")' id='testcase"+parsedata['rowDetails']['test_case_id']+"'>"+parsedata['rowDetails']['title']+"</a></td><td id='status"+parsedata['rowDetails']['test_case_id']+"'>"+parsedata['rowDetails']['status']+"</td><td id='bugid"+parsedata['rowDetails']['test_case_id']+"'>"+parsedata['rowDetails']['bugid']+"</td><td><a href='javascript:editcase("+parsedata['rowDetails']['build_id']+","+parsedata['rowDetails']['test_case_id']+","+parsedata['rowDetails']['device_id']+")'>Edit</a></td>";
              $("#rowid"+test_case_id).html(msg)
              if(parsedata['rowDetails']['status'] == 'Pass'){
                  document.getElementById("rowid"+test_case_id).style.backgroundColor = "rgb(134, 255, 132)";
              }else{
                  document.getElementById("rowid"+test_case_id).style.backgroundColor = "rgb(255, 155, 155)";
              }
            },
            complete:function(data){
                $("#testcasesload").html('')
            }
        })
    }
    $("#ustatusmodal").modal('toggle');
    iziToast.success({title: 'Success',message: 'Test case updated successfully',position: 'topRight'});
    return true;

}


//To Get test steps when selected fail
function getsteps(){
  var testcase_status = $("#testcase_status").val();
  var test_case_id = $("#test_case_id").val();
  var build_id = $("#build_id").val();
  var device_id = $("#device_id").val();
  var base_url = window.location.origin;
  if(testcase_status == '0'){
    jQuery.ajax({
      url: base_url+'/'+'testresults',
      data:{'test_case_id': test_case_id,'build_id':build_id, 'device_id': device_id},
      beforeSend: function(){
          $("#teststepsData").html('<i class="fa fa-circle-o-notch fa-spin" style="font-size:40px"></i>');
      },
      success:function(data){
          var testResults = '<h2>Select Test Steps which are failed</h2></br>'
          var jsondata = JSON.parse(data);
          for(i=0;i<jsondata['testresults'].length;i++){
              if(jsondata['testresults'][i]['test_result'] == 'Pass'){
                backgroundcolor = '#70DB93'
                checked = ''
              }else{
                backgroundcolor = '#EE6363'
                checked = 'checked'
              }
              testResults += "<p><input type='checkbox' "+checked+" id='c"+i+"' name='teststep_value[]' value="+jsondata['testresults'][i]['test_step_id']+"><label for='c"+i+"'>"+jsondata['testresults'][i]['test_step']+"</label></p>"
          }
          $("#teststepsData").html(testResults);
      }
    })
    $("#tstepsmodel").modal('toggle');
  }
}

$(document).ready(function(){
  pagination();
});


$("#importjsonform").validate({
  rules: {
    project: { required: true  },
    build: { required: true  },
    device: { required: true  },
    directory: {required:true }
  },
  messages: {
   project: "Please select project",
   build: "Please select build",
   device: "Please select device",
   directory: "Please Enter folder name"
  }
});
