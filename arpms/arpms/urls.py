"""arpms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin


from reports import urls, views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'', include(urls)),
    # url(r'^index/', views.index, name='index'),
    url(r'^searchbuild/', views.searchbuild, name='searchbuilds'),
    url(r'^buildcharts/', views.buildcharts, name='buildcharts'),
    url(r'^buildtestcases/', views.buildtestcases, name='buildtestcases'),
    url(r'^testresults/', views.testresults, name='testresults'),
    url(r'^updatestatus/', views.updatestatus, name='updatestatus'),
    url(r'^updatetestcase/', views.updatetestcase, name='updatetestcase'),
    url(r'^buildaction/', views.buildaction, name='buildaction'),
    url(r'^deletebuild/', views.deletebuild, name='deletebuild'),
    url(r'^createbuild/', views.createbuild, name='createbuild'),
    url(r'^upload/', views.upload, name="upload"),
    url(r'^uploadfile/', views.uploadfile, name="uploadfile"),
    url(r'^importjson/', views.importjson, name="importjson"),
    url(r'^importjsonfiles/', views.importjsonfiles, name="importjsonfiles")

]
