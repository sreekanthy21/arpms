import json
import os
import argparse
import MySQLdb
from time import gmtime, strftime
from datetime import datetime
import time

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


class GenerateReportData:

    def __init__(self, device_id=None, build_id=None, directory=None):
        self.device_id = device_id
        self.build_id = build_id
        self.directory = directory

    def __connection(self):
        connection = None
        try:
            connection = MySQLdb.connect("localhost", "root", "root", "etouch_auto")
        except Exception as e:
            print "Could not established db connection %s " % e.message
        return connection

    def insert_test_results(self, device_id, build_id, directory):
        connection = self.__connection()
        if not connection:
            return False
        cursor = connection.cursor()
        try:
            files = os.listdir(directory)
            for file in files:
                if '.json' not in file:
                    continue
                try:
                    with open(directory + '/' + file) as outfile:
                        data = json.load(outfile)
                        test_status = ''
                        test_executed_date = 0
                        for key, val in data.items():
                            for tskey, tsval in val.get('test_steps').items():
                                screen_genrtd, screen_diff = "", ""
                                if tsval.get("screen_generated") != '':
                                    screen_genrtd = "2019" + tsval.get("screen_generated").rsplit("2019")[1]
                                if tsval.get("screen_diff")  != '':
                                    screen_diff = "2019" + tsval.get("screen_diff").rsplit("2019")[1]

                                if tsval.get('status') == 0:
                                    test_status = '0'

                                fmt = '%Y-%m-%d %H:%M:%S'
                                end_time = datetime.strptime(tsval.get("end_time"), fmt)
                                start_time = datetime.strptime(tsval.get("start_time"), fmt)
                                executed_date = end_time - start_time
                                test_executed_date += executed_date.seconds

                                sql = "insert into reports_testresults" \
                                      " (test_result, screen_generated," \
                                      " screen_diff, executed, start_time," \
                                      " end_time, device_id, test_build_id," \
                                      " test_name_id, test_step_id," \
                                      " test_errors) values ('%s', '%s', '%s'," \
                                      " '%s', '%s', '%s', '%s', '%s', '%s'," \
                                      " '%s', '%s')" % (tsval.get('status'),
                                                        screen_genrtd,
                                                        screen_diff,
                                                        strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                                                        tsval.get("start_time"),
                                                        tsval.get("end_time"),
                                                        device_id, build_id,
                                                        val.get("test_id"),
                                                        tsval.get("test_step_id"),
                                                        connection.escape_string(tsval.get("error")))
                                cursor.execute(sql)
                                connection.commit()
                                device_log = "dat"+str(val.get("test_id"))+"logcat.txt"
                                server_log = "dat"+str(val.get("test_id"))+"server.txt"

                            if test_status == "":
                                test_status = '1'
                            sql = "insert into reports_testresultconsolidation" \
                                  " (test_status, bugid, bug_status," \
                                  " execution_time, executed_date," \
                                  " test_build_id, test_case_id," \
                                  " test_device_id, test_device_logs," \
                                  " test_server_logs) values ('%s', '%s', '%s'," \
                                  " '%s', '%s', '%s', '%s', '%s', '%s'," \
                                  " '%s')" % (test_status, '', '',
                                              test_executed_date, val.get('executed_date'),
                                              build_id, val.get('test_id'), device_id,
                                              device_log, server_log)
                            cursor.execute(sql)
                            connection.commit()
                except (IOError, IndexError, KeyError) as e:
                    print "Unable to parse the file %s" % e.message
            connection.close()
            print "All test cases import is done successfully!"
        except Exception as e:
            print "All test cases import is not done! %s" % e
            print sql
            print data


if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("--device_id", "--device_id", required=True, help="Please give device id to insert data")
    ap.add_argument("--build_id", "--build_id", required=True, help="Please give build id to insert data")
    ap.add_argument("--directory", "--directory", required=True, help="Please give json directory")
    args = vars(ap.parse_args())

    start = GenerateReportData()

    start.insert_test_results(args.get("device_id"), args.get("build_id"), args.get("directory"))
