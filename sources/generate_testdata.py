import csv
import MySQLdb
import os
from datetime import datetime

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


class GenerateTestData:

    def __init__(self, cases_csv=None, stesp_csv=None):
        self.test_cases_csv = cases_csv
        self.test_steps_csv = stesp_csv

    def __connection(self):
        connection = None
        try:
            connection = MySQLdb.connect("localhost", "root", "root", "etouch_auto")
        except Exception as e:
            print "Could not established db connection %s " % e.message
        return connection

    def insert_test_cases(self):
        connection = self.__connection()
        if not connection:
            return False
        cursor = connection.cursor()
        try:
            with open(BASE_DIR + '/sources/test_cases.csv', 'r') as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=',')
                for row in csv_reader:
                    sql = "insert into reports_testcases (test_feature," \
                          " test_title, test_name, test_suite_type, test_priority," \
                          " created, status, project_id) values ('%s', '%s', '%s', '%s'," \
                          " '%s', '%s', '%s', '%s')" % (row[1], connection.escape_string(row[2]),
                                                        connection.escape_string(row[3]), row[4],
                                                        row[5], datetime.today(), 1, 1)
                    cursor.execute(sql)
                    connection.commit()
            connection.close()
            print "All test cases import is done successfully!"
        except Exception as e:
            print "All test cases import is not done!"

    def insert_test_steps(self):
        connection = self.__connection()
        if not connection:
            return False
        cursor = connection.cursor()
        try:
            with open(BASE_DIR + '/sources/newtest_steps.csv', 'r') as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=',')
                for row in csv_reader:
                    test_steps = row[0].splitlines()
                    for step in test_steps:
                        sql = "INSERT INTO reports_teststeps(TEST_STEP, TEST_STEP_DESC, GOLDEN_SCREEN, CREATED," \
                              " TEST_ID) VALUES('%s', '%s', '%s', '%s', '%s')" % (connection.escape_string(step),
                                                                                  '', '', datetime.today(), row[4])
                        cursor.execute(sql)
                        connection.commit()
            connection.close()
            print "All test steps import is done successfully!"
        except Exception as e:
            print "All test steps import is not done! %s" % e


if __name__ == "__main__":

    data = GenerateTestData()
    data.insert_test_cases()
    data.insert_test_steps()
